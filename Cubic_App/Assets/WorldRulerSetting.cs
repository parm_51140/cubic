using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorldRulerSetting : MonoBehaviour
{
    public static WorldRulerSetting _worldruler;

    [SerializeField] GameObject ScaleObjectVert;
    [SerializeField] GameObject ScaleObjectHorz;
    [SerializeField] GameObject refVerticalPosX;
    [SerializeField] GameObject refHorizontalPosX;
    [SerializeField] GameObject backDropHorz;
    [SerializeField] GameObject backDropVert;
    [SerializeField] GameObject BaseHorz;
    [SerializeField] GameObject BaseVert;
    [SerializeField] GameObject ScaleType;
    [SerializeField] List<GameObject> scaleList;
    [SerializeField] bool isVertical;

    float maxScale;
    private void Awake()
    {
        _worldruler = this;
    }
    void Start()
    {     
        Vector2 anc = refVerticalPosX.gameObject.GetComponent<RectTransform>().position;
        Vector2 anc2 = refHorizontalPosX.gameObject.GetComponent<RectTransform>().position;
        Vector3 aaa = Camera.main.ScreenToWorldPoint(anc);
        Vector3 aaa2 = Camera.main.ScreenToWorldPoint(anc2);
        Vector3 ww = BaseVert.transform.position;
        Vector3 ww2 = BaseHorz.transform.position;
        ww.x = aaa.x;
        ww2.y = aaa2.y;
        BaseVert.transform.position = ww;
        BaseHorz.transform.position = ww2;


        CreateScale();

    }
    
    public void CreateScale()
    {
        float cameraScale = 30f / Main.MainControl.orbitCamera.orthographicSize;
        maxScale = 80;
        for (float i = 0f; i <= maxScale; i++)
        {
            //horizontalscale
            if (i % 5 == 0)//num +scale
            {
                GameObject oo = Instantiate(ScaleObjectHorz, new Vector3(0, 0, 0), Quaternion.identity, BaseHorz.transform);
                scaleList.Add(oo);
                oo.transform.localPosition = new Vector3(i * cameraScale, 0, 0);
                oo.GetComponent<WorldRulerScale>().SetScale(true, i, false);
                oo = Instantiate(ScaleObjectHorz, new Vector3(0, 0, 0), Quaternion.identity, BaseHorz.transform);
                scaleList.Add(oo);
                oo.transform.localPosition = new Vector3(-i * cameraScale, 0, 0);
                oo.GetComponent<WorldRulerScale>().SetScale(true, -i, false);

            }
            else//scale
            {
                GameObject oo = Instantiate(ScaleObjectHorz, new Vector3(0, 0, 0), Quaternion.identity, BaseHorz.transform);
                scaleList.Add(oo);
                oo.transform.localPosition = new Vector3(i * cameraScale, 0, 0);
                oo.GetComponent<WorldRulerScale>().SetScale(false, i, false);

                oo = Instantiate(ScaleObjectHorz, new Vector3(0, 0, 0), Quaternion.identity, BaseHorz.transform);
                scaleList.Add(oo);
                oo.transform.localPosition = new Vector3(-i * cameraScale, 0, 0);
                oo.GetComponent<WorldRulerScale>().SetScale(false, -i, false);
            }
            ///verticalscale
            if (i % 5 == 0)
            {
                GameObject oo = Instantiate(ScaleObjectVert, new Vector3(0, 0, 0), Quaternion.identity, BaseVert.transform);
                scaleList.Add(oo);
                oo.transform.localPosition = new Vector3(0, -i * cameraScale, 0);
                oo.GetComponent<WorldRulerScale>().SetScale(true, -i, true);

                oo = Instantiate(ScaleObjectVert, new Vector3(0, 0, 0), Quaternion.identity, BaseVert.transform);
                scaleList.Add(oo);
                oo.transform.localPosition = new Vector3(0, i * cameraScale, 0);
                oo.GetComponent<WorldRulerScale>().SetScale(true, i, true);


            }
            else
            {
                GameObject oo = Instantiate(ScaleObjectVert, new Vector3(0, 0, 0), Quaternion.identity, BaseVert.transform);
                scaleList.Add(oo);
                oo.transform.localPosition = new Vector3(0, -i * cameraScale, 0);
                oo.GetComponent<WorldRulerScale>().SetScale(false, -i, true);

                oo = Instantiate(ScaleObjectVert, new Vector3(0, 0, 0), Quaternion.identity, BaseVert.transform);
                scaleList.Add(oo);
                oo.transform.localPosition = new Vector3(0, i * cameraScale, 0);
                oo.GetComponent<WorldRulerScale>().SetScale(false, i, true);
            }

        }
    }
    private void Update()
    {

    }

    public void showRuler() 
    {
        ScaleType.transform.localScale = Vector3.one;
        BaseHorz.transform.localScale = Vector3.one;
        BaseVert.transform.localScale = Vector3.one;
        resetScale();
    }

    public void resetScale()
    {
        for (int i = 0; i < scaleList.Count; i++)
        {
            Destroy(scaleList[i].gameObject);
        }
        scaleList.Clear();
        CreateScale();
    }
    public void clearScale()
    {
        for (int i = 0; i < scaleList.Count; i++)
        {
            Destroy(scaleList[i].gameObject);
        }
        scaleList.Clear();
        BaseHorz.transform.localScale = Vector3.zero;
        BaseVert.transform.localScale = Vector3.zero;
        ScaleType.transform.localScale = Vector3.zero;
    }
}
