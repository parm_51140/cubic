using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanReshape : MonoBehaviour
{
    // Start is called before the first frame update

    public PathGenerator pathGenerator;
    [Range(2f, 15f)]
    public float height;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        pathGenerator.shapeExposure = height;
     //  pathGenerator.shapeExposure = (pathGenerator.shapeExposure + height*Mathf.Sin(Time.time))/2;
       
    }
}
