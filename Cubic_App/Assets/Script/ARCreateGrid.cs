using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARCreateGrid : MonoBehaviour
{
    [SerializeField] GameObject LineObjectScript;
    [SerializeField] float maxLineCm;
   public Vector3 MidPoint;

   

    public void CreateLine(float width, float dept)
    {
        float max = Mathf.Max(width, dept);
        MidPoint.x = Mathf.Round(max / 2) ;
        MidPoint.z = Mathf.Round(max / 2) ;
        
        for (float i = 0; i <= max; ++i)
        {
            GameObject lineObject = Instantiate(LineObjectScript, gameObject.transform);
            LineRenderer LineScript = lineObject.GetComponent<LineRenderer>();
            LineScript.SetPosition(0, new Vector3(i, 0, 0));
            LineScript.SetPosition(1, new Vector3(i, 0, max));
            if (i % 5 == 0&& i!=0 && i != max && i != -max) 
            {
                LineScript.startWidth=.001f;
                LineScript.endWidth=.001f;
              //  LineScript.SetWidth(.001f, .001f);
            }
            lineObject.name = "dept " + i;

        }
       
        for (float i = 0; i <= max; i++)
        {
            GameObject lineObject = Instantiate(LineObjectScript, gameObject.transform);
            LineRenderer LineScript = lineObject.GetComponent<LineRenderer>();
            LineScript.SetPosition(0, new Vector3(0,0,i));
            LineScript.SetPosition(1, new Vector3(max, 0,i));
            if (i % 5 == 0 && i != 0 && i != max) 
            {
                LineScript.startWidth = .001f;
                LineScript.endWidth = .001f;
            }
            lineObject.name = "width " + i;
        }
        
       // gameObject.transform.localScale = Vector3.one / 100;
       
        //gameObject.transform.position = new Vector3(-MidPoint.x,0,-MidPoint.y); 
    }
}
