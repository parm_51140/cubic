using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlasticThickness : MonoBehaviour
{
    public static PlasticThickness _thicknessUI;
    // [SerializeField] Slider sliderObject;
    public float CurrentThickness = 0.02f;
    [SerializeField] GameObject thickText;
    [SerializeField] GameObject thickTextGroup;
    [SerializeField] GameObject[] numObject;
    // [SerializeField] GameObject BasenumObject;

    [SerializeField] GameObject thicknessSlide;
    [SerializeField] GameObject DiskSlide;
    [SerializeField] GameObject DiscCloseBtn;

    [SerializeField] GameObject currthicknessGroup;
    [SerializeField] Text currthicknessText;

    public bool showThickness;
    public bool showDisc;
    int currIndex;
    float snappos;
    bool gosnap;
    // [SerializeField] Slider SliderValue;
    float CamWidth;

    private void Awake()
    {
        _thicknessUI = this;
    }
    void Start()
    {
        for (float i = .25f; i < 2.0f; i += .05f)
        {
            GameObject tt = Instantiate(thickText, thickTextGroup.transform);
            tt.name = i.ToString("F2");
            tt.GetComponent<Text>().text = "";
            // tt.GetComponent<Text>().text = i.ToString("F2");
        }
        CamWidth = Main.MainControl.mainCamera.pixelWidth;
        thickTextGroup.GetComponent<HorizontalLayoutGroup>().enabled = true;

    }
    public void ActiveSlide(bool act)
    {
        showThickness = act;
        if (act)
        {
            thicknessSlide.SetActive(true);
            ActiveDisc(false);
            // DiskSlide.transform.parent.gameObject.SetActive(true);
            // currthicknessGroup.SetActive(false);
        }
        else
        {
            thicknessSlide.SetActive(false);
            DiskSlide.transform.gameObject.SetActive(false);
        }
    }
    public void ActiveDisc(bool act)
    {
        showDisc = act;

        if (act)
        {
            thicknessSlide.GetComponent<Image>().raycastTarget = true;
            DiskSlide.transform.gameObject.SetActive(true);
            currthicknessGroup.SetActive(false);
            DiscCloseBtn.SetActive(true);
        }
        else
        {
            thicknessSlide.GetComponent<Image>().raycastTarget = false;
            DiskSlide.transform.gameObject.SetActive(false);
            currthicknessGroup.SetActive(true);
            DiscCloseBtn.SetActive(false);
            CurrentThickness = ((currIndex - 1) * .05f) + .25f;
            currthicknessText.text = CurrentThickness + " mm.";
        }
    }

    Vector3 touchBegan, touchEnd, touchCurrent, touchDelta;
    Vector3 tempPos;
    Vector3 startPos;
    Vector3 lastPos;
    Vector3 DragDelta;
    bool toLeft;
    float time = 0;
    float skip = 2000;
    Coroutine lerpC;
    float slidePos;

    private void Update()
    {
        if (!showThickness) return;

        //400 value/tap
        if (showDisc)
        {
            slidePos = -thickTextGroup.GetComponent<RectTransform>().anchoredPosition.x / 10;
            DiskSlide.GetComponent<RectTransform>().localEulerAngles = new Vector3(0, 0, slidePos);
            numIndexArrange();
        }

        if (gosnap)
        {
            Vector3 tempPos = thickTextGroup.GetComponent<RectTransform>().anchoredPosition;
            tempPos.x = tempPos.x + (snappos - tempPos.x) / 8;
            thickTextGroup.GetComponent<RectTransform>().anchoredPosition = tempPos;

            if (Mathf.Abs(tempPos.x - snappos) < 2f)
            {
                tempPos.x = snappos;
                thickTextGroup.GetComponent<RectTransform>().anchoredPosition = tempPos;
                gosnap = false;
            }
        }

    }
    void numIndexArrange()
    {

        int currentDiscPosID = Mathf.Clamp(Mathf.CeilToInt(slidePos / 40), 0, 36);//0-36
                                                                                  // print(currentDiscPosID);
        int DiscIndexNext = currentDiscPosID + 3 - (9 * (int)((currentDiscPosID + 3) / 9));
        int DiscIndexPrev = currentDiscPosID > 3 ? currentDiscPosID - 4 - (9 * (int)((currentDiscPosID - 4) / 9)) : 0;

        if (currentDiscPosID + 2 < 36)
        { numObject[DiscIndexNext].GetComponent<Text>().text = (0.25f + 0.05f * (currentDiscPosID + 2)) + ""; }
        else { numObject[DiscIndexNext].GetComponent<Text>().text = ""; }

        if (currentDiscPosID > 3)
        { numObject[DiscIndexPrev].GetComponent<Text>().text = (0.2f + 0.05f * (currentDiscPosID - 4)) + ""; }
        else
        {
            numObject[8].GetComponent<Text>().text = "";
            numObject[7].GetComponent<Text>().text = "";
        }

        foreach (GameObject go in numObject) { go.GetComponent<Text>().color = Color.white; }
        // int clampN = Mathf.Clamp((int)(slidePos / 40) - (9 * (int)((slidePos / 40) / 9)), 0, 36);
        currIndex = Mathf.Clamp((int)((slidePos + 15) / 40), 0, 36);
        numObject[currIndex % 9].GetComponent<Text>().color = Color.yellow;
    }



    public void TouchStart()
    {
        //  time = 0;//
        //  if (lerpC != null) StopCoroutine(lerpC);
        //  touchBegan = Input.mousePosition;
        //  startPos = thickTextGroup.GetComponent<RectTransform>().anchoredPosition;
        //  lastPos = startPos;
        gosnap = false;
        touchBegan = Input.mousePosition;
        startPos = thickTextGroup.GetComponent<RectTransform>().anchoredPosition;
    }
    public void StartMove()
    {
        //  DragDelta = Input.mousePosition;

    }
    public void EndtMove()
    {
        //  DragDelta = Input.mousePosition - DragDelta;
    }
    public void TouchMove()
    {
        DragDelta = (touchBegan - Input.mousePosition) * (1080f / Main.MainControl.mainCamera.pixelWidth);
        tempPos = thickTextGroup.GetComponent<RectTransform>().anchoredPosition;
        tempPos.x = Mathf.Clamp(startPos.x - DragDelta.x, -14600, 200);

        thickTextGroup.GetComponent<RectTransform>().anchoredPosition = tempPos;
        //   time += 1;
        //   touchCurrent = Input.mousePosition;
        //   tempPos = thickTextGroup.GetComponent<RectTransform>().anchoredPosition;
        //
        //   tempPos.x = startPos.x + (touchCurrent.x - touchBegan.x) / CamWidth * 1770;
        //   if (lastPos.x < tempPos.x) toLeft = false; else toLeft = true;
        //   thickTextGroup.GetComponent<RectTransform>().anchoredPosition = tempPos;
        //   lastPos = tempPos;

    }
    public void TouchEnd()
    {
        snappos = currIndex * -400;
        gosnap = true;
        // print(thickTextGroup.GetComponent<RectTransform>().anchoredPosition.x + "  " + currIndex * -400);
        //   print(time);
        //   if (lerpC != null) StopCoroutine(lerpC);
        //   skip = DragDelta.x * 5;
        //
        //   if (time > 10 && DragDelta.x > 200)
        //       lerpC = StartCoroutine(MoveSnap(0));
        //   else if (time <= 10 || (DragDelta.x > 900))
        //   {
        //       print("swipe");
        //       if (toLeft)
        //           lerpC = StartCoroutine(MoveSnap(skip));
        //       else
        //           lerpC = StartCoroutine(MoveSnap(-skip));
        //   }
    }

    IEnumerator MoveSnap(float increment)
    {
        // 400 = cell width
        Vector3 startpos = thickTextGroup.GetComponent<RectTransform>().anchoredPosition;
        Vector3 endpos = startpos;
        endpos.x = Mathf.Clamp(Mathf.Round((startpos.x - increment) / 400) * 400, -(400 * 36), 0);

        Vector3 temp;
        float t = 0f;
        while (t <= 1)
        {
            temp = Vector3.Lerp(startpos, endpos, t);
            thickTextGroup.GetComponent<RectTransform>().anchoredPosition = temp;

            t = t + (.05f * (1 - t + 0.01f));

            yield return null;
        }
        thickTextGroup.transform.localPosition = endpos;
        CurrentThickness = (-(endpos.x / 400 * 0.05f - 0.2f));

        yield return null;
    }
    public float SlideValue()
    {
        return 0;
    }
}
