using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ZXing;
using ZXing.QrCode;
using UnityEngine.UI;
using TMPro;

public class QRGEN : MonoBehaviour
{
    [SerializeField]
    private RawImage _rawImageReceiver;
    [SerializeField]
    private InputField _textInputField;

    private Texture2D _storeEncoderText;
    // Start is called before the first frame update
    void Start()
    {
        _storeEncoderText = new Texture2D(256, 256);
    }

    // Update is called once per frame
    private Color32 []Encode(string textForEncoding,int width,int height)
    {
        BarcodeWriter writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }
    public void OnClickEncoder()
    {
        EncoderTextToQRCode();
    }
    private void EncoderTextToQRCode()
    {
        string textWrite = string.IsNullOrEmpty(_textInputField.text) ? "You Should Write Something" :
            _textInputField.text;
        Color32[] _convertPixalToTexture = Encode(textWrite, _storeEncoderText.width, _storeEncoderText.height);
        _storeEncoderText.SetPixels32(_convertPixalToTexture);
        _storeEncoderText.Apply();

        _rawImageReceiver.texture = _storeEncoderText;

    }
}
