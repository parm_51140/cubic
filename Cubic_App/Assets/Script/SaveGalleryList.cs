using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class SaveGalleryList : MonoBehaviour
{
    [SerializeField] Image iconShow;
    [SerializeField] Image bg;
    [SerializeField] GameObject[] BTN;
    [SerializeField] GameObject[] paidIcon;

    [SerializeField] Text ObjectName;
    [SerializeField] Text ObjectSaveID;
    [SerializeField] Text ObjectSaveDate;

    [SerializeField] Text paid3d;
    [SerializeField] Text paidmeta;
    [SerializeField] Text paidcad;

    bool isselect;
    bool delselect;
    public int id;
    string rowId;
    public void setData(Sprite sp, string nm, string sid, int id)
    {
        iconShow.sprite = sp;
        ObjectName.text = nm.Split(" ")[0];
        ObjectSaveID.text = sid;
        //DateTime aa = Convert.ToDateTime(nm.Split(" ")[1]);
        ObjectSaveDate.text = Convert.ToDateTime(nm.Split(" ")[1]).ToString("dd MMM yyyy") + " " + nm.Split(" ")[2] + " " + nm.Split(" ")[3];
        rowId = sid + "";
        this.id = id;
    }
    public void _selectBtn()
    {
        if (isselect)
        {
            getSelected();
            SavedGallery._saveGallery.resetSelectList();

        }
        else
        {
            SavedGallery._saveGallery.resetSelectList();
            picked();
        }
    }
    public void pickOut()
    {
        isselect = false;
        bg.color = new Color(.149f, .149f, .149f, 1f);
        foreach (GameObject ta in BTN)
        { ta.gameObject.SetActive(false); }

    }
    public void picked()
    {
        isselect = true;
        bg.color = new Color(.27f, .27f, .27f, .27f);
        foreach (GameObject ta in BTN)
        { ta.gameObject.SetActive(true); }
    }
    public void toggleDeletebox()
    {
        if (!isselect)
        {
            _selectBtn();
            SavedGallery._saveGallery.SetActiveManageBox(true, rowId, id);
            delselect = true;
        }
        else
        {
            delselect = !delselect;
            SavedGallery._saveGallery.SetActiveManageBox(delselect, rowId, id);
        }

    }

    public void BtnCAD()
    {
        if (isselect)
        { StartCoroutine(SavedGallery._saveGallery.loadCadFile(id)); }
        else { _selectBtn(); }
    }

    public void getSelected()
    {
        LoadHolder._loadHold.setLoad(true);
        _BaseData._BaseDataSave.designID = rowId;
        StartCoroutine(SavedGallery._saveGallery.LoadSelectedData(id));
    }


}
