﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace dev.cubic
{
    public class MeshMod : MonoBehaviour
    {
        public MeshFilter meshFilter;
        public GameObject point;
        public PointTouch scaler;
        private List<Vector3> pointlist = new List<Vector3>();

        // Start is called before the first frame update
        void Start()
        {
            /*
              var ori = meshFilter.sharedMesh;
              var mspfy = new UnityMeshSimplifier.MeshSimplifier();
              mspfy.Initialize(ori);
              mspfy.SimplifyMesh(0.0f);
              var dst = mspfy.ToMesh();
              meshFilter.sharedMesh = dst;
            */
            meshFilter.sharedMesh.GetVertices(pointlist);
            /*
            for (var i = 0; i < pointlist.Count; i++)
            {
                var p = pointlist[i];
            //    pointlist[i] = new Vector3(p.x* scaler.scale, p.y* scaler.scale, p.z * scaler.scale);
            }
           // meshFilter.sharedMesh.SetVertices(pointlist);
            Debug.Log(pointlist.Count);
            */

        }

        // Update is called once per frame
        void Update()
        {
            if (scaler.mode == "scale")
            {
                var pointlist2 = new List<Vector3>(pointlist);
                Debug.Log(scaler.scale);
                for (var i = 0; i < pointlist.Count; i++)
                {
                    var p = pointlist2[i];
                    pointlist2[i] = new Vector3(p.x * (1 + scaler.scale / 5f), p.y * (1 + scaler.scale / 5f), p.z * (1 + scaler.scale / 5f));
                }
                meshFilter.sharedMesh.SetVertices(pointlist2);
            }
        }
    }
}
