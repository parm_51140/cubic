using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LigthTest : MonoBehaviour
{
    public Light mainlight;
    public Text lightValue;
    public Slider sl;
    public void setligthOB(GameObject goParent)
    {
        mainlight = goParent.GetComponentInChildren<Light>();
    }
    public void _valueChange() { mainlight.intensity = sl.value;
        lightValue.text = sl.value.ToString("F2");
    }
}