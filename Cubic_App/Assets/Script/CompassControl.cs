using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using dev.cubic;
public class CompassControl : MonoBehaviour
{
    [SerializeField] GameObject innerAxit;
    [SerializeField] GameObject Compass, CompassL, CompassR, CompassObject; //compass -1000,0,1000
    float comC, comL, comR;
    [SerializeField] Text textFront, textLeft, textRight;
    public bool IsVertical;
    bool doMove, toleft, totop;
    Vector3 compassBase;



    void Start()
    {
        compassBase = Compass.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {
       if (innerAxit.transform.localEulerAngles.z == 0&&!IsVertical)
       {
           if (textFront.text == "Front") { textLeft.text = "Left"; textRight.text = "Right"; }
           else if (textFront.text == "Left") { textLeft.text = "Back"; textRight.text = "Front"; }
           else if (textFront.text == "Back") { textLeft.text = "Right"; textRight.text = "Left"; }
           else if (textFront.text == "Right") { textLeft.text = "Front"; textRight.text = "Back"; }
       }
       else if (innerAxit.transform.localEulerAngles.z != 0 && !IsVertical)
       {
           if (textFront.text == "Front") { textRight.text = "Left"; textLeft.text = "Right"; }
           else if (textFront.text == "Left") { textRight.text = "Back"; textLeft.text = "Front"; }
           else if (textFront.text == "Back") { textRight.text = "Right"; textLeft.text = "Left"; }
           else if (textFront.text == "Right") { textRight.text = "Front"; textLeft.text = "Back"; }
       }
        if (innerAxit.transform.localEulerAngles.x > 89 && innerAxit.transform.localEulerAngles.x < 91 && IsVertical)
        {
            textFront.text = "Top";
        }
        else if (innerAxit.transform.localEulerAngles.x < 271 && innerAxit.transform.localEulerAngles.x > 269 && IsVertical)
        {
            textFront.text = "Bottom";
        }
        else if (innerAxit.transform.localEulerAngles.x > -1 && innerAxit.transform.localEulerAngles.x < 1 && IsVertical) { textFront.text = "Side"; }
        if (doMove&&!IsVertical)
        {
            if (!toleft)
            {
                if (Compass.transform.localPosition.x < 1000 - 1)
                {
                    comC = comC + (1000 - Compass.transform.localPosition.x) / 10;
                    comL = comL + (0 - CompassL.transform.localPosition.x) / 10;
                    Compass.transform.localPosition = new Vector3(comC, compassBase.y, compassBase.z);
                    CompassL.transform.localPosition = new Vector3(comL, compassBase.y, compassBase.z);
                }
                else
                {
                    CompassR.transform.localPosition = new Vector3(-1000, compassBase.y, compassBase.z);
                    doMove = false;
                }
            }
            else
            {
                if (Compass.transform.localPosition.x > -1000 + 1)
                {
                    comC = comC + (-1000 - Compass.transform.localPosition.x) / 10;
                    comR = comR + (-0 - CompassR.transform.localPosition.x) / 10;
                    Compass.transform.localPosition = new Vector3(comC, compassBase.y, compassBase.z);
                    CompassR.transform.localPosition = new Vector3(comR, compassBase.y, compassBase.z);

                }
                else
                {
                    CompassL.transform.localPosition = new Vector3(1000, compassBase.y, compassBase.z);
                    doMove = false;
                }
            }
        }
        else if (doMove && IsVertical)
        {
           // print("strr");
            if (!totop)
            {
                if (Compass.transform.localPosition.y < 1200 - 1)
                {
                    comC = comC + (1200 - Compass.transform.localPosition.y) / 10;
                    comL = comL + (0 - CompassL.transform.localPosition.y) / 10;
                    Compass.transform.localPosition = new Vector3(compassBase.x, comC, compassBase.z);
                    CompassL.transform.localPosition = new Vector3(compassBase.x, comL, compassBase.z);
                }
                else
                {
                    CompassR.transform.localPosition = new Vector3(0, -1200, compassBase.z);
                    doMove = false;
                }
            }
            else
            {
                if (Compass.transform.localPosition.y > -1200 + 1)
                {
                    comC = comC + (-1200 - Compass.transform.localPosition.y) / 10;
                    comR = comR + (-0 -CompassR.transform.localPosition.y) / 10;
                    Compass.transform.localPosition = new Vector3(0, comC, compassBase.z);
                    CompassR.transform.localPosition = new Vector3(0, comR, compassBase.z);

                }
                else
                {
                    CompassL.transform.localPosition = new Vector3(0, 1200, compassBase.z);
                    doMove = false;
                }
            }
        }




    }
    public void SwapLeft(bool userSwapformLeft)
    {
        if (IsVertical) { return; }
        Compass.transform.localPosition = new Vector3(0, compassBase.y, compassBase.z);
        CompassL.transform.localPosition = new Vector3(-1000, compassBase.y, compassBase.z);
        CompassR.transform.localPosition = new Vector3(1000, compassBase.y, compassBase.z);
        comC = 0;
        comR = 1000;
        comL = -1000;
        if (userSwapformLeft)
        {
            toleft = false;

        }
        else
        {
            toleft = true;
        }


        doMove = true;
    }

    public void swapTop(bool userSwapformTop)
    {
      
        if (!IsVertical) { return; }
        //print("top");
        Compass.transform.localPosition = new Vector3(0, 0, compassBase.z);
        CompassL.transform.localPosition = new Vector3(0, 1200, compassBase.z);
        CompassR.transform.localPosition = new Vector3(0, -1200, compassBase.z);
        comC = 0;
        comR = 1200;
        comL = -1200;
        if (userSwapformTop)
        {
            totop = false;

        }
        else
        {
            totop = true;
        }
        doMove = true;
    }
}
