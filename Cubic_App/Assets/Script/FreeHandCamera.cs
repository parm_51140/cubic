using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreeHandCamera : MonoBehaviour
{
    [SerializeField] Main mainControl;
    [SerializeField] Camera mainCamera;
    [SerializeField] GameObject VerticalAxis;//y
    [SerializeField] GameObject HorizontalAxis;//x

    float startDelta;
    float currentDelta;
    float scale;
    int touchCount;
    Vector3 down;
    Vector3 current;
    public Vector3 transformDelta;
    public Vector3 SaveEditView;
    Quaternion horizonQ, VerticalQ;


    float tempOrtho;
    void Start()
    {
        mainControl = Main.MainControl;
    }

    void Update()
    {

    }

    public void FreeHandMove(Vector3 Delta)
    {
        if (current.x > 90&& current.x < 270)
        {
            transformDelta = new Vector3(current.x + (Delta.y / 10), current.y + (Delta.x / 10), current.z);
        }
        else
        {
            transformDelta = new Vector3(current.x + (Delta.y / 10), current.y - (Delta.x / 10), current.z);
        }
        transformDelta.x %= 360;
        transformDelta.y %= 360;
        VerticalAxis.transform.localEulerAngles = new Vector3(transformDelta.x, 0, 0);
        HorizontalAxis.transform.localEulerAngles = new Vector3(0, transformDelta.y, 0);
    }
    public void SetFreehand()
    {
        current = transformDelta;
    }
    public void resetFreehand()//freehand to edit view
    {
        StartCoroutine(setRotateHor(((SaveEditView.y % 360) + 360) % 360, ((SaveEditView.x % 360) + 360) % 360, true));
    }
    public void clearFreehand()
    {
        SoundPanel._SoundPanel.UIClickPlay();
        transformDelta = new Vector3(45, 45, 0);
        StartCoroutine(setRotateHor(((transformDelta.y % 360) + 360) % 360, ((transformDelta.x % 360) + 360) % 360, false)); ;
    }
    public void backToFreehand()
    {       
        SaveEditView = new Vector3(VerticalAxis.transform.localEulerAngles.x, HorizontalAxis.transform.localEulerAngles.y, 0);
        StartCoroutine(setRotateHor(((transformDelta.y % 360) + 360) % 360, ((transformDelta.x % 360) + 360) % 360, false));
    }

    IEnumerator setRotateHor(float valueY, float valueX, bool setWaitTransform)
    {
        mainControl.WaitTransformToEdit = true;
        float t = 0f;
        Vector3 curr;
        Vector3 start = HorizontalAxis.transform.localEulerAngles;
        while (t < 1)
        {
            if (start.y <= 180)
                if (valueY <= 180)
                { curr = Vector3.Lerp(start, new Vector3(0, valueY, 0), t); }
                else
                { curr = Vector3.Lerp(start, new Vector3(0, valueY - 360, 0), t); }
            else
                curr = Vector3.Lerp(start, new Vector3(0, valueY + 360, 0), t);
            HorizontalAxis.transform.localEulerAngles = curr;

            t += 0.1f;
            yield return new WaitForSeconds(0.03f);
        }
        HorizontalAxis.transform.localEulerAngles = new Vector3(0, valueY, 0);
        yield return null;
        yield return setRotateVert(valueX, setWaitTransform);
    }
    IEnumerator setRotateVert(float vaule, bool setWaitTransform)
    {
        float t = 0f;
        Vector3 curr;
        Vector3 start = VerticalAxis.transform.localEulerAngles;

        while (t < 1)
        {
            if (start.x <= 180)
                if (vaule <= 180)
                { curr = Vector3.Lerp(start, new Vector3(vaule, 0, 0), t); }
                else
                { curr = Vector3.Lerp(start, new Vector3(vaule - 360, 0, 0), t); }
            else
                curr = Vector3.Lerp(start, new Vector3(vaule + 360, 0, 0), t);
            VerticalAxis.transform.localEulerAngles = curr;

            t += .1f;
            yield return new WaitForSeconds(0.03f);
        }

        VerticalAxis.transform.localEulerAngles = new Vector3(vaule, 0, 0);
        mainControl.WaitTransformToEdit = false;
        yield return null;
    }

}

