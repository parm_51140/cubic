using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class _randomTestTouch : MonoBehaviour
{
    public bool mouseDown;
    public GameObject go;

    public Texture texTest;
    public Material texMat;
    public RawImage texRaw;
    public RawImage texRaw2;
    public RenderTexture texRen;

    Vector2 mouseStart;
    Vector2 mouseSave;
    Vector2 mouseDelta;
    void Start()
    {
        StartCoroutine(imm());
    }
    IEnumerator imm()
    {
        texRaw.texture = texTest;
        texRaw.SetNativeSize();
        texRen.width = texTest.width;
        texRen.height = texTest.height;

        Texture2D tex2d = new Texture2D(texTest.width, texTest.height, TextureFormat.RGBA32, false);
        yield return new WaitForEndOfFrame();
       
      ///  RenderTexture.active = texRen;
      ///  tex2d.ReadPixels(new Rect(0, 0, texTest.width, texTest.height), 0, 0);
      ///
      ///  tex2d.Apply();

       // texRaw2.texture = tex2d;

        texMat.SetTexture("_BaseMap", texRen);
        yield return null;
    }


    void Update()
    {

    }
    public void pointerDown()
    {
        mouseStart = Input.mousePosition;
    }
    public void pointerUp()
    {
        mouseSave = mouseSave - mouseDelta;
    }
    public void pointerDrag()
    {
        mouseDelta = mouseStart - (Vector2)Input.mousePosition;
        go.transform.position = mouseSave - mouseDelta;

    }
}
