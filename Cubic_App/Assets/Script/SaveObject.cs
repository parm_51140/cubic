using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System;
using System.Runtime.InteropServices;
using static System.Net.Mime.MediaTypeNames;
using System.IO;

public class SaveObject : MonoBehaviour
{
    public static SaveObject _saveobjBtn;

    [SerializeField] UnityEngine.UI.Image btnImage;
    [SerializeField] string saveRowID;
    string linkDownload = "";

    bool activeBtn;

    private void Awake()
    {
        _saveobjBtn = this;
    }
    public void showSaveBtn(bool show)
    {
        gameObject.transform.localScale = show ? Vector3.one : Vector3.zero;
        activeBtn = true;
        activeSaveBtn();
    }
    public void setActiveSaveBtn(bool active)
    {
        activeBtn = active;
        btnImage.raycastTarget = activeBtn;
        if (activeBtn)
            btnImage.color = Color.white;
        else btnImage.color = new Color(.8f, .8f, .8f, .8f);

    }
    public void activeSaveBtn()
    {
        btnImage.raycastTarget = activeBtn;
        if (activeBtn)
            btnImage.color = Color.white;
        else btnImage.color = new Color(.8f, .8f, .8f, .8f);
    }

    public void loadObjectID(string idValue)
    {
        saveRowID = idValue;
    }

    public void saveShapeAndUpload()
    {
        //  if (!PlayerPrefs.HasKey("USERDATA"))
        //  {
        //      MainMenuGroupControl._mainmenugroupcontrol.forceCallMenu(7);
        //      SavedGallery._saveGallery.showGallery(false);
        //      UserLogin._userLogin.ShowUser();
        //  }
        //    else
        {
            setActiveSaveBtn(false);
            StartCoroutine(saveShapeData());
        }
    }
    IEnumerator saveShapeData()
    {
        CallBackNotice._callBackNotice._noticBark("Saving Model");
        LoadHolder._loadHold.setLoad(true);
        string getreq = _BaseData._BaseDataSave.targetLink + "?action=sendDesignData&id=" + _BaseData._BaseDataSave.userTag;
        print(getreq + " : shaveshapeData");
        objectSaveJson osj = new objectSaveJson();
        osj.id = "";
        osj.name = "Untitled " + System.DateTime.Now;
        switch (_BaseData._BaseDataSave.selectedID)
        {
            case 1: osj.type = "Box"; break;
            case 2: osj.type = "Cup"; break;
            case 3: osj.type = "SandwichBox"; break;
            case 4: osj.type = "Can"; break;
            case 5: osj.type = "Carton"; break;
        }
        osj.savedata = _BaseData._BaseDataSave.ObjectToJson();
        osj.savesticker = "sticker";
        osj.cad = "cad";
        string jss = JsonConvert.SerializeObject(osj);


        var uwr = new UnityWebRequest(getreq, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(jss);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success)
        {

            CallBackNotice._callBackNotice._noticBark("Saved Fail");
        }
        else
        {
            if (uwr.downloadHandler.text != "")
            {
                saveRowID = uwr.downloadHandler.text;
                _BaseData._BaseDataSave.designID = uwr.downloadHandler.text;
                print("save at : " + saveRowID);
                StartCoroutine(getDecal());
                CallBackNotice._callBackNotice._noticBark("Uploading Sticker");
            }
            else
            {
                StartCoroutine(saveShapeData());
            }
        }
        yield return null;
    }
    void returnActive() { setActiveSaveBtn(true); }


    Coroutine savetoDropBox;
    IEnumerator getDecal()
    {
        DecalCloudProperties dcp = new DecalCloudProperties();
        List<string> dcpl = new List<string>();

        for (int i = 0; i < DecalPropertiesSave.decalPropSave.Length; i++)
        {
            if (DecalPropertiesSave.decalPropSave[i].p_texture.name != "Clear")
            {
                yield return savetoDropBox = StartCoroutine(saveImage((Texture2D)DecalPropertiesSave.decalPropSave[i].p_texture));
                print(linkDownload);
                dcp.p_link = linkDownload;
            }
            else { dcp.p_link = ""; }
            // print(dcp.p_link);
            // dcp.p_link = DecalPropertiesSave.decalPropSave[i].p_link;
            dcp.p_name = DecalPropertiesSave.decalPropSave[i].p_texture.name;
            dcp.p_Position = Convec3tostr(DecalPropertiesSave.decalPropSave[i].p_Position);
            dcp.p_Rotation = Convec3tostr(DecalPropertiesSave.decalPropSave[i].p_Rotation);
            dcp.p_Scale = Convec3tostr(DecalPropertiesSave.decalPropSave[i].p_Scale);
            dcp.p_offset = DecalPropertiesSave.decalPropSave[i].p_offset;
            dcp.p_TexScale = DecalPropertiesSave.decalPropSave[i].p_TexScale;
            dcpl.Add(JsonConvert.SerializeObject(dcp));
        }
        string Ex = JsonConvert.SerializeObject(dcpl);
        yield return null;
        StartCoroutine(saveShapeDecal(Ex));
    }
    IEnumerator saveImage(Texture2D tex2d)
    {
        print(tex2d.name);

        Color[] pixels = tex2d.GetPixels();
        Texture2D tex = new Texture2D(tex2d.width, tex2d.height, TextureFormat.RGBA32, false);
        tex.SetPixels(pixels, 0);
        tex.Apply();

        byte[] bytes = tex.EncodeToPNG();
        Destroy(tex);

        // string pngData = Convert.ToBase64String(ImageConversion.EncodeToPNG(tex2d));
        string pngData = Convert.ToBase64String(bytes);
        string getreq = _BaseData._BaseDataSave.targetLink + "?action=uploadPicturetoDropbox&userID=" + _BaseData._BaseDataSave.userID +
           "&designID=" + saveRowID;
        print(getreq + " : uploadPicture");

        WWWForm form = new WWWForm();
        form.AddField("s", pngData);
        print("size : " + ImageConversion.EncodeToPNG(tex2d).Length + " : " + form.data.Length);
        using (var w = UnityWebRequest.Post(getreq, form))
        {
            Destroy(tex);
            yield return w.SendWebRequest();
            GUIUtility.systemCopyBuffer = w.downloadHandler.text;
            if (w.result != UnityWebRequest.Result.Success)
            {
                print("**" + w.downloadHandler.text);
                print("***" + w.error);
            }
            else
            {
                if (w.downloadHandler.text.IndexOf("https://") == 0)
                {

                    linkDownload = w.downloadHandler.text;

                    linkDownload = linkDownload.Substring(0, linkDownload.Length - 4) + "raw=1";
                }
                else if (w.downloadHandler.text == "")
                {
                    savetoDropBox = StartCoroutine(saveImage(tex2d));
                    yield break;
                }
                else { yield return null; }

            }
        }
        yield return null;
    }

    IEnumerator saveShapeDecal(string decalData)
    {
        CallBackNotice._callBackNotice._noticBark("Saving Sticker");
        string getreq = _BaseData._BaseDataSave.targetLink + "?action=updateDesignData&sheetID=" + _BaseData._BaseDataSave.userTag + "&designID=" + saveRowID
            + "&type=" + "sticker";

        print(getreq + " : shaveshapeDecal");
        //  GUIUtility.systemCopyBuffer = decalData;

        var uwr = new UnityWebRequest(getreq, "POST");
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(decalData);
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success)
        {

            CallBackNotice._callBackNotice._noticBark("Saved Fail");
        }
        else
        {
            print(uwr.downloadHandler.text);
            if (uwr.downloadHandler.text != "")
            {
                //  saveRowID = uwr.downloadHandler.text;
                //  print("save at : " + saveRowID);
                CallBackNotice._callBackNotice._noticBark("Saved");
            }
            else
            {
                CallBackNotice._callBackNotice._noticBark("SaveUnsuccess");
            }
        }
        LoadHolder._loadHold.setLoad(false);
        setActiveSaveBtn(true);
        Invoke("returnActive", 3);

        yield return null;
    }
    string Convec3tostr(Vector3 V)
    {
        vec3tostr temp = new vec3tostr();
        temp.x = V.x;
        temp.y = V.y;
        temp.z = V.z;
        return JsonConvert.SerializeObject(temp);
    }

    private void Update()
    {
        // if (Input.GetKeyUp(KeyCode.F)) { getDecal(); }
    }


}

