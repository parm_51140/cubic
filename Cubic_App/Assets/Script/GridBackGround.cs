using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridBackGround : MonoBehaviour
{
    public static GridBackGround _GridBackGround;
    [SerializeField] GameObject LineObjectPf;
    List<GameObject> lineList = new List<GameObject>();
    private void Awake()
    {
        _GridBackGround = this;
    }
    void Start()
    {
        createLineGrid();
    }
    public void createLineGrid()
    {
        float gap = 1f;
        //  if (Main.MainControl.orbitCamera.orthographicSize > 11) { gap = 5f; }
        float cameraScale = 30 / Main.MainControl.orbitCamera.orthographicSize;
        for (float i = 0f; i <= 50; i += gap)
        {
            GameObject lineObject = Instantiate(LineObjectPf, gameObject.transform);
            lineObject.transform.localPosition = new Vector3(i*cameraScale, 0, 150);
            LineRenderer LineScript = lineObject.GetComponent<LineRenderer>();
            // / LineScript.SetPosition(0, new Vector3((i * .3f * cameraScale), 10, 0));
            // / LineScript.SetPosition(1, new Vector3((i * .3f * cameraScale), -10, 0));
            // LineScript.SetPosition(0, new Vector3(i * cameraScale, 10, 0));
            // LineScript.SetPosition(1, new Vector3(i * cameraScale, -10, 0));
            // LineScript.startWidth = .0075f;
            // LineScript.endWidth = .0075f;
            LineScript.SetPosition(0, new Vector3(0 , 60, 0));
            LineScript.SetPosition(1, new Vector3(0 , -60, 0));
            LineScript.startWidth = .05f;
            LineScript.endWidth = .05f;
            lineList.Add(lineObject);
        }
        for (float i = 0f; i <= 50; i += gap)
        {
            GameObject lineObject = Instantiate(LineObjectPf, gameObject.transform);
            lineObject.transform.localPosition = new Vector3(-i * cameraScale, 0, 150);
            LineRenderer LineScript = lineObject.GetComponent<LineRenderer>();
            LineScript.SetPosition(0, new Vector3(0 , 60, 0));
            LineScript.SetPosition(1, new Vector3(0 , -60, 0));
            LineScript.startWidth = .05f;
            LineScript.endWidth = .05f;
            lineList.Add(lineObject);
        }
        for (float i = 0f; i <= 50; i += gap)
        {
            GameObject lineObject = Instantiate(LineObjectPf, gameObject.transform);
            lineObject.transform.localPosition = new Vector3(0, -i * cameraScale, 150);
            LineRenderer LineScript = lineObject.GetComponent<LineRenderer>();
            LineScript.SetPosition(0, new Vector3(60, 0, 0));
            LineScript.SetPosition(1, new Vector3(-60, 0, 0));
            LineScript.startWidth = .05f;
            LineScript.endWidth = .05f;
            lineList.Add(lineObject);
        }
        for (float i = 0f; i <= 50; i += gap)
        {
            GameObject lineObject = Instantiate(LineObjectPf, gameObject.transform);
            lineObject.transform.localPosition = new Vector3(0, i * cameraScale, 150);
            LineRenderer LineScript = lineObject.GetComponent<LineRenderer>();
            LineScript.SetPosition(0, new Vector3(60, 0, 0));
            LineScript.SetPosition(1, new Vector3(-60, 0, 0));
            LineScript.startWidth = .05f;
            LineScript.endWidth = .05f;
            lineList.Add(lineObject);
        }
        // for (float i = gap; i <= 40; i += gap)
        // {
        //     GameObject lineObject = Instantiate(LineObjectPf, gameObject.transform);
        //     LineRenderer LineScript = lineObject.GetComponent<LineRenderer>();
        //     // LineScript.SetPosition(0, new Vector3(-(i * .3f * cameraScale), 10, 0));
        //     // LineScript.SetPosition(1, new Vector3(-(i * .3f * cameraScale), -10, 0));
        //     LineScript.SetPosition(0, new Vector3((-i * cameraScale), 10, 0));
        //     LineScript.SetPosition(1, new Vector3((-i * cameraScale), -10, 0));
        //     LineScript.startWidth = .0075f;
        //     LineScript.endWidth = .0075f;
        //     lineList.Add(lineObject);
        // }
        // for (float i = 0f; i <= 60; i += gap)//down
        // {
        //     GameObject lineObject = Instantiate(LineObjectPf, gameObject.transform);
        //     LineRenderer LineScript = lineObject.GetComponent<LineRenderer>();
        //     //  LineScript.SetPosition(0, new Vector3(-10, -i * .3f * cameraScale, 0));
        //     //  LineScript.SetPosition(1, new Vector3(10, -i * .3f * cameraScale, 0));
        //     LineScript.SetPosition(0, new Vector3(-10, -i * cameraScale, 0));
        //     LineScript.SetPosition(1, new Vector3(10, -i * cameraScale, 0));
        //     LineScript.startWidth = .0075f;
        //     LineScript.endWidth = .0075f;
        //     lineList.Add(lineObject);
        // }
        // for (float i = gap; i <= 60; i += gap)
        // {
        //     GameObject lineObject = Instantiate(LineObjectPf, gameObject.transform);
        //     LineRenderer LineScript = lineObject.GetComponent<LineRenderer>();
        //     // LineScript.SetPosition(0, new Vector3(-10, i * .3f * cameraScale, 0));
        //     // LineScript.SetPosition(1, new Vector3(10, i * .3f * cameraScale, 0));
        //     LineScript.SetPosition(0, new Vector3(-10, i * cameraScale, 0));
        //     LineScript.SetPosition(1, new Vector3(10, i * cameraScale, 0));
        //     LineScript.startWidth = .0075f;
        //     LineScript.endWidth = .0075f;
        //     lineList.Add(lineObject);
        // }
    }
    public void resetGrideBG()
    {
        for (int i = 0; i < lineList.Count; i++)
        {
            Destroy(lineList[i].gameObject);
        }
        lineList.Clear();
        createLineGrid();
    }
    private void Update()
    {
        if (Input.anyKey) 
        {
            resetGrideBG();
            WorldRulerSetting._worldruler.resetScale();
        }
    }

}
