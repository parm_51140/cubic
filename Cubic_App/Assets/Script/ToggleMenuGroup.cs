using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleMenuGroup : MonoBehaviour
{
    [SerializeField] GameObject[] SubIcon;
    [SerializeField] GameObject verticalGroup;
    [SerializeField] GameObject IsoViewObj;
    float veticalGroupSize;
    Coroutine AnimToggle;
    bool isactive;
    public static ToggleMenuGroup _toggleMenu;
    private void Awake()
    {
        _toggleMenu = this;
    }
    void Start()
    {     //550 //510
        veticalGroupSize = SubIcon.Length * 150 + (SubIcon.Length * 20);
        isactive = false;
    }

    public void OpenToggle()
    {
        if (AnimToggle != null) { StopCoroutine(AnimToggle); }
        isactive = !isactive;
        AnimToggle = StartCoroutine(startToggleAnim(isactive));
        SoundPanel._SoundPanel.UIClickPlay();
    }
    public void forceClose()
    {
        isactive = false;
        AnimToggle = StartCoroutine(startToggleAnim(isactive));
    }
    public void setEnabledToggle(bool isEnable)
    {

        if (!isEnable)
        {
            forceClose();
            gameObject.GetComponent<Image>().color = Color.clear;
        }
        else
            gameObject.GetComponent<Image>().color = Color.white;
        gameObject.GetComponent<Button>().interactable = isEnable;

    }
    public void OpenToggle(bool input)
    {
        if (AnimToggle != null) { StopCoroutine(AnimToggle); }
        isactive = input;
        AnimToggle = StartCoroutine(startToggleAnim(isactive));
    }

    IEnumerator startToggleAnim(bool isopen)
    {
        float endValue = veticalGroupSize;
        Color endColor = new Color(1, 0.9f, 0, 1f);
        if (!isopen) { endValue = 0; endColor = Color.white; }
        float t = 0f;
        Vector2 sizeD = verticalGroup.GetComponent<RectTransform>().sizeDelta;
        float curr;
        float start = sizeD.y;
        Color currColor;
        Color startcolor = gameObject.GetComponent<Image>().color;
        while (t < 1)
        {

            curr = Mathf.Lerp(start, endValue, t);
            currColor = Color.Lerp(startcolor, endColor, t);
            verticalGroup.GetComponent<RectTransform>().sizeDelta = new Vector2(sizeD.x, curr);
            gameObject.GetComponent<Image>().color = currColor;

            t += 0.1f;
            yield return new WaitForSeconds(0.03f);
        }
        verticalGroup.GetComponent<RectTransform>().sizeDelta = new Vector2(sizeD.x, endValue);
        gameObject.GetComponent<Image>().color = endColor;

        yield return null;

    }

    public void ToggleSubmenuActive(int manuIndex, bool isActive)
    {
        if (!isActive)
        {
            SubIcon[manuIndex].GetComponent<Image>().color = new Color(202, 202, 202, .5f);
            SubIcon[manuIndex].GetComponent<Button>().interactable = false;
        }
        else
        {
            SubIcon[manuIndex].GetComponent<Image>().color = Color.white;
            SubIcon[manuIndex].GetComponent<Button>().interactable = true;
            // ToggleSubmenuUsing();
        }
    }
    public void ToggleSubmenuUsing()
    {
        foreach (GameObject go in SubIcon)
        {
            go.GetComponent<Image>().color = Color.white;
        }
        if (_BaseData._BaseDataSave.openIsoView)
        {
            SubIcon[0].GetComponent<Image>().color = new Color(1, 0.9f, 0, 1f);
        }
        if (_BaseData._BaseDataSave.openWireframe)
        {
            SubIcon[1].GetComponent<Image>().color = new Color(1, 0.9f, 0, 1f);
        }
        if (_BaseData._BaseDataSave.openLid)
        {
            SubIcon[2].GetComponent<Image>().color = new Color(1, 0.9f, 0, 1f);
        }

    }

    public void toggleSelect(int menu)// 1:lid  2:wire  3:Iso 
    {

        if (menu == 1)
        {
            _BaseData._BaseDataSave.openLid = !_BaseData._BaseDataSave.openLid;
            ObjectCover._objectcover.OpenCover();
        }
        else if (menu == 2)
        {
            if (TouchControl._ScreenTouchControl.IsEditing) return;
            _BaseData._BaseDataSave.openWireframe = !_BaseData._BaseDataSave.openWireframe;
            EditableObjectMaster._editmaster.activeWireframe();
            DecalControl2._decalControl._setActiveDecalObject(!_BaseData._BaseDataSave.openWireframe);
        }
        else if (menu == 3)
        {
            _BaseData._BaseDataSave.openIsoView = !_BaseData._BaseDataSave.openIsoView;
            IsoViewObj.SetActive(_BaseData._BaseDataSave.openIsoView);
            IsoViewObj.transform.localScale = Vector3.one;
        }
        ToggleSubmenuUsing();

    }

    public void forceCloseIso()
    {
        _BaseData._BaseDataSave.openIsoView = false;
        IsoViewObj.SetActive(_BaseData._BaseDataSave.openIsoView);
        ToggleSubmenuUsing();
    }
    public void forceCloseWireframe()
    {
        _BaseData._BaseDataSave.openWireframe = false;
        // ObjectReshape._reShapeIns.activeWireframe(_BaseData._BaseDataSave.openWireframe);
        EditableObjectMaster._editmaster.activeWireframe();
        ToggleSubmenuUsing();
    }

    void toggleWireFrame()
    {
        //  Main.MainControl.toggleWireFrame();
        ObjectReshape._reShapeIns.activeWireframe(_BaseData._BaseDataSave.openIsoView);
    }
    void toggleCover()
    {
        //ObjectCover._objectcover._lidToggleBtn();
        //ToggleSubmenuUsing();
    }
}
