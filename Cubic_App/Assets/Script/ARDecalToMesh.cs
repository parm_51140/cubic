using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ch.sycoforge.Decal;
using UnityEditor;

public class ARDecalToMesh : MonoBehaviour
{
    public ARDecal mainDecalCon;
    private void Start()
    {
        mainDecalCon = gameObject.GetComponent<ARDecal>();
    }
    public IEnumerator ToDecalMesh(GameObject decalObj, float delay,int id)
    {
        if (decalObj == null) { yield return null; }
        yield return new WaitForSeconds(delay);
        EasyDecal decal = decalObj.GetComponent<EasyDecal>();
        if (decal != null)
        {
            GameObject clone = new GameObject(decal.name + " (mesh clone)");
            mainDecalCon.meshDecal[id] = clone;
            clone.transform.parent = decalObj.transform.parent;
            clone.transform.localPosition = decal.transform.localPosition ;
            clone.transform.rotation = decal.Rotation;
            clone.transform.localScale = decal.transform.localScale;
            clone.AddComponent<MeshCollider>();
            MeshFilter filter = clone.AddComponent<MeshFilter>();
            Mesh m = Instantiate(decal.SharedMesh) as Mesh;
            m.name = "decal_mesh";
            filter.mesh = m;
            m.RecalculateNormals();

            MeshRenderer renderer = clone.AddComponent<MeshRenderer>();
            renderer.material = decal.DecalMaterial;
            mainDecalCon.decal[id].SetActive(false);
            mainDecalCon.meshDecal[id].SetActive(true);
        }
        if (id == 4) 
        {
            if(!_BaseData._BaseDataSave.openLid&& _BaseData._BaseDataSave.selectedID<3) mainDecalCon.meshDecal[4].SetActive(false);
            mainDecalCon.waitDecal = false;
        }
        yield return null;

    }
}


