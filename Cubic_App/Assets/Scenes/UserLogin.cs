using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Newtonsoft.Json;

public class UserLogin : MonoBehaviour
{
    public static UserLogin _userLogin;
    [SerializeField] GameObject UserDataCanvas, LoginCanvas;
    [Header("Data")]
    [SerializeField] RawImage profileImageData;
    [SerializeField] Text usernameData;
    [SerializeField] Text emailData;
    [SerializeField] Text LastLogin;
    [SerializeField] Text appVersion;
    [SerializeField] Text SubData;
    [Header("Line")]
    [SerializeField] Text usernameLine;
    jsonUserClass jsc = new jsonUserClass();


    public Coroutine gettingAPI;// check if base api valid

    string usercachLoad;
    private void Awake()
    {
        _userLogin = this;

    }

    private void Start()
    {
        if (!PlayerPrefs.HasKey("appCount"))
        {
            PlayerPrefs.SetInt("appCount", 1);
        }
        else
        {
            PlayerPrefs.SetInt("appCount", PlayerPrefs.GetInt("appCount", 0) + 1);
        }
        if (gettingAPI == null)
            gettingAPI = StartCoroutine(getAPIPath());

#if(UNITY_EDITOR)
        if (PlayerPrefs.HasKey("USERDATA")) PlayerPrefs.DeleteKey("USERDATA");
        // userdataPostLineTest();
        StartCoroutine(getUserTagTest());
#endif

        if (PlayerPrefs.GetString("USERDATA") == "LI.Key")
        {
            userdataPostLine();

        }
        else if (PlayerPrefs.GetString("USERDATA") == "GG.Key")
        {
            userdataPost();

        }

    }

    public void closeUser()
    {
        UserDataCanvas.transform.localScale = Vector3.zero;
        LoginCanvas.transform.localScale = Vector3.zero;
    }
    public void ShowUser()
    {
        closeUser();

        if (PlayerPrefs.HasKey("USERDATA"))
        {
            UserDataCanvas.transform.localScale = Vector3.one;

            usernameData.text = PlayerPrefs.GetString("USERName");

            appVersion.text = Application.version;

            if (PlayerPrefs.GetString("USERDATA") == "LI.Key")
                emailData.text = "Line Login";
            else
                emailData.text = PlayerPrefs.GetString("USERMail");

            LastLogin.text = System.DateTime.Now.ToString("d/MM/yyyy");
            SubData.text = "Free";
            StartCoroutine(loadProfilePicture());
            //  userdataPost();
        }
        else
        { LoginCanvas.transform.localScale = Vector3.one; }//show login when signout

    }

    private void Update()
    {

    }

    IEnumerator loadProfilePicture()
    {
        var www = UnityWebRequestTexture.GetTexture(PlayerPrefs.GetString("USERImgURL"));
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.LogError(www.error);
        }
        else
        {
            var texture = DownloadHandlerTexture.GetContent(www);
            profileImageData.texture = texture;
        }
        yield return null;
    }
    public void Logout()
    {
        if (PlayerPrefs.HasKey("USERDATA"))
        {
            PlayerPrefs.DeleteKey("USERDATA");
            PlayerPrefs.DeleteKey("USERKey");
            PlayerPrefs.DeleteKey("USERName");
            PlayerPrefs.DeleteKey("GiveName");
            PlayerPrefs.DeleteKey("FamilyName");
            PlayerPrefs.DeleteKey("USERMail");
            PlayerPrefs.DeleteKey("USERImgURL");
            _BaseData._BaseDataSave.userTag = "";
            _BaseData._BaseDataSave.userID = "";
            _BaseData._BaseDataSave.designID = "";
        }
        ShowUser();
    }
    string userlog;
    public void userdataPost()
    {
        jsonUserClass userdata = new jsonUserClass();
        userdata.logintype = "Google";
        userdata.loginid = PlayerPrefs.GetString("USERKey");
        userdata.email = PlayerPrefs.GetString("USERMail");
        userdata.username = PlayerPrefs.GetString("USERName");
        userdata.firstname = PlayerPrefs.GetString("GiveName");
        userdata.lastname = PlayerPrefs.GetString("FamilyName");
        userdata.phonenumber = "00";
        userdata.lastlogin = System.DateTime.Now.ToString("d/MM/yyyy");
        userdata.logintime = PlayerPrefs.GetInt("appCount", 0).ToString();
        userdata.accounttype = "1";
        userlog = JsonConvert.SerializeObject(userdata);
        StartCoroutine(Upload(userlog));

    }
    public void userdataPostLine()
    {
        jsonUserClass userdata = new jsonUserClass();
        userdata.logintype = "Line";
        userdata.loginid = PlayerPrefs.GetString("USERKey");
        userdata.email = PlayerPrefs.GetString("USERMail");
        userdata.username = PlayerPrefs.GetString("USERName");
        userdata.firstname = PlayerPrefs.GetString("GiveName");
        userdata.lastname = PlayerPrefs.GetString("FamilyName");
        userdata.phonenumber = "00";
        userdata.lastlogin = System.DateTime.Now.ToString("d/MM/yyyy");
        userdata.logintime = PlayerPrefs.GetInt("appCount", 0).ToString();
        userdata.accounttype = "1";
        userlog = JsonConvert.SerializeObject(userdata);
        StartCoroutine(Upload(userlog));
    }
    IEnumerator Upload(string dataJson)//send user to login log
    {
        if (_BaseData._BaseDataSave.targetLink == "" && gettingAPI == null)
        {
            yield return gettingAPI = StartCoroutine(getAPIPath());
        }
        var uwr = new UnityWebRequest
            (_BaseData._BaseDataSave.targetLink + "?action=sendUserData", "POST");

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(dataJson);

        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        //Send the request then wait here until it returns
        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success)
        {
            CallBackNotice._callBackNotice._noticBark("Login Error");
        }
        else
        {
            GUIUtility.systemCopyBuffer = uwr.downloadHandler.text;
            if (uwr.downloadHandler.text.IndexOf("Add new user and success create new Sheet") > -1)
            {
                CallBackNotice._callBackNotice._noticBark("Log in as new User");
                StartCoroutine(getUserTag());
            }
            else if (uwr.downloadHandler.text.IndexOf("change") > -1)
            {
                CallBackNotice._callBackNotice._noticBark("Login");
                StartCoroutine(getUserTag());
            }
            else
            {
                CallBackNotice._callBackNotice._noticBark("Please wait");
                yield return new WaitForSeconds(5);
                StartCoroutine(Upload(userlog));
            }
        }

    }
    public IEnumerator getAPIPath()
    {

        using (UnityWebRequest www = UnityWebRequest.Get("https://script.google.com/macros/s/AKfycby3MtxJ30FAxpjDOIO3gEq1Jq_Qu6F5VdHdexJ4jC0zNRmqJi4hMUkemRANtB3VWaWyzA/exec?action=getAPILink&Time=" + System.DateTime.Now))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                CallBackNotice._callBackNotice._noticBark("Get connection fail");
            }
            else
            {
                _BaseData._BaseDataSave.targetLink = www.downloadHandler.text;

            }
        }
        gettingAPI = null;
        // if (DeepLinkActive._deepLinkActive.startdeeplink) { DeepLinkActive._deepLinkActive.altDeepLinkActivated(); }
        yield return null;
    }

    IEnumerator getUserTag()//get userTag
    {
        // if (!PlayerPrefs.HasKey("USERDATA"))
        // {
        //     CallBackNotice._callBackNotice._noticBark("Guest Mode"); yield break;
        // }
        string getreq = _BaseData._BaseDataSave.targetLink + "?action=getUserData&loginid=" + PlayerPrefs.GetString("USERKey");
        

        using (UnityWebRequest www = UnityWebRequest.Get(getreq))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                CallBackNotice._callBackNotice._noticBark("Getting user's data fail");
            }
            else
            {
                // GUIUtility.systemCopyBuffer = www.downloadHandler.text;
                List<jsonUserClass> jsc = JsonConvert.DeserializeObject<List<jsonUserClass>>(www.downloadHandler.text);
                _BaseData._BaseDataSave.userTag = jsc[0].usersheetid;
                _BaseData._BaseDataSave.userID = jsc[0].ID;
                CallBackNotice._callBackNotice._noticBark("Login Success");
                //  CallBackNotice._callBackNotice._noticBark(DeepLinkActive._deepLinkActive.startdeeplink.ToString());
            }
        }

        yield return null;
    }

    //public IEnumerator getUserData()//get userData on savedUser dep
    //{
    //    string getreq = _BaseData._BaseDataSave.targetLink + "?action=getUserData&loginid=" + PlayerPrefs.GetString("USERKey");
    //    using (UnityWebRequest www = UnityWebRequest.Get(getreq))
    //    {
    //        yield return www.SendWebRequest();
    //
    //        if (www.result != UnityWebRequest.Result.Success)
    //        {
    //            CallBackNotice._callBackNotice._noticBark("LoadUserDataError");
    //        }
    //        else
    //        {
    //            usercachLoad = www.downloadHandler.text;
    //            CallBackNotice._callBackNotice._noticBark("Login Success");
    //        }
    //    }
    //    yield return null;
    //}
    public void userdataPostLineTest()
    {
        jsonUserClass userdata = new jsonUserClass();
        PlayerPrefs.SetString("USERKey", "123456");
        userdata.logintype = "Line";
        userdata.loginid = "123456";
        userdata.email = "testmail";
        userdata.username = "testname";
        userdata.firstname = "testfirstname";
        userdata.lastname = "testlastname";
        userdata.phonenumber = "00";
        userdata.lastlogin = System.DateTime.Now.ToString("d/MM/yyyy");
        userdata.logintime = PlayerPrefs.GetInt("appCount", 0).ToString();
        userdata.accounttype = "1";
        userlog = JsonConvert.SerializeObject(userdata);
        StartCoroutine(Upload(userlog));

        //StartCoroutine(getUserTagTest());

    }
    IEnumerator getUserTagTest()
    {
        if (_BaseData._BaseDataSave.targetLink == "" && gettingAPI == null)
        {
            yield return gettingAPI = StartCoroutine(getAPIPath());
        }
        else 
        {
            yield return new WaitUntil(() => gettingAPI == null);
        }
        string getreq = _BaseData._BaseDataSave.targetLink + "?action=getUserData&loginid=parm_51140@hotmail.com";
        //string getreq = _BaseData._BaseDataSave.targetLink + "?action=getUserData&loginid=Uc6d728784a2a2d4f9108292abba75779";
        //string getreq = _BaseData._BaseDataSave.targetLink + "?action=getUserData&loginid=123456";
        print(getreq);
        using (UnityWebRequest www = UnityWebRequest.Get(getreq))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                CallBackNotice._callBackNotice._noticBark("UserDataError");
            }
            else
            {
                GUIUtility.systemCopyBuffer = www.downloadHandler.text + "  AAAAAAA";
                List<jsonUserClass> jsc = JsonConvert.DeserializeObject<List<jsonUserClass>>(www.downloadHandler.text);
                _BaseData._BaseDataSave.userTag = jsc[0].usersheetid;
                _BaseData._BaseDataSave.userID = jsc[0].ID;
                CallBackNotice._callBackNotice._noticBark("Login Success");

            }
        }
        yield return null;
    }

}
