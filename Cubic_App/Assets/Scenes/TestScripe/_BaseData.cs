using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;

public class _BaseData : MonoBehaviour
{
    public static _BaseData _BaseDataSave;
    [Header("Network")]
    public string userTag;
    public string userID;
    public string designID;
    public string targetLink;

    [Header("MainMenu")]
    public int MainmenuSelect;
    public int SubmenuSelect;
    [Header("Freehand")]
    public bool usedEditCamera;
    [Header("ToggleMenu")]
    public bool openLid;
    public bool openWireframe;
    public bool openIsoView;
    [Header("Decal")]
    public int DecalActiveMode;//0:non  1:wrap  2:sticker
    public bool hasSaveDecal;//0:non  1:wrap  2:sticker

    [Header("Object")]
    public int selectedID;
    public float saveWidth;
    public float SaveLength;
    public float SaveHeight;
    public float Saveslope;
    public float Savecurve;
    public float SavePos;

    public jsonDataClass jsc;

    private void Awake()
    {
        if (_BaseDataSave == null)
        {
            _BaseDataSave = this;
        }
        // DontDestroyOnLoad(this.gameObject); }
        // else Destroy(this.gameObject) ;
    }
    private void Update()
    {

    }
    public string ObjectToJson()
    {
        jsc = new jsonDataClass();
        jsc.selectedID = selectedID;
        jsc.savewidth = saveWidth;
        jsc.savelength = SaveLength;
        jsc.saveheight = SaveHeight;
        jsc.saveslope = Saveslope;
        jsc.savecurve = Savecurve;
        jsc.decalMode = _BaseData._BaseDataSave.DecalActiveMode;
        switch (selectedID)
        {
            case 1: jsc.type = "Box"; break;
            case 2: jsc.type = "Cup"; break;
            case 3: jsc.type = "SandwichBox"; break;
            case 4: jsc.type = "Can"; break;
            case 5: jsc.type = "Carton"; break;
        }

        string sec = JsonConvert.SerializeObject(jsc);
        return sec;
    }


}

public class jsonDataClass//object shape data
{
    public int selectedID;
    public string type;
    public float savewidth;
    public float savelength;
    public float saveheight;
    public float saveslope;
    public float savecurve;
    public float savePos;
    public int decalMode;

}

public class jsonUserClass
{
    public string ID;
    public string logintype;
    public string loginid;
    public string email;
    public string username;
    public string firstname;
    public string lastname;
    public string phonenumber;
    public string memberdate;
    public string lastlogin;
    public string logintime;
    public string accounttype;
    public string usersheetid;
}

public class objectSaveJson
{
    public string id;
    public string name;
    public string type;
    public string savedata;
    public string savesticker;
    public string cad;
    public string deletedate;
    public string paid;
}
public class vec3tostr 
{
    public float x;
    public float y;
    public float z;
}

