using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SaveGalleryManage : MonoBehaviour
{
    public string selectID;
    public int ListID;

    public void dragg()
    {
        if (gameObject.activeSelf) { SavedGallery._saveGallery.SetActiveManageBox(false, "",-1); }
    }
    public void delSelected() //deleteDesign
    {
        StartCoroutine(delModel());
        SavedGallery._saveGallery.SetActiveManageBox(false, "",-1);
    }
    public void cadSelected()
    {
      //load cadfile
        SavedGallery._saveGallery.SetActiveManageBox(false,"",-1);
    }
    public void shareSelected()
    {
        shareDeeplink();
    }
    IEnumerator delModel()
    {
        string getreq = _BaseData._BaseDataSave.targetLink + "?action=deleteDesign&sheetid=" + _BaseData._BaseDataSave.userTag + "&designid=" + selectID;
        print(getreq);
        using (UnityWebRequest www = UnityWebRequest.Get(getreq))
        {
            yield return www.SendWebRequest();
            if (www.result != UnityWebRequest.Result.Success)
            {
                CallBackNotice._callBackNotice._noticBark("Delete Unsuccess");
            }
            else
            {
                CallBackNotice._callBackNotice._noticBark("Delete Success");
                SavedGallery._saveGallery.showGallery(true);
            }
        }
        yield return null;
    }
    public void getOpen()
    {
        LoadHolder._loadHold.setLoad(true);
        _BaseData._BaseDataSave.designID = selectID;
        StartCoroutine(SavedGallery._saveGallery.LoadSelectedData(ListID));
    }


    public void shareDeeplink() //APILINK+?action=getDesignData&sendersheetid=sendersheetID&designid=senderdesignID
    {
        new NativeShare().SetText("http://cubic.thaipet.co.th?" + _BaseData._BaseDataSave.userTag + "&" + selectID)
           .SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
           .Share();
    }
}
