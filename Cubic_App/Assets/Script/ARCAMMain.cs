using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ARCAMMain : MonoBehaviour
{
    [SerializeField] GameObject ARGroup;
    [SerializeField] ARShowObject showObject;
   
    void Start()
    {
        
      if (DontDestroy.DontDestroyData.ARSceneGroup == null)
        {
            DontDestroy.DontDestroyData.ARSceneGroup = ARGroup;
        }
    }
  

    void Update()
    {
      // if (Input.GetKeyDown(KeyCode.E)) 
      // {
      //     closeAR();
      //   
      // }
    }
   public void closeAR() 
    {
        Destroy(DontDestroy.DontDestroyData.ShowObj);
        Destroy(DontDestroy.DontDestroyData.ARSceneGroup);
        SceneManager.LoadScene("CubicMain");
       

    }
}
