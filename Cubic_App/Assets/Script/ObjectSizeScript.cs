using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSizeScript : MonoBehaviour
{
    public static ObjectSizeScript _objectSizeScript;
    [SerializeField] LineRenderer HorizontalLine;
    [SerializeField] LineRenderer[] HorizontalBrasket;
    [SerializeField] TextMesh HorizontalText;
    [SerializeField] LineRenderer VerticalLine;
    [SerializeField] LineRenderer[] VerticalBrasket;
    [SerializeField] TextMesh VerticalText;
   // [SerializeField] GameObject editableObject;
    Bounds objectBound;
    float offsetY = 1f;
    float offsetX = 1f;

    float ExtendHorizontal;
    float SizeHorizontal;
    float ExtendVertical;
    float SizeVertical;

    float sizeOffset = 10 / 3f;

    string currentHorizontalFace;
    string currenVerticalFace;

    public float SizeX,SizeY,SizeZ;

    private void Awake()
    {
        _objectSizeScript = this;
    }

    public void setObject(GameObject editObj)
    {
        // if (editableObject == null) { editableObject = editObj; }
      //  editableObject = EditableObjectMaster._editmaster
       // objectBound = EditableObjectMaster._editmaster.currentObjectRenderer.bounds;
      
    }

    public void ActiveSizeDetail(bool active)
    {
        if (active)
        {
            gameObject.transform.localScale = Vector3.one;
        }
        else
        {
            gameObject.transform.localScale = Vector3.zero;
        }

    }
    public void setsize() 
    {
        objectBound = EditableObjectMaster._editmaster.currentObjectRenderer.bounds;
        SizeX = objectBound.size.x;
        SizeY = objectBound.size.y;
        SizeZ = objectBound.size.z;
        if (_BaseData._BaseDataSave.selectedID == 4)
        {
            float addTop = EditableObjectMaster._editmaster.currentObject.GetComponent<_CanEdit>().CoverObject.GetComponent<Renderer>().bounds.size.y;
            float addBase = EditableObjectMaster._editmaster.currentObject.GetComponent<_CanEdit>().baseObject.GetComponent<Renderer>().bounds.size.y;
            float addring = EditableObjectMaster._editmaster.currentObject.GetComponent<_CanEdit>().ringObject.GetComponent<Renderer>().bounds.size.y;
            SizeY = addTop + addBase + addring;
        }
    }
    public void chceckCurrFace()
    {
       // if (!Main.MainControl.IsEditing) return;
        
        currentHorizontalFace = Main.MainControl.currHorizontalFace.ToString();// front back left right
        currenVerticalFace = Main.MainControl.currVertivalFace.ToString();// top side bottom
                                                                          // isShowFilp = Main.MainControl.revertVertical;
        
        if (currenVerticalFace == "Side")
        {
            if (currentHorizontalFace == "Front" || currentHorizontalFace == "Back")
            {
                //print("X:Y");
                ExtendHorizontal = objectBound.extents.x;
                SizeHorizontal = objectBound.size.x;
                ExtendVertical = objectBound.extents.y;
                SizeVertical = objectBound.size.y;
            }
            else if (currentHorizontalFace == "Left" || currentHorizontalFace == "Right")
            {
                //print("Z:Y");
                ExtendHorizontal = objectBound.extents.z;
                SizeHorizontal = objectBound.size.z;
                ExtendVertical = objectBound.extents.y;
                SizeVertical = objectBound.size.y;
            }
        }
        else
        {
            if (currentHorizontalFace == "Front" || currentHorizontalFace == "Back")
            {
                //print("X:Z");
                ExtendHorizontal = objectBound.extents.x;
                SizeHorizontal = objectBound.size.x;
                ExtendVertical = objectBound.extents.z;
                SizeVertical = objectBound.size.z;
            }
            else if (currentHorizontalFace == "Left" || currentHorizontalFace == "Right")
            {
                //print("Z:X");
                ExtendHorizontal = objectBound.extents.z;
                SizeHorizontal = objectBound.size.z;
                ExtendVertical = objectBound.extents.x;
                SizeVertical = objectBound.size.x;
            }
        }

        //.02 min
        float lineSize = 30f/Main.MainControl.orbitCamera.orthographicSize *.1f;
        if (lineSize < .1f) lineSize = .1f;
        if (HorizontalLine.startWidth != lineSize)
        {
            HorizontalLine.startWidth = lineSize;
            HorizontalLine.endWidth = lineSize;
            HorizontalBrasket[0].startWidth = lineSize;
            HorizontalBrasket[0].endWidth = lineSize;
            HorizontalBrasket[1].startWidth = lineSize;
            HorizontalBrasket[1].endWidth = lineSize;

            VerticalLine.startWidth = lineSize;
            VerticalLine.endWidth = lineSize;
            VerticalBrasket[0].startWidth = lineSize;
            VerticalBrasket[0].endWidth = lineSize;
            VerticalBrasket[1].startWidth = lineSize;
            VerticalBrasket[1].endWidth = lineSize;
        }
        setOB();
    }
 
    private void Update()
    {     
        if (TouchControl._ScreenTouchControl.IsEditing)
        {
            HorizontalText.fontSize = (int)(Main.MainControl.orbitCamera.orthographicSize / 30 * 40);
            VerticalText.fontSize = HorizontalText.fontSize;
            objectBound = EditableObjectMaster._editmaster.currentObjectRenderer.bounds;
            chceckCurrFace();
        }
        

    }
    void setOB()
    {
        float miny = (4f * Main.MainControl.orbitCamera.aspect * 16f / 9f) * Main.MainControl.orbitCamera.orthographicSize / 4.8f;
        float maxx = (2.3f * Main.MainControl.orbitCamera.aspect * 16f / 9f) * Main.MainControl.orbitCamera.orthographicSize / 4.8f;
        float ExtendVerticalOffset = ExtendVertical + offsetY;
        float ExtendHorizontalOffset = ExtendHorizontal + offsetX;
        if (ExtendHorizontalOffset > maxx) { ExtendHorizontalOffset = maxx; }
        if (ExtendVerticalOffset > miny) { ExtendVerticalOffset = miny; }


        HorizontalLine.transform.localPosition = new Vector3(0, -(ExtendVerticalOffset), 0);
        HorizontalLine.SetPosition(0, new Vector3(-ExtendHorizontal, 0, 0));
        HorizontalLine.SetPosition(1, new Vector3(ExtendHorizontal, 0, 0));
        HorizontalBrasket[0].transform.localPosition = new Vector3(-ExtendHorizontal, 0, 0);
        HorizontalBrasket[1].transform.localPosition = new Vector3(ExtendHorizontal, 0, 0);
        HorizontalText.text = (SizeHorizontal ).ToString("F2") + " cm";     

        VerticalLine.transform.localPosition = new Vector3(ExtendHorizontalOffset, 0, 0);
        VerticalLine.SetPosition(0, new Vector3(0, -ExtendVertical, 0));
        VerticalLine.SetPosition(1, new Vector3(0, ExtendVertical, 0));
        VerticalBrasket[0].transform.localPosition = new Vector3(0, -ExtendVertical, 0);
        VerticalBrasket[1].transform.localPosition = new Vector3(0, ExtendVertical, 0);
        VerticalText.text = (SizeVertical ).ToString("F2") + " cm";

        if (_BaseData._BaseDataSave.selectedID == 4 && Main.MainControl.currVertivalFace == Main.VertivalFace.Side)
        {
            float addTop = EditableObjectMaster._editmaster.currentObject.GetComponent<_CanEdit>().CoverObject.GetComponent<Renderer>().bounds.size.y;
            float addBase = EditableObjectMaster._editmaster.currentObject.GetComponent<_CanEdit>().baseObject.GetComponent<Renderer>().bounds.size.y;
            float addring = EditableObjectMaster._editmaster.currentObject.GetComponent<_CanEdit>().ringObject.GetComponent<Renderer>().bounds.size.y;

            HorizontalLine.transform.localPosition = new Vector3(0, -(ExtendVerticalOffset + addBase + addring), 0);

            VerticalLine.SetPosition(0, new Vector3(0, -(ExtendVertical+addBase+addring), 0));
            VerticalLine.SetPosition(1, new Vector3(0, ExtendVertical+addTop, 0));
            VerticalBrasket[0].transform.localPosition = new Vector3(0, -(ExtendVertical + addBase + addring), 0);
            VerticalBrasket[1].transform.localPosition = new Vector3(0, ExtendVertical+addTop, 0);
            VerticalText.text = (SizeVertical+addBase+addring+addTop).ToString("F2") + " cm";

        }

    }
}
