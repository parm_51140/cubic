﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace dev.cubic
{
    public class PointTouch : MonoBehaviour
    {
        [SerializeField] Text currentFace;
        enum State { none, Zoom, Swipe, Touch, squeez }
        State currTouchState = PointTouch.State.none;
        //  [SerializeField] Camera orbitCamera;
        //  [SerializeField] Camera mainCamera;
        [SerializeField] Main mainControl;
        [SerializeField] ObjectReshape reshape;
        // [SerializeField] DecalControl DecalController;
        [SerializeField] ObjectSizeScript obsize;
        //[SerializeField] FreeHandCamera freeHand;
        [SerializeField] MainMenuControl mainMenuControl;
        [SerializeField] GridBackGround gridBackGround;
        [SerializeField] DecalControl DecalCon;
        public bool hold = false;
        public Vector3 down;
        public Vector3 current;
        public RotationControl rotationControl;
        // public CompassControl compassControl, compassControlVert;
        public float downtime;
        public Text touchdebug;
        public string mode;
        float startDelta;
        float currentDelta;

        public float scale = 1;

        Vector3 scaleDimention;
        Vector3 MaxscaleDimention, MinscaleDimention;
        Vector3 savescaleDimention;

        Vector3 scaleTop, scaleBottom;
        Vector3 saveScaleTop, saveScaleBottom;

        int touchC = 1;
        int touchCT = 1;

        public EditModeControl editCon;//???
        Vector3 delta;
        CompassControl compassCon;
        // bool onEdit = false;
        public float tempOrtho;
        Coroutine autofitLerp;
        Bounds defaultBounds;
        float cameraThresh;

        public static PointTouch _pointTouch;
        private void Awake()
        {
           
            PointTouch._pointTouch = this;
        }
        void Start()
        {

            mainControl = Main.MainControl;
            reshape = ObjectReshape.objReshape.GetComponent<ObjectReshape>();
            tempOrtho = mainControl.orbitCamera.orthographicSize;
            if (DontDestroy.DontDestroyData.currPointTouch == null) { DontDestroy.DontDestroyData.currPointTouch = this; }

            if (DontDestroy.DontDestroyData.saved)
            {
                scaleDimention = DontDestroy.DontDestroyData.savescaleDimention;
                scaleTop = DontDestroy.DontDestroyData.saveScaleTop;
                scaleBottom = DontDestroy.DontDestroyData.saveScaleBottom;
                savescaleDimention = scaleDimention;
                saveScaleTop = scaleTop;
                saveScaleBottom = scaleBottom;
            }
            else
            {
                scaleDimention = Vector3.zero;
                savescaleDimention = Vector3.zero;
                saveScaleTop = Vector3.zero;
                saveScaleBottom = Vector3.zero;
            }
        }

        bool pointOnEvent(Vector2 value)
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                //  return true;
            }
            PointerEventData eventPos = new PointerEventData(EventSystem.current);
            eventPos.position = new Vector2(value.x, value.y);
            List<RaycastResult> result = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventPos, result);
            return result.Count > 0;
        }
        public void Update()
        {

            if (Input.touchCount == 2) //&& !mainControl.IsEditing)
            {
                if (pointOnEvent(Input.mousePosition)) { return; }
                Touch[] point = new Touch[2];
                point[0] = Input.GetTouch(0);
                point[1] = Input.GetTouch(1);
                if (Input.GetTouch(1).phase == TouchPhase.Began)
                {
                    startDelta = (point[0].position - point[1].position).magnitude;
                    currentDelta = startDelta;
                }

                if (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(1).phase == TouchPhase.Moved)
                {
                    currentDelta = (point[0].position - point[1].position).magnitude;
                    scale = (currentDelta - startDelta) / 1000f;     //+1              
                    mode = "camscale";
                }
            }
            else if (Input.touchCount > 0)
            {
                mode = "pointer";
            }
            touchC = Input.touchCount;
            if (touchC == 2) { touchCT = 2; }
            if (touchCT == 2 && touchC == 1)
            {
                savescaleDimention = scaleDimention;
                saveScaleTop = scaleTop;
                saveScaleBottom = scaleBottom;
                touchCT = 1;
                tempOrtho = mainControl.orbitCamera.orthographicSize;
                delta = Vector3.zero;
                scale = 0;
            }
        }
        private void OnMouseDown()
        {
            //  Vector2 aa = Input.mousePosition;
            cameraThresh = (Main.MainControl.mainCamera.orthographicSize / 4.8f);
            if (pointOnEvent(Input.mousePosition)) { return; }///////////////////////////
            down = Input.mousePosition;
            if (Input.touchCount <= 1)
            {
                if (!hold)
                {
                    hold = true;
                    downtime = Time.time;
                }
                if (_BaseData._BaseDataSave.MainmenuSelect !=3) { FreehandCamera_R.freehandCamera.SetStartFreehand(); }//!!!!!!!!!!
                else if (_BaseData._BaseDataSave.MainmenuSelect == 3 && MainMenuGroupControl._mainmenugroupcontrol.subMenuSelected ==3 && mainControl.IsEditingLabel)//editdec
                {
                    // mainControl.decalCon.startmoveDecal();
                    DecalControl2._decalControl.startmoveDecal();
                }
            }
            defaultBounds = reshape.ShapeDefaultBound;
            MaxscaleDimention.x = (60 / (defaultBounds.size.x * 10 / 3f) - 1);//
            MaxscaleDimention.z = (60 / (defaultBounds.size.z * 10 / 3f) - 1);//
            MaxscaleDimention.y = (10 / (defaultBounds.size.y * 10 / 3f) - 1);//

            MinscaleDimention.y = (1 / (defaultBounds.size.y * 10 / 3f) - 1);//
            MinscaleDimention.x = (1 / (defaultBounds.size.x * 10 / 3f) - 1);//
            MinscaleDimention.z = (1 / (defaultBounds.size.z * 10 / 3f) - 1);//
        }
        public void forceExitEdit()
        {
            mainControl.IsEditing = false;
            mainControl.ShapeSummartView(false);
            reshape.activeMaterial(false);
            reshape.refreashCollider();
            mainControl.toggleMenuActibeBtn(1, true);
          //  DecalControl2._decalControl.RefreashDecalPos();
           // mainControl.Hidecompass(mainControl.currentMainMenu != 1);
          //  mainControl.closeRuler();
        }
        private void OnMouseUp()
        {
            if (pointOnEvent(Input.mousePosition) || Main.MainControl.WaitEditRotation) { return; }
            if (_BaseData._BaseDataSave.MainmenuSelect == 3 && Input.touchCount <= 1 && currTouchState == PointTouch.State.Touch)
            {
                autoFitcam();
                if (delta.magnitude < 2f && MainMenuGroupControl._mainmenugroupcontrol.subMenuSelected == 1)//toggle editmode
                {
                    mainControl.IsEditing = !mainControl.IsEditing;
                    if (mainControl.IsEditing)
                    {
                        mainControl.ShapeSummartView(false);
                        mainControl.toggleMenuActibeBtn(1, false);
                    //  mainControl.Hidecompass(true);
                        reshape.activeMaterial(true);
                        reshape.refreashCollider();
                        obsize.chceckCurrFace();
                        ObjectCover._objectcover.hidecover(true);
                        // DecalCon.activeDecal(false);
                        DecalControl2._decalControl._setActiveDecalObject(false);
                    }
                    else
                    {
                        //mainControl.toggleWireFrame(false);
                        reshape.refreashCollider();
                        mainControl.toggleMenuActibeBtn(1, true);
                   //   mainControl.Hidecompass(false);
                        mainControl.ShapeSummartView(true);
                        reshape.activeMaterial(false);
                        ObjectCover._objectcover.hidecover(false);
                        //DecalCon.activeDecal(true) ;
                        DecalControl2._decalControl._setActiveDecalObject(true);
                      // DecalControl2._decalControl.RefreashDecalPos(); 
                        // mainControl.closeRuler();
                    }
                }
                else if (delta.magnitude < 2f && MainMenuGroupControl._mainmenugroupcontrol.subMenuSelected == 3)//toggle editmode Label
                {
                    if (mainControl.IsEditing)
                    {
                        mainControl.IsEditing = false;
                        reshape.refreashCollider();
                        mainControl.toggleMenuActibeBtn(1, true);                     
                        mainControl.ShapeSummartView(true);
                      // reshape.activeMaterial(false);
                        WorldRulerSetting._worldruler.clearScale();
                    }
                     mainControl.IsEditingLabel = !mainControl.IsEditingLabel;                 
                    DecalControl2._decalControl.ActiveEditDecal(!DecalControl2._decalControl.EditingDecal);

                }
                if (mainControl.IsEditing)
                {
                    WorldRulerSetting._worldruler.resetScale();
                    GridBackGround._GridBackGround.resetGrideBG();
                }
                reshape.refreshSize();
                currTouchState = PointTouch.State.none;
            }//end dimentional editShape

            if (currTouchState == PointTouch.State.Zoom)
            {
                tempOrtho = mainControl.orbitCamera.orthographicSize;
                gridBackGround.resetGrideBG();
                currTouchState = PointTouch.State.none;
                mainControl.resetRuler();
                gridBackGround.resetGrideBG();//????
            }//end Zoom
            if (currTouchState == PointTouch.State.squeez)
            {
                autoFitcam();
                if (mainControl.IsEditing)
                { mainControl.resetRuler(); gridBackGround.resetGrideBG(); }
                reshape.refreshSize();
                currTouchState = PointTouch.State.none;
            }//end facesize editShape
            savescaleDimention = scaleDimention;
            saveScaleTop = scaleTop;
            saveScaleBottom = scaleBottom;
            reshape.setShape();
            scale = 0;
            hold = false;
            if (_BaseData._BaseDataSave.MainmenuSelect != 3) { FreehandCamera_R.freehandCamera.SetStartFreehand(); }
        }// end mouse up
        private void OnMouseDrag()
        {
            if (Input.touchCount <= 1 && (currTouchState == PointTouch.State.none || currTouchState == PointTouch.State.Touch))
            {
                currTouchState = PointTouch.State.Touch;
                if (!hold)
                {
                    return;
                }
                current = Input.mousePosition;
                delta = (down - current);// *cameraThresh;
                ///rotateObect
                if (delta.magnitude > 30f)
                {
                    if (Time.time - downtime < 10f && !mainControl.IsEditing && currTouchState != PointTouch.State.Zoom && currTouchState != PointTouch.State.squeez)
                    {
                        if (_BaseData._BaseDataSave.MainmenuSelect == 3 && MainMenuGroupControl._mainmenugroupcontrol.subMenuSelected == 1 || (_BaseData._BaseDataSave.MainmenuSelect == 3 && !mainControl.IsEditingLabel))
                        {
                            if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y))
                            {
                                if (rotationControl.verticalStep % 4 == 0 || (rotationControl.verticalStep - 1) % 4 == 0)
                                {
                                    if (delta.x < 0)
                                    {
                                        rotationControl.Left();
                                        //compassControl.SwapLeft(true);
                                    }
                                    else
                                    {
                                        rotationControl.Right();
                                        // compassControl.SwapLeft(false);
                                    }
                                }
                                else
                                {
                                    if (delta.x > 0)
                                    {
                                        rotationControl.Left();
                                        //compassControl.SwapLeft(true);
                                    }
                                    else
                                    {                                     
                                        rotationControl.Right();
                                        //  compassControl.SwapLeft(false);
                                    }
                                }
                                hold = false;
                            }
                            else
                            {
                                if (delta.y > 0)
                                { rotationControl.Down(); }//compassControlVert.swapTop(false); }
                                else
                                {
                                    rotationControl.Up(); }//compassControlVert.swapTop(true); }
                                hold = false;
                            }
                        }//end step screenrotate
                        else if (_BaseData._BaseDataSave.MainmenuSelect == 3 && MainMenuGroupControl._mainmenugroupcontrol.subMenuSelected == 3)
                        {
                           // mainControl.decalCon.moveDecalSingle(delta);
                            DecalControl2._decalControl.moveDecalSingle(delta);
                        }
                        else if (_BaseData._BaseDataSave.MainmenuSelect == 4)
                        {
                            //  mainControl.freehand.FreeHandMove(delta);
                            FreehandCamera_R.freehandCamera.FreeHandMove(delta);
                        }
                    }//end screenrotate
                    else if (mainControl.IsEditing)//editshape 
                    {
                        if (down.x < mainControl.mainCamera.pixelWidth / 2)
                        {
                            delta.x = -delta.x;
                            delta.z = -delta.z;
                        }
                        if (down.y < mainControl.mainCamera.pixelHeight / 2)
                        {
                            delta.y = -delta.y;
                        }
                        if (mainControl.currVertivalFace == Main.VertivalFace.Side)
                        {
                            if (mainControl.currHorizontalFace == Main.HorizontalFace.Front || mainControl.currHorizontalFace == Main.HorizontalFace.Back)
                            {
                                scaleDimention.x = Mathf.Clamp(savescaleDimention.x + delta.x / 1000, -MaxscaleDimention.x, -MinscaleDimention.x);
                                scaleDimention.y = Mathf.Clamp(savescaleDimention.y + delta.y / 1000, -MaxscaleDimention.y, -MinscaleDimention.y);
                            }
                            else if (mainControl.currHorizontalFace == Main.HorizontalFace.Left || mainControl.currHorizontalFace == Main.HorizontalFace.Right)
                            {
                                scaleDimention.z = Mathf.Clamp(savescaleDimention.z + delta.x / 1000, -MaxscaleDimention.z, -MinscaleDimention.z);
                                scaleDimention.y = Mathf.Clamp(savescaleDimention.y + delta.y / 1000, -MaxscaleDimention.y, -MinscaleDimention.y);
                            }
                           
                            reshape.Scaler(scaleDimention, saveScaleTop, saveScaleBottom, true);
                        }
                        else if (mainControl.currVertivalFace == Main.VertivalFace.Top || mainControl.currVertivalFace == Main.VertivalFace.Bottom)
                        {

                            if (mainControl.currHorizontalFace == Main.HorizontalFace.Front || mainControl.currHorizontalFace == Main.HorizontalFace.Back)
                            {
                                scaleDimention.x = Mathf.Clamp(savescaleDimention.x + delta.x / 1000, -MaxscaleDimention.x, -MinscaleDimention.x);
                                scaleDimention.z = Mathf.Clamp(savescaleDimention.z + delta.y / 1000, -MaxscaleDimention.z, -MinscaleDimention.z);
                            }
                            else if (mainControl.currHorizontalFace == Main.HorizontalFace.Left || mainControl.currHorizontalFace == Main.HorizontalFace.Right)
                            {
                                scaleDimention.z = Mathf.Clamp(savescaleDimention.z + delta.x / 1000, -MaxscaleDimention.z, -MinscaleDimention.z);
                                scaleDimention.x = Mathf.Clamp(savescaleDimention.x + delta.y / 1000, -MaxscaleDimention.x, -MinscaleDimention.x);
                            }
                            reshape.Scaler(scaleDimention, saveScaleTop, saveScaleBottom, true);
                        }
                    }//end on edit                    
                }//end magnitudeCheck
            }//end input count <=1    
            else
            {
                if (!mainControl.IsEditing && !mainControl.IsEditingLabel)
                {
                    currTouchState = PointTouch.State.Zoom;
                    mainControl.orbitCamera.orthographicSize = Mathf.Clamp(tempOrtho - scale * 1.5f, 1.2f, 21.5f);//base4.8
                }
                else if (mainControl.IsEditing && !mainControl.IsEditingLabel)
                {
                    currTouchState = PointTouch.State.squeez;
                    if (down.y > (mainControl.mainCamera.pixelHeight / 2) + 100)//top scale
                    {
                        if (!mainControl.revertVertical)
                        {
                            if (mainControl.currHorizontalFace == Main.HorizontalFace.Front || mainControl.currHorizontalFace == Main.HorizontalFace.Back)
                            { scaleTop.x = Mathf.Clamp(saveScaleTop.x + scale, -MaxscaleDimention.x, -MinscaleDimention.x); }

                            else
                            { scaleTop.z = Mathf.Clamp(saveScaleTop.z + scale, -MaxscaleDimention.z, -MinscaleDimention.z); }
                            reshape.Scaler(savescaleDimention, scaleTop, saveScaleBottom, true);
                        }
                        else
                        {
                            if (mainControl.currHorizontalFace == Main.HorizontalFace.Front || mainControl.currHorizontalFace == Main.HorizontalFace.Back)
                            { scaleBottom.x = saveScaleBottom.x + scale; }
                            else { scaleBottom.z = saveScaleBottom.z + scale; }
                            reshape.Scaler(savescaleDimention, saveScaleTop, scaleBottom, false);
                        }
                    }
                    else
                    {
                        if (!mainControl.revertVertical)
                        {
                            if (mainControl.currHorizontalFace == Main.HorizontalFace.Front || mainControl.currHorizontalFace == Main.HorizontalFace.Back)
                            { scaleBottom.x = saveScaleBottom.x + scale; }
                            else { scaleBottom.z = saveScaleBottom.z + scale; }
                            reshape.Scaler(savescaleDimention, saveScaleTop, scaleBottom, false);
                        }
                        else
                        {
                            if (mainControl.currHorizontalFace == Main.HorizontalFace.Front || mainControl.currHorizontalFace == Main.HorizontalFace.Back)
                            { scaleTop.x = saveScaleTop.x + scale; }
                            else
                            { scaleTop.z = saveScaleTop.z + scale; }
                            reshape.Scaler(savescaleDimention, scaleTop, saveScaleBottom, true);
                        }
                    }

                }
                else if (!mainControl.IsEditing && DecalControl2._decalControl.EditingDecal)
                {
                    DecalControl2._decalControl.ScaleDecal(scale / 10);
                   // DecalCon.ScaleDecal(scale / 10);
                }//end isEditting check
            }//end input count > 1    
        }//end on mousedrag function
        void autoFitcam()
        {
            if (autofitLerp != null) { StopCoroutine(autofitLerp); }
            if (mainControl.currVertivalFace == Main.VertivalFace.Side)
            {
                if (reshape.renderBoundary().x > mainControl.orbitCamera.orthographicSize + .5f)
                {
                    // orbitCamera.orthographicSize = reshape.renderBoundary().x + .8f; tempOrtho = orbitCamera.orthographicSize;
                    autofitLerp = StartCoroutine(camLerp('x'));
                }
                if (reshape.renderBoundary().z > mainControl.orbitCamera.orthographicSize + .5f)
                {
                    //orbitCamera.orthographicSize = reshape.renderBoundary().z + .8f; tempOrtho = orbitCamera.orthographicSize;
                    autofitLerp = StartCoroutine(camLerp('z'));
                }
            }
            else
            {
                if (mainControl.currHorizontalFace == Main.HorizontalFace.Front || mainControl.currHorizontalFace == Main.HorizontalFace.Back)
                {
                    if (reshape.renderBoundary().x > mainControl.orbitCamera.orthographicSize + .5f)
                    {
                        autofitLerp = StartCoroutine(camLerp('x'));
                    }
                }
                else
                {
                    if (reshape.renderBoundary().z > mainControl.orbitCamera.orthographicSize + .5f)
                    {
                        autofitLerp = StartCoroutine(camLerp('z'));
                    }
                }
            }
        }
        IEnumerator camLerp(char axis)
        {
            float endValue = 00;
            float curr;
            float start = mainControl.orbitCamera.orthographicSize;
            float t = 0f;
            if (axis == 'x')
            { endValue = reshape.renderBoundary().x + 3.5f; }
            else
            { endValue = reshape.renderBoundary().z + 3.5f; }
            while (t < 1)
            {
                curr = Mathf.Lerp(start, endValue, t);
                mainControl.orbitCamera.orthographicSize = curr;
                t += 0.08f;
                yield return new WaitForSeconds(0.03f);
            }
            if (endValue > 21.5f) endValue = 21.5f;
            mainControl.orbitCamera.orthographicSize = endValue;
            tempOrtho = mainControl.orbitCamera.orthographicSize;
            mainControl.resetRuler();
            gridBackGround.resetGrideBG();
            obsize.chceckCurrFace();
            yield return null;
        }
        public void SaveAll()
        {
            DontDestroy.DontDestroyData.savescaleDimention = savescaleDimention;
            DontDestroy.DontDestroyData.saveScaleTop = saveScaleTop;
            DontDestroy.DontDestroyData.saveScaleBottom = saveScaleBottom;
           
        }

        public void ClearAll()
        {
            scaleDimention = Vector3.zero;
            savescaleDimention = Vector3.zero;
            scaleTop = Vector3.zero;
            scaleBottom = Vector3.zero;
            saveScaleTop = Vector3.zero;
            saveScaleBottom = Vector3.zero;
        }
    }
}
