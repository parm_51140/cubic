using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ARCoreToggle : MonoBehaviour
{
    [SerializeField] bool IsEditScene;

    [SerializeField] bool IsGridOn=false;
    [SerializeField] ARCAMMain ArcamMain;

  //  [SerializeField] bool IsplaneLineOn = false;
    [SerializeField] LineRenderer ARplaneLine;
    [SerializeField] Image ARplaneLinebtn;

    [SerializeField] GameObject gridGroup;
    [SerializeField] Image gridbtn;
    [SerializeField] Sprite[] gridImage;



    public void launchAR() 
    {
        Main.MainControl.startAR();
    }
   

    public void UIMouseDown()
    {
        if (ArcamMain != null)
        {
            ArcamMain.closeAR();
        }
    }

    public void UIGridToggle() 
    {
        if (IsGridOn)
        {
            IsGridOn = !IsGridOn;
            gridGroup.transform.localScale = Vector3.zero;
            gridbtn.sprite = gridImage[0];

        }
        else 
        {
            IsGridOn = !IsGridOn;
            gridGroup.transform.localScale = Vector3.one;
            gridbtn.sprite = gridImage[1];

        }
        
    }
    
    /*  public void UIPlaneLineToggle()
      {
          if (IsplaneLineOn)
          {
              IsplaneLineOn = !IsplaneLineOn;
              ARplaneLine.SetColors(new Color(16, 164, 112, 0), new Color(16, 164, 112, 0));
              ARplaneLinebtn.color = new Color(1, 1, 1, .5f);

          }
          else
          {
              IsplaneLineOn = !IsplaneLineOn;
              ARplaneLine.SetColors(new Color(16, 164, 112, 50), new Color(16, 164, 112, 50));
              ARplaneLinebtn.color = new Color(1, 1, 1, 1f);

          }

      }*/
}
