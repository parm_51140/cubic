using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.UI;

public class DecalSlide : MonoBehaviour
{
    public static DecalSlide _decalSlide;
    public bool isActive;
    [SerializeField] GameObject DiscObj;
    [SerializeField] DecalControl DecalCon;
    [SerializeField] Image[] IconImage;
    [SerializeField] Sprite[] DecalIconSprite;
    [SerializeField] Image CurrentDecalIcon;
    [SerializeField] Text CurrentDecalText;
    [SerializeField] GameObject ImportBtn;
    [SerializeField] GameObject reOpenSlideBtn;
    [SerializeField] GameObject removeDecalBtn;
    Vector3 startPos, currPos;
    float deltaX;
    float baseRot = 0;
    float targetRot = 0;
    float currRot = 0;
    bool hold;
    bool waitDecal;
    float timehold;
    private void Awake()
    {
        _decalSlide = this;
        // gameObject.transform.localScale
    }
    public void touchstart()
    {
        startPos = Input.mousePosition;
        baseRot = DiscObj.transform.localEulerAngles.z;
        hold = true;
        timehold = 0;
        deltaX = 0;
    }
    public void touchdrag()
    {
        timehold += Time.deltaTime;
        currPos = Input.mousePosition;
        deltaX = (startPos.x - currPos.x) *(1080f/Main.MainControl.mainCamera.pixelWidth)/10;
        DiscObj.transform.localEulerAngles = new Vector3(0, 0, Mathf.Clamp(baseRot + deltaX, -10, 75));
    }
    public void setRemove(bool canremove)
    {    
        if (canremove)
            removeDecalBtn.GetComponent<Image>().color = new Color(1, 1, 1, 1f);
        else 
            removeDecalBtn.GetComponent<Image>().color = new Color(1, 1, 1, .4f);
    }
    public void showAddRemove(bool show)
    {
        if (show)
        {
            removeDecalBtn.GetComponent<Image>().color = new Color(1, 1, 1, 1f);
            ImportBtn.GetComponent<Image>().color = new Color(1, 1, 1, 1f);
        }
        else
        {
            removeDecalBtn.GetComponent<Image>().color = new Color(1, 1, 1, .4f);
            ImportBtn.GetComponent<Image>().color = new Color(1, 1, 1, .4f);
        }
    }
    public void touchend()
    {

        currRot = DiscObj.transform.localEulerAngles.z;

        if (deltaX > 10 && timehold < .5f)
        {
            targetRot = Mathf.Clamp(targetRot + 30, 0, 60);
        }
        else if (deltaX < -10 && timehold < .5f)
        {
            targetRot = Mathf.Clamp(targetRot - 30, 0, 60);
        }
        else
        {
            //  print(DiscObj.transform.localEulerAngles.z);
            if (currRot < 15 || currRot > 340) targetRot = 0;
            else if (currRot < 45) targetRot = 30;
            else targetRot = 60;

        }
        // print(targetRot);
        hold = false;
    }
    public void forceSetDecal(int mode, bool callFunction) //0,1,2
    {
        foreach (Image i in IconImage)
        { 
            i.color = Color.white;
            i.transform.GetChild(0).GetComponent<Text>().color = Color.white;
        }
        DiscObj.transform.localEulerAngles = new Vector3(0, 0, mode * 30);
        IconImage[mode].color = Color.yellow;
        IconImage[mode].transform.GetChild(0).GetComponent<Text>().color = Color.yellow;
        targetRot = mode * 30;
        currRot = targetRot;
        if (callFunction)
            DecalModeSelect(mode);
        //  DecalCon.ModeOption(mode);
    }
    public void DecalDiscActive()
    {
        isActive = true;
        DecalControl2._decalControl.ActiveEditDecal(false);
        DiscObj.transform.gameObject.SetActive(true);
        gameObject.transform.localScale = Vector3.one;
        ImportBtn.SetActive(false);
        reOpenSlideBtn.SetActive(false);
        removeDecalBtn.SetActive(false);
    }
    public void DecalDiscDeactive()
    {
        //isActive = false;
        //DiscObj.transform.parent.gameObject.SetActive(false);
        //gameObject.transform.localScale = Vector3.zero;
        //ImportBtn.SetActive(true);
        //reOpenSlideBtn.SetActive(true);
        DecalModeSelect(_BaseData._BaseDataSave.DecalActiveMode);
    }
    public void DecalDiscHide()
    {
        isActive = false;
        DiscObj.transform.gameObject.SetActive(false);
        gameObject.transform.localScale = Vector3.zero;
        removeDecalBtn.SetActive(false);
        ImportBtn.SetActive(false);
        reOpenSlideBtn.SetActive(false);
    }
    public void DecalModeSelect(int mode)
    {
        isActive = false;
        DiscObj.transform.gameObject.SetActive(false);
        gameObject.transform.localScale = Vector3.zero;     
        if (mode == 0)
        {
            if (_BaseData._BaseDataSave.DecalActiveMode != mode)
            {
                _BaseData._BaseDataSave.DecalActiveMode = 0;
                DecalControl2._decalControl.ShowingDecal = false;
                DecalControl2._decalControl._setActiveDecalObject(false);
               //  DecalControl2._decalControl.SelectDecal();             
            }
            CurrentDecalIcon.sprite = DecalIconSprite[0];
            ImportBtn.SetActive(false);
            reOpenSlideBtn.SetActive(true);
            removeDecalBtn.SetActive(false);
            CurrentDecalText.text = "No Label";
        }
        else if (mode == 1)
        {
            if (_BaseData._BaseDataSave.DecalActiveMode != mode)
            {
                _BaseData._BaseDataSave.DecalActiveMode = 1;
                DecalControl2._decalControl.ShowingDecal = true;
                DecalControl2._decalControl._setActiveDecalObject(true);
                DecalControl2._decalControl.SelectDecal();
            }
            CurrentDecalIcon.sprite = DecalIconSprite[1];
            ImportBtn.SetActive(true);
            reOpenSlideBtn.SetActive(true);
            removeDecalBtn.SetActive(true);
            CurrentDecalText.text = "Wrap";
        }
        else if (mode == 2)
        {
            if (_BaseData._BaseDataSave.DecalActiveMode != mode)
            {
                _BaseData._BaseDataSave.DecalActiveMode = 2;
                DecalControl2._decalControl.ShowingDecal = true;
                DecalControl2._decalControl._setActiveDecalObject(true);
                DecalControl2._decalControl.SelectDecal();
            }
            CurrentDecalIcon.sprite = DecalIconSprite[2];
            ImportBtn.SetActive(true);
            reOpenSlideBtn.SetActive(true);
            removeDecalBtn.SetActive(true);
            CurrentDecalText.text = "Sticker";
        }
        DecalControl2._decalControl.CheckCurrFacel();
    }
    void setDecal()
    {
        if (waitDecal) return;
        waitDecal = true;
        // DecalCon.ModeOption((int)(targetRot / 30));
        DecalModeSelect((int)(targetRot / 30));
        waitDecal = false;

    }
    float lerpCount = 0;
    private void Update()
    {

        if (hold) return;
        if (targetRot != currRot)
        {
            lerpCount += Time.deltaTime;
            currRot = Mathf.LerpAngle(DiscObj.transform.localEulerAngles.z, targetRot, lerpCount);
            DiscObj.transform.localEulerAngles = new Vector3(0, 0, currRot);
            if (Mathf.Abs(targetRot - currRot) < 1f)
            {
                currRot = targetRot;
                lerpCount = 0;
                DiscObj.transform.localEulerAngles = new Vector3(0, 0, currRot);
                foreach (Image i in IconImage)
                {
                    i.color = Color.white;
                    i.transform.GetChild(0).GetComponent<Text>().color = Color.white;
                }
                int targetindex = (int)(targetRot / 30);
                IconImage[targetindex].color = Color.yellow;
                IconImage[targetindex].transform.GetChild(0).GetComponent<Text>().color = Color.yellow;

                Invoke("setDecal", 0.1f);
            }
        }
    }
}
