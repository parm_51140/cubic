using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetVol : MonoBehaviour
{
    public static GetVol _readVolumnObject;
    public MeshFilter meshFilter;
    public Material[] material;
    public float r = 0.05f;
    public int tablesize = 200;
    public bool fill = true;
    public bool invert = false;
    public bool drawfill = false;
    public float volumeDetect = 0;//
    public float InvertVolumeDetect = 0;
    public float topOfModel;
    public float bottomOfModel;
    public LineRenderer lr;

    Coroutine readingStat;
    private void Awake()
    {
        _readVolumnObject = this;
    }
    // Start is called before the first frame update
    private void OnEnable()
    {
        //meshFilter = ObjectReshape.objReshape.GetComponent<MeshFilter>();
        updateObj();
    }
    public void updateObj() 
    {
        switch (_BaseData._BaseDataSave.selectedID)
        {
            case 1: meshFilter = EditableObjectMaster._editmaster.currentObject.GetComponentInChildren<_BoxEdit>().mainObject.GetComponent<MeshFilter>(); break;
            case 2: meshFilter = EditableObjectMaster._editmaster.currentObject.GetComponentInChildren<_CupEdit>().mainObject.GetComponent<MeshFilter>(); break;
            case 3: meshFilter = EditableObjectMaster._editmaster.currentObject.GetComponentInChildren<_SandWichEdit>().mainObject.GetComponent<MeshFilter>(); break;
            case 4: meshFilter = EditableObjectMaster._editmaster.currentObject.GetComponentInChildren<_CanEdit>().mainObject.GetComponent<MeshFilter>(); break;
            case 5: meshFilter = EditableObjectMaster._editmaster.currentObject.GetComponentInChildren<_CartonEdit>().mainObject.GetComponent<MeshFilter>(); break;
        }

    }

    public void ReadVolumn()
    {
        if (readingStat != null) StopCoroutine(readingStat);
        tablesize = Mathf.RoundToInt(250 * 0.05f / r);
        getHeightSize();
        //StartCoroutine( GetInvertVolume(r * 2.0f));
        readingStat = StartCoroutine(GetVolume(r * 2.0f));

    }


    public void DetectTris(int i, int ip0, int ip1, int ip2, Vector3[] v)
    {
        Mesh m = new Mesh();
        //We need two arrays one to hold the vertices and one to hold the triangles
        Vector3[] VerteicesArray = new Vector3[3];
        int[] trianglesArray = new int[3];
        //lets add 3 vertices in the 3d space
        VerteicesArray[0] = v[ip0];
        VerteicesArray[1] = v[ip1];
        VerteicesArray[2] = v[ip2];
        //define the order in which the vertices in the VerteicesArray shoudl be used to draw the triangle
        trianglesArray[0] = 0;
        trianglesArray[1] = 1;
        trianglesArray[2] = 2;
        //add these two triangles to the mesh
        m.vertices = VerteicesArray;
        m.triangles = trianglesArray;
        m.RecalculateNormals();
        var facenormal = ((m.normals[0] + m.normals[1] + m.normals[2]) / 3).normalized;

        if (Mathf.Abs(facenormal.y) > 0.5f)
        {
            if (topOfModel < m.vertices[0].y)
            {
                topOfModel = m.vertices[0].y;
            }

            if (bottomOfModel > m.vertices[0].y)
            {
                bottomOfModel = m.vertices[0].y;
            }


        }

    }




    public float getHeightSize()
    {
        Mesh m = meshFilter.mesh;
        var tris = m.triangles;
        List<Vector3> vts = new List<Vector3>();
        m.GetVertices(vts);
        //Debug.Log(tris.Length / 3);
        Vector3[] v = vts.ToArray();

        for (int i = 0; i < tris.Length - 3; i += 3)
        {
            DetectTris(i, tris[i], tris[i + 1], tris[i + 2], v);
        }


        return topOfModel;
    }


    public float maxdeep = 0;
    public IEnumerator GetVolume(float frq)
    {

        if (!fill) yield break;
        volumeDetect = 0;
        maxdeep = 0;
        float y = topOfModel * .65f;//
        for (int i = -tablesize / 2; i < tablesize; i++)
        {

            for (int j = -tablesize / 2; j < tablesize; j++)
            {
                RaycastHit hit;

                if (Physics.Raycast(new Vector3(i * frq, y - (topOfModel - bottomOfModel) * 0.15f, j * frq), Vector3.down, out hit, Mathf.Infinity, 1 << 11))
                {
                    // Debug.DrawRay(new Vector3(i * frq, y - (topOfModel - bottomOfModel) * 0.15f, j * frq), Vector3.down * hit.distance, Color.red, 1000);
                    if (drawfill)
                    {
                        var line = lr;
                        line.positionCount += 2;

                        line.SetPosition(line.positionCount - 2, new Vector3(i * frq, y - (topOfModel - bottomOfModel) * 0.15f, j * frq));
                        line.SetPosition(line.positionCount - 1, new Vector3(i * frq, y - (topOfModel - bottomOfModel) * 0.15f - hit.distance, j * frq));
                    }
                    volumeDetect += Mathf.Pow((frq), 2) * hit.distance;
                    if (hit.distance > maxdeep)
                    {
                        maxdeep = hit.distance;
                    }

                }

            }
            yield return new WaitForEndOfFrame();
        }

    }

    private void Update()
    {
        //GameObject.Find("Text").GetComponent<Text>().text = ""+volumeDetect+" ml.";
    }
    public IEnumerator GetInvertVolume(float frq)
    {
        if (!invert) yield break;
        InvertVolumeDetect = 0;
        maxdeep = 0;
        var y = bottomOfModel;
        for (int i = -tablesize / 2; i < tablesize; i++)
        {
            for (int j = -tablesize / 2; j < tablesize; j++)
            {
                RaycastHit hit;
                // Debug.DrawRay(new Vector3(i * frq, y, j * frq), Vector3.up * 1000, Color.red, 1000);
                if (Physics.Raycast(new Vector3(i * frq, y, j * frq), Vector3.up, out hit, Mathf.Infinity, 1 >> 11))
                {


                    InvertVolumeDetect += Mathf.Pow((frq * 10), 2) * hit.distance * 10;
                    if (hit.distance > maxdeep)
                    {
                        maxdeep = hit.distance;
                    }
                }
            }
            yield return new WaitForEndOfFrame();
        }

    }
}
