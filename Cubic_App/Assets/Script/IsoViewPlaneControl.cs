using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using static System.Net.Mime.MediaTypeNames;

public class IsoViewPlaneControl : MonoBehaviour
{
    Vector2 topL, botL, topR, botR;
    Vector3 startPos, endPos, currentPs;

    public RawImage rt;
    public Camera cc;

    float activeThresh, inactiveTresh;
    Coroutine moveSnap;
    private void Start()
    {
        topL = new Vector2(-1227, 2725);
        botL = new Vector2(-1227, 300);
        topR = new Vector2(-27, 2725);
        botR = new Vector2(-233, 300);
      //  currentpos = "botR";
        activeThresh = 50;
        inactiveTresh = 10;
    }

    public void setStartPos()
    {
        startPos = Input.mousePosition;

    }
    private void Update()
    {
       
        //    print(gameObject.GetComponent<RectTransform>().anchoredPosition);
    }
    IEnumerator saveII()
    {
        yield return new WaitForEndOfFrame();
        Texture2D ss = new Texture2D(520, 520, TextureFormat.RGB24, false);
        ss.ReadPixels(new Rect(0, 0, 520, 520), 0, 0);
        ss.Apply();
        byte[] bytes = ss.EncodeToPNG();

        File.WriteAllBytes(@"C:\Users\Hello\Desktop\output.png", bytes);
        print("done");
        yield return null;
    }

    public void setEndPos()
    {
        RectTransform parentRect = gameObject.transform.parent.GetComponent<RectTransform>();

        Vector3 currPos = Input.mousePosition;
        Vector3 objpos = gameObject.GetComponent<RectTransform>().anchoredPosition;
        string nearside = "";
        if (currPos.x < Main.MainControl.mainCamera.pixelWidth / 2)
        {
            if (currPos.y < Main.MainControl.mainCamera.pixelHeight / 2)
            {

                if (currPos.x < currPos.y) nearside = "left";
                else nearside = "down";
            }
            else
            {
                if (currPos.x < Main.MainControl.mainCamera.pixelHeight - currPos.y) nearside = "left";
                else nearside = "top";
            }

        }
        else
        {
            if (currPos.y < Main.MainControl.mainCamera.pixelHeight / 2)
            {

                if (Main.MainControl.mainCamera.pixelWidth - currPos.x < currPos.y) nearside = "right";
                else nearside = "down";
            }
            else
            {
                if (Main.MainControl.mainCamera.pixelWidth - currPos.x < Main.MainControl.mainCamera.pixelHeight - currPos.y) nearside = "right";
                else nearside = "top";
            }
        }
     //   print(nearside);

        objpos.x = Mathf.Clamp(objpos.x, -687, -15);
        objpos.y = Mathf.Clamp(objpos.y, 200, parentRect.sizeDelta.y - 400);
      //  print(objpos.y + "  :  " + (parentRect.sizeDelta.y - 400));
        if (nearside == "top")
        {
            //  gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(objpos.x, parentRect.sizeDelta.y - 700, objpos.z);
            endPos = new Vector3(objpos.x, parentRect.sizeDelta.y - 400, objpos.z);
        }
        else if (nearside == "down")
        {
            // gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(objpos.x, 267, objpos.z);
            endPos = new Vector3(objpos.x, 200, objpos.z);
        }
        else if (nearside == "left")
        {
            // gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(-1227, objpos.y, objpos.z);
            endPos = new Vector3(-687, objpos.y, objpos.z);
        }
        else if (nearside == "right")
        {
            //  gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector3(-18, objpos.y, objpos.z);
            endPos = new Vector3(-15, objpos.y, objpos.z);
        }

        if (moveSnap != null) StopCoroutine(moveSnap);
        moveSnap = StartCoroutine(MoveIn());

    }


    public void Dragg()
    {
        Vector3 dragpos = Input.mousePosition;
        Vector2 rectSize = gameObject.GetComponent<RectTransform>().sizeDelta;

        dragpos.x = dragpos.x + rectSize.x / 4;
        dragpos.y = dragpos.y - rectSize.y / 4;
        gameObject.transform.position = new Vector3(dragpos.x, dragpos.y, gameObject.transform.position.z);
        // setEndPos();
    }
    IEnumerator MoveIn()
    {

        Vector3 curr;
        Vector3 start = gameObject.GetComponent<RectTransform>().anchoredPosition;
        float t = 0f;

        while (t < 1)
        {

            curr = Vector3.Lerp(start, endPos, t);
            gameObject.GetComponent<RectTransform>().anchoredPosition = curr;
            t += Time.deltaTime*10;
            yield return new WaitForEndOfFrame();
        }

        gameObject.GetComponent<RectTransform>().anchoredPosition = endPos;

        yield return null;

    }



}
