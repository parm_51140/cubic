using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DecalProperties
{
    public Texture p_texture;
    public Vector3 p_Position;
    public Vector3 p_Rotation;
    public Vector3 p_Scale;
    public float p_offset;
    public float p_TexScale;
    public string p_link;

}
public class DecalCloudProperties
{
    public string p_link;
    public string p_name;
    public string p_Position;
    public string p_Rotation;
    public string p_Scale;
    public float p_offset;
    public float p_TexScale;

}
public static class DecalPropertiesSave
{
    public static DecalProperties[] decalPropSave = new DecalProperties[10];//0-4 wrap  // 5-9 sticker
}

