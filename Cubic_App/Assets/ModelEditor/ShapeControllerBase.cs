using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UIElements;
//using static UnityEditor.Progress;

public class ShapeControllerBase : MonoBehaviour
{
    [System.Serializable]
    public class SideGroup
    {
        public GameObject[] sideElement;
    }

    [System.Serializable]
    public class VectorGroup
    {
        public List<Vector3> vElement;
    }

    [System.Serializable]
    public class AngleGroup
    {
        public GameObject[] angleElement;
        public Vector3[] anglePositon;
        public void AssignPosition()
        {
            anglePositon = new Vector3[angleElement.Length];
            for (int i = 0; i < angleElement.Length; i++)
            {
                anglePositon[i] = angleElement[i].transform.localPosition;
            }
        }
    }

    public AnimationCurve wallCurve;
    public AnimationCurve startWallCurve;
    public PathGenerator pathGenerator;
    public SurfaceGenerator lid;
    public MeshRenderer lidMesh;


    [Range(6f, 60f)]
    public float width;
    [Range(6f, 60f)]
    public float length;
    [Range(0.5f, 10f)]
    public float height;
    [Range(1f, 25f)]
    public float slope;

    [Range(0.1f, 60f)]
    public float flexibleCurve;

    public bool showLid = false;

    private float _curve;
    [SerializeField]
    public float curve
    {
        get { return _curve; }
        set
        {
            _curve = value;
            if (value > width / 2 || value > length / 2)
            {
                if (length / 2 > width / 2)
                {
                    _curve = width / 2 - 0.1f;
                }

                if (width / 2 > length / 2)
                {
                    _curve = length / 2 - 0.1f;
                }
                if (width / 2 == length / 2)
                {
                    _curve = width / 2 - 0.1f;
                }
            }
        }
    }




    [SerializeField]
    public List<SideGroup> allSide;
    [SerializeField]
    public List<VectorGroup> startPoints;
    [SerializeField]
    public List<AngleGroup> angleSide;
    [SerializeField]
    public List<VectorGroup> startAnglePoints;
    [SerializeField]
    public List<VectorGroup> originalPoints;

    public int wingStart = 1;

    void Start()
    {

        Backups();
    }


    void Update()
    {    

        curve = flexibleCurve;

        Reshape();
    }

    // Start is called before the first frame update
    public virtual void Backups()
    {
        //Base
        for (int i = 0; i < allSide.Count; i++)
        {
            originalPoints.Add(new VectorGroup());
            originalPoints[i].vElement = new List<Vector3>();
            for (int j = 0; j < allSide[i].sideElement.Length; j++)
            {
                originalPoints[i].vElement.Add(allSide[i].sideElement[j].transform.localPosition);
            }
        }

        //Wall

        Keyframe[] key = (Keyframe[])wallCurve.keys.Clone();
        startWallCurve = new AnimationCurve(key);
        for (int i = 0; i < wallCurve.keys.Length; i++)
        {
            wallCurve.MoveKey(i, new Keyframe(wallCurve.keys[i].time * 0.1f, wallCurve.keys[i].value));
        }

        //Angle
        for (int i = 0; i < angleSide.Count; i++)
        {
            startAnglePoints.Add(new VectorGroup());
            startAnglePoints[i].vElement = new List<Vector3>();
            for (int j = 0; j < angleSide[i].angleElement.Length; j++)
            {
                startAnglePoints[i].vElement.Add(angleSide[i].angleElement[j].transform.localPosition);
            }
        }

    }



    // Update is called once per frame
    public virtual void Reshape()

    {   //restore originalpoint to frist basepoint
        for (int i = 0; i < originalPoints.Count; i++)
        {

            for (int j = 0; j < allSide[i].sideElement.Length; j++)
            {
                allSide[i].sideElement[j].transform.localPosition = originalPoints[i].vElement[j];
            }
        }


        //Angle 1
        var angleItem = angleSide[0].angleElement[0];
        var anglePosition = angleItem.transform.localPosition;
        anglePosition.z = (startAnglePoints[0].vElement[3].z + (curve));
        angleItem.transform.localPosition = anglePosition;

        angleItem = angleSide[0].angleElement[1];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.x = (startAnglePoints[0].vElement[3].x + (curve));
        anglePosition.z = (startAnglePoints[0].vElement[3].z + (curve));
        angleItem.transform.localPosition = anglePosition;

        angleItem = angleSide[0].angleElement[2];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.x = (startAnglePoints[0].vElement[3].x + (curve));
        angleItem.transform.localPosition = anglePosition;


        //Angle2
        angleItem = angleSide[1].angleElement[0];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.z = (startAnglePoints[1].vElement[3].z + (-curve));
        angleItem.transform.localPosition = anglePosition;

        angleItem = angleSide[1].angleElement[1];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.x = (startAnglePoints[1].vElement[3].x + (curve));
        anglePosition.z = (startAnglePoints[1].vElement[3].z + (-curve));
        angleItem.transform.localPosition = anglePosition;

        angleItem = angleSide[1].angleElement[2];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.x = (startAnglePoints[1].vElement[3].x + (curve));
        angleItem.transform.localPosition = anglePosition;


        //Angle3
        angleItem = angleSide[2].angleElement[0];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.z = (startAnglePoints[2].vElement[3].z + (curve));
        angleItem.transform.localPosition = anglePosition;

        angleItem = angleSide[2].angleElement[1];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.x = (startAnglePoints[2].vElement[3].x + (-curve));
        anglePosition.z = (startAnglePoints[2].vElement[3].z + (curve));
        angleItem.transform.localPosition = anglePosition;

        angleItem = angleSide[2].angleElement[2];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.x = (startAnglePoints[2].vElement[3].x + (-curve));
        angleItem.transform.localPosition = anglePosition;

        //Angle4
        angleItem = angleSide[3].angleElement[0];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.z = (startAnglePoints[3].vElement[3].z + (-curve));
        angleItem.transform.localPosition = anglePosition;

        angleItem = angleSide[3].angleElement[1];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.x = (startAnglePoints[3].vElement[3].x + (-curve));
        anglePosition.z = (startAnglePoints[3].vElement[3].z + (-curve));
        angleItem.transform.localPosition = anglePosition;

        angleItem = angleSide[3].angleElement[2];
        anglePosition = angleItem.transform.localPosition;
        anglePosition.x = (startAnglePoints[3].vElement[3].x + (-curve));
        angleItem.transform.localPosition = anglePosition;




        startPoints.Clear();

        for (int i = 0; i < allSide.Count; i++)
        {
            startPoints.Add(new VectorGroup());
            startPoints[i].vElement = new List<Vector3>();
            for (int j = 0; j < allSide[i].sideElement.Length; j++)
            {
                startPoints[i].vElement.Add(allSide[i].sideElement[j].transform.localPosition);
            }
        }

        //Base
        for (int j = 0; j < allSide.Count; j++)
        {
            for (int i = 0; i < startPoints[j].vElement.Count; i++)
            {
                var item = allSide[j].sideElement[i];
                var position = item.transform.localPosition;
                if (j == 0)
                {
                    position.z = (startPoints[j].vElement[i].z + (-width / 2));

                }
                if (j == 1)
                {
                    position.z = (startPoints[j].vElement[i].z + (width / 2));

                }
                if (j == 2)
                {
                    position.x = (startPoints[j].vElement[i].x + (-length / 2));

                }
                if (j == 3)
                {
                    position.x = (startPoints[j].vElement[i].x + (length / 2));

                }
                //position.z = (startPoints[j].vElement[i].z + (-width / 2));
                item.transform.localPosition = position;
            }
        }

        pathGenerator.slope = slope;
        pathGenerator.shape = wallCurve;

        //pathGenerator.shapeExposure = height;
        for (int i = 0; i < wallCurve.keys.Length; i++)
        {
            if (i <= wingStart)
            {
                wallCurve.MoveKey(i, new Keyframe(startWallCurve.keys[i].time * slope, startWallCurve.keys[i].value * height));
            }
            else
            {
                wallCurve.MoveKey(i, new Keyframe((startWallCurve.keys[i].time + wallCurve.keys[wingStart].time - startWallCurve.keys[wingStart].time),
                    (startWallCurve.keys[i].value + wallCurve.keys[wingStart].value - startWallCurve.keys[wingStart].value)));
            }

        }


        //   lidMesh.GetComponent<MeshRenderer>().enabled = showLid;


        //lid
        lid.offset = new Vector3(0, 0.27f + (0.75f * height), 0);
        lid.expand = slope - ((slope * 0.37f));
    }

    public string ToJson()
    {
        for (int i = 0; i < angleSide.Count; i++)
        {
            angleSide[i].AssignPosition();
        }
        string jsonString = JsonUtility.ToJson(this);
        jsonString = jsonString.Substring(0, jsonString.Length - 1);
        jsonString += ",\"gap\":" + PlasticThickness._thicknessUI.CurrentThickness.ToString() + "}";
        GUIUtility.systemCopyBuffer = jsonString;
        return jsonString;
    }
}

