using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Compass3D : MonoBehaviour
{
    Main mainControl;
    [SerializeField] FreeHandCamera freehandValue;
    [SerializeField] Camera Compass3dCamera;
    [SerializeField] GameObject ShowPanel;
    [SerializeField] GameObject clearFreehandBtn;
    [SerializeField] Transform HorizontalLink;
    [SerializeField] Transform VerticalLink;
    [SerializeField] Transform HorizontalSelf;
    [SerializeField] Transform VerticalSelf;
  //  [SerializeField] TextMesh[] axisIndex;//x,y,z
    void Start()
    {
        mainControl = Main.MainControl;
    }

    // Update is called once per frame
    void Update()
    {
        if (_BaseData._BaseDataSave.MainmenuSelect !=4) 
        { ShowPanel.transform.localScale = Vector3.zero; clearFreehandBtn.transform.localScale = Vector3.zero; return; }
        else
        {
         //   print(freehandValue.transformDelta.x);
            ShowPanel.transform.localScale = Vector3.one;
            clearFreehandBtn.transform.localScale = Vector3.one;
            HorizontalSelf.transform.localEulerAngles = HorizontalLink.transform.localEulerAngles;
            VerticalSelf.transform.localEulerAngles = VerticalLink.transform.localEulerAngles;
            /*   VerticalSelf.transform.eulerAngles = new Vector3(freehandValue.transformDelta.y
                   , 0, freehandValue.transformDelta.z);*/
            Vector3 axisT = new Vector3(0, 0, 0);

            axisT = new Vector3
             (VerticalSelf.transform.localEulerAngles.x
             , HorizontalSelf.transform.localEulerAngles.y
             , HorizontalSelf.transform.localEulerAngles.z);
          // axisIndex[0].transform.eulerAngles = axisT;
          // axisIndex[1].transform.eulerAngles = axisT;
          // axisIndex[2].transform.eulerAngles = axisT;
        }
    }
   
}
