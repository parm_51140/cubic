using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditShapeDetail : MonoBehaviour
{  // [depricate]
    [SerializeField] Main mainControl;
    [SerializeField] TextMesh horizontalSize;
    [SerializeField] TextMesh VerticalSize;
    [SerializeField] Image horizontalLine;
    [SerializeField] Image verticalLine;
    [SerializeField] Image[] horizonend;
    [SerializeField] Image[] verticend;

    Vector3 Size;
    float camOffset;
    float positionOffset;
    float offsetX;
    bool isShowing;

    void Start()
    {
        mainControl = Main.MainControl;
        isShowing = false;
    }
    public void hideDetail()
    {
        horizontalLine.transform.localScale = Vector3.zero;
        horizonend[0].transform.localScale = Vector3.zero;
        horizonend[1].transform.localScale = Vector3.zero;
        verticalLine.transform.localScale = Vector3.zero;
        verticend[0].transform.localScale = Vector3.zero;
        verticend[1].transform.localScale = Vector3.zero;
        horizontalSize.transform.localScale = Vector3.zero;
        VerticalSize.transform.localScale = Vector3.zero;
        isShowing = false;
    }
    public void showDetail()
    {
        horizontalLine.transform.localScale = Vector3.one;
        horizonend[0].transform.localScale = Vector3.one;
        horizonend[1].transform.localScale = Vector3.one;
        verticalLine.transform.localScale = Vector3.one;
        verticend[0].transform.localScale = Vector3.one;
        verticend[1].transform.localScale = Vector3.one;
        horizontalSize.transform.localScale = Vector3.one;
        VerticalSize.transform.localScale = Vector3.one;
        isShowing = true;
    }
    public void shapeDetail(Vector3 Size, Vector3 objMin, Vector3 objMax)
    {
        if (!isShowing) { return; }
        float camOffset = 4.8f / mainControl.orbitCamera.orthographicSize;
        float positionOffset = .2f;
        float offsetX = horizontalSize.transform.position.x;
        if (mainControl.currVertivalFace == Main.VertivalFace.Side)
        {
            if (mainControl.currHorizontalFace == Main.HorizontalFace.Front || mainControl.currHorizontalFace == Main.HorizontalFace.Back)
            {
                if (!mainControl.revertVertical)
                {//// offsetscal3 : 3
                    horizontalSize.transform.position = new Vector3(offsetX, objMin.y * camOffset - positionOffset, 0);
                    horizontalSize.text = (Size.x/3 * 10).ToString("F2")+" cm";
                    horizontalLine.rectTransform.sizeDelta = new Vector2((objMax.x - objMin.x) * 100 * 2 * camOffset, 3);
                    horizonend[0].transform.localPosition = new Vector3(objMin.x * 100 * 2 * camOffset, 0, 0);
                    horizonend[1].transform.localPosition = new Vector3(objMax.x * 100 * 2 * camOffset, 0, 0);

                    VerticalSize.transform.position = new Vector3(objMax.x * camOffset + offsetX + (positionOffset), 0, 0);
                    VerticalSize.text = " " + (Size.y/3 * 10).ToString("F2") + " cm";
                    verticalLine.rectTransform.sizeDelta = new Vector2(3, (objMax.y - objMin.y) * 100 * 2 * camOffset);
                    verticalLine.transform.localPosition = new Vector3(objMax.x * camOffset + offsetX - positionOffset * 100, (objMax.y + objMin.y) * 100, 0);
                    verticend[0].transform.localPosition = new Vector3(objMax.x * camOffset + offsetX - positionOffset * 100, objMax.y * 100 * 2 * camOffset, 0);
                    verticend[1].transform.localPosition = new Vector3(objMax.x * camOffset + offsetX - positionOffset * 100, objMin.y * 100 * 2 * camOffset, 0);
                }
                else
                {
                    horizontalSize.transform.position = new Vector3(offsetX, -objMax.y * camOffset - positionOffset, 0);
                    horizontalSize.text = (Size.x/3 * 10).ToString("F2") + " cm";
                    horizontalLine.rectTransform.sizeDelta = new Vector2((objMax.x - objMin.x) * 100 * 2 * camOffset, 3);
                    horizonend[0].transform.localPosition = new Vector3(objMin.x * 100 * 2 * camOffset, 0, 0);
                    horizonend[1].transform.localPosition = new Vector3(objMax.x * 100 * 2 * camOffset, 0, 0);

                    VerticalSize.transform.position = new Vector3(objMax.x * camOffset + offsetX + (positionOffset), 0, 0);
                    VerticalSize.text = " " + (Size.y/3 * 10).ToString("F2") + " cm";
                    verticalLine.rectTransform.sizeDelta = new Vector2(3, (objMax.y - objMin.y) * 100 * 2 * camOffset);
                    verticalLine.transform.localPosition = new Vector3(objMax.x * camOffset + offsetX - positionOffset * 100, -(objMax.y + objMin.y) * 100, 0);
                    verticend[0].transform.localPosition = new Vector3(objMax.x * camOffset + offsetX - positionOffset * 100, -objMax.y * 100 * 2 * camOffset, 0);
                    verticend[1].transform.localPosition = new Vector3(objMax.x * camOffset + offsetX - positionOffset * 100, -objMin.y * 100 * 2 * camOffset, 0);

                }

            }//front back
            else
            {
                if (!mainControl.revertVertical)
                {
                    horizontalSize.transform.position = new Vector3(offsetX, objMin.y * camOffset - positionOffset, 0);
                    horizontalSize.text = (Size.z/3 * 10).ToString("F2") + " cm";
                    horizontalLine.rectTransform.sizeDelta = new Vector2((objMax.z - objMin.z) * 100 * 2*camOffset, 3);
                    horizonend[0].transform.localPosition = new Vector3(objMin.z * 100 * 2 * camOffset, 0, 0);
                    horizonend[1].transform.localPosition = new Vector3(objMax.z * 100 * 2 * camOffset, 0, 0);

                    VerticalSize.transform.position = new Vector3(objMax.z * camOffset + offsetX + positionOffset, 0, 0);
                    VerticalSize.text = " " + (Size.y/3 * 10).ToString("F2") + " cm";
                    verticalLine.rectTransform.sizeDelta = new Vector2(3, (objMax.y - objMin.y) * 100 * 2 * camOffset);
                    verticalLine.transform.localPosition = new Vector3(objMax.z * camOffset + offsetX - positionOffset * 100, (objMax.y + objMin.y) * 100, 0);
                    verticend[0].transform.localPosition = new Vector3(objMax.z * camOffset + offsetX - positionOffset * 100, objMax.y * 100 * 2 * camOffset, 0);
                    verticend[1].transform.localPosition = new Vector3(objMax.z * camOffset + offsetX - positionOffset * 100, objMin.y * 100 * 2 * camOffset, 0);
                }
                else
                {

                    horizontalSize.transform.position = new Vector3(offsetX, -objMax.y * camOffset - positionOffset, 0);
                    horizontalSize.text = (Size.z/3 * 10).ToString("F2") + " cm";
                    horizontalLine.rectTransform.sizeDelta = new Vector2((objMax.z - objMin.z) * 100 * 2 * camOffset, 3);
                    horizonend[0].transform.localPosition = new Vector3(objMin.z * 100 * 2 * camOffset, 0, 0);
                    horizonend[1].transform.localPosition = new Vector3(objMax.z * 100 * 2 * camOffset, 0, 0);

                    VerticalSize.transform.position = new Vector3(objMax.z * camOffset + offsetX + (positionOffset), 0, 0);
                    VerticalSize.text = " " + (Size.y/3 * 10).ToString("F2") + " cm";
                    verticalLine.rectTransform.sizeDelta = new Vector2(3, (objMax.y - objMin.y) * 100 * 2 * camOffset);
                    verticalLine.transform.localPosition = new Vector3(objMax.z * camOffset + offsetX - positionOffset * 100, -(objMax.y + objMin.y) * 100, 0);
                    verticend[0].transform.localPosition = new Vector3(objMax.z * camOffset + offsetX - positionOffset * 100, -objMax.y * 100 * 2 * camOffset, 0);
                    verticend[1].transform.localPosition = new Vector3(objMax.z * camOffset + offsetX - positionOffset * 100, -objMin.y * 100 * 2 * camOffset, 0);
                }
            }//left right
        }//sidde Face
        else
        {
            if (mainControl.currHorizontalFace == Main.HorizontalFace.Front || mainControl.currHorizontalFace == Main.HorizontalFace.Back)
            {
                horizontalSize.transform.position = new Vector3(offsetX, objMin.z * camOffset - positionOffset, 0);
                horizontalSize.text = (Size.x/3 * 10).ToString("F2") + " cm";
                horizontalLine.rectTransform.sizeDelta = new Vector2((objMax.x - objMin.x) * 100 * 2 * camOffset, 3);
                horizonend[0].transform.localPosition = new Vector3(objMin.x * 100 * 2 * camOffset, 0, 0);
                horizonend[1].transform.localPosition = new Vector3(objMax.x * 100 * 2 * camOffset, 0, 0);

                VerticalSize.transform.position = new Vector3(objMax.x * camOffset + offsetX + (positionOffset), 0, 0);
                VerticalSize.text = " " + (Size.z/3 * 10).ToString("F2") + " cm";
                verticalLine.rectTransform.sizeDelta = new Vector2(3, (objMax.z - objMin.z) * 100 * 2 * camOffset);
                verticalLine.transform.localPosition = new Vector3(objMax.z * camOffset + offsetX - positionOffset * 100, (objMax.z + objMin.z) * 100, 0);
                verticend[0].transform.localPosition = new Vector3(objMax.z * camOffset + offsetX - positionOffset * 100, objMax.z * 100 * 2 * camOffset, 0);
                verticend[1].transform.localPosition = new Vector3(objMax.z * camOffset + offsetX - positionOffset * 100, objMin.z * 100 * 2 * camOffset, 0);
            }
            else
            {
                horizontalSize.transform.position = new Vector3(offsetX, objMin.x * camOffset - positionOffset, 0);
                horizontalSize.text = (Size.z/3 * 10).ToString("F2") + " cm";
                horizontalLine.rectTransform.sizeDelta = new Vector2((objMax.z - objMin.z) * 100 * 2 * camOffset, 3);
                horizonend[0].transform.localPosition = new Vector3(objMin.z * 100 * 2 * camOffset, 0, 0);
                horizonend[1].transform.localPosition = new Vector3(objMax.z * 100 * 2 * camOffset, 0, 0);


                VerticalSize.transform.position = new Vector3(objMax.z * camOffset + offsetX + (positionOffset), 0, 0);
                VerticalSize.text = " " + (Size.z/3 * 10).ToString("F2") + " cm";
                verticalLine.rectTransform.sizeDelta = new Vector2(3, (objMax.x - objMin.x) * 100 * 2 * camOffset);
                verticalLine.transform.localPosition = new Vector3(objMax.x * camOffset + offsetX - positionOffset * 100, (objMax.x + objMin.x) * 100, 0);
                verticend[0].transform.localPosition = new Vector3(objMax.x * camOffset + offsetX - positionOffset * 100, objMax.x * 100 * 2 * camOffset, 0);
                verticend[1].transform.localPosition = new Vector3(objMax.x * camOffset + offsetX - positionOffset * 100, objMin.x * 100 * 2 * camOffset, 0);
            }

        }
    }

}
