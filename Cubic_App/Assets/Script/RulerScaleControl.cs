using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RulerScaleControl : MonoBehaviour
{
    [SerializeField] GameObject ScaleObj;
    [SerializeField] GameObject ScaleObjVert;
    [SerializeField] GameObject ScaleGroup;
    [SerializeField] Camera mainCamera;//getSize
    [SerializeField] Camera orbitCamera;//getOrtho
    [SerializeField] CanvasScaler canvas;
    [SerializeField] List<GameObject> scaleList;
    [SerializeField] bool IsHorizontal;
    public float temporthographic;//!<3
    public float ppu;
    void Start()
    {
        temporthographic = mainCamera.orthographicSize;//9.6 perfect //+-4.8:value

    }


    void Update()
    {

        //  if (Input.GetKey(KeyCode.V)) { resetScale(); }
    }

    void CreateScale()
    {
        int camWidth = mainCamera.pixelWidth;
        int camHeight = mainCamera.pixelHeight;

        int tick = 0;
        int addtick = 0;
        int scalePlot = 0;
        addtick = 100;
        scalePlot = 100;

        if (IsHorizontal)
        {
            for (float i =0; i < (camWidth/2) ; i += addtick)
            {
                if (tick % scalePlot == 0)
                {
                    GameObject oo = Instantiate(ScaleObj, ScaleGroup.transform,false);
                    oo.transform.localPosition = new Vector3(i * ( 4.8f/orbitCamera.orthographicSize )*3, camHeight, 0);
                  ScaleSetting os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(true, tick, 0);
                    scaleList.Add(oo);                 
                }
                else
                {
                  /*  GameObject oo = Instantiate(ScaleObj, new Vector3(i , camHeight, 0), Quaternion.identity, ScaleGroup.transform);
                    ScaleSetting os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(false, i, 0);
                    scaleList.Add(oo);
                    oo = Instantiate(ScaleObj, new Vector3(camWidth , mainCamera.pixelHeight, 0), Quaternion.identity, ScaleGroup.transform);
                    os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(false, i, 0);
                    scaleList.Add(oo);*/
                }
                tick += addtick;
            }
        }


    }
    public void resetScale()
    {
        for (int i = 0; i < scaleList.Count; i++)
        {
            Destroy(scaleList[i].gameObject);
        }
        scaleList.Clear();
        CreateScale();
    }
    public void clearScale()
    {
        for (int i = 0; i < scaleList.Count; i++)
        {
            Destroy(scaleList[i].gameObject);
        }
        scaleList.Clear();
    }

    /*
      if (orbitCamera.orthographicSize > 14f)
          {
              addtick = 50;
              scalePlot = 200;
          }
          else if (orbitCamera.orthographicSize > 5f)
          {
              addtick = 20;
              scalePlot = 100;
          }
          else
          {
              addtick = 10;
              scalePlot = 50;
          }
     if (IsHorizontal)
        {
            for (float i = (camWidth) * (orbitCamera.orthographicSize / 9.6f) / 2; i < (camWidth) * (orbitCamera.orthographicSize / 9.6f); i += addtick)
            {
                if (tick % scalePlot == 0)
                {
                    GameObject oo = Instantiate(ScaleObj, new Vector3(i / (orbitCamera.orthographicSize / 9.6f), camHeight, 0), Quaternion.identity, ScaleGroup.transform);
                    ScaleSetting os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(true, tick, 0);
                    scaleList.Add(oo);
                    oo = Instantiate(ScaleObj, new Vector3(camWidth - (i / (orbitCamera.orthographicSize / 9.6f)), mainCamera.pixelHeight, 0), Quaternion.identity, ScaleGroup.transform);
                    os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(true, -tick, 0);
                    scaleList.Add(oo);

                }
                else
                {
                    GameObject oo = Instantiate(ScaleObj, new Vector3(i / (orbitCamera.orthographicSize / 9.6f), camHeight, 0), Quaternion.identity, ScaleGroup.transform);
                    ScaleSetting os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(false, i, 0);
                    scaleList.Add(oo);
                    oo = Instantiate(ScaleObj, new Vector3(camWidth - (i / (orbitCamera.orthographicSize / 9.6f)), mainCamera.pixelHeight, 0), Quaternion.identity, ScaleGroup.transform);
                    os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(false, i, 0);
                    scaleList.Add(oo);
                }
                tick += addtick;
            }
        }
        else
        {
            for (float i = (camHeight) * (orbitCamera.orthographicSize / 9.6f) / 2; i < (camHeight) * (orbitCamera.orthographicSize / 9.6f); i += addtick)
            {
                if (tick % scalePlot == 0)
                {
                    GameObject oo = Instantiate(ScaleObjVert, new Vector3(0, i / (orbitCamera.orthographicSize / 9.6f), 0), Quaternion.identity, ScaleGroup.transform);
                    ScaleSetting os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(true, tick, 0);
                    scaleList.Add(oo);

                    oo = Instantiate(ScaleObjVert, new Vector3(0, (camHeight)-(i / (orbitCamera.orthographicSize / 9.6f)), 0), Quaternion.identity, ScaleGroup.transform);
                    os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(true, -tick, 0);
                    scaleList.Add(oo);
                }
                else
                {
                    GameObject oo = Instantiate(ScaleObjVert, new Vector3(0, i / (orbitCamera.orthographicSize / 9.6f), 0), Quaternion.identity, ScaleGroup.transform);
                    ScaleSetting os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(false, i, 0);
                    scaleList.Add(oo);
                    oo = Instantiate(ScaleObjVert, new Vector3(0, (camHeight) - (i / (orbitCamera.orthographicSize / 9.6f)), 0), Quaternion.identity, ScaleGroup.transform);
                    os = oo.GetComponent<ScaleSetting>();
                    os.SetScale(false, i, 0);
                    scaleList.Add(oo);
                }
                tick += addtick;
            }

        }
     
     */
}
