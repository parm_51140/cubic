using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MainMenuControl : MonoBehaviour
{
    [SerializeField] GameObject ScrollGroup;
    [SerializeField] RectTransform HighlightBtn;
    [SerializeField] Image HighlightBtnImage;
    [SerializeField] List<GameObject> subMenu;
    [SerializeField] Main mainControl;

    SubMenuControl sub;
    bool goSnap = false, snapleft = false;
    float snapPosition, mouseUpPosition;
    //  public int currentSelectMenu;//1 edit,2 view,3 wireframe,4 ar
    Vector3 down;
    Vector3 current;
    float startDelta;
    float currentDelta;
    Vector3 delta;
    bool pressHold, startSubMenu;
    float pressHoldTime;
    float startMenuPosition;
    [SerializeField] List<Image> mainmenuIcongroup;
    void Start()
    {
       
        mainControl = Main.MainControl;
        startMenuPosition = (mainmenuIcongroup.Count - 2) * 100 + 80;
        //  startMenuPosition = 290;
        snapPosition = startMenuPosition;
        mouseUpPosition = 0;
        startSubMenu = false;
        snapleft = true;
        goSnap = true;
        //fadeIcon(mainControl.currentMainMenu);
       // LaunchSubmenu(true);


    }
/*
    private void OnMouseDown()
    {
        goSnap = false;
        down = Input.mousePosition;
        // print(down.x);
        if (Input.mousePosition.x > 465 * Main.MainControl.mainCamera.pixelWidth / 1080f
            && Input.mousePosition.x < 615 * Main.MainControl.mainCamera.pixelWidth / 1080f && Input.touchCount < 2)
        {
            if (!pressHold)
                pressHoldTime = 0;
            pressHold = true;
        }

    }

    void Update()
    {
        if (goSnap)
        {
            if (snapleft)
            {
                if (mouseUpPosition > snapPosition - 0.5f)
                {
                    goSnap = false;
                    ScrollGroup.transform.localPosition = new Vector3(snapPosition, ScrollGroup.transform.localPosition.y, ScrollGroup.transform.localPosition.z);
                    return;
                }
                mouseUpPosition = mouseUpPosition + (snapPosition - mouseUpPosition) / 10;
                ScrollGroup.transform.localPosition = new Vector3(mouseUpPosition, ScrollGroup.transform.localPosition.y, ScrollGroup.transform.localPosition.z);
            }
            else
            {
                if (mouseUpPosition < snapPosition + 0.5f)
                {
                    goSnap = false;
                    ScrollGroup.transform.localPosition = new Vector3(snapPosition, ScrollGroup.transform.localPosition.y, ScrollGroup.transform.localPosition.z);
                    return;
                }
                mouseUpPosition = mouseUpPosition + (snapPosition - mouseUpPosition) / 10;
                ScrollGroup.transform.localPosition = new Vector3(mouseUpPosition, ScrollGroup.transform.localPosition.y, ScrollGroup.transform.localPosition.z);


            }
        }
        if (pressHold)
        {
            pressHoldTime += Time.deltaTime;
            HighlightBtn.sizeDelta = new Vector2(170, Mathf.Clamp(pressHoldTime/.3f*170, 0, 170));
            HighlightBtnImage.color = new Color(1, .7535f, 0, Mathf.Clamp(pressHoldTime / .3f * 1, 0, 1));
            if (pressHoldTime >= 0.3f && startSubMenu == false)
            {
                 gameObject.GetComponent<ScrollRect>().horizontal = false;
                LaunchSubmenu(false);
            }
        }


    }
    void switchSubmenu()
    {
        for (int i = 0; i < subMenu.Count; i++)
        {
            if (mainControl.currentMainMenu - 1 == i)
                subMenu[i].transform.localScale = Vector3.one;
            else
                subMenu[i].transform.localScale = Vector3.zero;
        }
    }
    void LaunchSubmenu(bool initial)
    {
        switchSubmenu();
        sub = subMenu[mainControl.currentMainMenu - 1].GetComponent<SubMenuControl>();
        sub.parentDown();
        if (initial) { sub.parentUp(); }
        else
            startSubMenu = true;
    }
    void fadeIcon(int currentSelect)
    {
        for (int i = 0; i < mainmenuIcongroup.Count; i++)
        {
            Color colorTemp = mainmenuIcongroup[i].color;
            if (i != currentSelect - 1)
            {
                colorTemp.a = 0.2f;
            }
            else { colorTemp.a = 1f; }
            mainmenuIcongroup[i].color = colorTemp;
        }
    }


    private void OnMouseDrag()
    {
        if (Input.touchCount > 1)
        { return; }
        else
        {
            current = Input.mousePosition;
            delta = down - current;
            if (delta.magnitude > 40 && !startSubMenu)
            {
                HighlightBtn.sizeDelta = new Vector2(150, 0);
                HighlightBtnImage.color = Color.clear;
                pressHold = false; gameObject.GetComponent<ScrollRect>().horizontal = true;
            }
            else if (startSubMenu) { sub.parentDrag(delta); }

        }
        // print(delta.magnitude);
    }
    //  public int subID() { return sub.CurrentSelect; }

    private void OnMouseUp()
    {
        HighlightBtn.sizeDelta = new Vector2(150, 0);
        mainControl.LastMainMenu = mainControl.currentMainMenu;
        pressHold = false;
        gameObject.GetComponent<ScrollRect>().horizontal = true;
        mouseUpPosition = ScrollGroup.transform.localPosition.x;
        float positionScaleOffset = Main.MainControl.mainCamera.pixelWidth / 1080f;


        if (delta.x == 0 && !startSubMenu)/// 1080 px screen ref position
        {

            if (Input.mousePosition.x > 465 * positionScaleOffset && Input.mousePosition.x < 615 * positionScaleOffset) { return; }
            else if (Input.mousePosition.x <= 155 * positionScaleOffset)
            {
                if (mainControl.currentMainMenu - 3 >= 1) mainControl.currentMainMenu -= 3; else mainControl.currentMainMenu = 1;
            }
            else if (Input.mousePosition.x > 155 * positionScaleOffset && Input.mousePosition.x <= 310 * positionScaleOffset)
            {
                if (mainControl.currentMainMenu - 2 >= 1) mainControl.currentMainMenu -= 2; else mainControl.currentMainMenu = 1;
            }
            else if (Input.mousePosition.x > 310 * positionScaleOffset && Input.mousePosition.x <= 465 * positionScaleOffset)
            {
                if (mainControl.currentMainMenu - 1 >= 1) mainControl.currentMainMenu -= 1; else mainControl.currentMainMenu = 1;
            }
            else if (Input.mousePosition.x > 615 * positionScaleOffset && Input.mousePosition.x <= 770 * positionScaleOffset)
            {
                if (mainControl.currentMainMenu + 1 <= 6) mainControl.currentMainMenu += 1; else mainControl.currentMainMenu = 6;
            }
            else if (Input.mousePosition.x > 770 * positionScaleOffset)
            {
                if (mainControl.currentMainMenu + 2 <= 6) mainControl.currentMainMenu += 2; else mainControl.currentMainMenu = 6;
            }
        }
        if (delta.magnitude < 90f && !startSubMenu)
        {
            if (delta.x > 0 && mainControl.currentMainMenu < 5)
            { mainControl.currentMainMenu++; }
            else if (delta.x < 0 && mainControl.currentMainMenu > 2)
            { mainControl.currentMainMenu--; }
        }
        else if (delta.magnitude >= 90f && !startSubMenu)///  dynamic scrollbar ref position
        {
            mouseUpPosition = ScrollGroup.transform.localPosition.x;
            if (mouseUpPosition > startMenuPosition - 75)// half btn width 75
            {
                mainControl.currentMainMenu = 1;
            }
            else if (mouseUpPosition <= startMenuPosition - 75 && mouseUpPosition > startMenuPosition - 190 - 75)
            {
                mainControl.currentMainMenu = 2;
            }
            else if (mouseUpPosition <= startMenuPosition - 190 - 75 && mouseUpPosition > startMenuPosition - (190 * 2) - 75)
            {
                mainControl.currentMainMenu = 3;
            }
            else if (mouseUpPosition <= startMenuPosition - (190 * 2) - 75 && mouseUpPosition > startMenuPosition - (190 * 3) - 75)
            {
                mainControl.currentMainMenu = 4;
            }
            else if (mouseUpPosition <= startMenuPosition - (190 * 3) - 75 && mouseUpPosition > startMenuPosition - (190 * 4) - 75)
            {
                mainControl.currentMainMenu = 5;
            }
            else if (mouseUpPosition <= startMenuPosition - (190 * 34) - 75 && mouseUpPosition > startMenuPosition - (190 * 5) - 75)
            {
                mainControl.currentMainMenu = 6;
            }
        }
        snapPosition = startMenuPosition - 190 * (mainControl.currentMainMenu - 1); //150 btn width + 40 space   
        fadeIcon(mainControl.currentMainMenu);
        if (mouseUpPosition > snapPosition)
        {
            snapleft = false;
        }
        else
        {
            snapleft = true;
        }

        LaunchSubmenu(true);
        if (sub != null)
        {
            sub.parentUp();
            startSubMenu = false;
        }
        goSnap = true;
        //1 : edit
        //2 : freehand
        //3 : info
        //4 : ar
        //5 : preset
        if (mainControl.currentMainMenu == 1)
        {
            if (mainControl.LastMainMenu != 1) mainControl.resetFreehand();///
            if (sub.CurrentSelect != 1) mainControl.cancleEditMode();
            mainControl.toggleMenuActibeBtn(0, true);
            mainControl.IsoViewToggle(true);
            mainControl.thicknessSlideToggle(mainControl.currentMainMenu == 1 && sub.CurrentSelect == 2);
            
            mainControl.resetRuler();

        }
        else
        {
            mainControl.cancleEditMode();       
            mainControl.IsoViewToggle(true);
            mainControl.toggleMenuActibeBtn(0, false);
            if (mainControl.LastMainMenu == 1) mainControl.backToFreehand();///               
            mainControl.thicknessSlideToggle(false);
          //  mainControl.onEditScene(false);
        }

        if (mainControl.currentMainMenu == 3)
        {
            // mainControl.ShowDecal(mainControl.currentMainMenu == 3);
            //  mainControl.activeCmeraBG(false);   
            //  mainControl.showWireFrame(mainControl.currentMainMenu == 3 && sub.CurrentSelect == 2);
        }

        else if (mainControl.currentMainMenu == 4)
        {
            mainControl.activeCmeraBG(mainControl.currentMainMenu == 4 && sub.CurrentSelect == 2);
        }

        if (mainControl.currentMainMenu == 5)
        {
            mainControl.swapmesh(sub.CurrentSelect);
        }

      //  mainControl.Hidecompass(mainControl.currentMainMenu != 1);// !!!!!

        mainControl.summaryToggle(mainControl.currentMainMenu == 3); //call detail menu
       
        // mainControl.ShowDecal(mainControl.currentMainMenu == 3);
        //  mainControl.IsoViewPanel(mainControl.currentMainMenu == 1);//&& !mainControl.WaitTransformToEdit);

        mainControl.ShapeSummartView(mainControl.currentMainMenu == 1 && !mainControl.IsEditing);//&& !mainControl.WaitTransformToEdit);
        mainControl.SetDecalOption(mainControl.currentMainMenu == 1 && sub.CurrentSelect == 3);
        mainControl.currentEditFaceSetting(mainControl.currentMainMenu == 1);
        ToggleMenuGroup._toggleMenu.setEnabledToggle(!(mainControl.currentMainMenu == 1 && sub.CurrentSelect ==2));
        if (mainControl.currentMainMenu != 6) userLogin.closeUser();
        else userLogin.ShowUser();
    }
    */
    //public void overrideMainMenu() 
    //{
    //    if (mainControl.currentMainMenu == 1)
    //    {
    //        if (mainControl.LastMainMenu != 1) mainControl.resetFreehand();///
    //        if (sub.CurrentSelect != 1) mainControl.cancleEditMode();
    //        mainControl.toggleMenuActibeBtn(0, true);
    //        mainControl.IsoViewToggle(true);
    //        mainControl.thicknessSlideToggle(mainControl.currentMainMenu == 1 && sub.CurrentSelect == 2);
           
    //        mainControl.resetRuler();

    //    }
    //    else
    //    {
          
    //        mainControl.IsoViewToggle(true);
    //        mainControl.toggleMenuActibeBtn(0, false);
    //     //   if (mainControl.LastMainMenu == 1) mainControl.backToFreehand();///               
    //        mainControl.thicknessSlideToggle(false);          
    //    }

    //    if (mainControl.currentMainMenu == 3)
    //    {
    //    }

    //    else if (mainControl.currentMainMenu == 4)
    //    {
    //        mainControl.activeCmeraBG(mainControl.currentMainMenu == 4 && sub.CurrentSelect == 2);
    //    }

    //    if (mainControl.currentMainMenu == 5)
    //    {
    //        mainControl.swapmesh(sub.CurrentSelect);
    //    }
       
    // //   mainControl.summaryToggle(mainControl.currentMainMenu == 3); //call detail menu

    //    mainControl.ShapeSummartView(mainControl.currentMainMenu == 1 && !mainControl.IsEditing);//&& !mainControl.WaitTransformToEdit);
    //    mainControl.SetDecalOption(mainControl.currentMainMenu == 1 && sub.CurrentSelect == 3);
    //    mainControl.currentEditFaceSetting(mainControl.currentMainMenu == 1);
    //   // ToggleMenuGroup._toggleMenu.setEnabledToggle(!(mainControl.currentMainMenu == 1 && sub.CurrentSelect == 2));
    //    if (mainControl.currentMainMenu != 6) userLogin.closeUser();
    //    else userLogin.ShowUser();
    //}


    [SerializeField] Share share;
    [SerializeField] UserLogin userLogin;

    public void fourceMove() 
    {
        if (PlayerPrefs.HasKey("USERDATA"))
        { share.ShareDetail(); }
        else
        {
            mainControl.currentMainMenu = 6;
            mouseUpPosition = ScrollGroup.transform.localPosition.x;
            snapPosition = startMenuPosition - 190 * (mainControl.currentMainMenu - 1); //150 btn width + 40 space   
            //fadeIcon(mainControl.currentMainMenu);
            snapleft = false;
            goSnap = true;
            //mainControl.summaryToggle(false);
            userLogin.ShowUser();
        }
    }

}
