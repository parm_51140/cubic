using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace dev.cubic
{

    public class RotationControl : MonoBehaviour
    {
        public static RotationControl _rotationCameraControl;
        public Transform rotationX;
        [SerializeField] Main mainControl;
        public float verticalRotation = 0;
        public float horizontalRotation = 0;
        public int horizontalStep = 0;
        public int verticalStep = 0;

        private void Awake()
        {
            _rotationCameraControl = this;
        }
        void Start()
        {
            mainControl = Main.MainControl;
        }
        float lastFrameX, lastFrameY;
        int frameCount;


        void Update()
        {
            
            if (!_BaseData._BaseDataSave.usedEditCamera||FreehandCamera_R.freehandCamera.startTranslate)
            {
                return;
            }
            else
            {
                horizontalRotation = horizontalRotation + (90f * horizontalStep - horizontalRotation) / 4f;
                verticalRotation = verticalRotation + (90f * verticalStep - verticalRotation) / 4f;
                transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, horizontalRotation, 0);
                rotationX.localEulerAngles = new Vector3(verticalRotation, 0, 0);

                if (Mathf.Abs(lastFrameX - horizontalRotation) < 5f && Mathf.Abs(lastFrameY - verticalRotation) < 5f)
                {
                    if (frameCount > 3 && Main.MainControl.WaitEditRotation == true)
                    {
                        Main.MainControl.WaitEditRotation = false;                                        
                        return;
                    }
                    else
                        frameCount++;

                }
                else
                {
                    frameCount = 0;
                    lastFrameX = horizontalRotation;
                    lastFrameY = verticalRotation;
                    Main.MainControl.WaitEditRotation = true;
                }
            }
            
        }


        public void Left()
        {
            horizontalStep++;
            SoundPanel._SoundPanel.ModelSwipePlay();

        }


        public void Right()
        {
            horizontalStep--;
            SoundPanel._SoundPanel.ModelSwipePlay();
        }

        public void Up()
        {
            verticalStep--;
            SoundPanel._SoundPanel.ModelSwipePlay();
        }


        public void Down()
        {
            verticalStep++;
            SoundPanel._SoundPanel.ModelSwipePlay();
        }

        public void ResetRotationStep()
        {
            verticalStep = 0;
            horizontalStep = 0;
        }

    }
}
