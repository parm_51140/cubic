using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace dev.cubic
{
    public class EditModeControl : MonoBehaviour
    {
        [SerializeField] Text CurrentfaceHorizrontal;
        [SerializeField] Text CurrentfaceVertical;
        [SerializeField] Transform axis_X, axis_Y;
        [SerializeField] Main mainControl;
        public float filp, Y_rotate, Z_rotate;
        string lastH = "", lastV = "";
        void Start()
        {

        }

        void Update()
        {
            filp = axis_X.localEulerAngles.z;// 0 ,180
            Y_rotate = axis_Y.localEulerAngles.y;
            Z_rotate = axis_X.localEulerAngles.x;

            if (Z_rotate > 89 && Z_rotate < 91)
            {
                mainControl.setVerticalFace(0);
                CurrentfaceVertical.text = "Top";
            }
            else if (Z_rotate > 269 && Z_rotate < 271) { mainControl.setVerticalFace(2); CurrentfaceVertical.text = "Bottom"; }
            else { mainControl.setVerticalFace(1); CurrentfaceVertical.text = "Side"; }

            if (filp == 0)
            {
                if (Y_rotate > -1 && Y_rotate < 1) { CurrentfaceHorizrontal.text = "Front"; mainControl.setHorizontalFace(0); }
                else if (Y_rotate > 89 && Y_rotate < 91) { CurrentfaceHorizrontal.text = "Left"; mainControl.setHorizontalFace(1); }
                else if (Y_rotate > 179 && Y_rotate < 181) { CurrentfaceHorizrontal.text = "Back"; mainControl.setHorizontalFace(2); }
                else if (Y_rotate > 269 && Y_rotate < 271) { CurrentfaceHorizrontal.text = "Right"; mainControl.setHorizontalFace(3); }
                mainControl.revertVertical = false;
            }
            else
            {

                if (Y_rotate > -1 && Y_rotate < 1) { CurrentfaceHorizrontal.text = "Back"; mainControl.setHorizontalFace(2); }
                else if (Y_rotate > 89 && Y_rotate < 91) { CurrentfaceHorizrontal.text = "Right"; mainControl.setHorizontalFace(3); }
                else if (Y_rotate > 179 && Y_rotate < 181) { CurrentfaceHorizrontal.text = "Front"; mainControl.setHorizontalFace(0); }
                else if (Y_rotate > 269 && Y_rotate < 271) { CurrentfaceHorizrontal.text = "Left"; mainControl.setHorizontalFace(1); }
                mainControl.revertVertical = true;
            }
            if(lastH != CurrentfaceHorizrontal.text||lastV!= CurrentfaceVertical.text) //on change face
            {
                lastH = CurrentfaceHorizrontal.text;
                lastV = CurrentfaceVertical.text;
                DecalControl2._decalControl.CheckCurrFacel();
            }
        }
    }
}
