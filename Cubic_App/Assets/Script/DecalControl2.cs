using ch.sycoforge.Util.Attributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Xml.Serialization;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DecalControl2 : MonoBehaviour
{
    public static DecalControl2 _decalControl;

    [Header("BaseValue")]
    [SerializeField] Texture Cleartexture;
    [SerializeField] Texture Wraptexture;
    [SerializeField] Vector3[] DefaultRotation;

    [Header("DecalObject")]
    [SerializeField] GameObject[] decal;//frblt

    [Header("DecalPropertieCurrent")]
    DecalProperties[] dss = new DecalProperties[10];
    [SerializeField] Material[] DecalMaterial;
    [SerializeField] Texture[] DecalSelectTexture;
    GameObject CurrentSelectDecal;
    //[SerializeField] DecalProperties[] DecalData; 
    //[SerializeField] Transform[] DecalTransform;

    [Header("DefaultTexture")]
    [SerializeField] Texture[] DefaultTextureBox;
    [SerializeField] Texture[] DefaultTextureCup;
    [SerializeField] Texture[] DefaultTextureSandW;
    [SerializeField] Texture[] DefaultTextureCan;
    [SerializeField] Texture[] DefaultTextureCarton;

    [Header("CropModule")]
    [SerializeField] DecalCrop decalCrop;
    //  [SerializeField] GameObject ImportSelect;
    public bool selectAble;
    public bool EditingDecal;
    public bool ShowingDecal;
    float roundObj;
    int faceID;
    Bounds objBound;
    private void Awake()
    {
        _decalControl = this;
        if (DecalPropertiesSave.decalPropSave[0] == null)
            for (int i = 0; i < DecalPropertiesSave.decalPropSave.Length; i++)
            {

                dss[i] = new DecalProperties();
                DecalPropertiesSave.decalPropSave[i] = dss[i];

            }
        else
            for (int i = 0; i < DecalPropertiesSave.decalPropSave.Length; i++)
            {

                dss[i] = new DecalProperties();

                dss[i] = DecalPropertiesSave.decalPropSave[i];

            }
    }

    void Start()
    {
        // print(DecalPropertiesSave.decalPropSave[0 + 5].p_Position + "SSSAA");
        // _BaseData._BaseDataSave.DecalActiveMode = 1;
        // Invoke("DefaultDecal", 1);
    }
    void Update()
    {
         
    }
    public void loadDefault(bool setdefault)
    {
        
        if (setdefault)
        {
            _BaseData._BaseDataSave.hasSaveDecal = false;
            DontDestroy.DontDestroyData.saved = true;
        }
        else 
        {
            _BaseData._BaseDataSave.hasSaveDecal = true;
        }

        DefaultDecal();
    }

    void DefaultDecal()//reshape Call after createShape
    {
        print(_BaseData._BaseDataSave.hasSaveDecal);
        if (!_BaseData._BaseDataSave.hasSaveDecal)
        {
            int obIndex = _BaseData._BaseDataSave.selectedID;
            for (int i = 0; i < DefaultTextureBox.Length; i++)
            {
                if (obIndex == 1)
                {
                    dss[i].p_texture =Wraptexture;
                    dss[4].p_texture = Cleartexture;
                    dss[i + 5].p_texture = DefaultTextureBox[i];
                }
                else if (obIndex == 2)
                {
                    dss[i].p_texture = Wraptexture;
                    dss[i + 5].p_texture = DefaultTextureCup[i];
                }
                else if (obIndex == 4)
                {
                    dss[i].p_texture = DefaultTextureCan[i];
                    dss[i + 5].p_texture = Cleartexture;
                    _BaseData._BaseDataSave.DecalActiveMode = 1;
                }
                else if (obIndex == 5)
                {
                    dss[i].p_texture = Wraptexture;
                    dss[i + 5].p_texture = DefaultTextureCarton[i];
                }
                else
                {
                    dss[i].p_texture = Cleartexture;
                    dss[i + 5].p_texture = Cleartexture;
                }
            }
        }

        DecalSlide._decalSlide.forceSetDecal(_BaseData._BaseDataSave.DecalActiveMode, false);
        InitCreateDecal();
        SetDecalPos();
        SetDecalScale();
        SetDecalMaterial();
        _BaseData._BaseDataSave.hasSaveDecal = true;
        _setActiveDecalObject(true);
        LoadHolder._loadHold.setLoad(false);
    }
    public void SelectDecal()
    {
        InitCreateDecal();
        SetDecalPos();
        SetDecalScale();
        SetDecalMaterial();
    }
    void InitCreateDecal()
    {
        objBound = EditableObjectMaster._editmaster.currentObjectRenderer.bounds;
        roundObj = objBound.size.x * 2 + objBound.size.z * 2;
    }
    void SetDecalPos()
    {

        if (!_BaseData._BaseDataSave.hasSaveDecal)// nosave
        {
            // dss[0].p_Position = new Vector3(0, 0, -(objBound.extents.z + 1f));
            // dss[1].p_Position = new Vector3(objBound.extents.x + 1f, 0, 0);
            // dss[2].p_Position = new Vector3(0, 0, objBound.extents.z + 1f);
            // dss[3].p_Position = new Vector3(-(objBound.extents.x + 1f), 0, 0);
            // dss[4].p_Position = new Vector3(0, objBound.extents.y + 1f, 0);
            dss[0].p_Position = new Vector3(0, 0, -(objBound.extents.z + 1));
            dss[1].p_Position = new Vector3(objBound.extents.x + 1, 0, 0);
            dss[2].p_Position = new Vector3(0, 0, objBound.extents.z + 1);
            dss[3].p_Position = new Vector3(-objBound.extents.x - 1, 0, 0);
            dss[4].p_Position = new Vector3(0, objBound.extents.y + .5f, 0);
            dss[5].p_Position = dss[0].p_Position;
            dss[6].p_Position = dss[1].p_Position;
            dss[7].p_Position = dss[2].p_Position;
            dss[8].p_Position = dss[3].p_Position;
            dss[9].p_Position = dss[4].p_Position;

            if (_BaseData._BaseDataSave.selectedID == 4) 
            {
                dss[0].p_Position = new Vector3(0, .31f, -(objBound.extents.z + 1));
                dss[1].p_Position = new Vector3(objBound.extents.x + 1, .31f, 0);
                dss[2].p_Position = new Vector3(0, .31f, objBound.extents.z + 1);
                dss[3].p_Position = new Vector3(-objBound.extents.x - 1, .31f, 0);
            }

            for (int i = 0; i < decal.Length; i++)
            {
                dss[i].p_Rotation = DefaultRotation[i];
                dss[i + 5].p_Rotation = DefaultRotation[i];
            }
            applyPos();
            //print(DecalPropertiesSave.decalPropSave[0 + 5].p_Position + "SSSAA");
        }
        else//>> saved
        {
            applyPos();
            // print(DecalPropertiesSave.decalPropSave[0 + 5].p_Position + "SSSAA");
        }
    }
    public void RefreashDecalPos()
    {
        InitCreateDecal();
        dss[0].p_Position = new Vector3(dss[0].p_Position.x, dss[0].p_Position.y, -(objBound.extents.z + 1f));
        dss[1].p_Position = new Vector3(objBound.extents.x + 1f, dss[1].p_Position.y, dss[1].p_Position.z);
        dss[2].p_Position = new Vector3(dss[2].p_Position.x, dss[2].p_Position.y, objBound.extents.z + 1f);
        dss[3].p_Position = new Vector3(-(objBound.extents.x + 1f), dss[3].p_Position.y, dss[3].p_Position.z);
        dss[4].p_Position = new Vector3(dss[4].p_Position.x, objBound.extents.y + .5f, dss[4].p_Position.z);
        dss[5].p_Position = new Vector3(dss[5].p_Position.x, dss[5].p_Position.y, -(objBound.extents.z + 1f));
        dss[6].p_Position = new Vector3(objBound.extents.x + 1f, dss[6].p_Position.y, dss[6].p_Position.z);
        dss[7].p_Position = new Vector3(dss[7].p_Position.x, dss[7].p_Position.y, objBound.extents.z + 1f);
        dss[8].p_Position = new Vector3(-(objBound.extents.x + 1f), dss[8].p_Position.y, dss[8].p_Position.z);
        dss[9].p_Position = new Vector3(dss[9].p_Position.x, objBound.extents.y + .5f, dss[9].p_Position.z);
        applyPos();
    }
    void SetDecalScale()
    {
      
        //Scale
        if (!_BaseData._BaseDataSave.hasSaveDecal)// nosave
        {
            dss[0].p_Scale = new Vector3(objBound.size.x, 1, objBound.extents.y);
            dss[1].p_Scale = new Vector3(objBound.size.z, 1, objBound.extents.y);
            dss[2].p_Scale = dss[0].p_Scale;
            dss[3].p_Scale = dss[1].p_Scale;

            dss[4].p_Scale = new Vector3((float)dss[4].p_texture.width / (float)dss[4].p_texture.height * objBound.size.z, 1.5f, objBound.size.z);

            dss[5].p_Scale = new Vector3((float)dss[5].p_texture.width / (float)dss[5].p_texture.height, 1, 1);
            dss[6].p_Scale = new Vector3((float)dss[6].p_texture.width / (float)dss[6].p_texture.height, 1, 1);
            dss[7].p_Scale = new Vector3((float)dss[7].p_texture.width / (float)dss[7].p_texture.height, 1, 1);
            dss[8].p_Scale = new Vector3((float)dss[8].p_texture.width / (float)dss[8].p_texture.height, 1, 1);
            dss[9].p_Scale = new Vector3((float)dss[9].p_texture.width / (float)dss[9].p_texture.height, 1.5f, 1);
            // dss[5].p_Scale = new Vector3((float)dss[9].p_texture.width / (float)dss[9].p_texture.height * objBound.size.z, 1, objBound.size.y);
            // dss[6].p_Scale = new Vector3((float)dss[6].p_texture.width / (float)dss[6].p_texture.height, 1, 1);
            // dss[7].p_Scale = new Vector3((float)dss[7].p_texture.width / (float)dss[7].p_texture.height * objBound.size.y, 1, objBound.size.y);
            // dss[8].p_Scale = new Vector3((float)dss[8].p_texture.width / (float)dss[8].p_texture.height, 1, 1);
            // dss[9].p_Scale = new Vector3((float)dss[9].p_texture.width / (float)dss[9].p_texture.height * objBound.size.z, 1, objBound.size.z);
            if (_BaseData._BaseDataSave.selectedID == 1)
            {
                dss[5].p_Scale = new Vector3(9.295044f, 1, 3.539245f);
                dss[7].p_Scale = new Vector3(8.6068f, 1, 3.539245f);
                dss[9].p_Scale = new Vector3(9.295044f, 1.5f, 15.984f);
                dss[4].p_Scale = new Vector3(9.295044f, 1.5f, 15.984f);
            }
            else if (_BaseData._BaseDataSave.selectedID == 2)
            {
                dss[9].p_Scale = new Vector3(6.019783f, 1.5f, 6.019783f);
                dss[4].p_Scale = new Vector3(6.019783f, 1.5f, 6.019783f);
            }
            else if (_BaseData._BaseDataSave.selectedID == 4)
            {
                //10.04
                // float sc = (float)dss[0].p_texture.width * 4 / (float)dss[0].p_texture.height;
                // dss[0].p_Scale = new Vector3(objBound.size.x*.8f, 1, objBound.extents.x * sc);
                // dss[1].p_Scale = new Vector3(objBound.size.z*.8f, 1, objBound.extents.z*sc);
                dss[0].p_Scale = new Vector3(objBound.size.x * .8f, 1, 13.05f);
                dss[1].p_Scale = new Vector3(objBound.size.z * .8f, 1, 13.05f);
                dss[2].p_Scale = dss[0].p_Scale;
                dss[3].p_Scale = dss[1].p_Scale;
            }
            else if (_BaseData._BaseDataSave.selectedID == 5)
            {
                dss[5].p_Scale = new Vector3(5.362115f, 1, 11f);
                dss[6].p_Scale = new Vector3(3.84f, 1, 11f);
                dss[7].p_Scale = new Vector3(5.5f, 1, 11f);
                dss[8].p_Scale = new Vector3(3.884928f, 1, 10.78242f);
                dss[9].p_Scale = new Vector3(5.083657f, 1.5f, 3.7f);
            }
            applyScale();
        }
        else //>> saved
        {
            applyScale();
        }     
    }
    void SetDecalMaterial()
    {
        //offset
        if (!_BaseData._BaseDataSave.hasSaveDecal)// nosave
        {
            for (int i = 0; i < dss.Length; i++)
            {
                dss[i].p_offset = 0;
                dss[i].p_TexScale = 1;
            }
            dss[1].p_offset = objBound.size.x / roundObj;//25%
            dss[2].p_offset = (objBound.size.x / roundObj) + (objBound.size.z / roundObj);//50%
            dss[3].p_offset = (objBound.size.x / roundObj) * 2 + (objBound.size.z / roundObj);//75% 

            if (_BaseData._BaseDataSave.selectedID == 4) 
            {
                dss[0].p_offset = .2f;
                dss[1].p_offset = .419f;
                dss[2].p_offset = .68f;
                dss[3].p_offset = .979f;
            }

            dss[0].p_TexScale = objBound.size.z / roundObj;
            dss[1].p_TexScale = objBound.size.x / roundObj;
            dss[2].p_TexScale = objBound.size.z / roundObj;
            dss[3].p_TexScale = objBound.size.x / roundObj;

            applyTexture();
        }
        else //>> saved
        {
            applyTexture();
        }
    }
    void applyPos()
    {
        if (_BaseData._BaseDataSave.DecalActiveMode == 1)//wrap
        {
            for (int i = 0; i < decal.Length; i++)
            {
                decal[i].transform.localPosition = DecalPropertiesSave.decalPropSave[i].p_Position;
                decal[i].transform.localEulerAngles = DecalPropertiesSave.decalPropSave[i].p_Rotation;
            }
        }
        else if (_BaseData._BaseDataSave.DecalActiveMode == 2)//sticker
        {
            for (int i = 0; i < decal.Length; i++)
            {
                decal[i].transform.localPosition = DecalPropertiesSave.decalPropSave[i + 5].p_Position;
                decal[i].transform.localEulerAngles = DecalPropertiesSave.decalPropSave[i + 5].p_Rotation;
            }
        }
    }
    void applyScale()
    {
      
        if (_BaseData._BaseDataSave.DecalActiveMode == 1)//wrap
        {
            for (int i = 0; i < decal.Length; i++)
            {
                decal[i].transform.localScale = DecalPropertiesSave.decalPropSave[i].p_Scale;
            }
        }
        else if (_BaseData._BaseDataSave.DecalActiveMode == 2)//sticker
        {
            for (int i = 0; i < decal.Length; i++)
            {
                decal[i].transform.localScale = DecalPropertiesSave.decalPropSave[i + 5].p_Scale;
            }
        }
      
    }
    void applyTexture()
    {
        //{ print(_BaseData._BaseDataSave.DecalActiveMode); }
        if (_BaseData._BaseDataSave.DecalActiveMode == 1)//wrap
        {
            for (int i = 0; i < DecalMaterial.Length; i++)
            {
                //print(DecalPropertiesSave.decalPropSave[i].p_texture.name);
                DecalMaterial[i].SetTexture("_BaseMap", DecalPropertiesSave.decalPropSave[i].p_texture);
                DecalMaterial[i].SetTextureOffset("_BaseMap", new Vector2(DecalPropertiesSave.decalPropSave[i].p_offset, 0));
                DecalMaterial[i].SetTextureScale("_BaseMap", new Vector2(DecalPropertiesSave.decalPropSave[i].p_TexScale, 1));
            }
            createSelectMAterial();
        }
        else if (_BaseData._BaseDataSave.DecalActiveMode == 2)//sticker
        {
            for (int i = 0; i < DecalMaterial.Length; i++)
            {
                //print(DecalPropertiesSave.decalPropSave[i + 5].p_texture.name);
                DecalMaterial[i].SetTexture("_BaseMap", DecalPropertiesSave.decalPropSave[i + 5].p_texture);
                DecalMaterial[i].SetTextureOffset("_BaseMap", new Vector2(DecalPropertiesSave.decalPropSave[i + 5].p_offset, 0));
                DecalMaterial[i].SetTextureScale("_BaseMap", new Vector2(DecalPropertiesSave.decalPropSave[i + 5].p_TexScale, 1));
            }
            createSelectMAterial();
        }
        // createSelectMAterial();
    }
    void createSelectMAterial()
    {
        for (int i = 0; i < DecalMaterial.Length; i++)
        {
            //print(DecalPropertiesSave.decalPropSave[i].p_texture.name + "***");
            Texture2D t2d;
            if (_BaseData._BaseDataSave.DecalActiveMode == 1)
                t2d = new Texture2D(DecalPropertiesSave.decalPropSave[i].p_texture.width, DecalPropertiesSave.decalPropSave[i].p_texture.height);
            else
                t2d = new Texture2D(DecalPropertiesSave.decalPropSave[i + 5].p_texture.width, DecalPropertiesSave.decalPropSave[i + 5].p_texture.height);
            t2d.name = "TempTexture";
            Texture2D texterCon = (Texture2D)DecalPropertiesSave.decalPropSave[i].p_texture;
            if (_BaseData._BaseDataSave.DecalActiveMode == 2)
                texterCon = (Texture2D)DecalPropertiesSave.decalPropSave[i + 5].p_texture;
            //   print(texterCon.name + " " + texterCon.width + " " + t2d.width);
            t2d.SetPixels(texterCon.GetPixels());
            float lineW = t2d.width * .05f;
            float lineH = t2d.height * .05f;
            for (int y = 0; y < t2d.height; y++)
            {
                for (int x = 0; x < t2d.width; x++)
                {
                    Color tt = t2d.GetPixel(x, y);
                    if (y < lineH || y > t2d.height - lineH)
                    { tt = Color.green; }
                    if (_BaseData._BaseDataSave.DecalActiveMode == 2 && (x < lineW || x > t2d.width - lineW))
                    {
                        tt = Color.green;
                    }
                    t2d.SetPixel(x, y, tt * 1.5f);
                }
            }
            t2d.Apply();
            DecalSelectTexture[i] = t2d;
            t2d = null;
        }
    }
    void createSelectMAterial(int ID)
    {
        Texture2D t2d = new Texture2D(DecalPropertiesSave.decalPropSave[ID + 5].p_texture.width, DecalPropertiesSave.decalPropSave[ID + 5].p_texture.height);
        t2d.name = "TempTexture";
        Texture2D texterCon = (Texture2D)DecalPropertiesSave.decalPropSave[ID + 5].p_texture;
        // if (_BaseData._BaseDataSave.DecalActiveMode == 2)
        //     texterCon = (Texture2D)DecalPropertiesSave.decalPropSave[ID + 5].p_texture;
        t2d.SetPixels(texterCon.GetPixels());
        float lineW = t2d.width * .05f;
        float lineH = t2d.height * .05f;
        for (int y = 0; y < t2d.height; y++)
        {
            for (int x = 0; x < t2d.width; x++)
            {
                Color tt = t2d.GetPixel(x, y);
                if (y < lineH || y > t2d.height - lineH)
                { tt = Color.green; }
                if (_BaseData._BaseDataSave.DecalActiveMode == 2 && (x < lineW || x > t2d.width - lineW))
                {
                    tt = Color.green;
                }
                t2d.SetPixel(x, y, tt * 1.5f);
            }
        }
        t2d.Apply();
        DecalSelectTexture[ID] = t2d;
        t2d = null;

    }
    public void _setActiveDecalObject(bool show)
    {
        ShowingDecal = show;
        if (_BaseData._BaseDataSave.DecalActiveMode == 0) ShowingDecal = false;
        foreach (GameObject go in decal)
        { 
            go.SetActive(ShowingDecal);   
        }   
        setActiveDecalLid();

    }
    public void setActiveDecalLid()
    {
       // print((ShowingDecal && _BaseData._BaseDataSave.openLid) + "******");
       
        decal[4].SetActive(ShowingDecal && _BaseData._BaseDataSave.openLid);
    }
    
    public void setEditDecalToggle(bool isedit)
    {
        EditingDecal = isedit;
    }

    public void CheckCurrFacel()
    {
        if (Main.MainControl.currVertivalFace == Main.VertivalFace.Side)
        {
            if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front)
            {
                CurrentSelectDecal = decal[0].gameObject;
                faceID = 0;
            }
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Right)//
            {
                CurrentSelectDecal = decal[1].gameObject; faceID = 1;
            }
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back)
            {
                CurrentSelectDecal = decal[2].gameObject; faceID = 2;
            }
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Left)//
            {
                CurrentSelectDecal = decal[3].gameObject; faceID = 3;
            }
        }
        else if (Main.MainControl.currVertivalFace == Main.VertivalFace.Top)
        { CurrentSelectDecal = decal[4].gameObject; faceID = 4; }
        else if (Main.MainControl.currVertivalFace == Main.VertivalFace.Bottom)
        { CurrentSelectDecal = null; faceID = 99; }

        // print(DecalMaterial[faceID].GetTexture("_MainTex").name);
        int c = 0;
        DecalSlide._decalSlide.showAddRemove(faceID < 5);
        if (faceID < 5)
        {
            if (_BaseData._BaseDataSave.DecalActiveMode == 1) c = faceID;
            else if (_BaseData._BaseDataSave.DecalActiveMode == 2) c = faceID + 5;
            if (dss[c].p_texture != null)
            {
               // print(dss[c].p_texture.name);
                if (dss[c].p_texture.name == "Clear")
                {
                    selectAble = false;
                    DecalSlide._decalSlide.setRemove(false);
                }
                else
                {
                    selectAble = true;
                    DecalSlide._decalSlide.setRemove(true);
                }
            }
        }
    }
    public void ActiveEditDecal(bool setActive)
    {
        CheckCurrFacel();
        EditingDecal = setActive;
        if (EditingDecal)
        {
            //  print(DecalSlide._decalSlide.isActive);
            if (DecalSlide._decalSlide.isActive) { DecalSlide._decalSlide.DecalDiscDeactive(); }
            if (_BaseData._BaseDataSave.DecalActiveMode == 1 && faceID != 4)
            {
                for (int i = 0; i < DecalMaterial.Length - 1; i++)
                {
                    DecalMaterial[i].SetTexture("_BaseMap", DecalSelectTexture[i]);
                }
            }
            else
            {
                DecalMaterial[faceID].SetTexture("_BaseMap", DecalSelectTexture[faceID]);
            }
            if ((_BaseData._BaseDataSave.DecalActiveMode == 2 && _BaseData._BaseDataSave.openLid) ||
                (_BaseData._BaseDataSave.DecalActiveMode == 1 && _BaseData._BaseDataSave.openLid && faceID == 4))
                handlerGroup.transform.localScale = Vector3.one;
            else
                handlerGroup.transform.localScale = Vector3.zero;
        }
        else
        {
            if (_BaseData._BaseDataSave.DecalActiveMode == 1)
            {
                for (int i = 0; i < DecalMaterial.Length; i++)
                {
                    DecalMaterial[i].SetTexture("_BaseMap", DecalPropertiesSave.decalPropSave[i].p_texture);
                }
            }
            else
            {
                DecalMaterial[faceID].SetTexture("_BaseMap", DecalPropertiesSave.decalPropSave[faceID + 5].p_texture);
            }
            handlerGroup.transform.localScale = Vector3.zero;
            SaveDecalData();
        }
    }
    Vector3 startmovePos;
    public void startmoveDecal()
    {
        startmovePos = CurrentSelectDecal.transform.localPosition;

        if (_BaseData._BaseDataSave.DecalActiveMode == 1)
            for (int i = 0; i < 4; i++)
                DecalPropertiesSave.decalPropSave[i].p_offset = DecalMaterial[i].GetTextureOffset("_BaseMap").x;

    }
    public void moveDecalSingle(Vector3 delta)
    {
        if (!EditingDecal) return;
        if (_BaseData._BaseDataSave.DecalActiveMode == 2)
        {
            if (Main.MainControl.currVertivalFace == Main.VertivalFace.Side)
            {
                float moveX, moveY;
                if (faceID == 0)
                {
                    Vector3 moveDelta = startmovePos - delta;
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.y, objBound.extents.y);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.x, objBound.extents.x);
                    CurrentSelectDecal.transform.localPosition = new Vector3(moveX, moveY, startmovePos.z);
                }
                else if (faceID == 1)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x, startmovePos.y - delta.y, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.z, objBound.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.y, objBound.extents.y);
                    CurrentSelectDecal.transform.localPosition = new Vector3(startmovePos.x, moveY, moveX);
                }
                else if (faceID == 2)
                {
                    Vector3 moveDelta = startmovePos - delta;
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.y, objBound.extents.y);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.x, objBound.extents.x);
                    CurrentSelectDecal.transform.localPosition = new Vector3(-moveX, moveY, startmovePos.z);
                }
                else if (faceID == 3)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x, startmovePos.y - delta.y, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.z, objBound.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.y, objBound.extents.y);
                    CurrentSelectDecal.transform.localPosition = new Vector3(startmovePos.x, moveY, -moveX);
                }
            }
            else if (faceID == 4)
            {
                float moveX, moveY;
                if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front)
                {
                    Vector3 moveDelta = startmovePos - delta;
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.z, objBound.extents.z);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.x, objBound.extents.x);
                    CurrentSelectDecal.transform.localPosition = new Vector3(moveX, startmovePos.y, moveY);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back)
                {
                    Vector3 moveDelta = startmovePos - delta;
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.z, objBound.extents.z);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.x, objBound.extents.x);
                    CurrentSelectDecal.transform.localPosition = new Vector3(-moveX, startmovePos.y, -moveY);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Left)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x, startmovePos.y - delta.y, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.z, objBound.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.x, objBound.extents.x);
                    CurrentSelectDecal.transform.localPosition = new Vector3(-moveY, startmovePos.y, moveX);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Right)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x, startmovePos.y - delta.y, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.z, objBound.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.x, objBound.extents.x);
                    CurrentSelectDecal.transform.localPosition = new Vector3(moveY, startmovePos.y, -moveX);
                }
            }
        }//StickerMove
        else if (_BaseData._BaseDataSave.DecalActiveMode == 1)
        {
            if (Main.MainControl.currVertivalFace == Main.VertivalFace.Side)
            {
                float moveY;
                Vector3 moveDeltaY = startmovePos - delta;
                float moveDelta = delta.x / 100;
                moveY = moveY = Mathf.Clamp(moveDeltaY.y, -objBound.extents.y, objBound.extents.y);
                DecalMaterial[0].SetTextureOffset("_BaseMap", new Vector2(moveDelta + DecalPropertiesSave.decalPropSave[0].p_offset, 0));
                DecalMaterial[1].SetTextureOffset("_BaseMap", new Vector2(moveDelta + DecalPropertiesSave.decalPropSave[1].p_offset, 0));
                DecalMaterial[2].SetTextureOffset("_BaseMap", new Vector2(moveDelta + DecalPropertiesSave.decalPropSave[2].p_offset, 0));
                DecalMaterial[3].SetTextureOffset("_BaseMap", new Vector2(moveDelta + DecalPropertiesSave.decalPropSave[3].p_offset, 0));

                decal[0].transform.localPosition = new Vector3(0, moveY, -(objBound.extents.z + 1f));
                decal[1].transform.localPosition = new Vector3(objBound.extents.x + 1f, moveY, 0);
                decal[2].transform.localPosition = new Vector3(0, moveY, objBound.extents.z + 1f);
                decal[3].transform.localPosition = new Vector3(-(objBound.extents.x + 1f), moveY, 0);
            }
            else if (Main.MainControl.currVertivalFace == Main.VertivalFace.Top)
            {
                float moveX, moveY;

                if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front)
                {
                    Vector3 moveDelta = startmovePos - delta;
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.z, objBound.extents.z);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.x, objBound.extents.x);
                    CurrentSelectDecal.transform.localPosition = new Vector3(moveX, startmovePos.y, moveY);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back)
                {
                    Vector3 moveDelta = startmovePos - delta;
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.z, objBound.extents.z);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.x, objBound.extents.x);
                    CurrentSelectDecal.transform.localPosition = new Vector3(-moveX, startmovePos.y, -moveY);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Left)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x, startmovePos.y - delta.y, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.z, objBound.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.x, objBound.extents.x);
                    CurrentSelectDecal.transform.localPosition = new Vector3(-moveY, startmovePos.y, moveX);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Right)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x, startmovePos.y - delta.y, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -objBound.extents.z, objBound.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -objBound.extents.x, objBound.extents.x);
                    CurrentSelectDecal.transform.localPosition = new Vector3(moveY, startmovePos.y, -moveX);
                }
            }
        }//WrapMove
    }
    public Text outt;
    public void ScaleDecal(float scaleIn)
    {
        // dss[5].p_Scale
        // outt.text = scaleIn + "";
        int block = faceID;
        if (_BaseData._BaseDataSave.DecalActiveMode == 2)
        {
            block += 5;
            if (faceID == 0 || faceID == 2)
            {

                Vector3 a = new Vector3(Mathf.Clamp(dss[block].p_Scale.x + scaleIn, .1f, objBound.size.x),
                    1,
                    Mathf.Clamp(dss[block].p_Scale.z + scaleIn, .1f, objBound.size.y));
                CurrentSelectDecal.transform.localScale = a;
            }
            else if (faceID == 1 || faceID == 3)
            {
                Vector3 a = new Vector3(Mathf.Clamp(dss[block].p_Scale.x + scaleIn, .1f, objBound.size.x),
                   1,
                    Mathf.Clamp(dss[block].p_Scale.z + scaleIn, .1f, objBound.size.z));
                CurrentSelectDecal.transform.localScale = a;
            }
            else if (faceID == 4)
            {
                Vector3 a = new Vector3(Mathf.Clamp(dss[block].p_Scale.x + scaleIn, 0.1f, objBound.size.x),
                   1,
                    Mathf.Clamp(dss[block].p_Scale.z + scaleIn, .1f, objBound.size.z));
                CurrentSelectDecal.transform.localScale = a;
            }
        }
        else if (_BaseData._BaseDataSave.DecalActiveMode == 1)
        {
            if (faceID != 4)
            {
                //  if (faceID == 0 || faceID == 2)
                //  {

                Vector3 a = new Vector3(Mathf.Clamp(dss[block].p_Scale.x, .1f, objBound.size.x),
                    1,
                    Mathf.Clamp(dss[block].p_Scale.z + scaleIn, .1f, objBound.size.y));
                decal[0].transform.localScale = a;
                decal[2].transform.localScale = a;
                // CurrentSelectDecal.transform.localScale = a;
                //  }
                //  else if (faceID == 1 || faceID == 3)
                //  {
                Vector3 a2 = new Vector3(Mathf.Clamp(dss[block].p_Scale.x, .1f, objBound.size.x),
                   1,
                    Mathf.Clamp(dss[block].p_Scale.z + scaleIn, .1f, objBound.size.z));
                // CurrentSelectDecal.transform.localScale = a;
                decal[1].transform.localScale = a2;
                decal[3].transform.localScale = a2;
                //  }
            }
            else if (faceID == 4)
            {
                Vector3 a = new Vector3(Mathf.Clamp(dss[block].p_Scale.x, 0.1f, objBound.size.x),
                   1,
                    Mathf.Clamp(dss[block].p_Scale.z + scaleIn, .1f, objBound.size.z));
                CurrentSelectDecal.transform.localScale = a;
            }
        }

    }
    [Header("RotateHandle")]
    Vector3 HandlerStartPos;
    Vector3 startRotateValue;
    [SerializeField] GameObject handlerObj;//z rott
    [SerializeField] GameObject handlerGroup;
    float FrontR, LeftR, RightR, BackR, TopR;

    public void setRotateHandler()//active handler
    {
        float a = 0;
        if (Main.MainControl.currVertivalFace == Main.VertivalFace.Top) a = TopR;
        else
        {
            if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front) a = FrontR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back) a = BackR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Right) a = RightR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Left) a = LeftR;
        }
        handlerObj.transform.localEulerAngles = new Vector3(0, 0, 0);
    }
    public void rotate_handler_Start()
    {
        HandlerStartPos = handlerObj.transform.position;
    }
    public void rotate_handler_Drag()
    {
        float angle = Mathf.Atan2(Input.mousePosition.y - HandlerStartPos.y, Input.mousePosition.x - HandlerStartPos.x) * 180 / Mathf.PI;
        float a = 0;
        if (Main.MainControl.currVertivalFace == Main.VertivalFace.Top) a = TopR;
        else
        {
            if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front) a = FrontR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back) a = BackR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Left) a = RightR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Right) a = LeftR;
        }
        handlerObj.transform.localEulerAngles = new Vector3(0, 0, a);

        if (faceID == 0)
        {
            CurrentSelectDecal.transform.localEulerAngles = new Vector3(startRotateValue.x + angle - 90, -90, 90);
            FrontR = angle - 90;
        }
        else if (faceID == 2)
        {
            CurrentSelectDecal.transform.localEulerAngles = new Vector3(startRotateValue.x + angle - 90, 90, 90);
            BackR = angle - 90;
        }
        else if (faceID == 1)
        {
            CurrentSelectDecal.transform.localEulerAngles = new Vector3(startRotateValue.x + angle - 90, 180, 90);
            RightR = angle - 90;
        }
        else if (faceID == 3)
        {
            CurrentSelectDecal.transform.localEulerAngles = new Vector3(startRotateValue.x - angle + 90, 180, -90);
            LeftR = angle - 90;
        }

        else if (faceID == 4)
        {
            CurrentSelectDecal.transform.localEulerAngles = new Vector3(-180, startRotateValue.x - angle - 90, -180);
            TopR = angle - 90;
        }
        //DontDestroy.DontDestroyData.DecalObjectR[faceID] = CurrentActiveDecal.transform.localEulerAngles;

    }
    public void ImportByUrl()
    {
        CheckCurrFacel();
        print(GUIUtility.systemCopyBuffer);
        StartCoroutine(DownloadImage(GUIUtility.systemCopyBuffer));
    }
    IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        {
            // CropPanelGroup.SetActive(false);
        }
        else
        {
            if (faceID == 5) yield return null;//bottom
            decalCrop.gameObject.SetActive(true);
            decalCrop._SetEditable(((DownloadHandlerTexture)request.downloadHandler).texture);
            //  ImportSelect.SetActive(false);
        }
    }

    public void ImportGallery()
    {
        CheckCurrFacel();
        // if(DecalMaterial[faceID].mainTexture.name);return;
        ActiveEditDecal(false);
        if (faceID == 5) return;
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            if (path != null)
            {
                Texture2D texture = NativeGallery.LoadImageAtPath(path, 512);// If the selected image's width and/or height is greater than 512px, down-scale the image
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }
                decalCrop.gameObject.SetActive(true);
                decalCrop._SetEditable(texture);
            }
        });
        selectAble = true;
    }
    public void removeDecal()
    {
        int blockindex = 99;
        if (_BaseData._BaseDataSave.DecalActiveMode == 1)
        {
            blockindex = faceID;
            if (faceID < 4)
            {
                for (int i = 0; i < 4; i++)
                {

                    dss[i].p_texture = Cleartexture;
                    DecalMaterial[i].SetTexture("_BaseMap", DecalPropertiesSave.decalPropSave[i].p_texture);
                }
            }
            else
            {
                dss[blockindex].p_Position = new Vector3(0, objBound.extents.y + 1f, 0);
                dss[blockindex].p_texture = Cleartexture;
                dss[blockindex].p_Scale = new Vector3((float)Cleartexture.width / (float)Cleartexture.height, 1, 1);
                decal[blockindex].transform.localPosition = DecalPropertiesSave.decalPropSave[blockindex].p_Position;
                decal[blockindex].transform.localScale = DecalPropertiesSave.decalPropSave[blockindex].p_Scale;
                DecalMaterial[faceID].SetTexture("_BaseMap", DecalPropertiesSave.decalPropSave[blockindex].p_texture);
            }
        }
        else if (_BaseData._BaseDataSave.DecalActiveMode == 2)
        {
            blockindex = faceID + 5;
            switch (faceID)
            {
                case 0:
                    dss[blockindex].p_Position = new Vector3(0, 0, -(objBound.extents.z + 1f)); break;
                case 1:
                    dss[blockindex].p_Position = new Vector3(objBound.extents.x + 1f, 0, 0); break;
                case 2:
                    dss[blockindex].p_Position = new Vector3(0, 0, objBound.extents.z + 1f); break;
                case 3:
                    dss[blockindex].p_Position = new Vector3(-(objBound.extents.x + 1f), 0, 0); break;
                case 4:
                    dss[blockindex].p_Position = new Vector3(0, objBound.extents.y + 1f, 0); break;
            }
            dss[blockindex].p_texture = Cleartexture;
            dss[blockindex].p_Scale = new Vector3((float)Cleartexture.width / (float)Cleartexture.height, 1, 1);
            decal[faceID].transform.localPosition = DecalPropertiesSave.decalPropSave[blockindex].p_Position;
            decal[faceID].transform.localScale = DecalPropertiesSave.decalPropSave[blockindex].p_Scale;
            DecalMaterial[faceID].SetTexture("_BaseMap", DecalPropertiesSave.decalPropSave[blockindex].p_texture);
        }
        selectAble = false;
        DecalSlide._decalSlide.setRemove(false);
    }

    public void SetCropValue(Texture t2d)
    {
        if (_BaseData._BaseDataSave.DecalActiveMode == 1)
        {
            if (faceID == 4)
            {
                DecalPropertiesSave.decalPropSave[4].p_texture = t2d;
                createSelectMAterial(4);
                importSetScale(4);
            }
            else
            {
                DecalPropertiesSave.decalPropSave[0].p_texture = t2d;
                DecalPropertiesSave.decalPropSave[1].p_texture = t2d;
                DecalPropertiesSave.decalPropSave[2].p_texture = t2d;
                DecalPropertiesSave.decalPropSave[3].p_texture = t2d;

                dss[0].p_Scale = new Vector3(objBound.size.x, 1, ((float)dss[0].p_texture.height / (float)dss[0].p_texture.width) * objBound.size.x);
                dss[1].p_Scale = new Vector3(objBound.size.z, 1, ((float)dss[0].p_texture.height / (float)dss[0].p_texture.width) * objBound.size.z);
                dss[2].p_Scale = dss[0].p_Scale;
                dss[3].p_Scale = dss[1].p_Scale;

                createSelectMAterial();
            }
        }
        else
        {
            DecalPropertiesSave.decalPropSave[faceID + 5].p_texture = t2d;
            createSelectMAterial(faceID);
            importSetScale(faceID + 5);
        }
        DecalSlide._decalSlide.setRemove(true);
        applyScale();
        applyTexture();
    }
    void importSetScale(int saveId)
    {
        float sideScale = 0;
        if (faceID == 0 || faceID == 2)
        { sideScale = objBound.size.x < objBound.size.y ? objBound.size.x : objBound.size.y; }
        else if (faceID == 1 || faceID == 3)
        { sideScale = objBound.size.z < objBound.size.y ? objBound.size.z : objBound.size.y; }
        else if (faceID == 4)
        {
            sideScale = objBound.size.x < objBound.size.z ? objBound.size.x : objBound.size.z;
        }

        if (DecalPropertiesSave.decalPropSave[saveId].p_texture.width < DecalPropertiesSave.decalPropSave[saveId].p_texture.height)
        {
            float nScale = (float)DecalPropertiesSave.decalPropSave[saveId].p_texture.width / (float)DecalPropertiesSave.decalPropSave[saveId].p_texture.height;
            DecalPropertiesSave.decalPropSave[saveId].p_Scale = new Vector3(nScale * sideScale, 1, sideScale);

        }
        else
        {
            float nScale = (float)DecalPropertiesSave.decalPropSave[saveId].p_texture.width / (float)DecalPropertiesSave.decalPropSave[saveId].p_texture.height;
            DecalPropertiesSave.decalPropSave[saveId].p_Scale = new Vector3(nScale * sideScale, 1, sideScale);
        }
        applyScale();

    }
    public void SaveDecalData()
    {
        if (_BaseData._BaseDataSave.DecalActiveMode == 1)
        {
            for (int i = 0; i < decal.Length; i++)
            {
                DecalPropertiesSave.decalPropSave[i].p_Position = decal[i].transform.localPosition;
                DecalPropertiesSave.decalPropSave[i].p_Rotation = decal[i].transform.localEulerAngles;
                DecalPropertiesSave.decalPropSave[i].p_Scale = decal[i].transform.localScale;
                DecalPropertiesSave.decalPropSave[i].p_offset = DecalMaterial[i].GetTextureOffset("_BaseMap").x;
            }

        }
        else if (_BaseData._BaseDataSave.DecalActiveMode == 2)
        {
            for (int i = 0; i < decal.Length; i++)
            {
                DecalPropertiesSave.decalPropSave[i + 5].p_Position = decal[i].transform.localPosition;
                DecalPropertiesSave.decalPropSave[i + 5].p_Rotation = decal[i].transform.localEulerAngles;
                DecalPropertiesSave.decalPropSave[i + 5].p_Scale = decal[i].transform.localScale;
            }
        }
        // print(DecalPropertiesSave.decalPropSave[0 + 5].p_Position + "SSSAA");
    }

    public void SaveImage()
    {
        int nu = faceID;
        if (_BaseData._BaseDataSave.DecalActiveMode == 2) nu += 5;

        //ToLoad
        //  Texture2D newPhoto = new Texture2D(dss[nu].p_texture.width, dss[nu].p_texture.height);
        //  newPhoto.LoadImage(Convert.FromBase64String(PlayerPrefs.GetString("PhotoSaved"))); 
        //  newPhoto.Apply(); 
        //  newUserPhoto = newPhoto;

        //ToSave Image from a Texture 2D which must be a "readable" and RGBA 32 format.

        string stringData = Convert.ToBase64String(((Texture2D)dss[nu].p_texture).EncodeToPNG());
        GUIUtility.systemCopyBuffer = stringData.ToString();

        //1 PlayerPrefs.SetString(PhotoSaved, stringData );

    }

    public Texture GetClearTex() 
    {

        return Cleartexture;
    
    }

}
