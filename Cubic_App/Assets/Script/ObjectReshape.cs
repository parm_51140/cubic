using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class ObjectReshape : MonoBehaviour
{
    [SerializeField] Main mainControl;
    public Material defaultMat;
    public Material heighLightMat;
    public Material onCameraMat;
    public MeshFilter startshape;
    public MeshFilter endshape;
    public MeshFilter targetshape;
    [HideInInspector] public List<Vector3> startvert;
    [HideInInspector] public List<Vector3> endvert;
    [HideInInspector] public List<Vector3> targetvert;
    [HideInInspector] public int[] tri;
    float sclX = 0;
    float sclY = 0;
    float sclZ = 0;
    float maxHeigh = 0;
    float minHeigh = 0;
    float fullHeigh = 0;
    float maxWidth = 0;
    float minWidth = 0;
    float fullWidth = 0;
    Vector3 objMin;
    Vector3 objMax;

    [SerializeField] TextMesh horizontalSize;
    [SerializeField] TextMesh VerticalSize;
    [SerializeField] LineRenderer VerticalLine;
    public MeshRenderer objRenderer;

    [SerializeField] LineRenderer horizontalLine;
    [SerializeField] ObjectSizeScript objSize;
    [SerializeField] WireframeShader objWireframe;

    [SerializeField] MeshFilter threadModel;
    [SerializeField] MeshFilter bowlModel;
    [SerializeField] MeshFilter SanwitchModel;
    [SerializeField] Mesh editmesh;

    public Bounds ShapeDefaultBound;
    public Bounds ShapeCurrentBound;

    public Camera OrbitCam;
    public static GameObject objReshape;
    public static ObjectReshape _reShapeIns;
    int currentObject = 0;
    private void Awake()
    {
        if (objReshape == null)
        {
            objReshape = this.gameObject;
        }
        // else { Destroy(this.gameObject); }

        if (_reShapeIns == null)
        {
            _reShapeIns = this;
        }
        //else { Destroy(_reShapeIns); _reShapeIns = this; }

        // if (DontDestroy.DontDestroyData.currReshape == null) 
        { DontDestroy.DontDestroyData.currReshape = this; }



    }
    public int currObjIndex() { return currentObject; }
    private void Start()
    {
        
     //  if (DontDestroy.DontDestroyData.saved == false)
     //  {         
     //      InitMesh(1); 
     //  }
      //  else if (DontDestroy.DontDestroyData.saved == true)
      //  {
      //      InitMesh(DontDestroy.DontDestroyData.currentObjectIndex);
      //      Scaler(DontDestroy.DontDestroyData.savescaleDimention, DontDestroy.DontDestroyData.saveScaleTop, DontDestroy.DontDestroyData.saveScaleBottom, true);
      //      refreashCollider();
      //      ObjectCover._objectcover.forceActiveLid();
      //      // Invoke("delayScale", 2);
      //  }
       
    }

    void InitMesh(int id)
    {
        currentObject = id;       
        Mesh useMesh = startshape.sharedMesh;
        if (id == 1) { useMesh = threadModel.sharedMesh; }
        else if (id == 2) { useMesh = bowlModel.sharedMesh; }
        else if (id == 3) { useMesh = SanwitchModel.sharedMesh; }

        //   Mesh mesh = startshape.sharedMesh;
        Mesh mesh = useMesh;
        // var meshSimplifier = new UnityMeshSimplifier.MeshSimplifier();
        // meshSimplifier.Initialize(mesh);
        // meshSimplifier.SimplifyMesh(1f);
        // startshape.sharedMesh = meshSimplifier.ToMesh();

        mesh = useMesh;
        //mesh = startshape.mesh;

        Mesh newmesh = new Mesh();
        newmesh = (Mesh)Instantiate(mesh);
        editmesh = newmesh;
        targetshape = gameObject.AddComponent<MeshFilter>();
        targetshape.sharedMesh = newmesh;
        targetshape.sharedMesh.RecalculateNormals();
        targetshape.sharedMesh.RecalculateBounds();
        targetshape.sharedMesh.GetVertices(targetvert);
        // startshape.sharedMesh.GetVertices(startvert);
        useMesh.GetVertices(startvert);
        
        ShapeDefaultBound = gameObject.GetComponent<Renderer>().bounds;
        ShapeCurrentBound = ShapeDefaultBound;
        setShape();
              
        if (DontDestroy.DontDestroyData.saved == false)
        {
            refreashCollider();
            ObjectCover._objectcover.forceActiveLid();
        }
        _BaseData._BaseDataSave.DecalActiveMode = 2;
      //  DecalControl2._decalControl.loadDefault(currentObject);
    }
    public void recalculateBound() { ShapeCurrentBound = gameObject.GetComponent<Renderer>().bounds; }

    public void SwapMesh(int objIndex)
    {
        if (objIndex == 2 && currentObject != 2)
        {
            Mesh mesh = bowlModel.sharedMesh;
            var meshSimplifier = new UnityMeshSimplifier.MeshSimplifier();
            meshSimplifier.Initialize(mesh);
            meshSimplifier.SimplifyMesh(1f);
            bowlModel.sharedMesh = meshSimplifier.ToMesh();

            mesh = bowlModel.mesh;

            Mesh newmesh = new Mesh();
            newmesh = (Mesh)Instantiate(mesh);

            targetshape.sharedMesh = newmesh;
            targetshape.sharedMesh.RecalculateNormals();
            targetshape.sharedMesh.RecalculateBounds();
            targetshape.sharedMesh.GetVertices(targetvert);
            bowlModel.sharedMesh.GetVertices(startvert);
            currentObject = 2;
        }
        else if (objIndex == 1 && currentObject != 1)
        {

            Mesh mesh = threadModel.sharedMesh;
            var meshSimplifier = new UnityMeshSimplifier.MeshSimplifier();
            meshSimplifier.Initialize(mesh);
            meshSimplifier.SimplifyMesh(1f);
            threadModel.sharedMesh = meshSimplifier.ToMesh();

            mesh = threadModel.mesh;

            Mesh newmesh = new Mesh();
            newmesh = (Mesh)Instantiate(mesh);

            targetshape.sharedMesh = newmesh;
            targetshape.sharedMesh.RecalculateNormals();
            targetshape.sharedMesh.RecalculateBounds();
            targetshape.sharedMesh.GetVertices(targetvert);
            threadModel.sharedMesh.GetVertices(startvert);
            currentObject = 1;
        }
        else if (objIndex == 3 && currentObject != 3)
        {
            Mesh mesh = SanwitchModel.sharedMesh;
            var meshSimplifier = new UnityMeshSimplifier.MeshSimplifier();
            meshSimplifier.Initialize(mesh);
            meshSimplifier.SimplifyMesh(1f);
            SanwitchModel.sharedMesh = meshSimplifier.ToMesh();

            mesh = SanwitchModel.mesh;

            Mesh newmesh = new Mesh();
            newmesh = (Mesh)Instantiate(mesh);

            targetshape.sharedMesh = newmesh;
            targetshape.sharedMesh.RecalculateNormals();
            targetshape.sharedMesh.RecalculateBounds();
            targetshape.sharedMesh.GetVertices(targetvert);
            SanwitchModel.sharedMesh.GetVertices(startvert);
            currentObject = 3;
        }
        ObjectCover._objectcover.setCover(1);
        // Main.MainControl.toggleWireFrame(false);
        ToggleMenuGroup._toggleMenu.forceCloseWireframe();
        ToggleMenuGroup._toggleMenu.forceCloseIso();
      //  DecalSlide._decalSlide.forceSetDecal(0, true);

        setShape();
        refreashCollider();
        ShapeDefaultBound = gameObject.GetComponent<Renderer>().bounds;
        ShapeCurrentBound = ShapeDefaultBound;
    //    DecalControl2._decalControl.loadDefault(currentObject);
       ///////// DecalControl._DCC.callDecal();
    }

    public void setShape()
    {
        fullHeigh = ShapeCurrentBound.size.y;
        fullWidth = ShapeCurrentBound.size.x;
        objSize.setObject(gameObject);
    }
    void Update()
    {
        
    }
    public void Scaler(Vector3 axis, Vector3 scaleTop, Vector3 scaleBot, bool istopValue)
    {
        int count = targetvert.Count;
        objMin = Vector3.zero;
        objMax = Vector3.zero;
        for (int i = 0; i < count; i++)
        {

            sclX = startvert[i].x;
            sclY = startvert[i].y;
            sclZ = startvert[i].z;


            sclX = startvert[i].x - startvert[i].x * axis.x;
            sclZ = startvert[i].z - startvert[i].z * axis.z;
            sclY = startvert[i].y - startvert[i].y * axis.y;

            //top          
            if (startvert[i].x > 0)
            {
                sclX = sclX + ((startvert[i].y + (fullHeigh / 2)) / fullHeigh * scaleTop.x);
                if (sclX < 0) sclX = 0;
            }
            else
            {
                sclX = sclX - ((startvert[i].y + (fullHeigh / 2)) / fullHeigh * scaleTop.x);
                if (sclX > 0) sclX = 0;
            }
            if (startvert[i].z > 0)
            {
                sclZ = sclZ + ((startvert[i].y + (fullHeigh / 2)) / fullHeigh * scaleTop.z);
                if (sclZ < 0) sclZ = 0;
            }
            else
            {
                sclZ = sclZ - ((startvert[i].y + (fullHeigh / 2)) / fullHeigh * scaleTop.z);
                if (sclZ > 0) sclZ = 0;
            }

            //bottom           
            if (startvert[i].x > 0)
            {
                sclX = sclX + (((fullHeigh / 2) - startvert[i].y) / fullHeigh * scaleBot.x);
                if (sclX < 0) sclX = 0;
            }
            else
            {
                sclX = sclX - (((fullHeigh / 2) - startvert[i].y) / fullHeigh * scaleBot.x);
                if (sclX > 0) sclX = 0;
            }
            if (startvert[i].z > 0)
            {

                sclZ = sclZ + (((fullHeigh / 2) - startvert[i].y) / fullHeigh * scaleBot.z);
                if (sclZ < 0) sclZ = 0;
            }
            else
            {
                sclZ = sclZ - (((fullHeigh / 2) - startvert[i].y) / fullHeigh * scaleBot.z);
                if (sclZ > 0) sclZ = 0;
            }
            targetvert[i] = new Vector3(sclX, sclY, sclZ);

        }

        objMax = gameObject.GetComponent<Renderer>().bounds.extents;
        objMin = gameObject.GetComponent<Renderer>().bounds.extents * -1;
        targetshape.sharedMesh.SetVertices(targetvert);
        targetshape.sharedMesh.RecalculateNormals();
        targetshape.sharedMesh.RecalculateBounds();
        objSize.chceckCurrFace();

    }

    public Vector3 renderBoundary()
    {
        return gameObject.GetComponent<Renderer>().bounds.size;
    }

    public void refreashCollider()
    {
        if (gameObject.GetComponents<MeshCollider>() == null)
        {
            gameObject.AddComponent<MeshCollider>();
        }
        else
        {
            foreach (MeshCollider mc in gameObject.GetComponents<MeshCollider>())
                Destroy(mc);
            gameObject.AddComponent<MeshCollider>();
        }
        recalculateBound();
    }
    public void refreshSize()
    {
        objSize.chceckCurrFace();
    }

    public void activeMaterial(bool active)
    {
        if (active)
        {
            Main.MainControl.toggleWireFrame(false);
            objRenderer.material = Main.MainControl.EditingtMat;
            objSize.ActiveSizeDetail(true);
            objSize.chceckCurrFace();
        }
        else
        {
            objRenderer.material = Main.MainControl.defaultMat;
            objSize.ActiveSizeDetail(false);
        }
    }
    public void activeWireframe(bool active)
    {
        objWireframe.enabled = active;
        objRenderer.enabled = !active;
    }
    void InitMesh2(int id) 
    { 
    
    }
}
