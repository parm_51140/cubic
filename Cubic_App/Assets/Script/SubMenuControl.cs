using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubMenuControl : MonoBehaviour
{
    [SerializeField] GameObject ScrollGroup;
    [SerializeField] List<Image> submenuIcongroup;
    [SerializeField] int maxSelect;
    [SerializeField] int MainMenuID;

    public int CurrentSelect;
    public bool setStart;
    float snapPosition, mouseUpPosition, xx;
    bool goSnap = false, snapleft = false;
    Vector3 current, delta, down;
    BoxCollider2D coll;
    float minWidth;
    void Start()
    {
        coll = gameObject.GetComponent<BoxCollider2D>();
        //hor = ScrollGroup.GetComponent<HorizontalLayoutGroup>();
        // int initialSelect = Mathf.CeilToInt((float)maxSelect / 2);
        // if (setStart)
        minWidth = 190 * maxSelect;
        {
            snapPosition = minWidth / 2 - 95;
            //mouseUpPosition = ScrollGroup.transform.localPosition.x-70;
            // CurrentSelect = 1;
            ScrollGroup.transform.localPosition = new Vector3(snapPosition, ScrollGroup.transform.localPosition.y, ScrollGroup.transform.localPosition.z);
        }
        if (MainMenuID == 5) forceSelect();

        if (mouseUpPosition > snapPosition)
        {
            snapleft = false;
        }
        else
        {
            snapleft = true;
        }
        goSnap = true;
    }

    public void parentDown()
    {
        xx = ScrollGroup.transform.localPosition.x;
        coll.enabled = true;
        showToggle();
    }

    public void parentDrag(Vector3 delta)
    {
        float movex = xx;
        movex = Mathf.Clamp(xx - delta.x, -(minWidth / 2), minWidth / 2);
        /*if (maxSelect < 2)
        {
            movex = Mathf.Clamp(xx - delta.x, -50f, 50);
            
        }

        else if (maxSelect == 2)

        {
            movex = Mathf.Clamp(xx - delta.x, -150, 150);

        }
        else
        {
            movex = Mathf.Clamp(xx - delta.x, -250, 250);

        }*/
        ScrollGroup.transform.localPosition = new Vector3(movex, ScrollGroup.transform.localPosition.y, ScrollGroup.transform.localPosition.z);
    }

    public void parentUp() //340 150 -40 -230 -420 //-190//   //  offset  190 //90 ++200
    {

        float minWidthH = minWidth / 2;
        mouseUpPosition = ScrollGroup.transform.localPosition.x;
        CurrentSelect = Mathf.Clamp(((int)(minWidthH - mouseUpPosition) / 190) + 1, 1, maxSelect);
        snapPosition = minWidthH - (95 + (190 * (CurrentSelect - 1)));
        //  print(snapPosition);
        /*
        float addMiddle = maxSelect % 2;
        float startgap = 90;
        float addPos;
        float maxWidth = maxSelect * 150 + (maxSelect - 1) * 40;
        float minPos = -(maxSelect * 75 + (maxSelect / 2) * 40);
        float maxPos = maxSelect * 75 + (maxSelect / 2) * 40;
        mouseUpPosition = ScrollGroup.transform.localPosition.x;

        CurrentSelect =Mathf.Clamp(((int)(maxPos - mouseUpPosition) / 170)+1,1,maxSelect);
        print(maxPos +"...."+ mouseUpPosition + "...." +( maxPos - mouseUpPosition) + "...." + CurrentSelect);*/

        // snapPosition = (maxSelect * 75 + (maxSelect / 2) * 40) - 40-(200*(CurrentSelect-1));
        //snapPosition = snapChoose + gap;

        //snapPosition = (mouseUpPosition + gap);
        //mathf.trunc(maxselect/2)+addmiddle


        /*  if (maxSelect < 2)
        {
            if (ScrollGroup.transform.localPosition.x > 0) { snapPosition = 0; CurrentSelect = 1; }
        }
        else if (maxSelect == 2)
        {
            if (ScrollGroup.transform.localPosition.x >= 0) { snapPosition = 93; CurrentSelect = 1; }
            else { snapPosition = -93; CurrentSelect = 2; }
        }
        else
        {
            if (ScrollGroup.transform.localPosition.x >= 100) { snapPosition = 205; CurrentSelect = 1; }//-10 -188
            else if(ScrollGroup.transform.localPosition.x >= 0) { snapPosition = 0; CurrentSelect = 2; }
            else { snapPosition = -188; CurrentSelect = 3; }
        }*/
        /*  if (ScrollGroup.transform.localPosition.x > 245) { snapPosition = 0; CurrentSelect = 1; }
          else if (ScrollGroup.transform.localPosition.x <= 245 && ScrollGroup.transform.localPosition.x > 55&&maxSelect>1) { snapPosition = -190; CurrentSelect = 2; }
          else if (ScrollGroup.transform.localPosition.x <= 55 && ScrollGroup.transform.localPosition.x > -135 && maxSelect > 2) { snapPosition = -40; CurrentSelect = 3; }
          else if (ScrollGroup.transform.localPosition.x <= -135 && ScrollGroup.transform.localPosition.x > -325 && maxSelect > 3) { snapPosition = -230; CurrentSelect = 4; }
          else if (ScrollGroup.transform.localPosition.x <= -325 && maxSelect > 4) { snapPosition = -420; CurrentSelect = 5; }
          else { snapPosition = 340-(190* (maxSelect - 1)); CurrentSelect = maxSelect; }*/

        if (mouseUpPosition > snapPosition)
        {
            snapleft = false;
        }
        else
        {
            snapleft = true;
        }
        goSnap = true;
        hideToggle();
        coll.enabled = false;
        Main.MainControl.currentSubmenuMenu = CurrentSelect;
    }

    void Update()
    {

        if (goSnap)
        {
            if (snapleft)
            {
                if (mouseUpPosition > snapPosition - 0.5f)
                {
                    goSnap = false;
                    ScrollGroup.transform.localPosition = new Vector3(snapPosition, ScrollGroup.transform.localPosition.y, ScrollGroup.transform.localPosition.z);
                    return;
                }
                mouseUpPosition = mouseUpPosition + (snapPosition - mouseUpPosition) / 10;
                ScrollGroup.transform.localPosition = new Vector3(mouseUpPosition, ScrollGroup.transform.localPosition.y, ScrollGroup.transform.localPosition.z);
            }
            else
            {
                if (mouseUpPosition < snapPosition + 0.5f)
                {
                    goSnap = false;
                    ScrollGroup.transform.localPosition = new Vector3(snapPosition, ScrollGroup.transform.localPosition.y, ScrollGroup.transform.localPosition.z);
                    return;
                }
                mouseUpPosition = mouseUpPosition + (snapPosition - mouseUpPosition) / 10;
                ScrollGroup.transform.localPosition = new Vector3(mouseUpPosition, ScrollGroup.transform.localPosition.y, ScrollGroup.transform.localPosition.z);
            }
        }
    }
    void hideToggle()
    {
        for (int i = 0; i < submenuIcongroup.Count; i++)
        {
            Color colorTemp = submenuIcongroup[i].color;
            if (i != CurrentSelect - 1)
            {
                colorTemp = Color.white;
                colorTemp.a = 0f;
                if (submenuIcongroup[i].transform.childCount > 0)
                {
                    submenuIcongroup[i].transform.GetChild(0).gameObject.SetActive(false);
                    submenuIcongroup[i].transform.GetChild(1).gameObject.SetActive(false);
                    submenuIcongroup[i].transform.GetChild(1).GetComponent<Text>().color = colorTemp;

                }
            }
            else
            {
                colorTemp = new Color(1, .9f, 0, 1f);
                if (submenuIcongroup[i].transform.childCount > 1)
                {
                    submenuIcongroup[i].transform.GetChild(0).gameObject.SetActive(true);
                    submenuIcongroup[i].transform.GetChild(1).gameObject.SetActive(true);
                    submenuIcongroup[i].transform.GetChild(1).GetComponent<Text>().color = Color.black;
                }
               
            }
            submenuIcongroup[i].color = colorTemp;
        }

    }
    public void showToggle()
    {
        for (int i = 0; i < maxSelect; i++)
        {
            Color colorTemp = submenuIcongroup[i].color;
            colorTemp.a = 0.5f;
            submenuIcongroup[i].color = colorTemp;
            if (submenuIcongroup[i].transform.childCount > 1)
            {
                submenuIcongroup[i].transform.GetChild(0).gameObject.SetActive(false);
                submenuIcongroup[i].transform.GetChild(1).gameObject.SetActive(true);
                submenuIcongroup[i].transform.GetChild(1).GetComponent<Text>().color = colorTemp;
            }
        }
    }
    void forceSelect()
    {
        if (DontDestroy.DontDestroyData.saved)
        {
            switch (_BaseData._BaseDataSave.selectedID)
            {
                case 1: snapPosition = 205; CurrentSelect = 1; break;
                case 2: snapPosition = 0; CurrentSelect = 2; break;
                default: snapPosition = -188; CurrentSelect = 3; break;
            }
        }
    }

}
