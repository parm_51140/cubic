using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SelectedEffectWireframe
{
    public class WireframeSkin : MonoBehaviour
    {
        public enum ETriggerMethod { MouseMove = 0, MousePress };
        [Header("Trigger")]
        public ETriggerMethod m_TriggerMethod = ETriggerMethod.MouseMove;
        public bool m_Persistent = false;
        [Header("Basic")]
        public Material m_MatWireframe;
        [Range(0f, 0.5f)] public float m_Width = 0.05f;
        public Color m_Color = Color.green;
        public bool m_Quad = false;
        public bool m_CullBack = true;
        [Header("Glow")]
        public bool m_Glow = false;
        public Color m_GlowColor = Color.white;
        [Range(0f, 1f)] public float m_GlowDist = 0.35f;
        [Range(0f, 1f)] public float m_GlowPower = 0.5f;

        Renderer m_Rd;
        Material[] m_BackupMaterials;
        Texture2D[] m_BackupMainTexs;
        bool m_IsMouseOn = false;

        void Awake()
        {
            m_Rd = GetComponent<Renderer>();

            // cache all original materials
            Material[] mats = m_Rd.materials;
            int len = mats.Length;
            m_BackupMaterials = new Material[len];
            m_BackupMainTexs = new Texture2D[len];
            for (int i = 0; i < len; i++)
            {
                m_BackupMaterials[i] = mats[i];
                m_BackupMainTexs[i] = mats[i].mainTexture as Texture2D;
            }
        }
        void UpdateSelfParameters()
        {
            Material[] mats = m_Rd.materials;
            for (int i = 0; i < mats.Length; i++)
            {
                if (m_Quad)
                    mats[i].EnableKeyword("ENABLE_QUAD");
                else
                    mats[i].DisableKeyword("ENABLE_QUAD");

                if (m_Glow)
                    mats[i].EnableKeyword("ENABLE_GLOW");
                else
                    mats[i].DisableKeyword("ENABLE_GLOW");
                mats[i].SetColor("_GlowColor", m_GlowColor);
                mats[i].SetFloat("_GlowDist", m_GlowDist);
                mats[i].SetFloat("_GlowPower", m_GlowPower);

                mats[i].mainTexture = m_BackupMainTexs[i];
                mats[i].SetFloat("_WireThickness", m_Width);
                mats[i].SetColor("_WireColor", m_Color);
                mats[i].SetInt("_Cull", m_CullBack ? (int)UnityEngine.Rendering.CullMode.Back : (int)UnityEngine.Rendering.CullMode.Off);
            }
        }
        void FxEnable(bool enable)
        {
            if (enable)
            {
                int len = m_Rd.materials.Length;
                Material[] mats = new Material[len];
                for (int i = 0; i < len; i++)
                    mats[i] = m_MatWireframe;
                m_Rd.materials = mats;
            }
            else
            {
                m_Rd.materials = m_BackupMaterials;
            }
        }

        public void ActiveWireframe(bool isOn) 
        {
            FxEnable(isOn);
            UpdateSelfParameters();
        }



    }
}