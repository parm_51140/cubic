using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetShapeViewTexture : MonoBehaviour
{
    [SerializeField] RawImage ri;
    [SerializeField] Camera orbit;

    public void SetTexture()
    {
        var tex = new RenderTexture(Camera.main.pixelWidth, Camera.main.pixelHeight, 16);
        orbit.targetTexture = tex;
        ri.texture = tex;
    }
}
