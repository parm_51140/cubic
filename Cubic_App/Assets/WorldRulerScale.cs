using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldRulerScale : MonoBehaviour
{
    [SerializeField] GameObject scaleLine;
    [SerializeField] TextMesh scaleNum;
    [SerializeField] GameObject back;
    public void SetScale(bool isMajor,float num,bool isvertical) 
    {
        if (!isvertical)
        {
            if (isMajor)
            {
                scaleLine.transform.localScale = new Vector3(.01f, .35f, 1);
                scaleNum.text = num + "";
            }
            else
            {
                scaleLine.transform.localScale = new Vector3(.01f, .2f, 1);
                scaleNum.text = "";
                back.transform.localScale = Vector3.zero;
            }
        }
        else
        {
            if (isMajor)
            {
                scaleLine.transform.localScale = new Vector3( .35f,.01f, 1);
                scaleNum.text = num + "";
                //scaleNum.transform.localPosition = new Vector3(.03f, .18f, -.077f);
            }
            else
            {
                scaleLine.transform.localScale = new Vector3( .2f,.01f, 1);
                scaleNum.text = "";
                back.transform.localScale = Vector3.zero;
            }
        }
    }
}
