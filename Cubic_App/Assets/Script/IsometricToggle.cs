using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IsometricToggle : MonoBehaviour
{

    bool toggleOn;
    float cameraWidth;
    Vector3 BasePos;
    float onPosX;
    float offPosX;
    void Start()
    {
        toggleOn = false;

        BasePos = gameObject.transform.localPosition;
        offPosX = gameObject.transform.localPosition.x;
        onPosX = offPosX - 330;

    }
    private void OnMouseDown()
    {
      //  1920-1080 705/354
      //1080-246 583/226
        toggleOn = !toggleOn;
        if (toggleOn)
        {
          
            gameObject.transform.localPosition = new Vector3(onPosX,BasePos.y,0 );

        }
        else
        {
            gameObject.transform.localPosition = new Vector3(offPosX, BasePos.y, 0);
        }
    }
    public void forceCloseIsoview()
    {

        gameObject.transform.localPosition = new Vector3(offPosX, BasePos.y, 0);
    }
   
}
   


