using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using UnityEngine.UI;
using System.IO;
using System;
using UnityEditor;

public class SavedGallery : MonoBehaviour
{
    public static SavedGallery _saveGallery;
    [SerializeField] GameObject groupObject;
    [SerializeField] GameObject ListParent;
    [SerializeField] GameObject ListObject;
    [SerializeField] SaveGalleryManage ManageBox;
    [SerializeField] List<GameObject> ListObjectList;
    [SerializeField] Sprite[] iconlist;

    List<objectSaveJson> osj;
    List<jsonDataClass> jdc;
    List<string> decalCloud;

    List<objectSaveJson> osjD;
    jsonDataClass jdcD;

    private void Awake()
    {
        _saveGallery = this;
    }

    public IEnumerator LoadSelectedData(int id)
    {
       
       
        _BaseData._BaseDataSave.saveWidth = jdc[id].savewidth;
        _BaseData._BaseDataSave.SaveLength = jdc[id].savelength;
        _BaseData._BaseDataSave.SaveHeight = jdc[id].saveheight;
        _BaseData._BaseDataSave.Saveslope = jdc[id].saveslope;
        _BaseData._BaseDataSave.Savecurve = jdc[id].savecurve;

        decalCloud = JsonConvert.DeserializeObject<List<string>>(osj[id].savesticker);
        for (int i = 0; i < decalCloud.Count; i++)
        {
            DecalCloudProperties dcp = JsonConvert.DeserializeObject<DecalCloudProperties>(decalCloud[i]);
            if (dcp.p_name != "Clear")
            {
                UnityWebRequest request = UnityWebRequestTexture.GetTexture(dcp.p_link);
                yield return request.SendWebRequest();
                if (request.result != UnityWebRequest.Result.Success)
                { CallBackNotice._callBackNotice._noticBark("Load Image Error"); }
                else
                {

                    DecalPropertiesSave.decalPropSave[i].p_texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
                }
            }
            else
            {
                DecalPropertiesSave.decalPropSave[i].p_texture = DecalControl2._decalControl.GetClearTex();
            }
            DecalPropertiesSave.decalPropSave[i].p_Position = str2vec3(dcp.p_Position);
            DecalPropertiesSave.decalPropSave[i].p_Rotation = str2vec3(dcp.p_Rotation);
            DecalPropertiesSave.decalPropSave[i].p_Scale = str2vec3(dcp.p_Scale);
            DecalPropertiesSave.decalPropSave[i].p_offset = dcp.p_offset;
            DecalPropertiesSave.decalPropSave[i].p_TexScale = dcp.p_TexScale;
        }
        MainMenuGroupControl._mainmenugroupcontrol.forceCallSubMenu(3, 1);
      
        _BaseData._BaseDataSave.DecalActiveMode = jdc[id].decalMode;
        DecalSlide._decalSlide.forceSetDecal(jdc[id].decalMode, false);
        DontDestroy.DontDestroyData.saved = true;
        EditableObjectMaster._editmaster.forceReSpawnObject(jdc[id].selectedID);
       
        SaveObject._saveobjBtn.loadObjectID(osj[id].id);

        MainMenuGroupControl._mainmenugroupcontrol.forceCallMenu(3);
        yield return null;
    }
    public IEnumerator LoadSelectedData()
    {
        CallBackNotice._callBackNotice._noticBark("Create Object");
        yield return new WaitForSeconds(.5f);
        _BaseData._BaseDataSave.saveWidth = jdcD.savewidth;
        _BaseData._BaseDataSave.SaveLength = jdcD.savelength;
        _BaseData._BaseDataSave.SaveHeight = jdcD.saveheight;
        _BaseData._BaseDataSave.Saveslope = jdcD.saveslope;
        _BaseData._BaseDataSave.Savecurve = jdcD.savecurve;
       
        decalCloud = JsonConvert.DeserializeObject<List<string>>(osjD[0].savesticker);
        Debug.Log("BBBBBBB 1");
        for (int i = 0; i < decalCloud.Count; i++)
        {
            
            DecalCloudProperties dcp = JsonConvert.DeserializeObject<DecalCloudProperties>(decalCloud[i]);
            if (dcp.p_name != "Clear")
            {
                UnityWebRequest request = UnityWebRequestTexture.GetTexture(dcp.p_link);
                yield return request.SendWebRequest();
                if (request.result != UnityWebRequest.Result.Success)
                { 
                    CallBackNotice._callBackNotice._noticBark("Load Image Error"); 
                }
                else
                {
                    DecalPropertiesSave.decalPropSave[i].p_texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
                }
            }
            else
            {
                DecalPropertiesSave.decalPropSave[i].p_texture = DecalControl2._decalControl.GetClearTex();
            }
            DecalPropertiesSave.decalPropSave[i].p_Position = str2vec3(dcp.p_Position);
            DecalPropertiesSave.decalPropSave[i].p_Rotation = str2vec3(dcp.p_Rotation);
            DecalPropertiesSave.decalPropSave[i].p_Scale = str2vec3(dcp.p_Scale);
            DecalPropertiesSave.decalPropSave[i].p_offset = dcp.p_offset;
            DecalPropertiesSave.decalPropSave[i].p_TexScale = dcp.p_TexScale;
        }
        MainMenuGroupControl._mainmenugroupcontrol.forceCallSubMenu(3, 1);
        MainMenuGroupControl._mainmenugroupcontrol.forceCallMenu(3);
        Debug.Log("BBBBBBB 2");
        DontDestroy.DontDestroyData.saved = true;
        _BaseData._BaseDataSave.DecalActiveMode = jdcD.decalMode;
        Debug.Log("BBBBBBB 3");
        DecalSlide._decalSlide.forceSetDecal(jdcD.decalMode, false);
        Debug.Log("BBBBBBB 4");
        EditableObjectMaster._editmaster.forceReSpawnObject(jdcD.selectedID);
        Debug.Log("BBBBBBB 5");
        SaveObject._saveobjBtn.loadObjectID(osjD[0].id);
      
        showGallery(false);
        yield return null;
    }

    public void showGallery(bool active)
    {
        groupObject.SetActive(active);
        if (active) StartCoroutine(loadGalleryData());
    }
    public void SetActiveManageBox(bool active, string id, int listID)
    {
        ManageBox.gameObject.SetActive(active);
        ManageBox.transform.position = Input.mousePosition;
        ManageBox.selectID = id;
        ManageBox.ListID = listID;
    }

    IEnumerator loadGalleryData()
    {
        if (_BaseData._BaseDataSave.userTag == "")
        {
            CallBackNotice._callBackNotice._noticBark("require Login");
            if (ListObjectList.Count > 0)
            {
                foreach (GameObject go in ListObjectList) { Destroy(go); }
                ListObjectList.Clear();
                jdc.Clear();
            }
            LoadHolder._loadHold.setLoad(false);
            yield break;
        }
        string getreq = _BaseData._BaseDataSave.targetLink + "?action=getDesignData&id=" + _BaseData._BaseDataSave.userTag;
        print("loadGalleryData : " + getreq);
        LoadHolder._loadHold.setLoad(true);
        using (UnityWebRequest www = UnityWebRequest.Get(getreq))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                LoadHolder._loadHold.setLoad(false);
                CallBackNotice._callBackNotice._noticBark("Load Error");
            }
            else
            {
                if (www.downloadHandler.text.IndexOf("<!DOCTYPE html>") == 0)
                {
                  //  print(www.downloadHandler.text.IndexOf("<!DOCTYPE html>") + "XXXXXXX");
                    LoadHolder._loadHold.setLoad(false);
                    CallBackNotice._callBackNotice._noticBark("No Data Save");
                }
                else if (www.downloadHandler.text != null)
                {
                    GUIUtility.systemCopyBuffer = www.downloadHandler.text;
                    osj = JsonConvert.DeserializeObject<List<objectSaveJson>>(www.downloadHandler.text);
                    jdc = new List<jsonDataClass>();
                    createList();
                }
                else
                {
                    LoadHolder._loadHold.setLoad(false);
                    CallBackNotice._callBackNotice._noticBark("No Data Save");
                }
            }
        }
        yield return null;
    }
    private void Update()
    {

    }
    public IEnumerator loadDeeplinkData(string userT, string rowI)
    {
       // if (_BaseData._BaseDataSave.targetLink == "") 
       // {
       //     // yield return StartCoroutine(UserLogin._userLogin.getAPIPath());
       //    // yield return _BaseData._BaseDataSave.targetLink != "";
       // }

        string getreq = _BaseData._BaseDataSave.targetLink + "?action=getShareData&sendersheetid=" + userT + "&designid=" + rowI;
        using (UnityWebRequest www = UnityWebRequest.Get(getreq))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                LoadHolder._loadHold.setLoad(false);
                CallBackNotice._callBackNotice._noticBark("bad request");
            }
            else
            {         
                if (www.downloadHandler.text != null)
                {
                    osjD = JsonConvert.DeserializeObject<List<objectSaveJson>>(www.downloadHandler.text);
                    jdcD = new jsonDataClass();
                    jdcD = JsonConvert.DeserializeObject<jsonDataClass>(osjD[0].savedata);
                    StartCoroutine(LoadSelectedData());
                }
                else
                {
                    LoadHolder._loadHold.setLoad(false);
                    CallBackNotice._callBackNotice._noticBark("Load fail");
                }
            }
        }
        yield return null;
    }
    public IEnumerator loadCadFile(int listID)
    {
        // CallBackNotice._callBackNotice._noticBark("Downloading CAD");
        //  print(osj[listID].cad + " row : " + listID + " Field : " + osj[listID].id);
        if (osj[listID].cad.IndexOf("dropbox") > -1)
        {
            osj[listID].cad = osj[listID].cad.Substring(0, osj[listID].cad.Length - 1);
            osj[listID].cad += "1";
            // System.Diagnostics.Process.Start(osj[listID].cad);


            using (UnityWebRequest www = UnityWebRequest.Get(osj[listID].cad))
            {
                yield return www.SendWebRequest();
                if (www.result != UnityWebRequest.Result.Success)
                {
                    print(www.result);
                    CallBackNotice._callBackNotice._noticBark("URL Error");
                }
                else
                {
                    CallBackNotice._callBackNotice._noticBark("Link Copied");
                    GUIUtility.systemCopyBuffer = osj[listID].cad;
                }
            }

        }
        else
        {

        }
        yield return null;
    }

    void createList()
    {
        if (ListObjectList.Count > 0)
        {
            foreach (GameObject go in ListObjectList) { Destroy(go); }
            ListObjectList.Clear();
            jdc.Clear();
        }

        for (int i = 0; i < osj.Count; i++)
        {
            jsonDataClass jdcT = JsonConvert.DeserializeObject<jsonDataClass>(osj[i].savedata);
            jdc.Add(jdcT);

            ListObjectList.Add(Instantiate(ListObject, ListParent.transform));
            ListObjectList[i].GetComponent<SaveGalleryList>().setData(iconlist[jdc[i].selectedID - 1], osj[i].name, osj[i].id, i);
            if (osj[i].deletedate != "")
            {
                ListObjectList[i].gameObject.SetActive(false);
            }
        }
        LoadHolder._loadHold.setLoad(false);
    }
    public void resetSelectList()
    {
        foreach (GameObject go in ListObjectList)
        { go.GetComponent<SaveGalleryList>().pickOut(); }
        SavedGallery._saveGallery.SetActiveManageBox(false, _BaseData._BaseDataSave.designID, -1);

    }
    Vector3 str2vec3(string st)
    {
        vec3tostr temp = new vec3tostr();
        temp = JsonConvert.DeserializeObject<vec3tostr>(st);
        return new Vector3(temp.x, temp.y, temp.z);
    }

}
