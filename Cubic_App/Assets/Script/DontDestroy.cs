using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DontDestroy : MonoBehaviour
{
    public static DontDestroy DontDestroyData;

    public MeshFilter BaseModelDuplicateMeshFilter;
    public Mesh BaseModelDuplicateMesh;
    public MeshRenderer BaseModelDuplicateRenderer;

    public GameObject blendPrefab;
    public GameObject ShowObj;
    public GameObject ShowObjBase;
    public GameObject ShowObjcantop;
    public GameObject ShowObjcanring;
    public GameObject ShowObjcanbase;
    public GameObject ShowObjLid;


    public GameObject mainSceneGroup;
    public GameObject ARSceneGroup;

    public Vector3[] DecalObjectT;
    public Vector3[] DecalObjectR;
    public Vector3[] DecalObjectS;
    public Material[] DecalObjectM;
    public Scene[] allScene;
    public Material[] material;

    public Texture[] decalUnwrap;
    public Texture[] decalWrap;

    public bool saved;
    public bool savedDecal;
    public bool savedDecaliswrap;
    public ObjectReshape currReshape;
    public dev.cubic.PointTouch currPointTouch;

    public int currentObjectIndex;
    public Vector3 savescaleDimention;
    public Vector3 saveScaleTop, saveScaleBottom;
    public bool lidIsOn;
    public bool DecalIsOn;
    public GameObject tempO;
    public List<GameObject> allObject = new List<GameObject>();
    void Awake()
    {

        saved = false;
        savedDecal = false;
        if (DontDestroyData == null)
        {
            DontDestroyData = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else { Destroy(this.gameObject); }

    }

    public void SaveData(GameObject obj)
    {
        ShowObj = Instantiate(blendPrefab, gameObject.transform);
        //  ModelDuplicateMeshFilter = ShowObj.GetComponent<MeshFilter>();
        // BaseModelDuplicateMeshFilter = obj.GetComponent<MeshFilter>();
        // ModelDuplicateMeshFilter.mesh = BaseModelDuplicateMeshFilter.mesh;

        ShowObj.GetComponent<MeshRenderer>().material = material[0];
        ShowObj.transform.localScale = Vector3.zero;

        lidIsOn = Main.MainControl.lidOn;
        if (ShowObjLid != null) ShowObjLid.SetActive(false);

        currPointTouch.SaveAll();
        currentObjectIndex = currReshape.currObjIndex();
        SceneManager.LoadScene("ARCAM");
    }
    public void SaveData()
    {

        switch (_BaseData._BaseDataSave.selectedID)
        {
            case 1: allObject = EditableObjectMaster._editmaster.currentObject.gameObject.GetComponent<_BoxEdit>().AllObject; break;
            case 2: allObject = EditableObjectMaster._editmaster.currentObject.gameObject.GetComponent<_CupEdit>().AllObject; break;
            case 3: allObject = EditableObjectMaster._editmaster.currentObject.gameObject.GetComponent<_SandWichEdit>().AllObject; break;
            case 4: allObject = EditableObjectMaster._editmaster.currentObject.gameObject.GetComponent<_CanEdit>().AllObject; break;
            case 5: allObject = EditableObjectMaster._editmaster.currentObject.gameObject.GetComponent<_CartonEdit>().AllObject; break;
        }

        for (int i = 0; i < allObject.Count; i++)
        {
            tempO = new GameObject("SaveObject[" + i + "]");
            tempO.AddComponent<MeshFilter>();
            tempO.AddComponent<MeshRenderer>();
            tempO.GetComponent<MeshFilter>().mesh = allObject[i].gameObject.GetComponent<MeshFilter>().mesh;
            tempO.GetComponent<MeshFilter>().mesh.RecalculateNormals();



            if (_BaseData._BaseDataSave.selectedID == 5)
                tempO.GetComponent<MeshRenderer>().material = material[3];
            else if (_BaseData._BaseDataSave.selectedID == 4)
            { 
                tempO.GetComponent<MeshRenderer>().material = material[2];
                if(i==0)
                tempO.GetComponent<MeshRenderer>().material = material[6];
            
            }
            else
                tempO.GetComponent<MeshRenderer>().material = material[0];

            tempO.transform.localScale = Vector3.zero;
            tempO.transform.parent = gameObject.transform;
            tempO.transform.position = allObject[i].gameObject.transform.position;
            allObject[i] = tempO;
            if (i == 0) ShowObj = tempO;
        }

        //  ShowObj = new GameObject("SaveObject");
        //  ShowObj.AddComponent<MeshFilter>();

        //ShowObj = new GameObject("SaveObject");
        //ShowObj.AddComponent<MeshFilter>();
        //ShowObj.GetComponent<MeshFilter>().mesh = EditableObjectMaster._editmaster.currentObjectRenderer.gameObject.GetComponent<MeshFilter>().mesh;
        //ShowObj.AddComponent<MeshRenderer>();
        //ShowObj.GetComponent<MeshRenderer>().material = material[0];
        //ShowObj.transform.localScale = Vector3.zero;
        //ShowObj.transform.parent = gameObject.transform;
        //
        //ShowObjBase = new GameObject("SaveObjectBase");
        //ShowObjBase.AddComponent<MeshFilter>();
        //ShowObjBase.GetComponent<MeshFilter>().mesh = EditableObjectMaster._editmaster.currenBaseRenderer.gameObject.GetComponent<MeshFilter>().mesh;
        //ShowObjBase.AddComponent<MeshRenderer>();
        //ShowObjBase.GetComponent<MeshRenderer>().material = material[0];
        //ShowObjBase.transform.localScale = Vector3.zero;
        //ShowObjBase.transform.parent = gameObject.transform;
        //if (_BaseData._BaseDataSave.selectedID == 4)
        //{
        //    ShowObjcanring = new GameObject("SaveObjectcanring");
        //    ShowObjcanring.AddComponent<MeshFilter>();
        //    ShowObjcanring.GetComponent<MeshFilter>().mesh = EditableObjectMaster._editmaster.objectCanRingbase.gameObject.GetComponent<MeshFilter>().mesh;
        //    ShowObjcanring.AddComponent<MeshRenderer>();
        //    ShowObjcanring.GetComponent<MeshRenderer>().material = material[0];
        //    ShowObjcanring.transform.localScale = Vector3.zero;
        //    ShowObjcanring.transform.parent = gameObject.transform;
        //
        //    ShowObjcantop = new GameObject("SaveObjectcanTop");
        //    ShowObjcantop.AddComponent<MeshFilter>();
        //    ShowObjcantop.GetComponent<MeshFilter>().mesh = EditableObjectMaster._editmaster.objectCanCover.gameObject.GetComponent<MeshFilter>().mesh;
        //    ShowObjcantop.AddComponent<MeshRenderer>();
        //    ShowObjcantop.GetComponent<MeshRenderer>().material = material[0];
        //    ShowObjcantop.transform.localScale = Vector3.zero;
        //    ShowObjcantop.transform.parent = gameObject.transform;
        //
        //    ShowObjcanbase = new GameObject("SaveObjectcanbase");
        //    ShowObjcanbase.AddComponent<MeshFilter>();
        //    ShowObjcanbase.GetComponent<MeshFilter>().mesh = EditableObjectMaster._editmaster.objectCanbase.gameObject.GetComponent<MeshFilter>().mesh;
        //    ShowObjcanbase.AddComponent<MeshRenderer>();
        //    ShowObjcanbase.GetComponent<MeshRenderer>().material = material[0];
        //    ShowObjcanbase.transform.localScale = Vector3.zero;
        //    ShowObjcanbase.transform.parent = gameObject.transform;
        //}





        if (ShowObjLid != null) ShowObjLid.SetActive(false);


        SceneManager.LoadScene("ARCAM");
    }

}
