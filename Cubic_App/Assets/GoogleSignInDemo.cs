﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase;
using Firebase.Auth;
using Google;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Rendering;
using UnityEngine.UI;

public class GoogleSignInDemo : MonoBehaviour
{
    [Serializable]
     class UserData
    {
        public string id;
        public string username;
        public string email;
        public string firstname;
        public string lastname;
    }

    public string json;
    public Text infoText;
    public string webClientId = "398200370985-7bl9ssrlooh2p2vffp4qbf7e05qkds1f.apps.googleusercontent.com";
    public RawImage rawImage;
    private FirebaseAuth auth;
    private GoogleSignInConfiguration configuration;
  //  public Text UsernameTxt, UserEmailTxt;
   // public GameObject LoginScreen, ProfileScreen;



    private void Awake()
    {
        configuration = new GoogleSignInConfiguration { WebClientId = webClientId, RequestEmail = true, RequestIdToken = true };
        CheckFirebaseDependencies();
    }

    private void CheckFirebaseDependencies()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                if (task.Result == DependencyStatus.Available)
                    auth = FirebaseAuth.DefaultInstance;
                else
                    AddToInformation("Could not resolve all Firebase dependencies: " + task.Result.ToString());
            }
            else
            {
                AddToInformation("Dependency check was not completed. Error : " + task.Exception.Message);
            }
        });
    }

    public void SignInWithGoogle() 
    {
        if (PlayerPrefs.HasKey("USERDATA"))
        { Share._shareSummaryDetail.ShareDetail(); }
        else
        OnSignIn();
    }
    public void SignOutFromGoogle() { OnSignOut(); }

    private void OnSignIn()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        GoogleSignIn.Configuration.RequestProfile = true;
        //AddToInformation("Calling SignIn");

        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }

    private void OnSignOut()
    {
        AddToInformation("Calling SignOut");
    //   UsernameTxt.text = null; 
    //   UserEmailTxt.text = null;
       rawImage.texture = null;
        GoogleSignIn.DefaultInstance.SignOut();

     //   LoginScreen.SetActive(true);
     //   ProfileScreen.SetActive(false);
    }

    public void OnDisconnect()
    {
        AddToInformation("Calling Disconnect");
        GoogleSignIn.DefaultInstance.Disconnect();
    }

    internal void OnAuthenticationFinished(Task<GoogleSignInUser> task)
    {
        if (task.IsFaulted)
        {
            IEnumerator<Exception> enumerator = task.Exception.InnerExceptions.GetEnumerator();
            if (enumerator.MoveNext())
            {
                GoogleSignIn.SignInException error = (GoogleSignIn.SignInException)enumerator.Current;
                AddToInformation("Got Error: " + error.Status + " " + error.Message);
            }
            else
            {
                AddToInformation("Got Unexpected Exception?!?" + task.Exception);
            }
        }
        else if (task.IsCanceled)
        {
            AddToInformation("Canceled");
        }
        else
        {
            PlayerPrefs.SetString("USERName", task.Result.DisplayName);
            PlayerPrefs.SetString("GiveName", task.Result.GivenName);
            PlayerPrefs.SetString("FamilyName", task.Result.FamilyName);
            PlayerPrefs.SetString("USERMail", task.Result.Email);
            PlayerPrefs.SetString("USERImgURL", task.Result.ImageUrl.ToString());
            PlayerPrefs.SetString("USERKey", task.Result.Email);
            PlayerPrefs.SetString("USERDATA", "GG.Key");          
            // StartCoroutine(DownloadImage(task.Result.ImageUrl.ToString(), rawImage));

            //  UserData userData = new UserData();
            //  userData.id = CreateMD5(task.Result.Email);
            //  userData.username = task.Result.DisplayName;
            //  userData.email = task.Result.Email;
            //  userData.firstname = task.Result.GivenName;
            //  userData.lastname = task.Result.FamilyName;
            //
            //  json = JsonUtility.ToJson(userData);
            //  StartCoroutine(Upload(json));

            //    LoginScreen.SetActive(false);
            //    ProfileScreen.SetActive(true);

            //AddToInformation("Google ID URL = " + task.Result);
            //AddToInformation("Email = " + task.Result.Email);
            //SignInWithGoogleOnFirebase(task.Result.IdToken);

            UserLogin._userLogin.userdataPost();
            UserLogin._userLogin.ShowUser();
        }
    }

    private void SignInWithGoogleOnFirebase(string idToken)
    {
        Credential credential = GoogleAuthProvider.GetCredential(idToken, null);

        auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
        {
            AggregateException ex = task.Exception;
            if (ex != null)
            {
                if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
                    AddToInformation("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
            }
            else
            {
                AddToInformation("Sign In Successful.");
            }
        });
    }

    public void OnSignInSilently()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        AddToInformation("Calling SignIn Silently");

        GoogleSignIn.DefaultInstance.SignInSilently().ContinueWith(OnAuthenticationFinished);
    }

    public void OnGamesSignIn()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = true;
        GoogleSignIn.Configuration.RequestIdToken = false;

        AddToInformation("Calling Games SignIn");

        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }

    /*public void CreateJson(Task<GoogleSignInUser> task)
    {
        UserData userData = new UserData();
        userData.id = CreateMD5(task.Result.Email);
        userData.username = task.Result.GivenName;
        userData.email = task.Result.Email;
        userData.firstname = task.Result.GivenName;
        userData.lastname = task.Result.FamilyName;

        json = JsonUtility.ToJson(userData);

        StartCoroutine(Upload(json));

    }*/

    IEnumerator Upload(string dataJson)
    {
        var uwr = new UnityWebRequest
            ("https://script.google.com/macros/s/AKfycbwnvi9LJPYK0-kXqZgFtROlv-1GxkPTAbBTGq7TaOdV9vw9h9T1y9Y7Z4UUCaMtin7dhQ/exec?action=addUser", "POST");

        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(dataJson);

        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        //Send the request then wait here until it returns
        yield return uwr.SendWebRequest();

        if (uwr.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("Error While Sending: " + uwr.error);
        }
        else
        {
            Debug.Log("Received: " + uwr.downloadHandler.text);
        }
    
    }


    public static string CreateMD5(string input)
    {
        // Use input string to calculate MD5 hash
        using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
        {
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            return Convert.ToBase64String(hashBytes) + Time.time; // .NET 5 +

            // Convert the byte array to hexadecimal string prior to .NET 5
            // StringBuilder sb = new System.Text.StringBuilder();
            // for (int i = 0; i < hashBytes.Length; i++)
            // {
            //     sb.Append(hashBytes[i].ToString("X2"));
            // }
            // return sb.ToString();
        }
    }


    private void AddToInformation(string str) { infoText.text += "\n" + str; }

    IEnumerator DownloadImage(string MediaUrl, RawImage YourRawImage)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.result != UnityWebRequest.Result.Success)
            Debug.Log(request.error);
        else
            YourRawImage.texture = ((DownloadHandlerTexture)request.downloadHandler).texture;
    }
}