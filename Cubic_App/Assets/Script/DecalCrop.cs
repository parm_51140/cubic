using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DecalCrop : MonoBehaviour
{
    public Texture baseTexture;
    [SerializeField] GameObject BasePanel;
    [SerializeField] RawImage editfield;
    [SerializeField] RawImage Showtex;
    [SerializeField] RawImage Showtex2;

    [Header("CircleCrop")]
    [SerializeField] Image[] circleCropBtn;
    //[SerializeField] Sprite[] circleCropBtnSp;
    [SerializeField] RectTransform circleCropArea;
    [SerializeField] Image circleCropAreaIm;
    [SerializeField] bool circleMode;

    [Header("UI")]
    [SerializeField] RectTransform[] handle;
    //[SerializeField] Text[] handleShow;
    [SerializeField] RectTransform[] VerLine;//0:0 1:3
    [SerializeField] RectTransform[] HorLine;
    public float[] handleVaule;

    float scale;
    float minSide;
    Vector2 scaleDelta;
    Vector2 inputSize;
    Vector2 panelSize;
    Vector2 showSize;
    Vector2 minPoint, maxPoint;

    void Start()
    {
        // _SetEditable(baseTexture);
        // setCircleMode();
    }
    public void _CirclemodeToggle() { circleMode = !circleMode; setCircleMode(); }
    public void setCircleMode()
    {
        SoundPanel._SoundPanel.UIClickPlay();
        handle[4].gameObject.SetActive(circleMode);
        circleCropArea.gameObject.SetActive(circleMode);
        circleCropBtn[0].color = Color.white;
        circleCropBtn[1].color = Color.white;
        if (circleMode)
        {
            minSide = HorLine[0].sizeDelta.x <= VerLine[0].sizeDelta.y ? HorLine[0].sizeDelta.x : VerLine[0].sizeDelta.y;
            circleCropArea.sizeDelta = new Vector2(minSide, minSide);
            circleCropBtn[1].color = Color.white - new Color(0, 0, 0, .5f);
        }
        else 
        {
            circleCropBtn[0].color = Color.white - new Color(0, 0, 0, .5f);
        }
       
    }

    public void _SetEditable(Texture inputTexture)
    {
        baseTexture = inputTexture;
        setCircleMode();
        inputSize = new Vector2(inputTexture.width, inputTexture.height);
        panelSize = new Vector2(880, 800); //raw image size

        //initiateEditableArea
        scale = panelSize.y / inputSize.y;
        editfield.rectTransform.sizeDelta = inputSize * scale;
        editfield.texture = inputTexture;

        if (editfield.rectTransform.sizeDelta.x > panelSize.x)
        {
            scale = panelSize.x / inputSize.x;
            editfield.rectTransform.sizeDelta = inputSize * scale;
            editfield.texture = inputTexture;
        }
        showSize = editfield.rectTransform.sizeDelta;
        scaleDelta = inputSize / showSize;
        minPoint = new Vector2(-showSize.x / 2, -showSize.y / 2);
        maxPoint = new Vector2(showSize.x / 2, showSize.y / 2);

        //initiateSlideBtn
        handle[0].anchoredPosition = minPoint;
        handle[1].anchoredPosition = new Vector2(maxPoint.x, minPoint.y);
        handle[2].anchoredPosition = maxPoint;
        handle[3].anchoredPosition = new Vector2(minPoint.x, maxPoint.y);
        handle[4].anchoredPosition = maxPoint + minPoint;
        //initiateLinePref
        VerLine[0].sizeDelta = new Vector2(4, handle[3].anchoredPosition.y - handle[0].anchoredPosition.y);
        HorLine[0].sizeDelta = new Vector2(handle[1].anchoredPosition.x - handle[0].anchoredPosition.x, 4);
        VerLine[1].sizeDelta = new Vector2(4, handle[3].anchoredPosition.y - handle[0].anchoredPosition.y);
        HorLine[1].sizeDelta = new Vector2(handle[1].anchoredPosition.x - handle[0].anchoredPosition.x, 4);
        //initiateaValue
        handleVaule[0] = handle[0].anchoredPosition.x;  //start x
        handleVaule[1] = handle[1].anchoredPosition.x; //end x
        handleVaule[2] = handle[0].anchoredPosition.y; //start y
        handleVaule[3] = handle[3].anchoredPosition.y; //end y
    }

    Vector2 dragStart;
    Vector2 handlerPos;
    float LimitGap = 90;
    public void _clickHandle(int handleID)
    {
        dragStart = Input.mousePosition;
        handlerPos = handle[handleID].anchoredPosition;
    }

    public void _dragHandle(int handleID)
    {   /// need optimize
        Vector2 delta = dragStart - (Vector2)Input.mousePosition;

        Vector2 clampValue = Vector2.zero;
        if (handleID == 0)
        {
            clampValue.x = Mathf.Clamp(handlerPos.x - delta.x, minPoint.x, handle[1].anchoredPosition.x - LimitGap);
            clampValue.y = Mathf.Clamp(handlerPos.y - delta.y, minPoint.y, handle[3].anchoredPosition.y - LimitGap);
            handle[0].anchoredPosition = new Vector2(clampValue.x, clampValue.y);
            handle[1].anchoredPosition = new Vector2(handle[1].anchoredPosition.x, clampValue.y);
            handle[3].anchoredPosition = new Vector2(clampValue.x, handle[3].anchoredPosition.y);
        }
        else if (handleID == 1)
        {
            clampValue.x = Mathf.Clamp(handlerPos.x - delta.x, handle[0].anchoredPosition.x + LimitGap, maxPoint.x);
            clampValue.y = Mathf.Clamp(handlerPos.y - delta.y, minPoint.y, handle[2].anchoredPosition.y - LimitGap);
            handle[1].anchoredPosition = new Vector2(clampValue.x, clampValue.y);
            handle[0].anchoredPosition = new Vector2(handle[0].anchoredPosition.x, clampValue.y);
            handle[2].anchoredPosition = new Vector2(clampValue.x, handle[2].anchoredPosition.y);
        }
        else if (handleID == 2)
        {
            clampValue.x = Mathf.Clamp(handlerPos.x - delta.x, handle[3].anchoredPosition.x + LimitGap, maxPoint.x);
            clampValue.y = Mathf.Clamp(handlerPos.y - delta.y, handle[1].anchoredPosition.y + LimitGap, maxPoint.y);
            handle[2].anchoredPosition = new Vector2(clampValue.x, clampValue.y);
            handle[1].anchoredPosition = new Vector2(clampValue.x, handle[1].anchoredPosition.y);
            handle[3].anchoredPosition = new Vector2(handle[3].anchoredPosition.x, clampValue.y);
        }
        else if (handleID == 3)
        {
            clampValue.x = Mathf.Clamp(handlerPos.x - delta.x, minPoint.x, handle[2].anchoredPosition.x - LimitGap);
            clampValue.y = Mathf.Clamp(handlerPos.y - delta.y, handle[0].anchoredPosition.y + LimitGap, maxPoint.y);
            handle[3].anchoredPosition = new Vector2(clampValue.x, clampValue.y);
            handle[2].anchoredPosition = new Vector2(handle[2].anchoredPosition.x, clampValue.y);
            handle[0].anchoredPosition = new Vector2(clampValue.x, handle[0].anchoredPosition.y);
        }

        handleVaule[0] = handle[0].anchoredPosition.x;
        handleVaule[1] = handle[1].anchoredPosition.x;
        handleVaule[2] = handle[0].anchoredPosition.y;
        handleVaule[3] = handle[3].anchoredPosition.y;
        handle[4].anchoredPosition = (handle[2].anchoredPosition + handle[0].anchoredPosition) / 2;

        VerLine[0].sizeDelta = new Vector2(4, handle[3].anchoredPosition.y - handle[0].anchoredPosition.y);
        HorLine[0].sizeDelta = new Vector2(handle[1].anchoredPosition.x - handle[0].anchoredPosition.x, 4);
        VerLine[1].sizeDelta = new Vector2(4, handle[3].anchoredPosition.y - handle[0].anchoredPosition.y);
        HorLine[1].sizeDelta = new Vector2(handle[1].anchoredPosition.x - handle[0].anchoredPosition.x, 4);

        if (circleMode)
        {
            minSide = HorLine[0].sizeDelta.x <= VerLine[0].sizeDelta.y ? HorLine[0].sizeDelta.x : VerLine[0].sizeDelta.y;
            circleCropArea.sizeDelta = new Vector2(minSide, minSide);
        }

    }
    public void _letHandle(int handleID)
    {

    }
    public void _closePanel()
    {
        handleVaule[0] = 0;
        handleVaule[1] = showSize.x * scaleDelta.x;
        handleVaule[2] = 0;
        handleVaule[3] = showSize.y * scaleDelta.y;
        cropped();
        SoundPanel._SoundPanel.UIClickPlay();
    }
    public void _saveValue()
    {
        cropped();
        SoundPanel._SoundPanel.UIClickPlay();

    }

    [SerializeField] Texture2D maskC;
    void cropped()
    { 
        SoundPanel._SoundPanel.UIClickPlay();
        float newWidth = (handleVaule[1] - handleVaule[0]) * scaleDelta.x;
        float newHeight = (handleVaule[3] - handleVaule[2]) * scaleDelta.y;

        maskC = new Texture2D((int)newWidth, (int)newHeight);
        if (circleMode)
        {
            float side = Mathf.Min(newWidth, newHeight) / 2;
            int xo = (int)side, yo = (int)side;// center of circle
            float r, rr;
            r = side;
            rr = Mathf.Pow(r, 2);

            //print(newWidth + " ," + newHeight + ", " + r + " ," + xo + ", " + yo);

            for (int i = xo - (int)r; i <= xo + (int)r; i++)
            {
                for (int j = yo - (int)r; j <= yo + (int)r; j++)
                    //if (Mathf.Abs(Mathf.Pow(i - xo, 2) + Mathf.Pow(j - yo, 2) - rr) <= r)
                    if (Mathf.Pow(xo - i, 2) + Mathf.Pow(yo - j, 2) < rr)
                    {
                        maskC.SetPixel(i + (int)((newWidth / 2 - side))
                            , j + (int)((newHeight / 2 - side)), Color.black);

                    }
            }
            //test
            // maskC.Apply();
            // Showtex2.texture = maskC;
            // Showtex2.SetNativeSize();
        }

        Texture2D t2d = new Texture2D((int)newWidth, (int)newHeight);
        t2d.name = "TempTexture";
        Texture2D texterCon = (Texture2D)baseTexture;
        int xx = 0, yy = 0;
        Color tt;
        Vector2Int startLoc = new Vector2Int((int)((handleVaule[0] + maxPoint.x) * scaleDelta.x), (int)((handleVaule[2] + maxPoint.y) * scaleDelta.y));
        Vector2Int endLoc = new Vector2Int((int)((handleVaule[1] + maxPoint.x) * scaleDelta.x), (int)((handleVaule[3] + maxPoint.y) * scaleDelta.y));
        for (int y = startLoc.y; y <= endLoc.y; y++)
        {
            xx = 0;
            for (int x = startLoc.x; x <= endLoc.x; x++)
            {
                xx++;
                tt = texterCon.GetPixel(x, y);

                if (circleMode)
                {
                    if (maskC.GetPixel(xx, yy) == Color.black)
                    {
                        tt = texterCon.GetPixel(x, y);
                        t2d.SetPixel(xx, yy, tt);
                    }
                    else
                    {
                        tt = texterCon.GetPixel(x, y);
                        t2d.SetPixel(xx, yy, Color.clear);
                    }
                }
                else
                    t2d.SetPixel(xx, yy, tt);
            }
            yy++;
        }
        t2d.Apply();
        baseTexture = t2d;

        if (circleMode)
        {

            float rad = Mathf.Min(newWidth, newHeight) ;
            Texture2D t2d2 = new Texture2D((int)rad, (int)rad);
            Vector2Int mid = new Vector2Int((int)(newWidth / 2), (int)(newHeight / 2));
            Color tx;
            for (int y = 0; y < rad; y++)
                for (int x = 0; x < rad; x++)
                {
                    tx = t2d.GetPixel(x+mid.x-(int)(rad/2), y+mid.y - (int)(rad / 2));
                    t2d2.SetPixel(x, y, tx);
                }
            t2d2.Apply();
            baseTexture = t2d2;
            t2d2 = null;
        }



        t2d = null;

        //  Showtex.texture = baseTexture;
        //  Showtex.SetNativeSize();

        // Main.MainControl.decalCon.SetCropValue(baseTexture);
        DecalControl2._decalControl.SetCropValue(baseTexture);
        gameObject.SetActive(false);
    }

}
