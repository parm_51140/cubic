using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARPlatte : MonoBehaviour
{
    [SerializeField] Material[] Mat;




    bool platteToggle = false;
    [SerializeField] GameObject platteGroup;
    public void plattegrouptoggle()
    {
        platteToggle = !platteToggle;
        platteGroup.SetActive(platteToggle);
    }

    public void setMat(int id)
    {
        if (_BaseData._BaseDataSave.selectedID > 3)
        { plattegrouptoggle(); return; }
        MeshRenderer mr = ARShowObject._aRShowObject.boxShow.GetComponent<MeshRenderer>();
        mr.material = Mat[id];
        for (int i = 0; i < ARShowObject._aRShowObject.boxShow.transform.childCount-2; i++)
        {
            MeshRenderer mr2 = ARShowObject._aRShowObject.boxShow.transform.GetChild(i).gameObject.GetComponent<MeshRenderer>();

            if (_BaseData._BaseDataSave.selectedID < 3 && i == ARShowObject._aRShowObject.boxShow.transform.childCount - 3)
            {
                plattegrouptoggle();
                return;
            }
            mr2.material = Mat[id];
            // if (id == 0) { mr2.material = Mat[1]; }
            // else
            // {
            //     if (id == 1)
            //     { Mat[0].color = new Color(.5f, .5f, .5f, 1); }
            //     else if (id == 2)
            //     { Mat[0].color = new Color(.1f, .1f, .1f, 1); }
            //     else if (id == 3)
            //     { Mat[0].color = new Color(.2f, 0, 0, 1); }
            //     else if (id == 4)
            //     { Mat[0].color = new Color(0, .3f, 0, 1); }
            //     mr2.material = Mat[0];
            // }
        }
        plattegrouptoggle();
    }
}
