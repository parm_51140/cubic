using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShapeSummary : MonoBehaviour
{
    public static ShapeSummary _volumscript;
    [SerializeField] GetVol volume;
    [SerializeField] Text summaryText;
    bool StartDelay = true;
    Coroutine delaytex;
    private void Awake()
    {
        _volumscript = this;
    }
    private void Start()
    {
        summaryText.text = "Calculating.";
      //  Invoke("RunVolume", 3);
       
    }

    public void RunVolume()
    {  //Approx 12345 mL.
        
        summaryText.text = "Calculating.";
        volume.gameObject.SetActive(true);
        volume.ReadVolumn();
        StartDelay = true;
        if (delaytex != null) StopCoroutine(delaytex);
        delaytex = StartCoroutine(delayText());

    }

    IEnumerator delayText()
    {
        yield return new WaitForSeconds(5);
        StartDelay = false;
    }

    public void CloseVolume()
    {
        summaryText.text = "";
        volume.gameObject.SetActive(false);
    }
    private void Update()
    {
        if (!StartDelay) { return; }
        else
        { summaryText.text = "Volumn " + (volume.volumeDetect).ToString("F2") + " mL."; }
    }
}
