using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using ZXing;
using ZXing.QrCode;
using UnityEngine.UI;

public class Share : MonoBehaviour
{
    Coroutine aaa = null;
    [SerializeField] GameObject renderUI;
    [SerializeField] RawImage qrBox;
    [SerializeField] RenderTexture renderTex;


    [SerializeField] ObjectSizeScriptShareImage shareImageLineCreate;
    [SerializeField] GameObject camlight;
    [SerializeField] GameObject toplight;
    [SerializeField] GameObject frontlight;
    public static Share _shareSummaryDetail;

    private void Awake()
    {
        _shareSummaryDetail = this;
    }
    public void ShareDetail()
    {
        if (aaa == null)
        {
            EncoderTextToQRCode();
            shareImageLineCreate.CreateLine();
            aaa = StartCoroutine(TakeScreenshotAndShare());
        }

    }
    private Color32[] Encode(string textForEncoding, int width, int height)
    {
        BarcodeWriter writer = new BarcodeWriter
        {
            Format = BarcodeFormat.QR_CODE,
            Options = new QrCodeEncodingOptions
            {
                Height = height,
                Width = width
            }
        };
        return writer.Write(textForEncoding);
    }
    private void EncoderTextToQRCode()
    {
        Texture2D _storeEncoderText = new Texture2D(256, 256);
        string textWrite = "http://cubic.thaipet.co.th?" + _BaseData._BaseDataSave.userTag + "&" + _BaseData._BaseDataSave.designID;
        Color32[] _convertPixalToTexture = Encode(textWrite, 256, 256);// qrBox.height
        _storeEncoderText.SetPixels32(_convertPixalToTexture);
        _storeEncoderText.Apply();

        qrBox.texture = _storeEncoderText;

    }
    public void ShareDetail2()
    {
        shareImageLineCreate.CreateLine();
        renderUI.transform.localScale = Vector3.one;
        sharelight(true);
    }

    //  public void onclick2()
    //  {
    //      StartCoroutine(LinkShare());
    //  }

    void sharelight(bool isshare)
    {
        camlight.SetActive(!isshare);
        toplight.SetActive(isshare);
        frontlight.SetActive(isshare);
    }

    private IEnumerator TakeScreenshotAndShare()
    {
        yield return new WaitForEndOfFrame();
        renderUI.transform.localScale = Vector3.one;

        sharelight(true);
        yield return new WaitForEndOfFrame();
        print(RenderTexture.active);
        // RectTransform rc = renderUI.GetComponent<RectTransform>();
        RenderTexture.active = renderTex;
        Texture2D Ma = new Texture2D(1080, 2320, TextureFormat.RGB24, false);
        Ma.ReadPixels(new Rect(0, 0, 1080, 2320), 0, 0);
        Ma.Apply();
        RenderTexture.active = null;

        string filePath = Path.Combine(Application.temporaryCachePath, "shared img.png");
        File.WriteAllBytes(filePath, Ma.EncodeToPNG());

        //To avoid memory leaks
        Destroy(Ma);

        new NativeShare().AddFile(filePath)
            .SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
            .Share();

        renderUI.transform.localScale = Vector3.zero;
        sharelight(false);
        aaa = null;



        // Share on WhatsApp only, if installed (Android only)
        //if( NativeShare.TargetExists( "com.whatsapp" ) )
        //	new NativeShare().AddFile( filePath ).AddTarget( "com.whatsapp" ).Share();	// Share on WhatsApp only, if installed (Android only)
        //if( NativeShare.TargetExists( "com.whatsapp" ) )
        //	new NativeShare().AddFile( filePath ).AddTarget( "com.whatsapp" ).Share();	// Share on WhatsApp only, if installed (Android only)
        //if( NativeShare.TargetExists( "com.whatsapp" ) )
        //	new NativeShare().AddFile( filePath ).AddTarget( "com.whatsapp" ).Share();
        yield return null;
    }

    private IEnumerator LinkShare()
    {
        yield return new WaitForEndOfFrame();

        new NativeShare().SetSubject("Test Share").SetText("Hello worlds!").SetUrl("https://github.com/yasirkula/UnityNativeShare")
            .SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
            .Share();

    }
}
