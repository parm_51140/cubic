using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class DecalControl : MonoBehaviour
{
    [SerializeField] GameObject[] decal;//frblt
    public Renderer editObj;
    [SerializeField] bool isAct;
    Vector3 startmovePos;
    float startmoveRot;
    GameObject CurrentActiveDecal;
    Bounds boundary;

    [SerializeField] Material[] LabelMat;

    [SerializeField] bool isWrap;
    public Vector3[] decalSavePos;
    public float[] decalSaveOffset = new float[4];
    public float[] decalCropValue = new float[4];

    //  [SerializeField] GameObject decalOptionBtn;
    //  [SerializeField] GameObject OptionSelectGroup;
    [SerializeField] GameObject ImportGroup;
    [SerializeField] GameObject ImportBtn;
    [SerializeField] GameObject reOpenSlideBtn;
    [SerializeField] Text ImportBtntext;
    [SerializeField] Sprite[] DecalIconSprite;
    [SerializeField] Image decalModeCurrent;

    [SerializeField] GameObject DecalDiscCanvas;
    [SerializeField] GameObject DecalDiscSlide;

    [SerializeField] Texture[] InputTextureWrap = new Texture[2];
    [SerializeField] Texture[] InputTextureUnWrap = new Texture[5];

    [SerializeField] Texture[] activeMaterial = new Texture[5];
    [SerializeField] Texture[] unActiveMaterial = new Texture[5];
    [SerializeField] Sprite[] modeIcon;
    [SerializeField] DecalCrop decalCrop;

    bool onshow;
    bool onEditScene;
    bool editable;
    int faceId;
    Vector3[] baseRotate = new Vector3[5];
    float roundObj;
    public int decalMode;
    static bool FirstStart = false;
    bool imported = false;
    [Header("defaultTexture")]
    [SerializeField] Texture[] DefaultTextureBox;
    [SerializeField] Texture[] DefaultTextureCup;
    [SerializeField] Texture[] DefaultTextureSandW;
    [SerializeField] Texture[] DefaultTextureCan;
    [SerializeField] Texture[] DefaultTextureCarton;

    public static DecalControl _DCC;

    private void Awake()
    {
        _DCC = this;
    }
    private void Start()
    {
        if (!FirstStart)
        { StartCoroutine(defaultShow()); FirstStart = true; }
        startmoveRot = 0;
        onshow = false;
        startRotateValue = decal[0].transform.localEulerAngles;

        if (!DontDestroy.DontDestroyData.savedDecal)
        {
            baseRotate[0] = decal[0].transform.localEulerAngles;
            baseRotate[1] = decal[1].transform.localEulerAngles;
            baseRotate[2] = decal[2].transform.localEulerAngles;
            baseRotate[3] = decal[3].transform.localEulerAngles;
            baseRotate[4] = decal[4].transform.localEulerAngles;

            DontDestroy.DontDestroyData.DecalObjectR[0] = baseRotate[0];
            DontDestroy.DontDestroyData.DecalObjectR[1] = baseRotate[1];
            DontDestroy.DontDestroyData.DecalObjectR[2] = baseRotate[2];
            DontDestroy.DontDestroyData.DecalObjectR[3] = baseRotate[3];
            DontDestroy.DontDestroyData.DecalObjectR[4] = baseRotate[4];
        }
        else
        {
            onshow = true;
            isWrap = DontDestroy.DontDestroyData.savedDecaliswrap;

            if (isWrap) { decalModeCurrent.sprite = DecalIconSprite[1]; }
            else { decalModeCurrent.sprite = DecalIconSprite[2]; }

            baseRotate[0] = DontDestroy.DontDestroyData.DecalObjectR[0];
            baseRotate[1] = DontDestroy.DontDestroyData.DecalObjectR[1];
            baseRotate[2] = DontDestroy.DontDestroyData.DecalObjectR[2];
            baseRotate[3] = DontDestroy.DontDestroyData.DecalObjectR[3];
            baseRotate[4] = DontDestroy.DontDestroyData.DecalObjectR[4];

            decal[0].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[0];
            decal[1].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[1];
            decal[2].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[2];
            decal[3].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[3];
            decal[4].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[4];


            InputTextureWrap[0] = DontDestroy.DontDestroyData.decalWrap[0];
            InputTextureWrap[1] = DontDestroy.DontDestroyData.decalWrap[1];

            InputTextureUnWrap[0] = DontDestroy.DontDestroyData.decalUnwrap[0];
            InputTextureUnWrap[1] = DontDestroy.DontDestroyData.decalUnwrap[1];
            InputTextureUnWrap[2] = DontDestroy.DontDestroyData.decalUnwrap[2];
            InputTextureUnWrap[3] = DontDestroy.DontDestroyData.decalUnwrap[3];
            InputTextureUnWrap[4] = DontDestroy.DontDestroyData.decalUnwrap[4];
            InitiateActiveMaterial();
        }
    }

    public void callDecal() { StartCoroutine(defaultShow()); ; }
    public void qHide(bool isWire)
    {
        foreach (GameObject aa in decal)
        {
            aa.SetActive(!isWire);
        }
    }

    IEnumerator defaultShow()
    {
        yield return new WaitForSeconds(1f);

        ObjectCover._objectcover.forceActiveLid();
        int obIndex = ObjectReshape._reShapeIns.currObjIndex();
        if (obIndex == 1)
        {
            for (int i = 0; i < InputTextureUnWrap.Length; i++)
            {
                InputTextureUnWrap[i] = DefaultTextureBox[i];
            }
            //DecalSlide._decalSlide.forceSetDecal(2, false);
            onshow = true;
            isWrap = false;
            ImportBtntext.text = "Sticker";
            decalModeCurrent.sprite = DecalIconSprite[2];
        }
        else if (obIndex == 2)
        {
            for (int i = 0; i < InputTextureUnWrap.Length; i++)
            {
                InputTextureUnWrap[i] = DefaultTextureCup[i];
            }
            DecalSlide._decalSlide.forceSetDecal(2, false);
            onshow = true;
            isWrap = false;
            ImportBtntext.text = "Sticker";
            decalModeCurrent.sprite = DecalIconSprite[2];
        }
        createActiveMaterial2();
        activeDecal(onshow);
        yield return null;
    }

    public void saveDecal()
    {
        if (!onshow) return;
        DontDestroy.DontDestroyData.savedDecal = true;
        DontDestroy.DontDestroyData.savedDecaliswrap = isWrap;
        DontDestroy.DontDestroyData.DecalObjectT[0] = decal[0].transform.localPosition;
        DontDestroy.DontDestroyData.DecalObjectT[1] = decal[1].transform.localPosition;
        DontDestroy.DontDestroyData.DecalObjectT[2] = decal[2].transform.localPosition;
        DontDestroy.DontDestroyData.DecalObjectT[3] = decal[3].transform.localPosition;
        DontDestroy.DontDestroyData.DecalObjectT[4] = decal[4].transform.localPosition;

        DontDestroy.DontDestroyData.DecalObjectR[0] = decal[0].transform.eulerAngles;
        DontDestroy.DontDestroyData.DecalObjectR[1] = decal[1].transform.eulerAngles;
        DontDestroy.DontDestroyData.DecalObjectR[2] = decal[2].transform.eulerAngles;
        DontDestroy.DontDestroyData.DecalObjectR[3] = decal[3].transform.eulerAngles;
        DontDestroy.DontDestroyData.DecalObjectR[4] = decal[4].transform.eulerAngles;

        DontDestroy.DontDestroyData.DecalObjectS[0] = decal[0].transform.localScale;
        DontDestroy.DontDestroyData.DecalObjectS[1] = decal[1].transform.localScale;
        DontDestroy.DontDestroyData.DecalObjectS[2] = decal[2].transform.localScale;
        DontDestroy.DontDestroyData.DecalObjectS[3] = decal[3].transform.localScale;
        DontDestroy.DontDestroyData.DecalObjectS[4] = decal[4].transform.localScale;

        DontDestroy.DontDestroyData.DecalObjectM[0] = LabelMat[0];
        DontDestroy.DontDestroyData.DecalObjectM[1] = LabelMat[1];
        DontDestroy.DontDestroyData.DecalObjectM[2] = LabelMat[2];
        DontDestroy.DontDestroyData.DecalObjectM[3] = LabelMat[3];
        DontDestroy.DontDestroyData.DecalObjectM[4] = LabelMat[4];

        DontDestroy.DontDestroyData.decalWrap[0] = InputTextureWrap[0];
        DontDestroy.DontDestroyData.decalWrap[1] = InputTextureWrap[1];

        DontDestroy.DontDestroyData.decalUnwrap[0] = InputTextureUnWrap[0];
        DontDestroy.DontDestroyData.decalUnwrap[1] = InputTextureUnWrap[1];
        DontDestroy.DontDestroyData.decalUnwrap[2] = InputTextureUnWrap[2];
        DontDestroy.DontDestroyData.decalUnwrap[3] = InputTextureUnWrap[3];
        DontDestroy.DontDestroyData.decalUnwrap[4] = InputTextureUnWrap[4];

    }

    void prepareDecal()// on changeTexture / changeMode
    {
        if (isWrap)
        {
            decalSaveOffset[0] = 0;//25  50 75          
            decalSaveOffset[1] = boundary.size.x / roundObj;
            decalSaveOffset[2] = (boundary.size.x / roundObj) + (boundary.size.z / roundObj);
            decalSaveOffset[3] = (boundary.size.x / roundObj) * 2 + (boundary.size.z / roundObj);

            decal[0].transform.localEulerAngles = baseRotate[0];
            decal[1].transform.localEulerAngles = baseRotate[1];
            decal[2].transform.localEulerAngles = baseRotate[2];
            decal[3].transform.localEulerAngles = baseRotate[3];
            decal[4].transform.localEulerAngles = baseRotate[4];

            LabelMat[0].SetTexture("_MainTex", InputTextureWrap[0]);
            LabelMat[3].SetTexture("_MainTex", InputTextureWrap[0]);
            LabelMat[2].SetTexture("_MainTex", InputTextureWrap[0]);
            LabelMat[1].SetTexture("_MainTex", InputTextureWrap[0]);
            LabelMat[4].SetTexture("_MainTex", InputTextureWrap[1]);

            LabelMat[0].SetTextureOffset("_MainTex", new Vector2(decalSaveOffset[0], 0));
            LabelMat[3].SetTextureOffset("_MainTex", new Vector2(decalSaveOffset[3], 0));
            LabelMat[2].SetTextureOffset("_MainTex", new Vector2(decalSaveOffset[2], 0));
            LabelMat[1].SetTextureOffset("_MainTex", new Vector2(decalSaveOffset[1], 0));

            LabelMat[0].SetTextureScale("_MainTex", new Vector2(decalSaveOffset[1], 1));
            LabelMat[3].SetTextureScale("_MainTex", new Vector2((boundary.size.z / roundObj), 1));
            LabelMat[2].SetTextureScale("_MainTex", new Vector2(decalSaveOffset[1], 1));
            LabelMat[1].SetTextureScale("_MainTex", new Vector2((boundary.size.z / roundObj), 1));

            decal[0].transform.localScale = new Vector3(boundary.extents.x * 2, 1, 1);
            decal[1].transform.localScale = new Vector3(boundary.extents.z * 2, 1, 1);
            decal[2].transform.localScale = decal[0].transform.localScale;
            decal[3].transform.localScale = decal[2].transform.localScale;
            decal[4].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[4];
        }
        else
        {
            decal[0].transform.localEulerAngles = DontDestroy.DontDestroyData.DecalObjectR[0];
            decal[1].transform.localEulerAngles = DontDestroy.DontDestroyData.DecalObjectR[1];
            decal[2].transform.localEulerAngles = DontDestroy.DontDestroyData.DecalObjectR[2];
            decal[3].transform.localEulerAngles = DontDestroy.DontDestroyData.DecalObjectR[3];
            decal[4].transform.localEulerAngles = DontDestroy.DontDestroyData.DecalObjectR[4];

            LabelMat[0].SetTexture("_MainTex", InputTextureUnWrap[0]);
            LabelMat[3].SetTexture("_MainTex", InputTextureUnWrap[3]);
            LabelMat[2].SetTexture("_MainTex", InputTextureUnWrap[2]);
            LabelMat[1].SetTexture("_MainTex", InputTextureUnWrap[1]);
            LabelMat[4].SetTexture("_MainTex", InputTextureUnWrap[4]);

            LabelMat[0].SetTextureOffset("_MainTex", new Vector2(0, 0));
            LabelMat[1].SetTextureOffset("_MainTex", new Vector2(0, 0));
            LabelMat[2].SetTextureOffset("_MainTex", new Vector2(0, 0));
            LabelMat[3].SetTextureOffset("_MainTex", new Vector2(0, 0));

            LabelMat[0].SetTextureScale("_MainTex", new Vector2(1, 1));
            LabelMat[3].SetTextureScale("_MainTex", new Vector2(1, 1));
            LabelMat[2].SetTextureScale("_MainTex", new Vector2(1, 1));
            LabelMat[1].SetTextureScale("_MainTex", new Vector2(1, 1));

            decal[0].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[0];
            decal[1].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[1];
            decal[2].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[2];
            decal[3].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[3];
            decal[4].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[4];
        }

    }
    void InitiateActiveMaterial()
    {
        int max = 5;
        if (isWrap)
        {
            unActiveMaterial[1] = InputTextureWrap[1];//top            
            unActiveMaterial[0] = InputTextureWrap[0];
            max = 2;
        }
        else
        {
            unActiveMaterial[0] = InputTextureUnWrap[0];
            unActiveMaterial[1] = InputTextureUnWrap[1];
            unActiveMaterial[2] = InputTextureUnWrap[2];
            unActiveMaterial[3] = InputTextureUnWrap[3];
            unActiveMaterial[4] = InputTextureUnWrap[4];

        }
        for (int i = 0; i < max; i++)
        {
            Texture2D t2d = new Texture2D(unActiveMaterial[i].width, unActiveMaterial[i].height);
            t2d.name = "TempTexture";
            Texture2D texterCon = (Texture2D)unActiveMaterial[i];
            t2d.SetPixels(texterCon.GetPixels());
            float lineW = t2d.width * .05f;
            float lineH = t2d.height * .05f;
            for (int y = 0; y < t2d.height; y++)
            {
                for (int x = 0; x < t2d.width; x++)
                {
                    Color tt = t2d.GetPixel(x, y);
                    if (y < lineH || y > t2d.height - lineH)
                    { tt = Color.green; }
                    if (!isWrap && (x < lineW || x > t2d.width - lineW))
                    {
                        tt = Color.green;
                    }
                    t2d.SetPixel(x, y, tt * 1.5f);
                }
            }
            t2d.Apply();
            activeMaterial[i] = t2d;
            t2d = null;
        }
        foreach (GameObject gg in decal)
        {
            gg.SetActive(onshow);
        }
        prepareDecal();

    }

    private void createActiveMaterial()//on import material
    {
        int max = 5;
        if (isWrap)
        {
            unActiveMaterial[1] = InputTextureWrap[1];//top           
            DontDestroy.DontDestroyData.DecalObjectS[4] = new Vector3((float)InputTextureUnWrap[1].width / (float)InputTextureUnWrap[1].height, 1, 1);
            unActiveMaterial[0] = InputTextureWrap[0];
            max = 2;
        }
        else
        {
            boundary = editObj.bounds;
            unActiveMaterial[0] = InputTextureUnWrap[0];
            unActiveMaterial[1] = InputTextureUnWrap[1];
            unActiveMaterial[2] = InputTextureUnWrap[2];
            unActiveMaterial[3] = InputTextureUnWrap[3];
            unActiveMaterial[4] = InputTextureUnWrap[4];

            DontDestroy.DontDestroyData.DecalObjectS[0] = new Vector3((float)InputTextureUnWrap[0].width / (float)InputTextureUnWrap[0].height, 1, 1);
            DontDestroy.DontDestroyData.DecalObjectS[1] = new Vector3((float)InputTextureUnWrap[1].width / (float)InputTextureUnWrap[1].height, 1, 1);
            DontDestroy.DontDestroyData.DecalObjectS[2] = new Vector3((float)InputTextureUnWrap[2].width / (float)InputTextureUnWrap[2].height, 1, 1);
            DontDestroy.DontDestroyData.DecalObjectS[3] = new Vector3((float)InputTextureUnWrap[3].width / (float)InputTextureUnWrap[3].height, 1, 1);
            DontDestroy.DontDestroyData.DecalObjectS[4] = new Vector3((float)InputTextureUnWrap[4].width / (float)InputTextureUnWrap[4].height, 1, 1);

        }
        for (int i = 0; i < max; i++)
        {
            Texture2D t2d = new Texture2D(unActiveMaterial[i].width, unActiveMaterial[i].height);
            t2d.name = "TempTexture";
            Texture2D texterCon = (Texture2D)unActiveMaterial[i];
            t2d.SetPixels(texterCon.GetPixels());
            float lineW = t2d.width * .05f;
            float lineH = t2d.height * .05f;
            for (int y = 0; y < t2d.height; y++)
            {
                for (int x = 0; x < t2d.width; x++)
                {
                    Color tt = t2d.GetPixel(x, y);
                    if (y < lineH || y > t2d.height - lineH)
                    { tt = Color.green; }
                    if (!isWrap && (x < lineW || x > t2d.width - lineW))
                    {
                        tt = Color.green;
                    }
                    t2d.SetPixel(x, y, tt * 1.5f);
                }
            }
            t2d.Apply();
            activeMaterial[i] = t2d;
            t2d = null;
        }
        prepareDecal();
    }
    private void createActiveMaterial2()
    {
        int max = 5;
        if (isWrap)
        {
            unActiveMaterial[1] = InputTextureWrap[1];//top           
            DontDestroy.DontDestroyData.DecalObjectS[4] = new Vector3((float)InputTextureUnWrap[1].width / (float)InputTextureUnWrap[1].height, 1, 1);
            unActiveMaterial[0] = InputTextureWrap[0];
            max = 2;
        }
        else
        {
            boundary = editObj.bounds;
            unActiveMaterial[0] = InputTextureUnWrap[0];
            unActiveMaterial[1] = InputTextureUnWrap[1];
            unActiveMaterial[2] = InputTextureUnWrap[2];
            unActiveMaterial[3] = InputTextureUnWrap[3];
            unActiveMaterial[4] = InputTextureUnWrap[4];


            DontDestroy.DontDestroyData.DecalObjectS[0] = new Vector3((float)InputTextureUnWrap[4].width / (float)InputTextureUnWrap[4].height * boundary.size.z, 1, boundary.size.y);
          //  DontDestroy.DontDestroyData.DecalObjectS[0] = new Vector3((float)InputTextureUnWrap[0].width / (float)InputTextureUnWrap[0].height * boundary.size.y, 1, boundary.size.y);
            DontDestroy.DontDestroyData.DecalObjectS[1] = new Vector3((float)InputTextureUnWrap[1].width / (float)InputTextureUnWrap[1].height, 1, 1);
            DontDestroy.DontDestroyData.DecalObjectS[2] = new Vector3((float)InputTextureUnWrap[4].width / (float)InputTextureUnWrap[4].height * boundary.size.z, 1, boundary.size.y);
            DontDestroy.DontDestroyData.DecalObjectS[3] = new Vector3((float)InputTextureUnWrap[3].width / (float)InputTextureUnWrap[3].height, 1, 1);
            DontDestroy.DontDestroyData.DecalObjectS[4] = new Vector3((float)InputTextureUnWrap[4].width / (float)InputTextureUnWrap[4].height * boundary.size.z, 1, boundary.size.z - .1f);
        }
        for (int i = 0; i < max; i++)
        {
            Texture2D t2d = new Texture2D(unActiveMaterial[i].width, unActiveMaterial[i].height);
            t2d.name = "TempTexture";
            Texture2D texterCon = (Texture2D)unActiveMaterial[i];
            t2d.SetPixels(texterCon.GetPixels());
            float lineW = t2d.width * .05f;
            float lineH = t2d.height * .05f;
            for (int y = 0; y < t2d.height; y++)
            {
                for (int x = 0; x < t2d.width; x++)
                {
                    Color tt = t2d.GetPixel(x, y);
                    if (y < lineH || y > t2d.height - lineH)
                    { tt = Color.green; }
                    if (!isWrap && (x < lineW || x > t2d.width - lineW))
                    {
                        tt = Color.green;
                    }
                    t2d.SetPixel(x, y, tt * 1.5f);
                }
            }
            t2d.Apply();
            activeMaterial[i] = t2d;
            t2d = null;
        }
        prepareDecal();
    }

    public void activeDecal(bool x)// start decal on edit mode check & select editmode
    {
        if (!onshow)
        {
            foreach (GameObject gg in decal)
            {
                gg.SetActive(false);
            }
            return;
        }

        boundary = editObj.bounds;
        foreach (GameObject gg in decal)
        {
            gg.SetActive(x);
        }
        if (isWrap)
        {
            decal[0].transform.localPosition = new Vector3(0, 0, -(boundary.extents.z + 1f));
            decal[1].transform.localPosition = new Vector3(boundary.extents.x + 1f, 0, 0);
            decal[2].transform.localPosition = new Vector3(0, 0, boundary.extents.z + 1f);
            decal[3].transform.localPosition = new Vector3(-(boundary.extents.x + 1f), 0, 0);
            decal[4].transform.localPosition = new Vector3(0, boundary.extents.y + 1f, 0);
        }
        else
        {
            if (DontDestroy.DontDestroyData.savedDecal)
            {
                decalSavePos[0] = DontDestroy.DontDestroyData.DecalObjectT[0] - new Vector3(0, 0, -(boundary.extents.z + 1f));
                decalSavePos[1] = DontDestroy.DontDestroyData.DecalObjectT[1] - new Vector3(boundary.extents.x + 1f, 0, 0);
                decalSavePos[2] = DontDestroy.DontDestroyData.DecalObjectT[2] - new Vector3(0, 0, boundary.extents.z + 1f);
                decalSavePos[3] = DontDestroy.DontDestroyData.DecalObjectT[3] - new Vector3(-(boundary.extents.x + 1f), 0, 0);
                decalSavePos[4] = DontDestroy.DontDestroyData.DecalObjectT[4] - new Vector3(0, boundary.extents.y + 1f, 0);
                createActiveMaterial();
            }
            decal[0].transform.localPosition = decalSavePos[0] + new Vector3(0, 0, -(boundary.extents.z + 1f));
            decal[1].transform.localPosition = decalSavePos[1] + new Vector3(boundary.extents.x + 1f, 0, 0);
            decal[2].transform.localPosition = decalSavePos[2] + new Vector3(0, 0, boundary.extents.z + 1f);
            decal[3].transform.localPosition = decalSavePos[3] + new Vector3(-(boundary.extents.x + 1f), 0, 0);
            decal[4].transform.localPosition = decalSavePos[4] + new Vector3(0, boundary.extents.y + 1f, 0);
        }
        roundObj = boundary.size.x * 2 + boundary.size.z * 2;
        prepareDecal();
    }

    public void ActiveEditmode(bool isActive)
    {
        if (!onshow || Main.MainControl.WaitEditRotation) return;
        CheckCurrDecal();
        if (isActive)
        {
            if (isWrap)
            {
                if (faceId < 4)
                {
                    LabelMat[0].SetTexture("_MainTex", activeMaterial[0]);
                    LabelMat[1].SetTexture("_MainTex", activeMaterial[0]);
                    LabelMat[2].SetTexture("_MainTex", activeMaterial[0]);
                    LabelMat[3].SetTexture("_MainTex", activeMaterial[0]);
                }
                else
                {
                    LabelMat[4].SetTexture("_MainTex", activeMaterial[1]);
                    if (_BaseData._BaseDataSave.openLid)
                        handlerGroup.transform.localScale = Vector3.one;
                }
            }
            else
            {
                if (faceId < 5)
                    LabelMat[faceId].SetTexture("_MainTex", activeMaterial[faceId]);
                if (faceId == 4 && !_BaseData._BaseDataSave.openLid) { }
                else
                    handlerGroup.transform.localScale = Vector3.one;
            }
            editable = true;
        }
        else
        {
            handlerGroup.transform.localScale = Vector3.zero;
            if (isWrap)
            {
                decalSaveOffset[0] = LabelMat[0].GetTextureOffset("_MainTex").x;
                decalSaveOffset[1] = LabelMat[1].GetTextureOffset("_MainTex").x;
                decalSaveOffset[2] = LabelMat[2].GetTextureOffset("_MainTex").x;
                decalSaveOffset[3] = LabelMat[3].GetTextureOffset("_MainTex").x;

                LabelMat[0].SetTexture("_MainTex", unActiveMaterial[0]);
                LabelMat[1].SetTexture("_MainTex", unActiveMaterial[0]);
                LabelMat[2].SetTexture("_MainTex", unActiveMaterial[0]);
                LabelMat[3].SetTexture("_MainTex", unActiveMaterial[0]);
                LabelMat[4].SetTexture("_MainTex", unActiveMaterial[1]);
            }
            else
            {
                LabelMat[0].SetTexture("_MainTex", InputTextureUnWrap[0]);
                LabelMat[1].SetTexture("_MainTex", InputTextureUnWrap[1]);
                LabelMat[2].SetTexture("_MainTex", InputTextureUnWrap[2]);
                LabelMat[3].SetTexture("_MainTex", InputTextureUnWrap[3]);
                LabelMat[4].SetTexture("_MainTex", InputTextureUnWrap[4]);

            }
            editable = false;
        }

        //if (!isWrap && Main.MainControl.currVertivalFace != Main.VertivalFace.Top && editable)
        //{ handlerGroup.transform.localScale = Vector3.one; }
        //else if (!isWrap && Main.MainControl.currVertivalFace == Main.VertivalFace.Top && Main.MainControl.lidOn && editable)
        //{ handlerGroup.transform.localScale = Vector3.one; }
        //else if (isWrap && Main.MainControl.currVertivalFace == Main.VertivalFace.Top && Main.MainControl.lidOn && editable)
        //{ handlerGroup.transform.localScale = Vector3.one; }
        //else { handlerGroup.transform.localScale = Vector3.zero; }

        Main.MainControl.IsEditingLabel = isActive;
    }

    void CheckCurrDecal()
    {
        if (Main.MainControl.currVertivalFace == Main.VertivalFace.Side)
        {
            if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front)
            {
                CurrentActiveDecal = decal[0].gameObject;
                faceId = 0;
            }
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Right)//
            {
                CurrentActiveDecal = decal[1].gameObject; faceId = 1;
            }
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back)
            {
                CurrentActiveDecal = decal[2].gameObject; faceId = 2;
            }
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Left)//
            {
                CurrentActiveDecal = decal[3].gameObject; faceId = 3;
            }
        }
        else if (Main.MainControl.currVertivalFace == Main.VertivalFace.Top)
        { CurrentActiveDecal = decal[4].gameObject; faceId = 4; }
        else if (Main.MainControl.currVertivalFace == Main.VertivalFace.Bottom)
        { CurrentActiveDecal = null; faceId = 5; }

    }
    public void startmoveDecal()
    {
        CheckCurrDecal();
        startmovePos = CurrentActiveDecal.transform.localPosition;
    }
    public void moveDecalSingle(Vector3 delta)
    {
        if (!editable) return;
        if (!isWrap)
        {
            if (Main.MainControl.currVertivalFace == Main.VertivalFace.Side)
            {
                float moveX, moveY;

                if (faceId == 0)
                {
                    Vector3 moveDelta = startmovePos - delta / 200;
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.y, boundary.extents.y);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.x, boundary.extents.x);
                    CurrentActiveDecal.transform.localPosition = new Vector3(moveX, moveY, startmovePos.z);
                }
                else if (faceId == 1)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x / 200, startmovePos.y - delta.y / 200, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.z, boundary.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.y, boundary.extents.y);
                    CurrentActiveDecal.transform.localPosition = new Vector3(startmovePos.x, moveY, moveX);
                }
                else if (faceId == 2)
                {
                    Vector3 moveDelta = startmovePos - delta / 200;
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.y, boundary.extents.y);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.x, boundary.extents.x);
                    CurrentActiveDecal.transform.localPosition = new Vector3(-moveX, moveY, startmovePos.z);
                }
                else if (faceId == 3)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x / 200, startmovePos.y - delta.y / 200, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.z, boundary.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.y, boundary.extents.y);
                    CurrentActiveDecal.transform.localPosition = new Vector3(startmovePos.x, moveY, -moveX);
                }

            }
            else if (faceId == 4)
            {
                float moveX, moveY;

                if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front)
                {
                    Vector3 moveDelta = startmovePos - delta / 200;
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.z, boundary.extents.z);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.x, boundary.extents.x);
                    CurrentActiveDecal.transform.localPosition = new Vector3(moveX, startmovePos.y, moveY);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back)
                {
                    Vector3 moveDelta = startmovePos - delta / 200;
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.z, boundary.extents.z);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.x, boundary.extents.x);
                    CurrentActiveDecal.transform.localPosition = new Vector3(-moveX, startmovePos.y, -moveY);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Left)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x / 200, startmovePos.y - delta.y / 200, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.z, boundary.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.x, boundary.extents.x);
                    CurrentActiveDecal.transform.localPosition = new Vector3(-moveY, startmovePos.y, moveX);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Right)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x / 200, startmovePos.y - delta.y / 200, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.z, boundary.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.x, boundary.extents.x);
                    CurrentActiveDecal.transform.localPosition = new Vector3(moveY, startmovePos.y, -moveX);
                }
            }
        }//end !iswarp check
        else
        {
            if (Main.MainControl.currVertivalFace == Main.VertivalFace.Side)
            {
                float moveY;
                Vector3 moveDeltaY = startmovePos - delta / 200;
                float moveDelta = startmoveRot + delta.x / 200;
                moveY = moveY = Mathf.Clamp(moveDeltaY.y, -boundary.extents.y, boundary.extents.y);
                LabelMat[0].SetTextureOffset("_MainTex", new Vector2(moveDelta + decalSaveOffset[0], 0));
                LabelMat[1].SetTextureOffset("_MainTex", new Vector2(moveDelta + decalSaveOffset[1], 0));
                LabelMat[2].SetTextureOffset("_MainTex", new Vector2(moveDelta + decalSaveOffset[2], 0));
                LabelMat[3].SetTextureOffset("_MainTex", new Vector2(moveDelta + decalSaveOffset[3], 0));

                //\\
                decal[0].transform.localPosition = new Vector3(0, moveY, -(boundary.extents.z + 1f));
                decal[1].transform.localPosition = new Vector3(boundary.extents.x + 1f, moveY, 0);
                decal[2].transform.localPosition = new Vector3(0, moveY, boundary.extents.z + 1f);
                decal[3].transform.localPosition = new Vector3(-(boundary.extents.x + 1f), moveY, 0);
            }
            else if (Main.MainControl.currVertivalFace == Main.VertivalFace.Top)
            {
                float moveX, moveY;

                if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front)
                {
                    Vector3 moveDelta = startmovePos - delta / 200;
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.z, boundary.extents.z);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.x, boundary.extents.x);
                    CurrentActiveDecal.transform.localPosition = new Vector3(moveX, startmovePos.y, moveY);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back)
                {
                    Vector3 moveDelta = startmovePos - delta / 200;
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.z, boundary.extents.z);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.x, boundary.extents.x);
                    CurrentActiveDecal.transform.localPosition = new Vector3(-moveX, startmovePos.y, -moveY);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Left)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x / 200, startmovePos.y - delta.y / 200, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.z, boundary.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.x, boundary.extents.x);
                    CurrentActiveDecal.transform.localPosition = new Vector3(-moveY, startmovePos.y, moveX);
                }
                else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Right)//sw
                {
                    Vector3 moveDelta = new Vector3(startmovePos.z - delta.x / 200, startmovePos.y - delta.y / 200, 0);
                    moveX = Mathf.Clamp(moveDelta.x, -boundary.extents.z, boundary.extents.z);
                    moveY = Mathf.Clamp(moveDelta.y, -boundary.extents.x, boundary.extents.x);
                    CurrentActiveDecal.transform.localPosition = new Vector3(moveY, startmovePos.y, -moveX);
                }
            }
        }
        decalSavePos[0] = decal[0].transform.localPosition - new Vector3(0, 0, -(boundary.extents.z + 1f));
        decalSavePos[1] = decal[1].transform.localPosition - new Vector3(boundary.extents.x + 1f, 0, 0);
        decalSavePos[2] = decal[2].transform.localPosition - new Vector3(0, 0, boundary.extents.z + 1f);
        decalSavePos[3] = decal[3].transform.localPosition - new Vector3(-(boundary.extents.x + 1f), 0, 0);
        decalSavePos[4] = decal[4].transform.localPosition - new Vector3(0, boundary.extents.y + 1f, 0);
    }
    public void ScaleDecal(float scaleIn)
    {
        if ((faceId == 0 || faceId == 2)
            && decal[faceId].transform.localScale.x + scaleIn < boundary.size.x
            && decal[faceId].transform.localScale.z + scaleIn < boundary.size.y
            )
        {
            Vector3 a = new Vector3(Mathf.Clamp(decal[faceId].transform.localScale.x + scaleIn, 0.1f, boundary.size.x),
                1,
                Mathf.Clamp(decal[faceId].transform.localScale.z + scaleIn, .1f, boundary.size.y));
            CurrentActiveDecal.transform.localScale = a;
        }
        else if ((faceId == 1 || faceId == 3)
            && decal[faceId].transform.localScale.x + scaleIn < boundary.size.x
            && decal[faceId].transform.localScale.z + scaleIn < boundary.size.y
            )
        {
            Vector3 a = new Vector3(Mathf.Clamp(decal[faceId].transform.localScale.x + scaleIn, 0.1f, boundary.size.x),
               1,
                Mathf.Clamp(decal[faceId].transform.localScale.z + scaleIn, .1f, boundary.size.z));
            CurrentActiveDecal.transform.localScale = a;
        }
        else if ((faceId == 4)
           && decal[faceId].transform.localScale.x + scaleIn < boundary.size.x
           && decal[faceId].transform.localScale.z + scaleIn < boundary.size.z
           )
        {
            Vector3 a = new Vector3(Mathf.Clamp(decal[faceId].transform.localScale.x + scaleIn, 0.1f, boundary.size.x),
               1,
                Mathf.Clamp(decal[faceId].transform.localScale.z + scaleIn, .1f, boundary.size.z));
            CurrentActiveDecal.transform.localScale = a;
        }

        DontDestroy.DontDestroyData.DecalObjectS[faceId] = CurrentActiveDecal.transform.localScale;

    }
    private void Update()
    {
        //  print(onshow);
        DontDestroy.DontDestroyData.DecalIsOn = onshow;
        decal[4].SetActive(_BaseData._BaseDataSave.openLid && onshow && !Main.MainControl.IsEditing && !_BaseData._BaseDataSave.openWireframe);
    }

    Vector3 HandlerStartPos;
    Vector3 startRotateValue;
    [SerializeField] GameObject handlerObj;//z rott
    [SerializeField] GameObject handlerGroup;
    float FrontR, LeftR, RightR, BackR, TopR;

    public void setRotateHandler()//active handler
    {
        CheckCurrDecal();
        float a = 0;
        if (Main.MainControl.currVertivalFace == Main.VertivalFace.Top) a = TopR;
        else
        {
            if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front) a = FrontR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back) a = BackR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Right) a = RightR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Left) a = LeftR;
        }
        handlerObj.transform.localEulerAngles = new Vector3(0, 0, a);
    }
    public void rotate_handler_Start()
    {
        HandlerStartPos = handlerObj.transform.position;
    }

    public void rotate_handler_Drag()
    {
        float angle = Mathf.Atan2(Input.mousePosition.y - HandlerStartPos.y, Input.mousePosition.x - HandlerStartPos.x) * 180 / Mathf.PI;
        float a = 0;
        if (Main.MainControl.currVertivalFace == Main.VertivalFace.Top) a = TopR;
        else
        {
            if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front) a = FrontR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back) a = BackR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Left) a = RightR;
            else if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Right) a = LeftR;
        }
        handlerObj.transform.localEulerAngles = new Vector3(0, 0, a);

        if (faceId == 0)
        {
            CurrentActiveDecal.transform.localEulerAngles = new Vector3(startRotateValue.x + angle - 90, -90, 90);
            FrontR = angle - 90;
        }
        else if (faceId == 2)
        {
            CurrentActiveDecal.transform.localEulerAngles = new Vector3(startRotateValue.x + angle - 90, 90, 90);
            BackR = angle - 90;
        }
        else if (faceId == 1)
        {
            CurrentActiveDecal.transform.localEulerAngles = new Vector3(startRotateValue.x + angle - 90, 180, 90);
            RightR = angle - 90;
        }
        else if (faceId == 3)
        {
            CurrentActiveDecal.transform.localEulerAngles = new Vector3(startRotateValue.x - angle + 90, 180, -90);
            LeftR = angle - 90;
        }

        else if (faceId == 4)
        {
            CurrentActiveDecal.transform.localEulerAngles = new Vector3(-180, startRotateValue.x - angle - 90, -180);
            TopR = angle - 90;
        }
        DontDestroy.DontDestroyData.DecalObjectR[faceId] = CurrentActiveDecal.transform.localEulerAngles;

    }

    public void showMode(bool active)//on mainmenu select
    {
        //   decalOptionBtn.gameObject.SetActive(active);
        DecalDiscCanvas.SetActive(active);//???
        DecalDiscSlide.SetActive(active);//???
        ImportBtn.SetActive(false);
        reOpenSlideBtn.SetActive(false);
        // ToggleMenuGroup._toggleMenu.setEnabledToggle(false);
        //  ImportBtn.SetActive(active && onshow);
        if (!active)
        {
            // OptionSelectGroup.SetActive(false);
            ImportGroup.SetActive(false);
            handlerGroup.transform.localScale = Vector3.zero;
        }
        else { setRotateHandler(); }
    }

    public void _openModeSelect()
    {
        //   ImportGroup.SetActive(false);
        //   if (!OptionSelectGroup.activeSelf)
        //       OptionSelectGroup.SetActive(true);
        //   else
        //       OptionSelectGroup.SetActive(false);
    }
    public void _openImportSelect()
    {

        if (!ImportGroup.activeSelf)
            ImportGroup.SetActive(true);
        else
            ImportGroup.SetActive(false);
    }
    public void exitDecalEdit()
    {
        handlerGroup.transform.localScale = Vector3.zero;
        setRotateHandler();
        ImportGroup.SetActive(false);
        reOpenSlideBtn.SetActive(false);
        ImportBtn.SetActive(false);
        // OptionSelectGroup.SetActive(false);
    }
    public void ModeOption(int mode)
    {

        if (mode == 0)
        {
            onshow = false;
            handlerGroup.transform.localScale = Vector3.zero;
            setRotateHandler();
            decalModeCurrent.sprite = DecalIconSprite[0];
            ImportBtn.SetActive(false);
            reOpenSlideBtn.SetActive(true);
            ImportBtntext.text = "No Label";
            reOpenSlideBtn.GetComponent<RectTransform>().anchoredPosition = new Vector3(0, 470, 0);

        }
        else if (mode == 1)
        {
            onshow = true;
            isWrap = true;
            ImportBtntext.text = "Wrap";
            setRotateHandler();
            createActiveMaterial();
            decalModeCurrent.sprite = DecalIconSprite[1];
            ImportBtn.SetActive(true);
            reOpenSlideBtn.SetActive(true);
            reOpenSlideBtn.GetComponent<RectTransform>().anchoredPosition = new Vector3(-90, 470, 0);
        }
        else if (mode == 2)
        {
            onshow = true;
            isWrap = false;
            ImportBtntext.text = "Sticker";
            if (imported)
                createActiveMaterial();
            else createActiveMaterial2();
            decalModeCurrent.sprite = DecalIconSprite[2];
            ImportBtn.SetActive(true);
            reOpenSlideBtn.SetActive(true);
            reOpenSlideBtn.GetComponent<RectTransform>().anchoredPosition = new Vector3(-90, 470, 0);
        }
        //  ToggleMenuGroup._toggleMenu.setEnabledToggle(true);
        activeDecal(onshow);
        reOpenSlideBtn.GetComponent<Image>().sprite = DecalIconSprite[mode];
        ImportGroup.SetActive(false);
    }

    public void ImportByUrl()
    {
        CheckCurrDecal();
        print(GUIUtility.systemCopyBuffer);
        StartCoroutine(DownloadImage(GUIUtility.systemCopyBuffer));
    }
    IEnumerator DownloadImage(string MediaUrl)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(MediaUrl);
        yield return request.SendWebRequest();
        if (request.result == UnityWebRequest.Result.ConnectionError || request.result == UnityWebRequest.Result.ProtocolError)
        { ImportGroup.SetActive(false); }
        else
        {
            if (faceId == 5) yield return null;//bottom
            decalCrop.gameObject.SetActive(true);
            decalCrop._SetEditable(((DownloadHandlerTexture)request.downloadHandler).texture);
            ImportGroup.SetActive(false);
        }
    }

    public void ImportGallery()
    {
        CheckCurrDecal();
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            if (path != null)
            {
                Texture2D texture = NativeGallery.LoadImageAtPath(path, 512);// If the selected image's width and/or height is greater than 512px, down-scale the image
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }
                if (faceId == 5) return;//bottom
                decalCrop.gameObject.SetActive(true);
                decalCrop._SetEditable(texture);
                ImportGroup.SetActive(false);
            }
        });
    }

    public void SetCropValue(Texture t2d)
    {
        if (isWrap)
            if (faceId == 4) { InputTextureWrap[1] = t2d; }
            else InputTextureWrap[0] = t2d;
        else
        { InputTextureUnWrap[faceId] = t2d; imported = true; }
        createActiveMaterial();
    }

}
