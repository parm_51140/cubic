using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UHTController : MonoBehaviour
{
    // Start is called before the first frame update
    [System.Serializable]
    public class SideGroup
    {
        public GameObject[] sideElement;
    }

    [System.Serializable]
    public class VectorGroup
    {
        public List<Vector3> vElement;
    }


       

    [Range(3.7f, 20f)]
    public float width;
    [Range(5.1f, 20f)]
    public float length;
    [Range(5f, 20f)]
    public float height;

    public SurfaceGenerator surfaceGenerator;
    public GameObject highestPosition;

    public bool showLid = true;

    [SerializeField] public List<SideGroup> allSide;
    [SerializeField] public List<VectorGroup> startPoints;
    [SerializeField] public List<VectorGroup> originalPoints;

    void Start()
    {
        Backups();
    }


    void Update()
    {
        Reshape();
    }

    // Start is called before the first frame update
    public virtual void Backups()
    {
        //Base
        for (int i = 0; i < allSide.Count; i++)
        {
            originalPoints.Add(new VectorGroup());
            originalPoints[i].vElement = new List<Vector3>();
            for (int j = 0; j < allSide[i].sideElement.Length; j++)
            {
                originalPoints[i].vElement.Add(allSide[i].sideElement[j].transform.localPosition);
            }
        }

        

       

    }



    // Update is called once per frame
    public virtual void Reshape()

    {   //restore originalpoint to frist basepoint
        for (int i = 0; i < originalPoints.Count; i++)
        {

            for (int j = 0; j < allSide[i].sideElement.Length; j++)
            {
                allSide[i].sideElement[j].transform.localPosition = originalPoints[i].vElement[j];
            }
        }


        startPoints.Clear();

        for (int i = 0; i < allSide.Count; i++)
        {
            startPoints.Add(new VectorGroup());
            startPoints[i].vElement = new List<Vector3>();
            for (int j = 0; j < allSide[i].sideElement.Length; j++)
            {
                startPoints[i].vElement.Add(allSide[i].sideElement[j].transform.localPosition);
            }
        }

        float highestPoint = highestPosition.transform.position.y;

        //Base
        for (int j = 0; j < allSide.Count; j++)
       {
           for (int i = 0; i < startPoints[j].vElement.Count; i++)
           {
               var item = allSide[j].sideElement[i];
               var position = item.transform.localPosition;
               if (j == 0)
               {
                   position.z = (startPoints[j].vElement[i].z + (-width / 2));
                    position.y = (startPoints[j].vElement[i].y + (height / 2 - highestPoint));


               }
               if (j == 1)
               {
                   position.z = (startPoints[j].vElement[i].z + (width / 2));
                    position.y = (startPoints[j].vElement[i].y + (height / 2 - highestPoint));


               }
               if (j == 2)
               {
                   position.x = (startPoints[j].vElement[i].x + (-length / 2));
                    position.y = (startPoints[j].vElement[i].y + (height / 2 - highestPoint));


               }
               if (j == 3)
               {
                   position.x = (startPoints[j].vElement[i].x + (length / 2));
                    position.y = (startPoints[j].vElement[i].y + (height / 2 - highestPoint));


               }
                if (j == 4)
                {
                    position.y = (startPoints[j].vElement[i].y + ((height / 2 - highestPoint)) - (height * 0.17f));

                }
               //position.z = (startPoints[j].vElement[i].z + (-width / 2));
               item.transform.localPosition = position;
           }
       }

        surfaceGenerator.extrude = -height;
        //pathGenerator.shapeExposure = height;

     


    }

    
}
