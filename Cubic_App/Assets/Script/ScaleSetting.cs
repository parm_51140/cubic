using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScaleSetting : MonoBehaviour
{
    [SerializeField] RectTransform scaleObject;
    [SerializeField] Text numberText;
    [SerializeField] bool isHorizontalScale;
    public void SetScale(bool isMajor, float number, float Position)
    {
        if (isHorizontalScale)
        {
            if (isMajor)
            {
              // scaleObject.sizeDelta = new Vector2(6, 150);
               scaleObject.sizeDelta = new Vector2(6, 1900);
                numberText.text = "  " + number + "";

            }
            else
            {
               scaleObject.sizeDelta = new Vector2(6, 100);
                numberText.text = "";
            }
        }
        else
        {
            if (isMajor)
            {
                scaleObject.sizeDelta = new Vector2(1700, 6);
             //   scaleObject.sizeDelta = new Vector2(120, 6);
                numberText.text = "  "+number + "";

            }
            else
            {
                scaleObject.sizeDelta = new Vector2(70, 6);
                numberText.text = "";
            }

        }
    }
}
