using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARFadeAnimFx : MonoBehaviour
{
    [SerializeField] Text textShow;
    [SerializeField] RectTransform IconImage;
    [SerializeField] float rotatespeed;
    [SerializeField] float scaleSpeed;
    float iconRotate;
    float iconScale;
    char count;
    void Start()
    {
        InvokeRepeating("settext", 1, 1);
    }

    void Update()
    {
        iconRotate = iconRotate< 359f ? iconRotate += Time.deltaTime* rotatespeed : 0;
        iconScale=Mathf.PingPong(Time.time/4, .5f) +.7f;
       // print(iconScale);
        IconImage.transform.localEulerAngles = new Vector3(0, 0, iconRotate);
        IconImage.transform.localScale = new Vector3(iconScale, iconScale, iconScale);

    }
    void settext() //Detecting Plane
    {
        if (count == 0) { textShow.text = "Plane Detection in Progress   . ."; count++; }
        else if (count == 1) { textShow.text = "Plane Detection in Progress .   ."; count++; }
        else if (count == 2) { textShow.text = "Plane Detection in Progress . .  "; count = (char)0; }     
    }

    private void OnDisable()
    {
        this.enabled = false;
    }
}
