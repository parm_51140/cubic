using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CupController : MonoBehaviour
{
    // Start is called before the first frame update
    [System.Serializable]
    public class SideGroup
    {
        public GameObject[] sideElement;
    }

    [System.Serializable]
    public class VectorGroup
    {
        public List<Vector3> vElement;
    }


    public AnimationCurve wallCurve;
    public AnimationCurve startWallCurve;
    public PathGenerator pathGenerator;
    public SurfaceGenerator lid;
    public MeshRenderer lidMesh;

    [Range(3f, 52.54f)]
    public float width;
    [Range(3f, 52.54f)]
    public float length;
    [Range(0.5f, 20f)]
    public float height;
    [Range(1f, 7)]
    public float slope;

    public bool showLid = false;


    [SerializeField] public List<SideGroup> allSide;
    [SerializeField] public List<VectorGroup> startPoints;
    [SerializeField] public List<VectorGroup> originalPoints;

    public int wingStart = 1;

    void Start()
    {
        Backups();
    }


    void Update()
    {
        Reshape();
    }

    // Start is called before the first frame update
    public virtual void Backups()
    {
        //Base
        for (int i = 0; i < allSide.Count; i++)
        {
            originalPoints.Add(new VectorGroup());
            originalPoints[i].vElement = new List<Vector3>();
            for (int j = 0; j < allSide[i].sideElement.Length; j++)
            {
                originalPoints[i].vElement.Add(allSide[i].sideElement[j].transform.localPosition);
            }
        }

        //Wall

        Keyframe[] key = (Keyframe[])wallCurve.keys.Clone();
        startWallCurve = new AnimationCurve(key);
        for (int i = 0; i < wallCurve.keys.Length; i++)
        {
            wallCurve.MoveKey(i, new Keyframe(wallCurve.keys[i].time * 0.1f, wallCurve.keys[i].value));
        }



    }



    // Update is called once per frame
    public virtual void Reshape()

    {   //restore originalpoint to frist basepoint
        for (int i = 0; i < originalPoints.Count; i++)
        {

            for (int j = 0; j < allSide[i].sideElement.Length; j++)
            {
                allSide[i].sideElement[j].transform.localPosition = originalPoints[i].vElement[j];
            }
        }


        startPoints.Clear();

        for (int i = 0; i < allSide.Count; i++)
        {
            startPoints.Add(new VectorGroup());
            startPoints[i].vElement = new List<Vector3>();
            for (int j = 0; j < allSide[i].sideElement.Length; j++)
            {
                startPoints[i].vElement.Add(allSide[i].sideElement[j].transform.localPosition);
            }
        }

        //Base
        for (int j = 0; j < allSide.Count; j++)
        {
            for (int i = 0; i < startPoints[j].vElement.Count; i++)
            {
                var item = allSide[j].sideElement[i];
                var position = item.transform.localPosition;
                if (j == 0)
                {
                    position.z = (startPoints[j].vElement[i].z + (-width / 2));

                }
                if (j == 1)
                {
                    position.z = (startPoints[j].vElement[i].z + (width / 2));

                }
                if (j == 2)
                {
                    position.x = (startPoints[j].vElement[i].x + (-length / 2));

                }
                if (j == 3)
                {
                    position.x = (startPoints[j].vElement[i].x + (length / 2));

                }
                //position.z = (startPoints[j].vElement[i].z + (-width / 2));
                item.transform.localPosition = position;
            }
        }

        pathGenerator.slope = slope;
        pathGenerator.shape = wallCurve;

        //pathGenerator.shapeExposure = height;
        for (int i = 0; i < wallCurve.keys.Length; i++)
        {
            if (i <= wingStart)
            {
                wallCurve.MoveKey(i, new Keyframe(startWallCurve.keys[i].time * slope, startWallCurve.keys[i].value * height));
            }


            else
            {

                wallCurve.MoveKey(i, new Keyframe((startWallCurve.keys[i].time + wallCurve.keys[wingStart].time - startWallCurve.keys[wingStart].time),
                   startWallCurve.keys[i].value + wallCurve.keys[wingStart].value - startWallCurve.keys[wingStart].value));
            }

        }

        //LID
      //  lidMesh.GetComponent<MeshRenderer>().enabled = showLid;

        lid.offset = new Vector3(0f, 0.23f + (0.777777f*height), 0f);
        lid.expand = slope;// - ((slope*0.35f));


    }
}
