using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WebCam : MonoBehaviour
{
    public WebCamTexture webcamTexture;
    public RenderTexture cam;   
    void Start()
    {
        WebCamDevice[] devices = WebCamTexture.devices;
        for (var i = 0; i < devices.Length; i++)
        {
            Debug.Log(devices[i].name);
            Debug.Log(devices[i].isFrontFacing);
        }
        if(devices.Length > 0)
        {
            webcamTexture = new WebCamTexture();
            Renderer renderer = GetComponent<Renderer>();
            renderer.material.mainTexture = webcamTexture;
            webcamTexture.Play();

        }
    }

    void Update()
    {
        
    }
}
