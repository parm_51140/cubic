using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadHolder : MonoBehaviour
{
    public static LoadHolder _loadHold;
    [SerializeField] GameObject fadePlane;
    [SerializeField] GameObject TextLoad;
    [SerializeField] GameObject RotateCircle;
    public bool isActive;
    float rotate = 0;
    private void Awake()
    {
        _loadHold = this;
    }

    public void setLoad(bool act)
    {
        if (isActive == act) return;
        isActive = act;
        fadePlane.SetActive(isActive);
        TextLoad.SetActive(isActive);
        RotateCircle.SetActive(isActive);
        //float addscale = Main.MainControl.orbitCamera.orthographicSize / 30;
        //RotateCircle.transform.localScale = new Vector3(addscale, addscale, addscale);
    }

    void Update()
    {
        if (isActive)
        {
            rotate += Time.deltaTime * 120;
            if (rotate > 360) rotate = 0;

            RotateCircle.transform.eulerAngles = new Vector3(0, 0, rotate);
        }
    }
}
