using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ObjectSummary : MonoBehaviour
{
    public static ObjectSummary _objectSummary;
    [SerializeField] Text ObjSummaryResult;
    [SerializeField] Text MaterialText;
    [SerializeField] Text[] ShareSummaryResult;

    [SerializeField] Text[] allText;
    [SerializeField] Image[] allBtn;
    [SerializeField] Image backdrop;

    [SerializeField] GetVol getVolScript;
    [SerializeField] GameObject EditableObject;
    [SerializeField] PlasticThickness PlasticThickness;
    [SerializeField] SetShareScrene setShare;

    float width, dept, high, cap, weight;
    float transformValue;
    Color backdropcolor = new Color(0, 0, 0, .8f);
    Bounds objBound;
    Coroutine fadeCoroutine;

    bool isShow;

    private void Awake()
    {
        _objectSummary = this;

    }
    public void ShowSummary(bool setactive)//call
    {
        if (setactive == isShow) return;

        isShow = setactive;
        if (setactive)
        {
            objBound = EditableObject.GetComponent<Renderer>().bounds;
            width = ObjectSizeScript._objectSizeScript.SizeX;
            dept = ObjectSizeScript._objectSizeScript.SizeZ;
            high = ObjectSizeScript._objectSizeScript.SizeY;

            if (_BaseData._BaseDataSave.selectedID == 4) { cap = 3.14f * dept / 2 * high; }
            else if (_BaseData._BaseDataSave.selectedID == 5) { cap = width * dept * high; }
            else if (_BaseData._BaseDataSave.selectedID == 3) { cap = width * dept * high / 2; }
            else
                cap = getVolScript.volumeDetect;// (volume.volumeDetect / 27).ToString("F2") 

            weight = (PlasticThickness._thicknessUI.CurrentThickness * 13 / 0.2f) / 100 * (width * dept);

            if (_BaseData._BaseDataSave.selectedID == 4) { MaterialText.text = "Aluminium\t\t   "; }
            else if (_BaseData._BaseDataSave.selectedID == 5) { MaterialText.text = "PE, Aluminium and Paperboard"; }
            else { MaterialText.text = "PET\t\t\t\t  "; }

            ObjSummaryResult.text = "" +
         width.ToString("#,##0.00") + "\n" +
         high.ToString("#,##0.00") + "\n" +
         dept.ToString("#,##0.00") + "\n" +
         cap.ToString("#,##0.00") + "\n" +
         weight.ToString("#,##0.00") + "\n" +
         "1.38" + "\n" +
         PlasticThickness._thicknessUI.CurrentThickness.ToString("#,##0.00") + "   0.01";


            ShareSummaryResult[0].text = MaterialText.text;
            ShareSummaryResult[1].text = width.ToString("#,##0.00");
            ShareSummaryResult[2].text = high.ToString("#,##0.00");
            ShareSummaryResult[3].text = dept.ToString("#,##0.00");
            ShareSummaryResult[4].text = cap.ToString("#,##0.00");
            ShareSummaryResult[5].text = weight.ToString("#,##0.00");
            ShareSummaryResult[6].text = "1.38";
            ShareSummaryResult[7].text = PlasticThickness._thicknessUI.CurrentThickness.ToString("#,##0.00");


            // if (fadeCoroutine != null) 
            // { StopCoroutine(fadeCoroutine); }
            // fadeCoroutine = StartCoroutine(fadeIn(true));
        }
        //  else
        //  {
        //      if (!gameObject.activeSelf) return;
        //      if (fadeCoroutine != null)
        //      { StopCoroutine(fadeCoroutine); }
        //    //  fadeCoroutine = StartCoroutine(fadeIn(false));
        //  }

    }
    private void Update()
    {
        if (isShow && transformValue < 1)
        {
            gameObject.transform.localScale = Vector3.one;
            transformValue += .1f;
            backdrop.GetComponent<Image>().color = Color.Lerp(backdrop.GetComponent<Image>().color, backdropcolor, transformValue);
            foreach (Text tex in allText)
            {
                tex.GetComponent<Text>().color = Color.Lerp(tex.GetComponent<Text>().color, Color.white, transformValue); //textColor;
            }
            foreach (Image Imm in allBtn)
            {
                Imm.GetComponent<Image>().color = Color.Lerp(Imm.GetComponent<Image>().color, Color.white, transformValue); //textColor;
            }
        }
        else if (!isShow && transformValue > 0)
        {
            transformValue -= .1f;
            backdrop.GetComponent<Image>().color = Color.Lerp(backdrop.GetComponent<Image>().color, Color.clear, transformValue);
            foreach (Text tex in allText)
            {
                tex.GetComponent<Text>().color = Color.Lerp(tex.GetComponent<Text>().color, Color.clear, transformValue); //textColor;
            }
            foreach (Image Imm in allBtn)
            {
                Imm.GetComponent<Image>().color = Color.Lerp(Imm.GetComponent<Image>().color, Color.clear, transformValue); //textColor;
            }
            if (transformValue <= 0)
                gameObject.transform.localScale = Vector3.zero;
        }

       


    }
    public void _LoginCheck()
    {
      // Share._shareSummaryDetail.ShareDetail();
      // return;//debug

        if (PlayerPrefs.HasKey("USERDATA"))
        {
            if (_BaseData._BaseDataSave.designID == "") 
            {
                CallBackNotice._callBackNotice._noticBark("Model Doesn't Saved");
                return;
            }
                Share._shareSummaryDetail.ShareDetail();

            //  new NativeShare().SetText("http://cubic.thaipet.co.th?" + _BaseData._BaseDataSave.userTag + "&" + _BaseData._BaseDataSave.designID)
            // .SetCallback((result, shareTarget) => Debug.Log("Share result: " + result + ", selected app: " + shareTarget))
            // .Share();
        }
        else
        {
            MainMenuGroupControl._mainmenugroupcontrol.forceCallMenu(7);
            ObjectSummary._objectSummary.ShowSummary(false);
            UserLogin._userLogin.ShowUser();
        }
    }
    public void _cadLoginCheck()
    {

        if (PlayerPrefs.HasKey("USERDATA"))
        {
            if (_BaseData._BaseDataSave.designID == "")
                CallBackNotice._callBackNotice._noticBark("require saved first");
            else if (_BaseData._BaseDataSave.selectedID == 1)
                StartCoroutine(CadExport());
            else
                CallBackNotice._callBackNotice._noticBark("not support");
        }
        else
        {
            MainMenuGroupControl._mainmenugroupcontrol.forceCallMenu(7);
            ObjectSummary._objectSummary.ShowSummary(false);
            UserLogin._userLogin.ShowUser();
        }
    }

    IEnumerator CadExport()
    {
        allBtn[2].color = Color.white / 2;
        allBtn[2].raycastTarget = false;
        string aaa = "Box";
        switch (_BaseData._BaseDataSave.selectedID)
        {
            case 1: aaa = "Box"; break;
            case 2: aaa = "Cup"; break;
            case 3: aaa = "SandwichBox"; break;
            case 4: aaa = "Can"; break;
            case 5: aaa = "Carton"; break;
        }
        ShapeControllerBase scc = EditableObjectMaster._editmaster.currentObject.GetComponent<_BoxEdit>().controller;
        string getreq = _BaseData._BaseDataSave.targetLink + "?action=createCAD&userid=" + _BaseData._BaseDataSave.userID + "&designid="
            + _BaseData._BaseDataSave.designID + "&sheetid=" + _BaseData._BaseDataSave.userTag + "&designtype=" + aaa;

        print(getreq);
        var uwr = new UnityWebRequest(getreq, "POST");
        ///GUIUtility.systemCopyBuffer = scc.ToJson();
        byte[] jsonToSend = new System.Text.UTF8Encoding().GetBytes(scc.ToJson());
        uwr.uploadHandler = (UploadHandler)new UploadHandlerRaw(jsonToSend);
        uwr.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        uwr.SetRequestHeader("Content-Type", "application/json");

        yield return uwr.SendWebRequest();
        if (uwr.result != UnityWebRequest.Result.Success)
        {
            CallBackNotice._callBackNotice._noticBark("CAD Upload fail");
            allBtn[2].color = Color.white;
            allBtn[2].raycastTarget = true;
        }
        else
        {
            if (uwr.downloadHandler.text.IndexOf("Add CAD To CADSheet") > -1)
                CallBackNotice._callBackNotice._noticBark("CadUploaded");
            else
            {
                CallBackNotice._callBackNotice._noticBark("CAD Upload fail");
                allBtn[2].color = Color.white;
                allBtn[2].raycastTarget = true;
            }
        }
        yield return null;
    }
}
