using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectSizeScriptShareImage : MonoBehaviour
{
   public static ObjectSizeScriptShareImage _sharerefLine;
    [SerializeField] Renderer editableObject;
    [SerializeField] LineRenderer WidthLine;
    [SerializeField] TextMesh WidthText;
    [SerializeField] GameObject[] WidthClose;
    [SerializeField] GameObject[] WidthCone;
    [SerializeField] LineRenderer LenghtLine;
    [SerializeField] TextMesh LenghtText;
    [SerializeField] GameObject[] LenghtClose;
    [SerializeField] GameObject[] LenghtCone;
    [SerializeField] LineRenderer HightLine;
    [SerializeField] TextMesh HightText;
    [SerializeField] GameObject[] HightClose;
    [SerializeField] GameObject[] HightCone;
    [SerializeField] Camera IsoCam;
   
    Bounds objectBound;
    float offsetY = .25f;
    float offsetX = .25f;

    [SerializeField] float ExtendWidth;
    [SerializeField] float SizeWidth;
    [SerializeField] float ExtendLenght;
    [SerializeField] float SizeLenght;
    [SerializeField] float ExtendHight;
    [SerializeField] float SizeHight;

    float sizeOffset = 10 / 3f;

    private void Awake()
    {
        _sharerefLine = this;
    }

    // Update is called once per frame
    public void CreateLine()
    {

        float lineSize = 30f/IsoCam.orthographicSize*.1f;
        objectBound = EditableObjectMaster._editmaster.currentObjectRenderer.bounds;

        ExtendWidth = objectBound.extents.x;
        SizeWidth = objectBound.size.x;

        ExtendLenght = objectBound.extents.z;
        SizeLenght = objectBound.size.z;

        ExtendHight = objectBound.extents.y;
        SizeHight = objectBound.size.y;

        WidthLine.transform.localPosition = new Vector3(0, -(ExtendHight + offsetY), -ExtendLenght*1.2f);
        WidthLine.SetPosition(0, new Vector3(-ExtendWidth, 0, 0));
        WidthLine.SetPosition(1, new Vector3(ExtendWidth, 0, 0));
        WidthClose[0].transform.localPosition = new Vector3(ExtendWidth, 0, 0);
        WidthClose[1].transform.localPosition = new Vector3(-ExtendWidth, 0, 0);
        WidthCone[0].transform.localPosition = new Vector3(ExtendWidth - 1f, 0, 0);
        WidthCone[1].transform.localPosition = new Vector3(-ExtendWidth + 1f, 0, 0);
        WidthText.text = "Width";

        LenghtLine.transform.localPosition = new Vector3((ExtendWidth + offsetY)*1.2f, -(ExtendHight + offsetY), 0);
        LenghtLine.SetPosition(0, new Vector3(0, 0, -ExtendLenght));
        LenghtLine.SetPosition(1, new Vector3(0, 0, ExtendLenght));
        LenghtClose[0].transform.localPosition = new Vector3(0, 0, ExtendLenght);
        LenghtClose[1].transform.localPosition = new Vector3(0, 0, -ExtendLenght);
        LenghtCone[0].transform.localPosition = new Vector3(0, 0, ExtendLenght - 1f);
        LenghtCone[1].transform.localPosition = new Vector3(0, 0, -ExtendLenght + 1f);
        LenghtText.text = "Length";

        HightLine.transform.localPosition = new Vector3(ExtendWidth*1.2f, 2.4f, ExtendLenght);
        HightLine.SetPosition(0, new Vector3(0, -ExtendHight, 0));
        HightLine.SetPosition(1, new Vector3(0, ExtendHight, 0));
        HightClose[0].transform.localPosition = new Vector3(0, ExtendHight, 0);
        HightClose[1].transform.localPosition = new Vector3(0, -ExtendHight, 0);
        HightCone[1].transform.localPosition = new Vector3(0, ExtendHight - 1f, 0);
        HightCone[0].transform.localPosition = new Vector3(0, -ExtendHight + 1f, 0);
        HightText.text = "Height";

        if (lineSize < .05f) lineSize = .05f;

        WidthLine.startWidth = lineSize;
        WidthLine.endWidth = lineSize;
        LenghtLine.startWidth = lineSize;
        LenghtLine.endWidth = lineSize;
        HightLine.startWidth = lineSize;
        HightLine.endWidth = lineSize;

        WidthText.fontSize = (int)(26/ IsoCam.orthographicSize * 50);
        LenghtText.fontSize = WidthText.fontSize;
        HightText.fontSize = WidthText.fontSize;
    }
}