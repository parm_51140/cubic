using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPanel : MonoBehaviour
{
    public static SoundPanel _SoundPanel;
    [SerializeField] AudioSource soundSource;
    [SerializeField] AudioClip UIClick;
    [SerializeField] AudioClip ModelSwipe;
    [SerializeField] AudioClip SubMenuSwipe;
    [SerializeField] AudioClip WheelSwipe;
    void Awake()
    {
        _SoundPanel = this;
    }

    public void UIClickPlay()
    { soundSource.clip = UIClick; soundSource.Play(); }
    public void ModelSwipePlay() 
    { soundSource.clip = ModelSwipe; soundSource.Play(); }
    public void SubMenuSwipePlay()
    { soundSource.clip = SubMenuSwipe; soundSource.Play(); }
    public void WheelSwipePlay() 
    { soundSource.clip = WheelSwipe; soundSource.Play(); }
}
