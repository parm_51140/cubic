using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class ARPlaneFade : MonoBehaviour
{
    MeshFilter selfMesh;
    GameObject pl;
    Material matt;
    float tri;
    float temptri;
  //  float DelayFade = 3;
    Coroutine fadeOut;
    Coroutine checker;
    [SerializeField] GameObject visiblePlane;

    private void Start()
    {
        selfMesh = gameObject.GetComponent<MeshFilter>();

        pl = Instantiate(visiblePlane, gameObject.transform);
        pl.GetComponent<MeshFilter>().mesh = selfMesh.mesh;

        checker = StartCoroutine(triCheck());

    }
    void Update()
    {
           selfMesh = gameObject.GetComponent<MeshFilter>();
           pl.GetComponent<MeshFilter>().mesh = selfMesh.mesh;      
    }
    IEnumerator triCheck()
    {
        yield return new WaitForSeconds(.5f);
        if (fadeOut != null) StopCoroutine(fadeOut);
        fadeOut = StartCoroutine(faded());        
    }

    IEnumerator faded()
    {
        Color startColor = new Color(1, 1, 1, 0f);
        Color endColor = new Color(1, 1, 1, .2f);
        Color TmpC;
        float t = 0f;
        while (t < 1)
        {
            TmpC = Color.Lerp(startColor, endColor, Mathf.Sin(t*Mathf.PI));
            pl.GetComponent<MeshRenderer>().material.color = TmpC;
            t =t+ .02f;
            yield return new WaitForSeconds(.03f);

        }
        pl.GetComponent<MeshRenderer>().enabled = false;
        yield return null;
        checker = null;
    }


}
