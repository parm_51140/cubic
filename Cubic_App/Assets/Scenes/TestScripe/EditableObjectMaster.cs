using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.Rendering.DebugUI;

public class EditableObjectMaster : MonoBehaviour
{
    public static EditableObjectMaster _editmaster;
    [Header("Misc")]
    [SerializeField] WireframeShader WireframeObject;
    [SerializeField] Material editingMaterial;
    [SerializeField] Material standardMaterial;
    [SerializeField] Material CoverMaterial;
    [Header("ObjectPrefab")]
    [SerializeField] GameObject objectBox;
    [SerializeField] GameObject objectCup;
    [SerializeField] GameObject objectSandwich;
    [SerializeField] GameObject objectCan;
    [SerializeField] GameObject objectCarton;

    public GameObject currentObject;
    public Renderer currentObjectRenderer;
    public Renderer BaseObjectRenderer;

    float Width;
    float Length;
    float Height;
    float slope;
    float curve;
    float Pos;

    ShapeControllerBase BoxObjController;
    CupController CupObjController;
    SandWichController SandWObjController;
    CanReshape CanObjController;
    UHTController CartonObjController;

    _BoxEdit boxedit;
    _CupEdit cupedit;
    _SandWichEdit sandWedit;
    _CanEdit canedit;
    _CartonEdit cartonedit;
    List<GameObject> allObjList = new List<GameObject>();

    [HideInInspector] bool setdefaultDecal;


    private void Awake()
    {
        _editmaster = this;
    }
    private void Start()
    {
        if (DeepLinkActive._deepLinkActive.startdeeplink)
        {

            return;
        }
        if (DontDestroy.DontDestroyData.saved == false)
        {
            setdefaultDecal = true;
            SpawnObject(1);

        }
        else if (DontDestroy.DontDestroyData.saved == true)
        {
            setdefaultDecal = false;
            FreehandCamera_R.freehandCamera._clearFreehand();
            SpawnObject(_BaseData._BaseDataSave.selectedID);

        }

        // if (DeepLinkActive._deepLinkActive.startdeeplink) { DeepLinkActive._deepLinkActive.altDeepLinkActivated(); }
    }

    public void SpawnObject(int objID)
    {
        LoadHolder._loadHold.setLoad(true);
        _BaseData._BaseDataSave.selectedID = objID;
        FreehandCamera_R.freehandCamera._clearFreehand();
        if (objID == 1)
        {
            currentObject = Instantiate(objectBox, new Vector3(0, 0, 0), Quaternion.identity);
            boxedit = currentObject.transform.GetComponent<_BoxEdit>();
            BoxObjController = boxedit.controller;
            currentObjectRenderer = boxedit.mainObject.GetComponent<Renderer>();
            allObjList = boxedit.AllObject;


        }
        else if (objID == 2)
        {
            currentObject = Instantiate(objectCup, new Vector3(0, 0, 0), Quaternion.identity);
            cupedit = currentObject.transform.GetComponent<_CupEdit>();
            CupObjController = cupedit.controller;
            currentObjectRenderer = cupedit.mainObject.GetComponent<Renderer>();
            allObjList = cupedit.AllObject;
        }
        else if (objID == 3)
        {
            currentObject = Instantiate(objectSandwich, new Vector3(0, 0, 0), Quaternion.identity);
            sandWedit = currentObject.transform.GetComponent<_SandWichEdit>();
            SandWObjController = sandWedit.controller;
            currentObjectRenderer = sandWedit.mainObject.GetComponent<Renderer>();
            allObjList = sandWedit.AllObject;
        }
        else if (objID == 4)
        {
            currentObject = Instantiate(objectCan, new Vector3(0, 0, 0), Quaternion.identity);
            canedit = currentObject.transform.GetComponent<_CanEdit>();
            CanObjController = canedit.controller;
            currentObjectRenderer = canedit.mainObject.GetComponent<Renderer>();
            allObjList = canedit.AllObject;
        }
        else if (objID == 5)
        {
            currentObject = Instantiate(objectCarton, new Vector3(0, 0, 0), Quaternion.identity);
            cartonedit = currentObject.transform.GetComponent<_CartonEdit>();
            CartonObjController = cartonedit.controller;
            currentObjectRenderer = cartonedit.mainObject.GetComponent<Renderer>();
            allObjList = cartonedit.AllObject;
        }
        foreach (GameObject go in allObjList)
        {
            go.GetComponent<Renderer>().enabled = false;
        }
    
        setDefultObjectSize();
    }
    public void reSpawnObject(int id)
    {
        if (_BaseData._BaseDataSave.selectedID != id)
        {
            _BaseData._BaseDataSave.designID = "";
            DontDestroy.DontDestroyData.saved = false;
            setdefaultDecal = true;
            DecalControl2._decalControl._setActiveDecalObject(false);
            if (_BaseData._BaseDataSave.openWireframe) activeWireframeAlt(false);
            if (currentObject != null) Destroy(currentObject);
            SpawnObject(id);
        }
    }
    public void forceReSpawnObject(int id)
    {
        Debug.Log("EEEEEEEE"+id);
        setdefaultDecal = true;
        DecalControl2._decalControl._setActiveDecalObject(false);
        if (_BaseData._BaseDataSave.openWireframe) activeWireframeAlt(false);
        if (currentObject != null) Destroy(currentObject);
        SpawnObject(id);
    }

    void Update()
    {
        // x = GetComponent<MeshRenderer>().bounds; 
        //  print(-currentObjectRenderer.bounds.extents.y);
        // print(currentObjectRenderer.bounds.size);

        //box  23.7,1.24,14.6,
    }
    void setDefultObjectSize()
    {
        //checksave
        if (_BaseData._BaseDataSave.selectedID == 1)
        {
            if (DontDestroy.DontDestroyData.saved)
            {
                BoxObjController.width = _BaseData._BaseDataSave.saveWidth;
                BoxObjController.length = _BaseData._BaseDataSave.SaveLength;
                BoxObjController.height = _BaseData._BaseDataSave.SaveHeight;
                BoxObjController.slope = _BaseData._BaseDataSave.Saveslope;
                BoxObjController.flexibleCurve = _BaseData._BaseDataSave.Savecurve;
            }
            else
            {
                BoxObjController.width = 11.27567f;
                BoxObjController.length = 20.282f;
                BoxObjController.height = 4.323805f;
                BoxObjController.slope = 3.58f;
                BoxObjController.flexibleCurve = 3.8f;
            }
        }
        else if (_BaseData._BaseDataSave.selectedID == 2)
        {
            if (DontDestroy.DontDestroyData.saved)
            {
                CupObjController.width = _BaseData._BaseDataSave.saveWidth;
                CupObjController.length = _BaseData._BaseDataSave.SaveLength;
                CupObjController.height = _BaseData._BaseDataSave.SaveHeight;
                CupObjController.slope = _BaseData._BaseDataSave.Saveslope;
            }
            else
            {
                CupObjController.width = 4.38f;
                CupObjController.length = 4.38f;
                CupObjController.height = 10.01f;
                CupObjController.slope = 1;
            }
        }
        else if (_BaseData._BaseDataSave.selectedID == 4)//can
        {
            if (DontDestroy.DontDestroyData.saved)
            {
                CanObjController.height = _BaseData._BaseDataSave.SaveHeight;
            }
            else
            {
                CanObjController.height = 12.85f;
            }
        }
      
        // Invoke("delayAct", 1f);
        StartCoroutine(delayAct());
    }
    IEnumerator delayAct()
    {

        //ShapeSummary._volumscript.RunVolume();
        // print(currentObjectRenderer.bounds.extents.y);

        yield return new WaitForSeconds(1f);

        ObjectSizeScript._objectSizeScript.setsize();

        currentObject.transform.localPosition = new Vector3(0, -currentObjectRenderer.bounds.extents.y, 0);
        if (_BaseData._BaseDataSave.selectedID == 4)
        {
            currentObject.transform.localPosition = new Vector3(0, -currentObjectRenderer.bounds.extents.y
                - currentObject.gameObject.GetComponent<_CanEdit>().ringObject.GetComponent<Renderer>().bounds.size.y, 0);
        }

        foreach (GameObject go in allObjList)
        {
            go.GetComponent<Renderer>().enabled = true;
        }

        yield return new WaitForEndOfFrame();

        if (_BaseData._BaseDataSave.selectedID != 3 && _BaseData._BaseDataSave.selectedID != 4)
        {
            if (!DontDestroy.DontDestroyData.saved)
            {
                _BaseData._BaseDataSave.DecalActiveMode = 2;
                ObjectCover._objectcover.forceActiveLid();
                DecalControl2._decalControl.loadDefault(true);
            }
            else
            {
                if (_BaseData._BaseDataSave.openLid) { ObjectCover._objectcover.forceActiveLid(); }
                else { ObjectCover._objectcover.forceCloseLid(); }
                DecalControl2._decalControl.loadDefault(false);
            }
        }
        else if (_BaseData._BaseDataSave.selectedID == 4)
        {
            if (!DontDestroy.DontDestroyData.saved)
            {
                _BaseData._BaseDataSave.DecalActiveMode = 1;
                ObjectCover._objectcover.forceActiveLid();
                DecalControl2._decalControl.loadDefault(true);
            }
            else
            {
                if (_BaseData._BaseDataSave.openLid) { ObjectCover._objectcover.forceActiveLid(); }
                else { ObjectCover._objectcover.forceCloseLid(); }
                DecalControl2._decalControl.loadDefault(false);
            }
        }
        else
        {
            DecalControl2._decalControl.loadDefault(true);
            _BaseData._BaseDataSave.DecalActiveMode = 0;
        }
        if (_BaseData._BaseDataSave.selectedID == 4)
        {

            currentObject.gameObject.GetComponent<_CanEdit>().CoverObject.transform.localPosition
              = new Vector3(0, currentObject.transform.localPosition.y * -2
              - currentObject.gameObject.GetComponent<_CanEdit>().ringObject.GetComponent<Renderer>().bounds.extents.y, 0);

        }
        ShapeSummary._volumscript.RunVolume(); 
        saveAll();
        DecalControl2._decalControl.RefreashDecalPos();
        if (_BaseData._BaseDataSave.openWireframe) activeWireframeAlt(true);
    }
    // 1 Width; 2 Length; 3 Height; 4 slope; 5 curve;
    public void EditWidth(float value)
    {
        if (_BaseData._BaseDataSave.selectedID == 1)
        {
            BoxObjController.width = Mathf.Clamp(value, 4, 60);
        }
        else if (_BaseData._BaseDataSave.selectedID == 2)
        {
            CupObjController.width = Mathf.Clamp(value, 4, 60);
            CupObjController.length = Mathf.Clamp(value, 4, 60);
        }
    }
    public void EditLength(float value)
    {
        if (_BaseData._BaseDataSave.selectedID == 1)
        {
            BoxObjController.length = Mathf.Clamp(value, 4, 60);
        }
        else if (_BaseData._BaseDataSave.selectedID == 2)
        {
            CupObjController.length = Mathf.Clamp(value, 4, 60);
            CupObjController.width = Mathf.Clamp(value, 4, 60);
        }
    }
    public void EditHeight(float value)
    {
        if (_BaseData._BaseDataSave.selectedID == 1)
        {
            BoxObjController.height = Mathf.Clamp(value, .5f, 10);
            currentObject.transform.localPosition = new Vector3(0, -currentObjectRenderer.bounds.extents.y, 0);
        }
        else if (_BaseData._BaseDataSave.selectedID == 2)
        {
            CupObjController.height = Mathf.Clamp(value, .5f, 20);
            currentObject.transform.localPosition = new Vector3(0, -currentObjectRenderer.bounds.extents.y, 0);
        }
        else if (_BaseData._BaseDataSave.selectedID == 4)
        {
            CanObjController.height = Mathf.Clamp(value, 2f, 15);
            currentObject.gameObject.GetComponent<_CanEdit>().CoverObject.transform.localPosition
                = new Vector3(0, currentObject.transform.localPosition.y * -2
              - currentObject.gameObject.GetComponent<_CanEdit>().ringObject.GetComponent<Renderer>().bounds.extents.y, 0);
            currentObject.transform.localPosition = new Vector3(0, -currentObjectRenderer.bounds.extents.y
                - currentObject.gameObject.GetComponent<_CanEdit>().ringObject.GetComponent<Renderer>().bounds.size.y, 0);
        }

    }
    public void EditSlope(float value)
    {
        if (_BaseData._BaseDataSave.selectedID == 1)
        {
            BoxObjController.slope = Mathf.Clamp(value * 2 + _BaseData._BaseDataSave.Saveslope, 1, 25);
        }
        else if (_BaseData._BaseDataSave.selectedID == 2)
        {
            CupObjController.slope = Mathf.Clamp(value + _BaseData._BaseDataSave.Saveslope, 1, 7);
        }
    }
    public void EditCurv(float value)
    {
        if (_BaseData._BaseDataSave.selectedID == 1)
        {
            BoxObjController.flexibleCurve = Mathf.Clamp(value + _BaseData._BaseDataSave.Savecurve, .1f, 8);
        }

    }

    public void saveAll()
    {
        if (_BaseData._BaseDataSave.selectedID == 1)
        {
            _BaseData._BaseDataSave.saveWidth = BoxObjController.width;
            _BaseData._BaseDataSave.SaveLength = BoxObjController.length;
            _BaseData._BaseDataSave.SaveHeight = BoxObjController.height;
            _BaseData._BaseDataSave.Saveslope = BoxObjController.slope;
            _BaseData._BaseDataSave.Savecurve = BoxObjController.flexibleCurve;

        }
        else if (_BaseData._BaseDataSave.selectedID == 2)
        {
            _BaseData._BaseDataSave.saveWidth = CupObjController.width;
            _BaseData._BaseDataSave.SaveLength = CupObjController.length;
            _BaseData._BaseDataSave.SaveHeight = CupObjController.height;
            _BaseData._BaseDataSave.Saveslope = CupObjController.slope;

        }
        else if (_BaseData._BaseDataSave.selectedID == 4)
        {
            _BaseData._BaseDataSave.SaveHeight = CanObjController.height;
        }
        _BaseData._BaseDataSave.SavePos = -currentObjectRenderer.bounds.extents.y;
    }
    public void activeEditMaterial(bool isactive)
    {
        if (isactive)
        {
            foreach (GameObject go in allObjList)
            {
                go.GetComponent<Renderer>().material = editingMaterial;
            }
            if (_BaseData._BaseDataSave.selectedID < 3)
            {
                allObjList[allObjList.Count - 1].GetComponent<Renderer>().enabled = false;
            }
            if (allObjList[0].GetComponent<PathGenerator>())
            { allObjList[0].GetComponent<PathGenerator>().enabled = true; }

            ObjectSizeScript._objectSizeScript.ActiveSizeDetail(true);
            ObjectSizeScript._objectSizeScript.chceckCurrFace();
        }
        else
        {
            foreach (GameObject go in allObjList)
            {
                go.GetComponent<Renderer>().material = standardMaterial;
            }
            if (_BaseData._BaseDataSave.selectedID < 3)
            {
                allObjList[allObjList.Count - 1].GetComponent<Renderer>().enabled = true;
                allObjList[allObjList.Count - 1].GetComponent<Renderer>().material = CoverMaterial;
            }
            if (allObjList[0].GetComponent<PathGenerator>())
            { allObjList[0].GetComponent<PathGenerator>().enabled = false; }

            ObjectSizeScript._objectSizeScript.ActiveSizeDetail(false);
        }
        DecalControl2._decalControl._setActiveDecalObject(!isactive);
    }

    public void activeWireframe()
    {
        bool active = _BaseData._BaseDataSave.openWireframe;
        List<GameObject> WireframeSwapList = new List<GameObject>();
        switch (_BaseData._BaseDataSave.selectedID)
        {
            case 1: WireframeSwapList = boxedit.AllObject; break;
            case 2: WireframeSwapList = cupedit.AllObject; break;
            case 3: WireframeSwapList = sandWedit.AllObject; break;
            case 4: WireframeSwapList = canedit.AllObject; break;
            case 5: WireframeSwapList = cartonedit.AllObject; break;
        }
        // int max = _BaseData._BaseDataSave.openLid ? WireframeSwapList.Count : WireframeSwapList.Count - 1;
        if (active)
        {
            foreach (GameObject go in WireframeSwapList)
            {
                go.GetComponent<WireframeShader>().enabled = true;
                go.GetComponent<Renderer>().enabled = false;
            }
            if (!_BaseData._BaseDataSave.openLid)
                WireframeSwapList[WireframeSwapList.Count - 1].transform.GetChild(0).gameObject.SetActive(false);

            ObjectSizeScript._objectSizeScript.ActiveSizeDetail(false);
            ObjectSizeScript._objectSizeScript.chceckCurrFace();
        }
        else
        {
            foreach (GameObject go in WireframeSwapList)
            {
                go.GetComponent<Renderer>().enabled = true;
                go.GetComponent<WireframeShader>().enabled = false;
                Destroy(go.transform.GetChild(go.transform.childCount - 1).gameObject);
            }
            if (!_BaseData._BaseDataSave.openLid)
                WireframeSwapList[WireframeSwapList.Count - 1].GetComponent<Renderer>().enabled = false;
            ObjectSizeScript._objectSizeScript.ActiveSizeDetail(false);
        }
        //  DecalControl2._decalControl._setActiveDecalObject(_BaseData._BaseDataSave.DecalActiveMode != 0);

    }
    public void activeWireframeAlt(bool x)
    {
        _BaseData._BaseDataSave.openWireframe = x;
        bool ac = x;
        List<GameObject> WireframeSwapList = new List<GameObject>();
        switch (_BaseData._BaseDataSave.selectedID)
        {
            case 1: WireframeSwapList = boxedit.AllObject; break;
            case 2: WireframeSwapList = cupedit.AllObject; break;
            case 3: WireframeSwapList = sandWedit.AllObject; break;
            case 4: WireframeSwapList = canedit.AllObject; break;
            case 5: WireframeSwapList = cartonedit.AllObject; break;
        }

        if (ac)
        {
            foreach (GameObject go in WireframeSwapList)
            {
                go.GetComponent<WireframeShader>().enabled = true;
                go.GetComponent<Renderer>().enabled = false;
            }
            if (!_BaseData._BaseDataSave.openLid)
                WireframeSwapList[WireframeSwapList.Count - 1].transform.GetChild(0).gameObject.SetActive(false);
            ObjectSizeScript._objectSizeScript.ActiveSizeDetail(false);
            ObjectSizeScript._objectSizeScript.chceckCurrFace();
        }
        else
        {
            foreach (GameObject go in WireframeSwapList)
            {
                go.GetComponent<Renderer>().enabled = true;
                go.GetComponent<WireframeShader>().enabled = false;
                Destroy(go.transform.GetChild(go.transform.childCount - 1).gameObject);
            }
            if (!_BaseData._BaseDataSave.openLid)
                WireframeSwapList[WireframeSwapList.Count - 1].GetComponent<Renderer>().enabled = false;
            ObjectSizeScript._objectSizeScript.ActiveSizeDetail(false);
        }
    }
    public void setactiveLid(bool setActive)
    {
        switch (_BaseData._BaseDataSave.selectedID)
        {
            case 1:
                {
                    if (_BaseData._BaseDataSave.openWireframe)
                        boxedit.CoverObject.transform.GetChild(0).gameObject.SetActive(setActive);
                    else
                        boxedit.CoverObject.GetComponent<Renderer>().enabled = setActive;
                    break;
                }
            case 2:
                {
                    if (_BaseData._BaseDataSave.openWireframe)
                        cupedit.CoverObject.transform.GetChild(0).gameObject.SetActive(setActive);
                    else
                        cupedit.CoverObject.GetComponent<Renderer>().enabled = setActive;
                    break;
                }
            case 4:
                {
                    if (_BaseData._BaseDataSave.openWireframe)
                        canedit.CoverObject.transform.GetChild(0).gameObject.SetActive(setActive);
                    else
                        canedit.CoverObject.GetComponent<Renderer>().enabled = setActive;
                    break;
                }
        }
    }
}


