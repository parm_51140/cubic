using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuGroupControl : MonoBehaviour
{
    public static MainMenuGroupControl _mainmenugroupcontrol;
    Main mainControl; // remove 
    [SerializeField] Share share;// remove
    [SerializeField] UserLogin userLogin;// remove
    SubMenuBar submenuControl;

    [SerializeField] RectTransform MainMenuGroup;
    [SerializeField] RectTransform SubMenuGroupParent;
    RectTransform SubMenuGroup;

    Vector3 barStartPos;
    public Vector2 ScreenScale;

    public bool submoving;
    public bool movesnap;
    bool holding = false;
    bool isshowSubmenu;

    float holdtime;
    float startTouch, deltaTouch;
    float SnapPos;

    public int subMenuSelected;
    int menuCount;
    int menuSelected;//1-menuCount 
    public int lastMenuSelected;

    private void Awake()
    {
        _mainmenugroupcontrol = this;
    }
    void Start()
    {
        mainControl = Main.MainControl;

        ScreenScale = gameObject.transform.parent.transform.localScale;
        menuCount = MainMenuGroup.childCount;
        if (DontDestroy.DontDestroyData.saved)
            forceCallMenu(5);
        else
            forceCallMenu(3);
    }

    void Update()
    {
        if (holding)
        {
            holdtime += Time.deltaTime;
        }
        if (movesnap)
        {
            Vector3 tempPos = MainMenuGroup.anchoredPosition;
            tempPos.x = tempPos.x + (SnapPos - tempPos.x) / 8;
            MainMenuGroup.anchoredPosition = tempPos;
            if (SnapPos - tempPos.x > -1 && SnapPos - tempPos.x < 1)
            {
                movesnap = false;
                //  if (lastMenuSelected != _BaseData._BaseDataSave.MainmenuSelect)
                //  {
                //
                //      showSubmenuGroup(_BaseData._BaseDataSave.MainmenuSelect);
                //      launchMainMenu();
                //  }
            }
        }
    }
    public void mouseDown()
    {
        SoundPanel._SoundPanel.UIClickPlay();
        if (movesnap) movesnap = false;
        holding = true;
        holdtime = 0;
        deltaTouch = 0;
        startTouch = Input.mousePosition.x;
        barStartPos = MainMenuGroup.anchoredPosition * ScreenScale.x;
        lastMenuSelected = menuSelected;
    }
    public void mouseDrag()
    {
        deltaTouch = (Input.mousePosition.x - startTouch);
        Vector3 temppos = barStartPos;
        temppos.x = Mathf.Clamp(barStartPos.x + deltaTouch, -MainMenuGroup.sizeDelta.x * ScreenScale.x, 0);
        MainMenuGroup.anchoredPosition = temppos / ScreenScale.x;
    }
    public void mouseUp()
    {
        holding = false;
        if (deltaTouch != 0)
        {
            menuSelected = (int)Mathf.Floor(MainMenuGroup.anchoredPosition.x / -190f) + 1; //icon width 150 + pad left 20 + pad right 20      
            menuSelected = Mathf.Clamp(menuSelected, 1, menuCount);
        }
        else // 1080px 65 + 190*menucount   Input.mousePosition.x/ScreenScale.x
        {
            int touchBlock = Mathf.FloorToInt((Input.mousePosition.x / ScreenScale.x - 65) / 190);

            if (touchBlock != 2)
                menuSelected = Mathf.Clamp(menuSelected + (touchBlock - 2), 1, menuCount);
        }
        _BaseData._BaseDataSave.MainmenuSelect = menuSelected;
        SnapPos = _BaseData._BaseDataSave.MainmenuSelect * -190 + 95;

        showSubmenuGroup(_BaseData._BaseDataSave.MainmenuSelect);
        launchMainMenu();

        selectedMenuColor();
        movesnap = true;


    }

    public void forceCallMenu(int menu)
    {
        menuSelected = menu;
        _BaseData._BaseDataSave.MainmenuSelect = menu;
        SnapPos = _BaseData._BaseDataSave.MainmenuSelect * -190 + 95;
        movesnap = true;
        showSubmenuGroup(_BaseData._BaseDataSave.MainmenuSelect);
        selectedMenuColor();
        launchMainMenu();
    }
    public void forceCallSubMenu(int mainmenu,int submenu)
    {
        print(mainmenu + " " + submenu);
        SubMenuGroupParent.transform.GetChild(mainmenu - 1).gameObject.GetComponent<SubMenuBar>().forceCallSub(submenu);
       // launchMainMenu();
    }

    void selectedMenuColor()
    {
        for (int i = 0; i < menuCount; i++)
        {
            MainMenuGroup.transform.GetChild(i).GetComponent<Image>().color = Color.white;
            MainMenuGroup.transform.GetChild(i).GetChild(0).GetComponent<Text>().color = Color.white;
        }
        MainMenuGroup.transform.GetChild(_BaseData._BaseDataSave.MainmenuSelect - 1).GetComponent<Image>().color = Color.yellow;
        MainMenuGroup.transform.GetChild(_BaseData._BaseDataSave.MainmenuSelect - 1).GetChild(0).GetComponent<Text>().color = Color.yellow;

    }

    public void showSubmenuGroup(int currMainMenu)
    {
        if (SubMenuGroup != null) SubMenuGroup.gameObject.SetActive(false);
        SubMenuGroup = SubMenuGroupParent.transform.GetChild(currMainMenu - 1).GetComponent<RectTransform>();
        SubMenuGroup.gameObject.SetActive(true);
        submenuControl = SubMenuGroup.GetComponent<SubMenuBar>();
    }

    public void launchMainMenu()
    {

        if (_BaseData._BaseDataSave.MainmenuSelect == 3)
        {
            if (lastMenuSelected != 3) FreehandCamera_R.freehandCamera.SwapCameraMoveMode(false);
            SaveObject._saveobjBtn.showSaveBtn(true);
            PlasticThickness._thicknessUI.ActiveSlide(false);
            DecalSlide._decalSlide.DecalDiscHide();
            if (subMenuSelected == 1)
            {
               
                if (TouchControl._ScreenTouchControl.IsEditingDecal) TouchControl._ScreenTouchControl.forceExitEdit();
                ToggleMenuGroup._toggleMenu.setEnabledToggle(true);
                if (DecalControl2._decalControl.EditingDecal)
                { DecalControl2._decalControl.ActiveEditDecal(false); }
            }                             
            if (subMenuSelected == 2)
            {
                PlasticThickness._thicknessUI.ActiveSlide(true);               
                if (_BaseData._BaseDataSave.openWireframe) ToggleMenuGroup._toggleMenu.toggleSelect(2);
                TouchControl._ScreenTouchControl.forceExitEdit();
            }
           
            if (subMenuSelected == 3)
            {
                if (_BaseData._BaseDataSave.openWireframe) ToggleMenuGroup._toggleMenu.toggleSelect(2);
                if (TouchControl._ScreenTouchControl.IsEditing) TouchControl._ScreenTouchControl.forceExitEdit();
                DecalSlide._decalSlide.DecalDiscActive();
            }
            WorldRulerSetting._worldruler.resetScale();
            ToggleMenuGroup._toggleMenu.ToggleSubmenuActive(0, true);
            _BaseData._BaseDataSave.usedEditCamera = true;
            mainControl.currentEditFaceSetting(true);

        }
        else
        {
            SaveObject._saveobjBtn.showSaveBtn(false);
            _BaseData._BaseDataSave.usedEditCamera = false;
            ToggleMenuGroup._toggleMenu.setEnabledToggle(true);
            ToggleMenuGroup._toggleMenu.ToggleSubmenuActive(0, false);
            mainControl.currentEditFaceSetting(false);
            ToggleMenuGroup._toggleMenu.forceCloseIso();
            DecalSlide._decalSlide.DecalDiscHide();
            if (DecalControl2._decalControl.EditingDecal) { DecalControl2._decalControl.ActiveEditDecal(false); }
            if (lastMenuSelected == 3) FreehandCamera_R.freehandCamera.SwapCameraMoveMode(true);
            PlasticThickness._thicknessUI.ActiveSlide(false);
            TouchControl._ScreenTouchControl.forceExitEdit();
        }

        if (_BaseData._BaseDataSave.MainmenuSelect == 5)
        {
            mainControl.activeCmeraBG(subMenuSelected == 2);
        }
        else if (_BaseData._BaseDataSave.MainmenuSelect == 1)
        {
            FreehandCamera_R.freehandCamera._clearFreehand();
            //  mainControl.swapmesh(subMenuSelected);
            EditableObjectMaster._editmaster.reSpawnObject(subMenuSelected);
        }
       
        SavedGallery._saveGallery.showGallery(_BaseData._BaseDataSave.MainmenuSelect == 2);
        ObjectSummary._objectSummary.ShowSummary(_BaseData._BaseDataSave.MainmenuSelect == 6);

        if (_BaseData._BaseDataSave.MainmenuSelect == 7)
            userLogin.ShowUser();
        else userLogin.closeUser();


        mainControl.ShapeSummartView(_BaseData._BaseDataSave.MainmenuSelect == 3 && !mainControl.IsEditing);
        // mainControl.SetDecalOption(_BaseData._BaseDataSave.MainmenuSelect == 3 && subMenuSelected == 3);



        //  ToggleMenuGroup._toggleMenu.setEnabledToggle(!(_BaseData._BaseDataSave.MainmenuSelect == 3 && subMenuSelected == 2));



    }

}
