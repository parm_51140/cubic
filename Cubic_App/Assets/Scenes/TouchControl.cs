using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static TMPro.SpriteAssetUtilities.TexturePacker_JsonArray;

public class TouchControl : MonoBehaviour
{
    public static TouchControl _ScreenTouchControl;

    public ShapeControllerBase boxController;
    public Camera ShowCamera;//pos

    public Text touchStat;
    public Text touchCount;
    public Text touchDelta;

    public bool IsEditing;
    public bool IsEditingDecal;

    Touch[] point = new Touch[2];

    float pinchDelta;
    float pinchDeltaPast;
    float dragDelta;

    int direction;// 1:up  2:down 3:right  4:left
    bool isPinch;
    bool swipable;
    bool hold;


    Vector2 camScale;

    Vector3 pastDrag;
    Vector3 startDrag;
    Vector3 dragVector;

    float saveOrtho;
    private void Awake()
    {
        _ScreenTouchControl = this;
    }

    private void Start()
    {
        camScale = new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight);
        saveOrtho = Main.MainControl.orbitCamera.orthographicSize;
    }
    private void Update()
    {

    }
    public void forceExitEdit()
    {

        if (IsEditing)
        {
            IsEditing = false;
            ShapeSummary._volumscript.RunVolume();
            EditableObjectMaster._editmaster.activeEditMaterial(false);
            saveOrtho = Main.MainControl.orbitCamera.orthographicSize;
            DecalControl2._decalControl.RefreashDecalPos();
            WorldRulerSetting._worldruler.clearScale();
            EditableObjectMaster._editmaster.saveAll();
         
        }
        if (IsEditingDecal)
        {
            IsEditingDecal = false;
            DecalControl2._decalControl.ActiveEditDecal(false);
            DecalControl2._decalControl.SaveDecalData();
        }

    }


    public void _Press()
    {
        if (Input.touchCount > 1)
        {
            point[0] = Input.GetTouch(0);
            point[1] = Input.GetTouch(1);
            pinchDelta = (point[0].position - point[1].position).magnitude;
            pinchDeltaPast = pinchDelta;
            hold = false;
        }
        else
        {
            startDrag = Input.mousePosition;
            pastDrag = startDrag;
            hold = true;
        }

        EditableObjectMaster._editmaster.saveAll();
    }
    public void _Drag()
    {
        if (pastDrag != Input.mousePosition) //has dragValue
        {
            hold = false;
            if (Input.touchCount > 1)
            {
                point[0] = Input.GetTouch(0);
                point[1] = Input.GetTouch(1);
                // pinchDelta = (point[0].position - point[1].position).magnitude;
                pinchDelta = pinchDeltaPast - (point[0].position - point[1].position).magnitude;
                pinceAction();

            }
            else
            {
                Vector3 vectorDirection = Input.mousePosition - pastDrag;
                if (Mathf.Abs(vectorDirection.y) > Mathf.Abs(vectorDirection.x))
                {
                    if (vectorDirection.y > 0) direction = 1;
                    else direction = 2;
                }
                else
                {
                    if (vectorDirection.x > 0) direction = 3;
                    else direction = 4;
                }
                dragVector = startDrag - Input.mousePosition;
                dragAction();
            }
            pastDrag = Input.mousePosition;
        }
    }
    public void _UP()
    {
        hold = false;

        if ((startDrag - Input.mousePosition).magnitude < 10)// press
        {
            if (_BaseData._BaseDataSave.MainmenuSelect == 3 && !_BaseData._BaseDataSave.openWireframe)
            {

                if (MainMenuGroupControl._mainmenugroupcontrol.subMenuSelected == 1)
                {
                    IsEditing = !IsEditing;
                    EditableObjectMaster._editmaster.activeEditMaterial(IsEditing);
                    if (IsEditing)
                    {
                        WorldRulerSetting._worldruler.showRuler();
                        ObjectSizeScriptShareImage._sharerefLine.CreateLine();
                        
                    }
                    else
                    {
                       
                        ShapeSummary._volumscript.RunVolume();
                        DecalControl2._decalControl.RefreashDecalPos();
                        WorldRulerSetting._worldruler.clearScale();
                        saveOrtho = Main.MainControl.orbitCamera.orthographicSize;
                        EditableObjectMaster._editmaster.saveAll();
                    }
                }
                if (MainMenuGroupControl._mainmenugroupcontrol.subMenuSelected == 3 && DecalControl2._decalControl.selectAble)
                {
                    IsEditingDecal = !IsEditingDecal;
                    if (IsEditingDecal)
                    {

                        DecalControl2._decalControl.startmoveDecal();
                        ObjectSizeScriptShareImage._sharerefLine.CreateLine();
                    }
                    else
                    {
                        DecalControl2._decalControl.SaveDecalData();
                        EditableObjectMaster._editmaster.saveAll();
                    }
                    DecalControl2._decalControl.ActiveEditDecal(IsEditingDecal);
                }
            }
        }
        else // drag
        {
            if (_BaseData._BaseDataSave.MainmenuSelect == 4) { FreehandCamera_R.freehandCamera.SetStartFreehand(); }
            if (IsEditing)
            {
                checkCameraSize();
            }
            else if (IsEditingDecal)
            {

                DecalControl2._decalControl.startmoveDecal();
            }
        }
        saveOrtho = Main.MainControl.orbitCamera.orthographicSize;
        swipable = true;
    }

    void dragAction()// direction 1:up  2:down 3:right  4:left
    {
        if (IsEditing)
        {
           
            if (Main.MainControl.currVertivalFace == Main.VertivalFace.Side)
            {
                if (direction == 1 || direction == 2)
                {
                    float dragValue = dragVector.y / camScale.y * 10;
                    if (startDrag.y > camScale.y / 2)
                    {
                        dragValue = -dragValue;
                    }
                    EditableObjectMaster._editmaster.EditHeight(dragValue + _BaseData._BaseDataSave.SaveHeight);
                }

                else if (direction == 3 || direction == 4)
                {
                    float dragValue = dragVector.x / camScale.x * 10;

                    if (startDrag.x > camScale.x / 2)
                    {
                        dragValue = -dragValue;
                    }
                    if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front || Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back)
                    {
                        EditableObjectMaster._editmaster.EditLength(dragValue + _BaseData._BaseDataSave.SaveLength);
                    }
                    else
                    {
                        EditableObjectMaster._editmaster.EditWidth(dragValue + _BaseData._BaseDataSave.saveWidth);
                    }
                }
            }
            else
            {
                if (direction == 1 || direction == 2)
                {
                    float dragValue = dragVector.y / camScale.y * 10;
                    if (startDrag.y > camScale.y / 2)
                    {
                        dragValue = -dragValue;
                    }
                    if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front || Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back)
                    {
                        EditableObjectMaster._editmaster.EditWidth(dragValue + _BaseData._BaseDataSave.saveWidth);
                    }
                    else
                    {
                        EditableObjectMaster._editmaster.EditLength(dragValue + _BaseData._BaseDataSave.SaveLength);
                    }
                }
                else if (direction == 3 || direction == 4)
                {
                    float dragValue = dragVector.x / camScale.x * 10;

                    if (startDrag.x > camScale.x / 2)
                    {
                        dragValue = -dragValue;
                    }
                    if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front || Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back)
                    {
                        EditableObjectMaster._editmaster.EditLength(dragValue + _BaseData._BaseDataSave.SaveLength);
                    }
                    else
                    {
                        EditableObjectMaster._editmaster.EditWidth(dragValue + _BaseData._BaseDataSave.saveWidth);
                    }
                }
            }
        }
        else if (IsEditingDecal)
        {
            DecalControl2._decalControl.moveDecalSingle(dragVector / camScale.x * 10);
            // DecalControl2._decalControl.ScaleDecal(-dragVector.x / camScale.x * 10);
        }
        else//non editvalue
        {
            if (_BaseData._BaseDataSave.MainmenuSelect == 3)
            {
                if (swipable)
                {

                    if (direction == 3)
                    {
                        if (dev.cubic.RotationControl._rotationCameraControl.verticalRotation > 179 && dev.cubic.RotationControl._rotationCameraControl.verticalRotation < 271)
                            dev.cubic.RotationControl._rotationCameraControl.Right();
                        else
                            dev.cubic.RotationControl._rotationCameraControl.Left();
                    }
                    else if (direction == 4)
                    {
                        if (dev.cubic.RotationControl._rotationCameraControl.verticalRotation > 179 && dev.cubic.RotationControl._rotationCameraControl.verticalRotation < 271)
                            dev.cubic.RotationControl._rotationCameraControl.Left();
                        else
                            dev.cubic.RotationControl._rotationCameraControl.Right();
                    }

                    else if (direction == 2)
                    {
                        dev.cubic.RotationControl._rotationCameraControl.Down();
                    }
                    else if (direction == 1)
                    {
                        dev.cubic.RotationControl._rotationCameraControl.Up();
                    }
                    swipable = false;
                }
            }
            else if (_BaseData._BaseDataSave.MainmenuSelect == 4)
            { FreehandCamera_R.freehandCamera.FreeHandMove(dragVector); }
        }

    }///end dragAction()

    void pinceAction()
    {
        float pinchValue = (pinchDelta / camScale.x * 10);
        if (_BaseData._BaseDataSave.MainmenuSelect == 4)
        {
            Main.MainControl.orbitCamera.orthographicSize = Mathf.Clamp(saveOrtho + pinchValue, 15, 50);
            //saveOrtho = Main.MainControl.orbitCamera.orthographicSize;
        }
        else if (_BaseData._BaseDataSave.MainmenuSelect == 3)
        {
            if (IsEditing)
            {
               
                if (Main.MainControl.currVertivalFace == Main.VertivalFace.Side)
                {
                    EditableObjectMaster._editmaster.EditSlope(-pinchValue);
                }
                else if (Main.MainControl.currVertivalFace == Main.VertivalFace.Top)
                {
                    //touchDelta.text = (pinchValue + "  " + _BaseData._BaseDataSave.Savecurve);
                    EditableObjectMaster._editmaster.EditCurv(-pinchValue);
                }
            }
            else if (IsEditingDecal)
            {

                DecalControl2._decalControl.ScaleDecal(-pinchValue);
            }
            else
            {
                Main.MainControl.orbitCamera.orthographicSize = Mathf.Clamp(saveOrtho + pinchValue, 15, 50);
            }
        }
    }

    void checkCameraSize()
    {
        float camSize = Main.MainControl.orbitCamera.orthographicSize;
        float objSide = 0;

        if (Main.MainControl.currHorizontalFace == Main.HorizontalFace.Front || Main.MainControl.currHorizontalFace == Main.HorizontalFace.Back)
        {
            objSide = EditableObjectMaster._editmaster.currentObjectRenderer.bounds.size.x;
        }
        else
        {
            objSide = EditableObjectMaster._editmaster.currentObjectRenderer.bounds.size.z;
        }

        if (objSide > camSize) { StartCoroutine(cameraLerp(objSide)); }
    }

    IEnumerator cameraLerp(float boundValue)
    {
        float camTarget = Mathf.Clamp(boundValue + 10, 15, 50);
        float camStart = Main.MainControl.orbitCamera.orthographicSize;
        float curr, t = 0;

        while (t < 1)
        {
            curr = Mathf.Lerp(camStart, camTarget, t);
            Main.MainControl.orbitCamera.orthographicSize = curr;
            t += 0.08f;
            yield return new WaitForSeconds(0.03f);
        }
        Main.MainControl.orbitCamera.orthographicSize = camTarget;
        GridBackGround._GridBackGround.resetGrideBG();
        WorldRulerSetting._worldruler.resetScale();
        yield return null;
    }


}
