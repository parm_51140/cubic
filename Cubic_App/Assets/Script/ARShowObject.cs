using Google.Impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class ARShowObject : MonoBehaviour
{
    // public LigthTest lt;
    public GameObject boxObj;
    public GameObject lidObj;
    public AR_OUT ar_out;
    public GameObject LightSet;
    public ARCreateGrid gridPlaneScript;
    public GameObject gridPlane;

    public GameObject boxShow;
    public GameObject boxShowBase;
    public GameObject canShowring;
    public GameObject canShowTop;
    public GameObject canShowbase;
    public GameObject LightOb;


    ARRaycastManager _aRRaycastManager;
    Vector2 touchPos;

    public bool ismovemode;
    public Text rr;
    public Sprite[] moveToggle;
    public Image moveModeBtn;

    Vector3 StartTouch;
    bool hold;
    Vector3 opos;
    float dragVAlue;

    float frustumHeight;
    float frustumWidth;

    [SerializeField] ARDecal dc;

    Camera cam;
    RaycastHit hit;
    Ray ray;
    static List<ARRaycastHit> hits = new List<ARRaycastHit>();
    public Image FadeBlockIm;
    public static ARShowObject _aRShowObject;

    float rotatesave;
    float rotatestart;
    float boxRotate;
    bool holdrotate;
    float fingerRotate;

    private void Awake()
    {
        _aRShowObject = this;
    }
    private void Start()
    {

        _aRRaycastManager = GetComponent<ARRaycastManager>();
        ismovemode = false;
        moveModeBtn.color = Color.clear;
        cam = Camera.main;

        // print(pos);
        // frustumHeight = 2.0f * blockPlane.transform.localPosition.z * Mathf.Tan(Camera.main.fieldOfView * 0.5f * Mathf.Deg2Rad);
        //  frustumWidth = frustumHeight * Camera.main.aspect;
        // print(moveModeBtn.sprite.name);     
#if !UNITY_EDITOR
        boxObj = DontDestroy.DontDestroyData.ShowObj;
#else
        Invoke("instantCreate", 1);
#endif


        // Invoke("instantCreate", 1);
    }
    void instantCreate()
    {

        for (int i = 0; i < DontDestroy.DontDestroyData.allObject.Count; i++)
        {
            GameObject tempG;
            if (i == 0)
            {//hit.point
                tempG = Instantiate(DontDestroy.DontDestroyData.allObject[i], hit.point, Quaternion.identity);
                tempG.layer = 11;
                boxShow = tempG;
            }
            else
            {
                tempG = Instantiate(DontDestroy.DontDestroyData.allObject[i], gameObject.transform.localPosition, Quaternion.identity);
                tempG.transform.parent = boxShow.transform;
                tempG.layer = 11;
                tempG.transform.localPosition = Vector3.zero;
            }
            tempG.transform.localScale = Vector3.one;
            if (i == DontDestroy.DontDestroyData.allObject.Count - 1)
            {
                tempG.layer = 12;
                if (_BaseData._BaseDataSave.selectedID < 3 && !_BaseData._BaseDataSave.openLid)
                {
                    tempG.SetActive(false);
                }
                else if (_BaseData._BaseDataSave.selectedID == 4)
                {
                    float offset = DontDestroy.DontDestroyData.allObject[4].transform.localPosition.y * 2
                        + boxShow.transform.GetChild(2).gameObject.GetComponent<Renderer>().bounds.extents.y;
                    tempG.transform.localPosition = new Vector3(0, offset, 0);
                }
            }
            tempG.AddComponent<MeshCollider>();

        }
        boxShow.transform.localScale = Vector3.one / 100;
        //  boxShow = Instantiate(DontDestroy.DontDestroyData.ShowObj, hit.point, Quaternion.identity);
        //  boxShow.GetComponentInChildren<MeshRenderer>().gameObject.AddComponent<MeshCollider>();
        //  boxShow.transform.localScale = Vector3.one;
        //
        //  boxShowBase = Instantiate(DontDestroy.DontDestroyData.ShowObjBase, boxShow.transform);
        //  boxShowBase.transform.localScale = Vector3.one;
        //  boxShowBase.GetComponentInChildren<MeshRenderer>().gameObject.AddComponent<MeshCollider>();
        //
        //  if(_BaseData._BaseDataSave.selectedID==4)
        //  {
        //      canShowbase = Instantiate(DontDestroy.DontDestroyData.ShowObjcanbase, boxShow.transform);         
        //      canShowbase.transform.localScale = Vector3.one;         
        //
        //      canShowring = Instantiate(DontDestroy.DontDestroyData.ShowObjcanring, boxShow.transform);           
        //      canShowring.transform.localScale = Vector3.one;
        //
        //      canShowTop = Instantiate(DontDestroy.DontDestroyData.ShowObjcantop, boxShow.transform);
        //      canShowTop.transform.localPosition = new Vector3(0, boxShow.GetComponentInChildren<MeshRenderer>().bounds.size.y, 0);
        //      canShowTop.transform.localScale = Vector3.one;
        //  }
        //  boxShow.transform.localScale = Vector3.one / 100;
        //
        //  lidObj.transform.parent = boxShow.transform;
        //  lidObj.SetActive(true);
        //  lidObj.transform.localScale = Vector3.one;
        //  lidObj.transform.localPosition = new Vector3(0, boxShow.GetComponentInChildren<Renderer>().bounds.extents.y * 10, 0);
        //  GameObject ll = Instantiate(LightSet, boxShow.transform);
        //  lt.setligthOB(ll);
        dc.gameObject.transform.parent = boxShow.transform;
       
        dc.transform.localPosition = new Vector3(0, -_BaseData._BaseDataSave.SavePos, 0);
        if (_BaseData._BaseDataSave.selectedID == 4)
        { dc.transform.localPosition = new Vector3(0, -_BaseData._BaseDataSave.SavePos + boxShow.transform.GetChild(2).gameObject.GetComponent<Renderer>().bounds.extents.y*100, 0);
            print(-_BaseData._BaseDataSave.SavePos + "   " + boxShow.transform.GetChild(2).gameObject.GetComponent<Renderer>().bounds.extents.y);
        }
        dc.transform.localScale = Vector3.one;
        dc.editObj = boxShow.GetComponentInChildren<Renderer>();
        dc.lidObj = lidObj;
        dc.SetSelf();

        LightOb = Instantiate(LightSet, boxShow.transform.position, Quaternion.identity);
        if (gridPlane == null)
        {
            gridPlaneScript.CreateLine(boxShow.GetComponent<Renderer>().bounds.size.x * 100 + 15, boxShow.GetComponent<Renderer>().bounds.size.z * 100 + 15);
            gridPlane = gridPlaneScript.gameObject;
            gridPlane.transform.parent = boxShow.transform;
            gridPlane.transform.localPosition = new Vector3(-gridPlaneScript.MidPoint.x, -boxShow.GetComponent<Renderer>().bounds.size.y * 10, -gridPlaneScript.MidPoint.x);
            gridPlane.transform.rotation = boxShow.transform.rotation;
            //  gridPlane.transform.localPosition = gridPlaneScript.MidPoint;
        }
    }
    public void SwapMode()
    {
        ismovemode = !ismovemode;
        if (ismovemode)
        { moveModeBtn.sprite = moveToggle[0]; }
        else
        { moveModeBtn.sprite = moveToggle[1]; }
    }
    private void OnDisable()
    {
        Destroy(boxShow);
    }

    bool TryGetTouch(out Vector2 touchPos)
    {
        if (Input.touchCount > 0)
        {
            touchPos = Input.GetTouch(0).position;
            return true;
        }
        touchPos = default;
        return false;
    }

    bool checkTouchUI(Vector2 pose)
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {

            return false;
        }
        PointerEventData eventPos = new PointerEventData(EventSystem.current);
        eventPos.position = new Vector2(pose.x, pose.y);
        List<RaycastResult> result = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventPos, result);

        return result.Count > 0;

    }
    Vector3 pos = new Vector3(.5f, .5f, 0);
    void Update()
    {
        if (LightOb != null)
        {
            LightOb.transform.position = boxShow.transform.position;
            LightOb.transform.eulerAngles = boxShow.transform.eulerAngles;
        }

        //ray = cam.ViewportPointToRay(pos);
        if (!TryGetTouch(out Vector2 touchPos) && boxShow == null)
        {
            ray = cam.ViewportPointToRay(pos);
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.distance > 1) return;
                instantCreate();
                ar_out.obj = boxShow;
                opos = boxShow.transform.eulerAngles;
                if (gridPlane == null)
                {
                    gridPlaneScript.CreateLine(boxShow.GetComponent<Renderer>().bounds.size.x * 100 + 15, boxShow.GetComponent<Renderer>().bounds.size.z * 100 + 15);
                    gridPlane = gridPlaneScript.gameObject;
                    gridPlane.transform.parent = boxShow.transform;
                    gridPlane.transform.localPosition = new Vector3(-gridPlaneScript.MidPoint.x, -boxShow.GetComponent<Renderer>().bounds.size.y * 10, -gridPlaneScript.MidPoint.x);
                    gridPlane.transform.rotation = boxShow.transform.rotation;
                }
                StartCoroutine(FadeBlock());
            }

            hold = false;
            moveModeBtn.color = Color.clear;
            return;
        }
        if (_aRRaycastManager.Raycast(touchPos, hits, TrackableType.PlaneWithinPolygon) && !checkTouchUI(touchPos) && Input.touchCount == 1)
        {
            if (holdrotate) return;
            var hitPos = hits[0].pose;
            if (Input.touchCount == 1)
            {
                if (!hold)
                {
                    hold = true;
                    StartTouch = hitPos.position;
                }
                if (Vector3.Distance(StartTouch, hitPos.position) < .1f) return;
                moveModeBtn.color = Color.white;
                moveModeBtn.sprite = moveToggle[0];
                boxShow.transform.position = hitPos.position;
                Vector3 boxOffset = boxShow.transform.position;
                boxOffset.y = boxOffset.y + boxShow.GetComponentInChildren<Renderer>().bounds.extents.y / 10;
                boxShow.transform.position = boxOffset;

                rr.text = hitPos.position + " " + boxShow.transform.position;

            }

        }
        if (Input.touchCount == 1)
        {
            if (boxShow != null && holdrotate)
            {
                rotatesave = boxShow.transform.eulerAngles.y;
                holdrotate = false;
            }
        }
        else if (Input.touchCount == 2)//rotate
        {
            moveModeBtn.color = Color.white;
            moveModeBtn.sprite = moveToggle[1];
            fingerRotate = angleMove(Input.GetTouch(0).position, Input.GetTouch(1).position);
            if (!holdrotate)
            {
                rotatestart = fingerRotate;
                holdrotate = true;
            }
            Vector3 t = boxShow.transform.eulerAngles;
            t.y = rotatesave + (fingerRotate - rotatestart);


            boxShow.transform.eulerAngles = t;

        }
        else if (Input.touchCount == 0)
        {
            // rr.text = rotatesave + "blank";
            if (boxShow != null)
            {
                holdrotate = false;
                hold = false;
                rotatesave = boxShow.transform.eulerAngles.y;
                //  rotatesave = boxShow.transform.eulerAngles.y - fingerRotate; 
            }
        }

    }
    float angleMove(Vector2 pointA, Vector2 pointB)
    {
        float xDelta = pointA.x - pointB.x;
        float yDelta = pointA.y - pointB.y;
        return (float)Mathf.Atan2(xDelta, yDelta) * (float)(180f / Mathf.PI);
    }
    IEnumerator FadeBlock()
    {

        Color startColor = new Color(0, 0, 0, .5f);
        Color endColor = Color.clear;
        Color TmpC;
        float t = 0f;
        while (t < 1)
        {
            TmpC = Color.Lerp(startColor, endColor, t);
            FadeBlockIm.color = TmpC;
            t = t + .02f;
            yield return new WaitForSeconds(.03f);
        }
        FadeBlockIm.gameObject.SetActive(false);
        yield return null;

    }
}
