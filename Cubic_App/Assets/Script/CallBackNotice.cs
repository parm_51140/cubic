using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CallBackNotice : MonoBehaviour
{
    public static CallBackNotice _callBackNotice;
    [SerializeField] Text noticText;
    [SerializeField] RectTransform noticBox;
    Coroutine noticprocess;
    private void Awake()
    {
        _callBackNotice = this;
    }

    public void _noticBark(string word)
    {
        noticText.text = word;
        noticBox.gameObject.SetActive(true);
        if (noticprocess != null) StopCoroutine(noticprocess);
        noticprocess = StartCoroutine(delayNotice());
    }
    IEnumerator delayNotice() 
    {
        yield return new WaitForSeconds(2f);
        noticBox.gameObject.SetActive(false);

    }

}
