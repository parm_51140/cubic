using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using dev.cubic;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class Main : MonoBehaviour
{
    [SerializeField] MainMenuControl mainMenu;
    [SerializeField] ObjectReshape reshape;
    [SerializeField] SubMenuControl subMenu;
    public FreeHandCamera freehand;
    [SerializeField] IsometricToggle isoView;
   //wf
    [SerializeField] ShapeSummary shapessummary;
    [SerializeField] RotationControl rotateOrbitControl;
    [SerializeField] ObjectSummary objectSummaryToggle;
    public ObjectCover objCover;
    [SerializeField] ToggleMenuGroup toggleMenu;
    public DecalControl decalCon;

    public Camera mainCamera;
    public Camera orbitCamera;
    [SerializeField] PointTouch TouchPanel;
   
    [SerializeField] GameObject arSwitch;
    [SerializeField] GameObject CameraBackGround;
    [SerializeField] GameObject ThicknessSlide;
    [SerializeField] GameObject CurrentEditFace;

    [SerializeField] GameObject IsoViewPanel;

    [SerializeField] WorldRulerSetting RulerHorz;
    [SerializeField] WorldRulerSetting RulerVert;

    public Material defaultMat;
    public Material EditingtMat;
   

    public enum HorizontalFace { Front, Left, Back, Right };
    public enum VertivalFace { Top, Side, Bottom };
    public bool revertVertical;
    public HorizontalFace currHorizontalFace;
    public VertivalFace currVertivalFace;



    public int currentMainMenu = 1;
    public int LastMainMenu = 1;
    public int currentSubmenuMenu = 1;
    public int ObjScaleOffset = 3;
    public int CoverType = 0;

    public bool IsEditing;
    public bool IsEditingLabel;
    public bool WaitTransformToEdit;
    public bool WaitEditRotation;
    public bool lidOn;
    public static Main MainControl;
    public static Scene ARSCENE;
    public bool istoggleWireFrame = false;
    public bool istoggleIsoCam = false;
    public DecalControl decalControl;
    public GetShapeViewTexture shapeView;

    private void Awake()
    {
     
        MainControl = this;         

        shapeView.SetTexture();

    }
    void Start()
    {

     //  reshape = ObjectReshape.objReshape.GetComponent<ObjectReshape>();
     //  currentMainMenu = 1;
     //  currentSubmenuMenu = 1;
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);
        }
#endif
    }

    private void Update()
    {

    }
    public void startAR()// not Use
    {          
       // DontDestroy.DontDestroyData.ShowObjLid.SetActive(false);
        DontDestroy.DontDestroyData.saved = true;
        //  DontDestroy.DontDestroyData.SaveData(ObjectReshape.objReshape);
        DontDestroy.DontDestroyData.SaveData();
    }
    public void toggleMenuActibeBtn(int btnIndex, bool isActive)// notUse
    {
        toggleMenu.ToggleSubmenuActive(btnIndex, isActive);
    }
   // public void setMainMenu(int mainmenu) { currentMainMenu = mainmenu; }
   // public void setSubMenu(int submenu) { currentSubmenuMenu = submenu; }

    public void toggleWireFrame()// notUse
    {
        
        istoggleWireFrame = !istoggleWireFrame;
        reshape.activeWireframe(istoggleWireFrame);
        toggleMenu.ToggleSubmenuUsing();
        decalControl.activeDecal(!istoggleWireFrame);
    }
    public void toggleWireFrame(bool value)// notUse
    {
       
        istoggleWireFrame = value;
        reshape.activeWireframe(istoggleWireFrame);
        decalControl.activeDecal(!istoggleWireFrame&&IsEditing);
        toggleMenu.ToggleSubmenuUsing();
    }

  //public void Hidecompass(bool com)
  //{
  //    if (com)
  //    {
  //        compass.transform.localScale = Vector3.zero;
  //        compassV.transform.localScale = Vector3.zero;
  //    }
  //
  //    else
  //    {
  //        compass.transform.localScale = Vector3.one;
  //        compassV.transform.localScale = Vector3.one;
  //    }
  //}
    public void resetCamScale()// notUse
    {
        mainCamera.orthographicSize = 9.6f;
    }
    public void cancleEditMode() 
    { 
        TouchPanel.forceExitEdit();
        decalCon.exitDecalEdit();
    }

    public void closeRuler()// notUse
    {
        RulerHorz.clearScale();
      //  RulerVert.clearScale();
    }
    public void resetRuler()// notUse
    {
        RulerHorz.resetScale();
       // RulerVert.resetScale();
    }

    public void resetFreehand()//no use
    {
        //  freehand.resetFreehand();
        FreehandCamera_R.freehandCamera.SwapCameraMoveMode(false);
    }

    public void backToFreehand()//no use
    {
        // freehand.backToFreehand();
        FreehandCamera_R.freehandCamera.SwapCameraMoveMode(true);
    }

    public void activeCmeraBG(bool active)//change to launce ar
    {
        // CameraBackGround.SetActive(active); reshape.activeOnCameraMaterial(active);
        if (active)
            arSwitch.GetComponent<ARCoreToggle>().launchAR();
    }

    public void setVerticalFace(int TopeNum) { currVertivalFace = (VertivalFace)TopeNum; }
    public void setHorizontalFace(int SideNum) { currHorizontalFace = (HorizontalFace)SideNum; }
    public void IsoViewToggle()
    {
        istoggleIsoCam = !istoggleIsoCam;
        if (istoggleIsoCam && _BaseData._BaseDataSave.MainmenuSelect ==3)
        { IsoViewPanel.transform.localScale = Vector3.one; }
        else
        { IsoViewPanel.transform.localScale = Vector3.zero; }
    }
    public void IsoViewToggle(bool isHide)
    {
        if (istoggleIsoCam && _BaseData._BaseDataSave.MainmenuSelect == 3)
        { IsoViewPanel.transform.localScale = Vector3.one; }
        else
        { IsoViewPanel.transform.localScale = Vector3.zero; }
    }
    public void thicknessSlideToggle(bool actice) 
    { 
        //ThicknessSlide.GetComponent<PlasticThickness>().ActiveSlide(actice); 
    }
    public void ShapeSummartView(bool active)
    {

        if (active)
        {
            shapessummary.gameObject.transform.localScale = Vector3.one;
            shapessummary.RunVolume();
        }
        else
        {
            shapessummary.CloseVolume();
            shapessummary.gameObject.transform.localScale = Vector3.zero;
        }
    }
    public void swapmesh(int sub)
    {
        
        reshape.SwapMesh(sub);
        TouchPanel.ClearAll();
    }
  //  public void summaryToggle(bool isMenu3)
  //  {
  //    //  objectSummaryToggle.openbtn(isMenu3);
  //  }
  
    public void SetDecalOption(bool sett)
    {
        decalCon.showMode(sett);

    }
    public void currentEditFaceSetting(bool set)
    {
        CurrentEditFace.SetActive(set);
    }
    // public void setCoverBtnActive(bool x) { objCover.CoverButtonActive(x); }
}

