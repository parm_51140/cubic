using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeepLinkActive : MonoBehaviour
{
    public static DeepLinkActive _deepLinkActive;
    public bool startdeeplink;
    string saveUrl;
    private void Awake()
    {
        _deepLinkActive = this;
        Application.deepLinkActivated += onDeepLinkActivated;
        if (!string.IsNullOrEmpty(Application.absoluteURL))
        {
            startdeeplink = true;
            saveUrl = Application.absoluteURL;
        }
    }
    private void Start()
    {
     
    }
    private void onDeepLinkActivated(string url)//on deep link enter
    {
        startdeeplink = true;
        saveUrl = url;

        if (url == "" || url == "NULL" || string.IsNullOrEmpty(url))
        {
            CallBackNotice._callBackNotice._noticBark("Wrong url Format");
        }
        else
        {
            CallBackNotice._callBackNotice._noticBark("Loading...");
            LoadHolder._loadHold.setLoad(true);
            string headerspilt = url.Split("?")[1];
            string senduserTag = headerspilt.Split("&")[0];
            string sendrowID = headerspilt.Split("&")[1];
            StartCoroutine(SavedGallery._saveGallery.loadDeeplinkData(senduserTag, sendrowID));
        }
    }
    public void altDeepLinkActivated()
    {
        if (saveUrl == "" || saveUrl == "NULL" || string.IsNullOrEmpty(saveUrl))
        {
            CallBackNotice._callBackNotice._noticBark("Wrong url Format");
        }
        else
        {
            CallBackNotice._callBackNotice._noticBark("Loading.....");
            LoadHolder._loadHold.setLoad(true);
            string headerspilt = saveUrl.Split("?")[1];
            string senduserTag = headerspilt.Split("&")[0];
            string sendrowID = headerspilt.Split("&")[1];
            Debug.LogError(saveUrl);
            StartCoroutine(SavedGallery._saveGallery.loadDeeplinkData(senduserTag, sendrowID));
        }
    }
    public void altDeepLinkActivated(string eaa)//no use
    {
        if (eaa == "" || eaa == "NULL" || string.IsNullOrEmpty(saveUrl))
        {
            CallBackNotice._callBackNotice._noticBark("Wrong url Format");
        }
        else
        {
            CallBackNotice._callBackNotice._noticBark("Loading.....");
            LoadHolder._loadHold.setLoad(true);
            string headerspilt = eaa.Split("?")[1];
            string senduserTag = eaa.Split("&")[0];
            string sendrowID = eaa.Split("&")[1];
            Debug.LogError(eaa);
            StartCoroutine(SavedGallery._saveGallery.loadDeeplinkData(senduserTag, sendrowID));
        }
    }
    private void Update()
    {
        if (Input.GetKeyUp(KeyCode.D)) 
        {
            saveUrl ="http://cubic.thaipet.co.th?1QbiAS7tL-H2urRDHy5ccKViUqRdgBXRMj-X6Bw9cetQ&ID106";
            altDeepLinkActivated();
        }

    }

}
