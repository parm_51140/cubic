﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BlendShape : MonoBehaviour
{
   // public static BlendShape _shapeIns;
    public Slider slider;
    public MeshFilter startshape;
    public MeshFilter endshape;
    public MeshFilter targetshape;
   
    public List<Vector3> startvert;
    public List<Vector3> endvert;
    public List<Vector3> targetvert;
    public int[] tri;

  // private void Awake()
  // {
  //     if (_shapeIns != null)
  //     {
  //         Destroy(_shapeIns.gameObject);
  //         _shapeIns = this;
  //     }
  //     else { _shapeIns = this; }
  //
  //    
  //
  // }
    void Start()
    {
        startshape.sharedMesh.GetVertices(startvert);
        endshape.sharedMesh.GetVertices(endvert);
        tri = startshape.sharedMesh.GetTriangles(0);
        Debug.Log(startvert.Count);
        Debug.Log(endvert.Count);


        targetshape.sharedMesh.SetVertices(startvert);      
        targetshape.sharedMesh.SetTriangles(tri, 0);

        targetshape.sharedMesh.RecalculateNormals();
        targetshape.sharedMesh.RecalculateBounds();

        //targetvert = new List<Vector3>();
        targetshape.sharedMesh.GetVertices(targetvert);
    }

    // Update is called once per frame
    void Update()
    {
       // targetvert.Clear();
        for (int i = 0; i < targetvert.Count; i++)
       // for (int i = 0; i < startvert.Count; i++)
        {
            //targetvert.Add(Vector3.Lerp(startvert[i],endvert[i],slider.value)); 
            targetvert[i]=new Vector3( startvert[i].x *slider.value,targetvert[i].y, targetvert[i].z);
        }

        targetshape.sharedMesh.SetVertices(targetvert);
        targetshape.sharedMesh.SetTriangles(tri, 0);
        targetshape.sharedMesh.RecalculateNormals();
        targetshape.sharedMesh.RecalculateBounds();

    }
}
