using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARDecal : MonoBehaviour
{
    public GameObject[] decal;
    public GameObject[] meshDecal;
    [SerializeField] ARDecalToMesh decaltomesh;
    public Renderer editObj;
    public GameObject lidObj;
    bool toggleDecal = false;
    bool initial = true;
    public bool waitDecal = true;
    public static ARDecal _ARDecal;
    public void SetSelf()
    {

        if (_BaseData._BaseDataSave.DecalActiveMode == 2)
        {
            for (int i = 0; i < decal.Length; i++)
            {
                decal[i].transform.localPosition = DecalPropertiesSave.decalPropSave[i + 5].p_Position ;
                decal[i].transform.localEulerAngles = DecalPropertiesSave.decalPropSave[i + 5].p_Rotation;
                decal[i].transform.localScale = DecalPropertiesSave.decalPropSave[i + 5].p_Scale ;
           //     if (i == 4) decal[i].transform.localScale *= .97f;
            }
        }
        else if (_BaseData._BaseDataSave.DecalActiveMode == 1)
        {
            for (int i = 0; i < decal.Length; i++)
            {
                decal[i].transform.localPosition = DecalPropertiesSave.decalPropSave[i].p_Position ;
                decal[i].transform.localEulerAngles = DecalPropertiesSave.decalPropSave[i].p_Rotation;
                decal[i].transform.localScale = DecalPropertiesSave.decalPropSave[i].p_Scale ;
                if (i <= 4 && _BaseData._BaseDataSave.selectedID == 4) { decal[i].transform.localScale *= .98f; }
            }
        }
        else if (_BaseData._BaseDataSave.DecalActiveMode == 0)
        { return; }
        /*
        decal[0].transform.localPosition = DontDestroy.DontDestroyData.DecalObjectT[0] / 3;
        decal[1].transform.localPosition = DontDestroy.DontDestroyData.DecalObjectT[1] / 3;
        decal[2].transform.localPosition = DontDestroy.DontDestroyData.DecalObjectT[2] / 3;
        decal[3].transform.localPosition = DontDestroy.DontDestroyData.DecalObjectT[3] / 3;
       

        decal[0].transform.localEulerAngles = DontDestroy.DontDestroyData.DecalObjectR[0];
        decal[1].transform.localEulerAngles = DontDestroy.DontDestroyData.DecalObjectR[1];
        decal[2].transform.localEulerAngles = DontDestroy.DontDestroyData.DecalObjectR[2];
        decal[3].transform.localEulerAngles = DontDestroy.DontDestroyData.DecalObjectR[3];
        decal[4].transform.localEulerAngles = DontDestroy.DontDestroyData.DecalObjectR[4];
 
        decal[0].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[0] / 3;
        decal[1].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[1] / 3;
        decal[2].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[2] / 3;
        decal[3].transform.localScale = DontDestroy.DontDestroyData.DecalObjectS[3] / 3;
*/
         // Vector3 tt = DontDestroy.DontDestroyData.DecalObjectS[4] / 3;
         // tt.y = 0;
         // decal[4].transform.localScale = tt;
         // tt = DontDestroy.DontDestroyData.DecalObjectT[4] / 3;
         // tt.y = lidObj.transform.localPosition.y + 0.01f;
      // Vector3 tt = decal[4].transform.localPosition;
      // tt.y = lidObj.transform.localPosition.y + 0.01f;
      // decal[4].transform.localPosition = tt;

        StartCoroutine(decaltomesh.ToDecalMesh(decal[0], 0f, 0));
        StartCoroutine(decaltomesh.ToDecalMesh(decal[1], 0f, 1));
        StartCoroutine(decaltomesh.ToDecalMesh(decal[2], 0f, 2));
        StartCoroutine(decaltomesh.ToDecalMesh(decal[3], 0f, 3));
        StartCoroutine(decaltomesh.ToDecalMesh(decal[4], 0f, 4));


    }
    private void Update()
    {
        //  print(initial);

        // if (initial)
        // {
        //     if (!waitDecal)
        //     {
        //         // print(DontDestroy.DontDestroyData.DecalIsOn+"aaaaaa");
        //
        //         meshDecal[0].SetActive(DontDestroy.DontDestroyData.DecalIsOn);
        //         meshDecal[1].SetActive(DontDestroy.DontDestroyData.DecalIsOn);
        //         meshDecal[2].SetActive(DontDestroy.DontDestroyData.DecalIsOn);
        //         meshDecal[3].SetActive(DontDestroy.DontDestroyData.DecalIsOn);
        //         meshDecal[4].SetActive(DontDestroy.DontDestroyData.DecalIsOn && _BaseData._BaseDataSave.openLid);
        //         initial = false;
        //     }
        //
        // }
    }

    public void ShowDecal()
    {
        toggleDecal = !toggleDecal;
        if (toggleDecal)
        {
            meshDecal[0].SetActive(true);
            meshDecal[1].SetActive(true);
            meshDecal[2].SetActive(true);
            meshDecal[3].SetActive(true);
            meshDecal[4].SetActive(true &&_BaseData._BaseDataSave.openLid);
        }
        else
        {
            meshDecal[0].SetActive(false);
            meshDecal[1].SetActive(false);
            meshDecal[2].SetActive(false);
            meshDecal[3].SetActive(false);
            meshDecal[4].SetActive(false);
        }

    }
}
