using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectCover : MonoBehaviour
{
    public static ObjectCover _objectcover;
    [SerializeField] GameObject[] lid;
    [SerializeField] GameObject[] wrap;
    [SerializeField] Sprite[] Icon;
    [SerializeField] Material clearMat;
    [SerializeField] ObjectReshape reshape;
    GameObject currentLid;
    public int lidState;

    private void Awake()
    {
        _objectcover = this;
    }
    private void Start()
    {
        //lidState = 0;
        if (_BaseData._BaseDataSave.openLid) { }
        // lidState = 1;
        // Invoke("delaySetCover", 1f);
    }
    void delaySetCover()
    {
        // setCover(lidState);
    }

    public void forceCloseLid()
    {
        _BaseData._BaseDataSave.openLid = false;
        ToggleMenuGroup._toggleMenu.ToggleSubmenuUsing();  
        OpenCover();
    }
    public void forceActiveLid()
    {
        _BaseData._BaseDataSave.openLid = true;
        ToggleMenuGroup._toggleMenu.ToggleSubmenuUsing();
        OpenCover();
    }

    public void _lidToggleBtn()
    {     
        OpenCover();    
    }
    public void OpenCover()
    {
        DecalControl2._decalControl.setActiveDecalLid();
        EditableObjectMaster._editmaster.setactiveLid(_BaseData._BaseDataSave.openLid);

    }

    public void OpenCover(int x)
    {
        DecalControl2._decalControl.setActiveDecalLid();
        if (!_BaseData._BaseDataSave.openLid)
        {
            if (DontDestroy.DontDestroyData.ShowObjLid != null)
                Destroy(DontDestroy.DontDestroyData.ShowObjLid);
            return;
        }
        DecalControl2._decalControl.setActiveDecalLid();
        int objID = _BaseData._BaseDataSave.selectedID - 1;
        float topPos = ObjectReshape._reShapeIns.ShapeCurrentBound.extents.y + .01f;
        dev.cubic.PointTouch._pointTouch.SaveAll();

        if (currentLid != null) Destroy(currentLid);
        if (DontDestroy.DontDestroyData.ShowObjLid != null)
            Destroy(DontDestroy.DontDestroyData.ShowObjLid);
        currentLid = Instantiate(wrap[objID], new Vector3(0, topPos, 0), Quaternion.identity);

        currentLid.transform.localScale = new Vector3(
            currentLid.transform.localScale.x - DontDestroy.DontDestroyData.savescaleDimention.x
            , 1
            , currentLid.transform.localScale.z - DontDestroy.DontDestroyData.savescaleDimention.z) * 3;

        currentLid.GetComponentInChildren<MeshRenderer>().material = clearMat;
        currentLid.GetComponentInChildren<MeshRenderer>().gameObject.AddComponent<MeshCollider>();
        DontDestroy.DontDestroyData.ShowObjLid = currentLid;
        DontDestroyOnLoad(currentLid);
    }


    public void setCover(int type)// 0 null, 1 plasticwrap, 2 lid
    {

        int objID = reshape.currObjIndex() - 1;
        float topPos = reshape.gameObject.GetComponent<Renderer>().bounds.extents.y + .02f;
        //callbackbtn
        if (type != 0)
            dev.cubic.PointTouch._pointTouch.SaveAll();
        //  dotype[1] = type1;
        switch (type)
        {
            case 1:
                if (currentLid != null) Destroy(currentLid);
                currentLid = Instantiate(wrap[objID], new Vector3(0, topPos, 0), Quaternion.identity);
                currentLid.transform.localScale = new Vector3(
                    currentLid.transform.localScale.x - DontDestroy.DontDestroyData.savescaleDimention.x
                    , 1
                    , currentLid.transform.localScale.z - DontDestroy.DontDestroyData.savescaleDimention.z) * 3;
                currentLid.GetComponentInChildren<MeshRenderer>().material = clearMat;
                currentLid.GetComponentInChildren<MeshRenderer>().gameObject.AddComponent<MeshCollider>();

                DontDestroy.DontDestroyData.ShowObjLid = currentLid;
                DontDestroyOnLoad(currentLid);

                Main.MainControl.lidOn = true;
                break;
            case 2: break;
            default: if (currentLid != null) Destroy(currentLid); Main.MainControl.lidOn = false; break;
        }


    }


    Vector3 temp = Vector3.zero;
    public void hidecover(bool hide)
    {
        if (currentLid == null) return;

        if (hide)
        {
            temp = currentLid.transform.localScale;
            currentLid.transform.localScale = Vector3.zero;
        }
        else
            OpenCover();
    }


}

