using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FreehandCamera_R : MonoBehaviour
{
    public static FreehandCamera_R freehandCamera;
    [SerializeField] GameObject VerticalAxis;//y
    [SerializeField] GameObject HorizontalAxis;//x
    Vector3 current;
    bool FreehandCameraMode;
    public bool startTranslate;
    float TranslateValue = 0;

    public Vector3 transformDelta;

    Vector3 targetTransform;
    Vector3 startTransform;
    float tempTransformX;
    float tempTransformY;
    Vector3 EditViewAngleSave;
    Vector3 FreeHandViewAngleSave;

    void Awake()
    {
        freehandCamera = this;
    }

    void Update()
    {
       
        if (startTranslate)
        {
            TranslateValue += .1f;

            tempTransformX = Mathf.LerpAngle(startTransform.x, targetTransform.x, TranslateValue);
            tempTransformY = Mathf.LerpAngle(startTransform.y, targetTransform.y, TranslateValue);
            VerticalAxis.transform.localEulerAngles = new Vector3(tempTransformX, 0, 0);
            HorizontalAxis.transform.localEulerAngles = new Vector3(0, tempTransformY, 0);

            if (TranslateValue > 1) startTranslate = false;

        }

    }
    public void SetStartFreehand()
    {
        current = FreeHandViewAngleSave;
    }
    public void _clearFreehand()
    {
        SoundPanel._SoundPanel.UIClickPlay();
        targetTransform = new Vector3(45, 45, 0);
        if (_BaseData._BaseDataSave.selectedID == 4) { targetTransform = new Vector3(45, 0, 0); }
        startTransform = new Vector3(VerticalAxis.transform.localEulerAngles.x, HorizontalAxis.transform.localEulerAngles.y, 0);
        FreeHandViewAngleSave = targetTransform;
        SetStartFreehand();
        TranslateValue = 0;
        startTranslate = true;
    }
    public void FreeHandMove(Vector3 Delta)
    {
        if (current.x > 90 && current.x < 270)
        {
            transformDelta = new Vector3(current.x + (Delta.y / 10), current.y + (Delta.x / 10), current.z);
        }
        else
        {
            transformDelta = new Vector3(current.x + (Delta.y / 10), current.y - (Delta.x / 10), current.z);
        }
        transformDelta.x %= 360;
        transformDelta.y %= 360;
        VerticalAxis.transform.localEulerAngles = new Vector3(transformDelta.x, 0, 0);
        HorizontalAxis.transform.localEulerAngles = new Vector3(0, transformDelta.y, 0);
        FreeHandViewAngleSave = transformDelta;
    }
    public void SwapCameraMoveMode(bool isEdit_to_Freenand)
    {
        if (FreehandCameraMode == isEdit_to_Freenand) return;
        FreehandCameraMode = isEdit_to_Freenand;

        if (FreehandCameraMode)
        {
            EditViewAngleSave = new Vector3(VerticalAxis.transform.localEulerAngles.x, HorizontalAxis.transform.localEulerAngles.y, 0);
            targetTransform = FreeHandViewAngleSave;
        }
        else
        {
            targetTransform = EditViewAngleSave;
        }
        startTransform = new Vector3(VerticalAxis.transform.localEulerAngles.x, HorizontalAxis.transform.localEulerAngles.y, 0);
        TranslateValue = 0;
        startTranslate = true;

    }
}
