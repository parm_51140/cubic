using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubMenuBar : MonoBehaviour
{
    RectTransform selfRect;

    [SerializeField] Image[] brasket;//l 0 r 1

    Vector2 screenScale;
    Vector3 barStartPos;

    [SerializeField] bool selectable;

    bool activeSlide;
    bool movesnap;

    int subCount;
    int subMenuSelected = 1;//1-subCount
    int lastSubMenuSelect;

    float startTouch, deltaTouch;
    float SnapPos;

    void Start()
    {
        subCount = gameObject.transform.childCount;
        selfRect = gameObject.GetComponent<RectTransform>();
        screenScale = MainMenuGroupControl._mainmenugroupcontrol.ScreenScale;
        //showSelectIcon();
    }
    private void OnEnable()
    {
        if (!selectable) gameObject.transform.localScale = Vector3.zero;
        _BaseData._BaseDataSave.SubmenuSelect = subMenuSelected;
        MainMenuGroupControl._mainmenugroupcontrol.subMenuSelected = subMenuSelected;
        showAllIcon();
    }

    void Update()
    {
        if (movesnap)
        {
            Vector3 tempPos = selfRect.anchoredPosition;
            tempPos.x = tempPos.x + (SnapPos - tempPos.x) / 8;
            selfRect.anchoredPosition = tempPos;
            if (SnapPos - tempPos.x > -1 && SnapPos - tempPos.x < 1)
            {
                movesnap = false;
                MainMenuGroupControl._mainmenugroupcontrol.submoving = false;
                MainMenuGroupControl._mainmenugroupcontrol.subMenuSelected = subMenuSelected;
                if(lastSubMenuSelect!=subMenuSelected)
                MainMenuGroupControl._mainmenugroupcontrol.launchMainMenu();//
                showSelectIcon();
            }
        }
    }
    private void OnDisable()
    {
        showSelectIcon();
    }

    public void mouseDown()
    {
        if (!selectable) return;
        startTouch = Input.mousePosition.x;
        barStartPos = selfRect.anchoredPosition * screenScale.x;
        deltaTouch = 0;
        lastSubMenuSelect = subMenuSelected;
        showAllIcon();
    }
    public void mouseDrag()
    {
        deltaTouch = (Input.mousePosition.x - startTouch);
        Vector3 temppos = barStartPos;
        temppos.x = Mathf.Clamp(barStartPos.x + deltaTouch, -selfRect.sizeDelta.x * screenScale.x, 0);
        selfRect.anchoredPosition = temppos / screenScale.x;
    }
    public void mouseUp()
    {
        if (!selectable) return;
        SoundPanel._SoundPanel.SubMenuSwipePlay();
        if (deltaTouch != 0)
        {
            subMenuSelected = (int)Mathf.Floor(selfRect.anchoredPosition.x / -190f) + 1;
            subMenuSelected = Mathf.Clamp(subMenuSelected, 1, subCount);
            SnapPos = subMenuSelected * -190 + 95;
            movesnap = true;
        }
        else
        {
            int touchBlock = Mathf.FloorToInt((Input.mousePosition.x / screenScale.x - 65) / 190);
            if (touchBlock == 2)
            {
                if (!activeSlide)
                    showAllIcon();
                else
                {
                    showSelectIcon();
                  //  MainMenuGroupControl._mainmenugroupcontrol.launchMainMenu();
                }
                return;
            }
            if (touchBlock != 2)
            {
                if (!activeSlide) return;
                subMenuSelected = Mathf.Clamp(subMenuSelected + (touchBlock - 2), 1, subCount);
                SnapPos = subMenuSelected * -190 + 95;
                MainMenuGroupControl._mainmenugroupcontrol.submoving = true;
                movesnap = true;
            }
        }


        showSelectIcon();
        _BaseData._BaseDataSave.SubmenuSelect = subMenuSelected;
    }
    public void forceCallSub(int menu) 
    {
        subMenuSelected = Mathf.Clamp(menu, 1, subCount);
        SnapPos = subMenuSelected * -190 + 95;
        Vector3 tempPos = selfRect.anchoredPosition;
        tempPos.x=subMenuSelected * -190 + 95;
        selfRect.anchoredPosition = tempPos;
        showSelectIcon();
    }
    public void forceCallMenu(int menu)
    {
        subMenuSelected = menu;
        _BaseData._BaseDataSave.SubmenuSelect = 1;
        _BaseData._BaseDataSave.MainmenuSelect = subMenuSelected;
        SnapPos = subMenuSelected * -190 + 95;
        movesnap = true;
    }
    void showAllIcon()
    {
        activeSlide = true;
        foreach (Transform aa in gameObject.transform)
        {
            aa.GetComponent<Image>().color = new Color(1, 1, 1, .5f);
            aa.transform.GetChild(0).GetComponent<Text>().color = new Color(1, 1, 1, .5f);
        }
        gameObject.transform.GetChild(subMenuSelected - 1).GetComponent<Image>().color = Color.yellow;
        gameObject.transform.GetChild(subMenuSelected - 1).transform.GetChild(0).GetComponent<Text>().color = Color.yellow;
        foreach (Image bk in brasket) { bk.color = Color.clear; }

    }
    void showSelectIcon()
    {
       
        activeSlide = false;
        foreach (Transform aa in gameObject.transform)
        {
            aa.GetComponent<Image>().color = Color.clear;
            aa.transform.GetChild(0).GetComponent<Text>().color = Color.clear;
        }
        foreach (Image bk in brasket) { bk.color = Color.clear; }
        if (!selectable) return;

        gameObject.transform.GetChild(subMenuSelected - 1).GetComponent<Image>().color = Color.yellow;
        gameObject.transform.GetChild(subMenuSelected - 1).transform.GetChild(0).GetComponent<Text>().color = Color.yellow;
        if (subMenuSelected == 1)
        {
            brasket[1].color = Color.yellow;
        }
        else if (subMenuSelected == subCount)
        {
            brasket[0].color = Color.yellow;
        }
        else
            foreach (Image bk in brasket) { bk.color = Color.yellow; }
    }

}
