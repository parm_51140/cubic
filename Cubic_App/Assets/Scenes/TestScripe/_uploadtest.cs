using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class _uploadtest : MonoBehaviour
{
    [SerializeField] string path;
    [SerializeField] string UserTag;
    public Texture2D tex2d;
    void Start()
    {
        
    }

    public void _startAction() 
    {
        StartCoroutine(getAPIPath());
    }

    IEnumerator getAPIPath()
    {
        using (UnityWebRequest www = UnityWebRequest.Get("https://script.google.com/macros/s/AKfycby3MtxJ30FAxpjDOIO3gEq1Jq_Qu6F5VdHdexJ4jC0zNRmqJi4hMUkemRANtB3VWaWyzA/exec?action=getAPILink"))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                print("get path error!!");
            }
            else
            {
                path = www.downloadHandler.text;
                print("get path : "+path);
                StartCoroutine(getUserTagTest());
            }
        }

        yield return null;
    }
    IEnumerator getUserTagTest()
    {
        string getreq = _BaseData._BaseDataSave.targetLink + "?action=getUserData&loginid=parm_51140@hotmail.com";
        using (UnityWebRequest www = UnityWebRequest.Get(getreq))
        {
            yield return www.SendWebRequest();

            if (www.result != UnityWebRequest.Result.Success)
            {
                print("get tag error");
            }
            else
            {
                UserTag = www.downloadHandler.text;
                print("get tag : "+UserTag);
                convertTex();
            }
        }
        yield return null;
    }
    void convertTex() 
    {
        if (tex2d == null) 
        {
            print("Null texture ");
            return;
        }
        StartCoroutine(saveImage());   
    }
    IEnumerator saveImage() 
    {     

        byte[] pngData = ImageConversion.EncodeToPNG(tex2d);

        string getreq = path + "?action=uploadPicturetoDropbox&useID=" + UserTag +
           "&designID=" + "ID3" + "&sticker=" + "xxx.png";
        print(getreq + " : uploadPicture");







        yield return null;
    }

}
